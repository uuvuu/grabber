
#alias predi='cd /home/jacob/crypto && mono --aot -O=all CryptoBallPredictor-Server.exe -d -noinput 2>&1 | tee /home/jacob/crypto/log/predi.log & disown'
#alias predi='cd /home/jacob/crypto && mono CryptoBallPredictor-Server.exe -d -noinput 2>&1 | tee /home/jacob/crypto/log/predi.log & disown'
alias predi='cd /home/jacob/crypto && mono -O=all CryptoBallPredictor-Server.exe -d -noinput 2>&1 | tee /home/jacob/crypto/log/predi.log & disown'

#alias trade='cd /home/jacob/crypto && mono --aot -O=all CryptoBallTrader-Server.exe -d -noinput 2>&1 | tee /home/jacob/crypto/log/trade.log & disown'
#alias trade='cd /home/jacob/crypto && mono CryptoBallTrader-Server.exe -d -noinput 2>&1 | tee /home/jacob/crypto/log/trade.log & disown'
alias trade='cd /home/jacob/crypto && mono -O=all CryptoBallTrader-Server.exe -d -noinput 2>&1 | tee /home/jacob/crypto/log/trade.log & disown'

#alias tradet='cd /home/jacob/crypto && mono --aot -O=all CryptoBallRandomTrader-Server.exe -d -noinput 2>&1 | tee /home/jacob/crypto/log/tradet.log & disown'
#alias tradet='cd /home/jacob/crypto && mono CryptoBallRandomTrader-Server.exe -d -noinput 2>&1 | tee /home/jacob/crypto/log/tradet.log & disown'
alias tradet='cd /home/jacob/crypto && mono -O=all CryptoBallRandomTrader-Server.exe -d -noinput 2>&1 | tee /home/jacob/crypto/log/tradet.log & disown'

alias logalert='cd /home/jacob/crypto && mono -O=all LogFileAnalyzer.exe > analyzer.log & disown'

alias ls_mono='ps aux | grep mono | grep -v grep'

alias export_candles='mongoexport --host localhost --db gdax_database --collection btc_usd_candles --csv --out btc_usd_candles.csv --fields time,product,low,high,open,close,volume,source,interpolated,_id --sort "{time: 1}"'

#stop_mono() {
#  pids=`ps aux | grep mono | grep -v grep | awk '{print $2}'`
#  if [ -n "$pids" ]; then
#    echo Killing $pids
#    kill $pids
#  fi
#}

stop_mono() {
  echo shutdown > /var/tmp/crypto_predictor
  echo shutdown > /var/tmp/crypto_trader
}

cp_mono() {
  echo shutdown > /var/tmp/crypto_predictor
  echo shutdown > /var/tmp/crypto_trader

  cp -rp /home/russell/shared/LatestBuild/* /home/jacob/crypto
#  cp -rp /home/jacob/crypto/NetworkState/* /home/jacob/crypto
}

restore_bins() {
  cp -rp /home/jacob/crypto/NetworkState/* /home/jacob/crypto
}

push_mono_prod() {
  ssh jacob@104.131.68.34 "rm -rf ~russell/shared/LatestBuild"
  scp -r /home/russell/shared/LatestBuild jacob@104.131.68.34:/home/russell/shared/LatestBuild
}
