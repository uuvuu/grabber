
import * as lolex from 'lolex';

import { AsyncContainerModule, ContainerModule, injectable } from 'inversify';
import { CandleReaderConfig, CandleReaderConfigOptionsGroup } from '../../../domain/candle/CandleReaderConfigOptionsGroup';

import { DateUtils } from '../../../utils/DateUtils';

import { Candle } from '../../../domain/candle/entities/Candle';
import { CandleModule, TYPES as CANDLE_TYPES } from '../../../domain/candle/module';
import { CandleRepository } from '../../../domain/candle/repositories/CandleRepository';
import { TYPES as CONFIG_TYPES } from '../../../infrastructure/config/module';
import { ProductConfigOptionsGroup } from '../../../infrastructure/config/ProductConfigOptionsGroup';
import { ModelFactoryModule } from '../../../infrastructure/data-access/factories/module';
import { referenceContainer } from '../../../infrastructure/inversify';
import { LoggerConfigOptionsGroup } from '../../../infrastructure/logger/LoggerConfigOptionsGroup';
import { LoggerModule } from '../../../infrastructure/logger/module';
import { ArgvConfig } from '../../../testing/infrastructure/config/ArgvConfig';
import { DatabaseModule } from '../../../testing/infrastructure/data-access/unit/module';

import { MissingCandles } from '../MissingCandles';
import { MissingCandlesModule, TYPES } from '../module';


const DEPENDENCIES: ContainerModule[] = [
  CandleModule,
  LoggerModule,
  MissingCandlesModule,
  ModelFactoryModule
];

const ASYNC_DEPENDENCIES: AsyncContainerModule[] = [
  DatabaseModule
];


describe('MissingCandles', () => {
  @injectable()
  class TestConfig extends ArgvConfig {
    constructor() {
      super();

      this.configOptionsGroups.push(new CandleReaderConfigOptionsGroup());
      this.configOptionsGroups.push(new LoggerConfigOptionsGroup('MissingCandlesTest'));
      this.configOptionsGroups.push(new ProductConfigOptionsGroup());
    }
  }

  const DATES = [
    '2018-07-27T07:06:00.000Z',
    '2018-07-27T07:07:00.000Z',
    '2018-07-27T07:08:00.000Z',
    '2018-07-27T07:09:00.000Z',
    '2018-07-27T07:10:00.000Z',
    '2018-07-27T07:11:00.000Z',
    '2018-07-27T07:12:00.000Z',
    '2018-07-27T07:13:00.000Z',
    '2018-07-27T07:14:00.000Z',
    '2018-07-27T07:15:00.000Z'
  ];

  let clock;

  let missingCandles: MissingCandles;
  let candleRepository: CandleRepository;

  let walkDatePairSpy;

  let config: TestConfig & CandleReaderConfig;

  let originalArgv;

  async function missingCandleCallback(date: Date) {
    console.log(date.toISOString());
  }

  beforeAll(async () => {
    clock = lolex.install({ now: new Date('2018-07-27T07:16:46.117Z'), shouldAdvanceTime: true });

    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper(['-n 10']);

    referenceContainer.bind<TestConfig>(CONFIG_TYPES.Config).to(TestConfig).inSingletonScope();

    referenceContainer.load(...DEPENDENCIES);

    try {
      await referenceContainer.loadAsync(...ASYNC_DEPENDENCIES);
    } catch (error) {
      // let this error be handled/logged at the caller level
      throw error;
    }

    missingCandles = referenceContainer.get<MissingCandles>(TYPES.MissingCandles);

    candleRepository = referenceContainer.get<CandleRepository>(CANDLE_TYPES.CandleRepository);

    config = referenceContainer.get<TestConfig>(CONFIG_TYPES.Config) as TestConfig & CandleReaderConfig;

    candleRepository.findManyByPage = jest.spyOn(candleRepository, 'findManyByPage').mockImplementation(() => {
      return [] as Candle.Candle[];
    });

    walkDatePairSpy = jest.spyOn(DateUtils, 'walkDatePair');

    console.log = jest.spyOn(console, 'log').mockImplementation(() => {});
  });

  afterAll(() => {
    // @ts-ignore
    console.log.mockRestore();

    referenceContainer.unbindAll();

    process.argv = originalArgv;

    clock.uninstall();
  });

  beforeEach(() => {
  });

  afterEach(() => {
    // @ts-ignore
    console.log.mockClear();

    walkDatePairSpy.mockClear();

    // @ts-ignore
    candleRepository.findManyByPage.mockClear();
  });

  test('0 pages returned',  async () => {
    expect.assertions(DATES.length - 1 + 3);

    await missingCandles.processCandles(config.startDate, config.endDate, missingCandleCallback);

    expect(candleRepository.findManyByPage).toHaveBeenCalledTimes(1);
    expect(walkDatePairSpy).toHaveBeenCalledTimes(1);
    expect(console.log).toHaveBeenCalledTimes(10);

    DATES.forEach((date, index) => {
      if (index < DATES.length - 1) {
        expect(console.log).toHaveBeenNthCalledWith(index + 1, date);
      }
    });
  });

  test('1 pages returned, 1 candle in 1st position of page', async () => {
    expect.assertions(DATES.length - 1 + 3);

    // @ts-ignore
    candleRepository.findManyByPage.mockReturnValueOnce([ { time: config.startDate } ])
      .mockReturnValue([] as Candle.Candle[]);

    await missingCandles.processCandles(config.startDate, config.endDate, missingCandleCallback);

    expect(candleRepository.findManyByPage).toHaveBeenCalledTimes(2);
    expect(walkDatePairSpy).toHaveBeenCalledTimes(2);
    expect(console.log).toHaveBeenCalledTimes(9);

    DATES.forEach((date, index) => {
      if (index !== 0) {
        expect(console.log).toHaveBeenNthCalledWith(index, date);
      }
    });
  });

  test('1 pages returned, 1 candle in 2nd position of page', async () => {
    expect.assertions(DATES.length - 1 + 3);

    const candleDate = new Date(config.startDate);
    candleDate.setTime(config.startDate.getTime() + config.durationInMillis);

    // @ts-ignore
    candleRepository.findManyByPage.mockReturnValueOnce([ { time: candleDate } ])
      .mockReturnValue([] as Candle.Candle[]);

    await missingCandles.processCandles(config.startDate, config.endDate, missingCandleCallback);

    expect(candleRepository.findManyByPage).toHaveBeenCalledTimes(2);
    expect(walkDatePairSpy).toHaveBeenCalledTimes(2);
    expect(console.log).toHaveBeenCalledTimes(9);

    DATES.forEach((date, index) => {
      if (index < 1) {
        expect(console.log).toHaveBeenNthCalledWith(index + 1, date);
      } else if (index > 1) {
        expect(console.log).toHaveBeenNthCalledWith(index, date);
      }
    });
  });

  test('1 pages returned, 1 candle in last position of page', async () => {
    expect.assertions(DATES.length - 1 + 3);

    const candleDate = new Date(config.startDate);
    candleDate.setTime(config.startDate.getTime() + config.durationInMillis * (config.numberOfCandles - 1));

    // @ts-ignore
    candleRepository.findManyByPage.mockReturnValueOnce([ { time: candleDate } ])
      .mockReturnValue([] as Candle.Candle[]);

    await missingCandles.processCandles(config.startDate, config.endDate, missingCandleCallback);

    expect(candleRepository.findManyByPage).toHaveBeenCalledTimes(2);
    expect(walkDatePairSpy).toHaveBeenCalledTimes(1);
    expect(console.log).toHaveBeenCalledTimes(9);

    DATES.forEach((date, index) => {
      if (index < 9) {
        expect(console.log).toHaveBeenNthCalledWith(index + 1, date);
      } else if (index > 9) {
        expect(console.log).toHaveBeenNthCalledWith(index, date);
      }
    });
  });

  test('1 pages returned, 2 consecutive candles', async () => {
    expect.assertions(DATES.length - 2 + 3);

    const candleDate = new Date(config.startDate);
    candleDate.setTime(config.startDate.getTime() + config.durationInMillis);

    const candleDate2 = new Date(config.startDate);
    candleDate2.setTime(config.startDate.getTime() + config.durationInMillis * 2);

    candleRepository.findManyByPage =
      jest.spyOn(candleRepository, 'findManyByPage').mockImplementation(() => {
        return [ { time: candleDate }, { time: candleDate2} ];
      });

    // @ts-ignore
    candleRepository.findManyByPage.mockReturnValueOnce([ { time: candleDate }, { time: candleDate2} ])
      .mockReturnValue([] as Candle.Candle[]);

    await missingCandles.processCandles(config.startDate, config.endDate, missingCandleCallback);

    expect(candleRepository.findManyByPage).toHaveBeenCalledTimes(2);
    expect(walkDatePairSpy).toHaveBeenCalledTimes(2);
    expect(console.log).toHaveBeenCalledTimes(8);

    DATES.forEach((date, index) => {
      if (index < 1) {
        expect(console.log).toHaveBeenNthCalledWith(index + 1, date);
      } else if (index > 2) {
        expect(console.log).toHaveBeenNthCalledWith(index - 1, date);
      }
    });
  });

  test('1 pages returned, 2 nonconsecutive candles', async () => {
    expect.assertions(DATES.length - 2 + 3);

    const candleDate = new Date(config.startDate);
    candleDate.setTime(config.startDate.getTime() + config.durationInMillis);

    const candleDate2 = new Date(config.startDate);
    candleDate2.setTime(config.startDate.getTime() + config.durationInMillis * 3);

    // @ts-ignore
    candleRepository.findManyByPage.mockReturnValueOnce([ { time: candleDate }, { time: candleDate2} ])
      .mockReturnValue([] as Candle.Candle[]);

    await missingCandles.processCandles(config.startDate, config.endDate, missingCandleCallback);

    expect(candleRepository.findManyByPage).toHaveBeenCalledTimes(2);
    expect(walkDatePairSpy).toHaveBeenCalledTimes(3);
    expect(console.log).toHaveBeenCalledTimes(8);

    DATES.forEach((date, index) => {
      if (index < 1) {
        expect(console.log).toHaveBeenNthCalledWith(index + 1, date);
      } else if (index === 2) {
        expect(console.log).toHaveBeenNthCalledWith(index, date);
      } else if (index > 3) {
        expect(console.log).toHaveBeenNthCalledWith(index - 1, date);
      }
    });
  });

  test('2 pages returned', async () => {
    expect.assertions(DATES.length - 2 + 3);

    const candleDate = new Date(config.startDate);
    candleDate.setTime(config.startDate.getTime() + config.durationInMillis);

    const candleDate2 = new Date(config.startDate);
    candleDate2.setTime(config.startDate.getTime() + config.durationInMillis * 2);

    // @ts-ignore
    candleRepository.findManyByPage.mockReturnValueOnce([ { time: candleDate }])
      .mockReturnValueOnce([ { time: candleDate2 } ])
      .mockReturnValue([] as Candle.Candle[]);

    await missingCandles.processCandles(config.startDate, config.endDate, missingCandleCallback);

    expect(candleRepository.findManyByPage).toHaveBeenCalledTimes(3);
    expect(walkDatePairSpy).toHaveBeenCalledTimes(3);
    expect(console.log).toHaveBeenCalledTimes(8);

    DATES.forEach((date, index) => {
      if (index < 1) {
        expect(console.log).toHaveBeenNthCalledWith(index + 1, date);
      } else if (index > 2) {
        expect(console.log).toHaveBeenNthCalledWith(index - 1, date);
      }
    });
  });

  test('2 pages returned, nonconsecutive dates', async () => {
    expect.assertions(DATES.length - 2 + 3);

    const candleDate = new Date(config.startDate);
    candleDate.setTime(config.startDate.getTime() + config.durationInMillis);

    const candleDate2 = new Date(config.startDate);
    candleDate2.setTime(config.startDate.getTime() + config.durationInMillis * 3);

    // @ts-ignore
    candleRepository.findManyByPage.mockReturnValueOnce([ { time: candleDate }])
      .mockReturnValueOnce([ { time: candleDate2 } ])
      .mockReturnValue([] as Candle.Candle[]);

    await missingCandles.processCandles(config.startDate, config.endDate, missingCandleCallback);

    expect(candleRepository.findManyByPage).toHaveBeenCalledTimes(3);
    expect(walkDatePairSpy).toHaveBeenCalledTimes(3);
    expect(console.log).toHaveBeenCalledTimes(8);

    DATES.forEach((date, index) => {
      if (index < 1) {
        expect(console.log).toHaveBeenNthCalledWith(index + 1, date);
      } else if (index === 2) {
        expect(console.log).toHaveBeenNthCalledWith(index, date);
      } else if (index > 3) {
        expect(console.log).toHaveBeenNthCalledWith(index - 1, date);
      }
    });
  });
});
