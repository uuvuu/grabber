
import { AsyncContainerModule } from 'inversify';

import { bootstrap } from '../../../infrastructure/bootstrap';
import { referenceContainer } from '../../../infrastructure/inversify';
import { TYPES } from '../../../infrastructure/module';
import { DatabaseModule } from '../../../testing/infrastructure/data-access/unit/module';
import { argvHelper } from '../../../testing/utils/argv';

import { App } from '../App';
import { Config } from '../Config';
import { DEPENDENCIES } from '../module';


const ASYNC_DEPENDENCIES: AsyncContainerModule[] = [
  DatabaseModule
];


describe('App', () => {
  let app: App;
  let originalArgv;

  beforeEach(() => {
    originalArgv = process.argv;
  });

  afterEach(() => {
    // this breaks because when running DatabaseModule, it is getting config and logger from
    // the reference container, instead of them being injected
    referenceContainer.unbindAll();
    process.argv = originalArgv;
  });

  test('bootstraps successfully', async () => {
    expect.assertions(1);

    process.argv = argvHelper(['-n 10']);

    app = await bootstrap<Config, App>(App, TYPES.App, Config, referenceContainer, DEPENDENCIES, ASYNC_DEPENDENCIES);

    expect(app).toBeDefined();

    // we need to 'manually' disconnect in this test as we haven't actually run the
    // app yet
    // @ts-ignore
    await app._databaseClient.disconnect();
  });

  test('bootstraps loads modules only once', async () => {
    expect.assertions(2);

    process.argv = argvHelper(['-n 10']);

    const loadModulesSpy = jest.spyOn(referenceContainer, 'load');

    app = await bootstrap<Config, App>(App, TYPES.App, Config, referenceContainer, DEPENDENCIES, ASYNC_DEPENDENCIES);
    const app2 = await bootstrap<Config, App>(App, TYPES.App, Config, referenceContainer, DEPENDENCIES, ASYNC_DEPENDENCIES);

    expect(app).toEqual(app2);
    expect(loadModulesSpy).toHaveBeenCalledTimes(1);

    // we need to 'manually' disconnect in this test as we haven't actually run the
    // app yet
    // @ts-ignore
    await app._databaseClient.disconnect();
  });

  test('bootstraps fails gracefully', async () => {
    expect.assertions(1);

    process.argv = argvHelper(['-n 10']);

    const errorMessage = 'loadAsync failed';
    referenceContainer.loadAsync = jest.spyOn(referenceContainer, 'loadAsync').mockImplementation(() => {
      throw errorMessage;
    });

    await expect(bootstrap<Config, App>(App, TYPES.App, Config, referenceContainer, DEPENDENCIES, ASYNC_DEPENDENCIES)).rejects.toEqual(errorMessage);

    // @ts-ignore
    referenceContainer.loadAsync.mockRestore();
  });
});




