
import { inject, injectable } from 'inversify';

import { CandleConfig } from '../../domain/candle/CandleConfigOptionsGroup';
import { Candle } from '../../domain/candle/entities/Candle';
import { TYPES as CANDLE_TYPES } from '../../domain/candle/module';
import { CandleRepository } from '../../domain/candle/repositories/CandleRepository';
import { TYPES as CONFIG_TYPES } from '../../infrastructure/config/module';
import { DateUtils } from '../../utils/DateUtils';


@injectable()
class MissingCandles {
  private _startDate;
  private _endDate;
  private _processedCandlesCount = 0;
  private _missingCandlesCount = 0;

  private _missingCandleCallback: (date: Date) => Promise<any>;

  public constructor(
    @inject(CONFIG_TYPES.Config) private readonly _config: CandleConfig,
    @inject(CANDLE_TYPES.CandleRepository) protected readonly _candleRepository: CandleRepository) {
  }

  public async processCandles(startDate: Date, endDate: Date, callback: (date: Date) => Promise<any>) {
    this._startDate = startDate;
    this._endDate = endDate;

    this._missingCandleCallback = callback;

    let nextPageDate = new Date(startDate);

    // candles are assumed to be in ascending order by time
    let candles = await this._candleRepository.findManyByPage(nextPageDate);

    this._processedCandlesCount += candles.length;

    if (candles.length === 0) {
      const datePair = { startDate: nextPageDate, endDate: new Date(endDate) };

      await DateUtils.walkDatePair(datePair, this._config.durationInMillis, (date: Date) => this.processMissingCandle(date));
    } else {
      while (candles.length > 0) {
        nextPageDate = await this.processPage(nextPageDate, candles);
        candles = await this._candleRepository.findManyByPage(nextPageDate);

        this._processedCandlesCount += candles.length;
      }

      if (nextPageDate < endDate) {
        const datePair = { startDate: nextPageDate, endDate: endDate };
        await DateUtils.walkDatePair(datePair, this._config.durationInMillis, (date: Date) => this.processMissingCandle(date));
      }
    }
  }

  get stats() {
    return {
      startDate: new Date(this._startDate),
      endDate: new Date(this._endDate),
      processedCandlesCount: this._processedCandlesCount,
      missingCandlesCount: this._missingCandlesCount
    };
  }

  protected async processPage(nextPageDate: Date, candles: Candle.Candle[]) {
    let datePair = { startDate: nextPageDate, endDate: new Date(candles[0].time) };

    await DateUtils.walkDatePair(datePair, this._config.durationInMillis, (date: Date) => this.processMissingCandle(date));

    for (let i = 0; i < candles.length - 1; i++) {
      // walk dates if there is a gap more than 1 interval between candles
      if (candles[i].time.getTime()  < candles[i + 1].time.getTime() - this._config.durationInMillis) {
        nextPageDate = new Date(candles[i].time.getTime() + this._config.durationInMillis);

        datePair = { startDate: nextPageDate, endDate: candles[i + 1].time };

        await DateUtils.walkDatePair(datePair, this._config.durationInMillis, (date: Date) => this.processMissingCandle(date));
      }
    }

    nextPageDate.setTime(candles[candles.length - 1].time.getTime() + this._config.durationInMillis);

    return nextPageDate;
  }

  protected async processMissingCandle(date: Date) {
    this._missingCandlesCount++;

    return this._missingCandleCallback(date);
  }
}


export { MissingCandles };
