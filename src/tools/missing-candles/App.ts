
import { inject, injectable } from 'inversify';

import { CandleConfig } from '../../domain/candle/CandleConfigOptionsGroup';
import { CandleReaderConfig } from '../../domain/candle/CandleReaderConfigOptionsGroup';
import { App as BaseApp } from '../../infrastructure/App';
import { TYPES as CONFIG_TYPES } from '../../infrastructure/config/module';
import { DatabaseClient } from '../../infrastructure/data-access/DatabaseClient';
import { TYPES as DB_TYPES } from '../../infrastructure/data-access/module';
import { Logger } from '../../infrastructure/logger/Logger';
import { TYPES as LOGGER_TYPES } from '../../infrastructure/logger/module';

import { Config } from './Config';
import { MissingCandles } from './MissingCandles';
import { TYPES } from './module';


@injectable()
class App extends BaseApp<Config> {
  public constructor(
    @inject(CONFIG_TYPES.Config) readonly _config: Config & CandleConfig & CandleReaderConfig,
    @inject(LOGGER_TYPES.Logger) readonly _logger: Logger,
    @inject(DB_TYPES.DatabaseClient) readonly _databaseClient: DatabaseClient,
    @inject(TYPES.MissingCandles) protected readonly _missingCandles: MissingCandles) {

    super(_config, _logger);

    this.registerComponent(this._databaseClient);
  }

  protected async _run() {
    console.log();
    console.log();
    console.log('-----------------------------------------');
    await this._missingCandles.processCandles(
      this._config.startDate, this._config.endDate, async (date) => {
        console.log(`${date.toISOString()}`);
      });

    console.log('-----------------------------------------');
    console.log();
    console.log();
  }

  protected async terminateHook() {
    const stats = this._missingCandles.stats;

    console.log();
    this._logger.log('info', `Processed candles from ${stats.startDate.toISOString()} to ${stats.endDate.toISOString()}`);
    this._logger.log('info', `processedCandlesCount = ${stats.processedCandlesCount}`);
    this._logger.log('info', `missingCandlesCount = ${stats.missingCandlesCount}`);
    console.log();
  }
}


export { App };
