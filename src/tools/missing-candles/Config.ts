
import { Command } from 'commander';
import { inject, injectable } from 'inversify';

import { CandleReaderConfigOptionsGroup } from '../../domain/candle/CandleReaderConfigOptionsGroup';
import { TYPES } from '../../infrastructure/cli/module';
import { Config as BaseConfig } from '../../infrastructure/config/Config';
import { LoggerConfigOptionsGroup } from '../../infrastructure/logger/LoggerConfigOptionsGroup';
import { MongoConfigOptionsGroup } from '../../infrastructure/data-access/MongoConfigOptionsGroup';
import { ProductConfigOptionsGroup } from '../../infrastructure/config/ProductConfigOptionsGroup';


@injectable()
class Config extends BaseConfig {
  private static readonly VERSION = `0.0.1`;
  private static readonly DESCRIPTION = `A tool for reporting missing candles in the DB.`;

  constructor(@inject(TYPES.Command) _program: Command, @inject(TYPES.ARGV) _argv: string[]) {
    super(_program, _argv);

    this.configOptionsGroups.push(new CandleReaderConfigOptionsGroup());
    this.configOptionsGroups.push(new LoggerConfigOptionsGroup('MissingCandles'));
    this.configOptionsGroups.push(new MongoConfigOptionsGroup());
    this.configOptionsGroups.push(new ProductConfigOptionsGroup());
  }

  protected get version(): string {
    return Config.VERSION;
  }

  protected get description(): string {
    return Config.DESCRIPTION;
  }
}


export { Config };
