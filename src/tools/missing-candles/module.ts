
import 'reflect-metadata';

import { AsyncContainerModule, ContainerModule, interfaces } from 'inversify';

import { CandleModule } from '../../domain/candle/module';

import { CliModule } from '../../infrastructure/cli/module';
import { DatabaseModule } from '../../infrastructure/data-access/module';
import { ModelFactoryModule } from '../../infrastructure/data-access/factories/module';
import { LoggerModule } from '../../infrastructure/logger/module';

import { MissingCandles } from './MissingCandles';


const TYPES = {
  MissingCandles: Symbol.for('MissingCandles')
};

const MissingCandlesModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<MissingCandles>(TYPES.MissingCandles).to(MissingCandles).inSingletonScope();
  }
);

const DEPENDENCIES: ContainerModule[] = [
  CandleModule,
  CliModule,
  MissingCandlesModule,
  ModelFactoryModule,
  LoggerModule
];

const ASYNC_DEPENDENCIES: AsyncContainerModule[] = [
  DatabaseModule
];


export { ASYNC_DEPENDENCIES, DEPENDENCIES, MissingCandlesModule, TYPES };
