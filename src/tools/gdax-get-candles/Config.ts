
import { Command } from 'commander';
import { inject, injectable } from 'inversify';

import { GdaxNetworkConfigOptionsGroup } from '../../gdax/network/config/GdaxNetworkConfigOptionsGroup';
import { GdaxCandleNetworkConfigOptionsGroup } from '../../gdax/network/api/candle/GdaxCandleNetworkConfigOptionsGroup';
import { TYPES } from '../../infrastructure/cli/module';
import { Config as BaseConfig } from '../../infrastructure/config/Config';
import { MongoConfigOptionsGroup } from '../../infrastructure/data-access/MongoConfigOptionsGroup';
import { LoggerConfigOptionsGroup } from '../../infrastructure/logger/LoggerConfigOptionsGroup';
import { ProductConfigOptionsGroup } from '../../infrastructure/config/ProductConfigOptionsGroup';


@injectable()
class Config extends BaseConfig {
  private static readonly VERSION = `0.0.1`;
  private static readonly DESCRIPTION = `A tool for retrieving candles from GDAX and storing them in a Mongo DB.`;

  public constructor(@inject(TYPES.Command) _program: Command, @inject(TYPES.ARGV) _argv: string[]) {
    super(_program, _argv);

    this.configOptionsGroups.push(new GdaxCandleNetworkConfigOptionsGroup());
    this.configOptionsGroups.push(new GdaxNetworkConfigOptionsGroup());
    this.configOptionsGroups.push(new LoggerConfigOptionsGroup('GdaxGetCandles'));
    this.configOptionsGroups.push(new MongoConfigOptionsGroup());
    this.configOptionsGroups.push(new ProductConfigOptionsGroup());
  }

  protected get version(): string {
    return Config.VERSION;
  }

  protected get description(): string {
    return Config.DESCRIPTION;
  }
}

export { Config };
