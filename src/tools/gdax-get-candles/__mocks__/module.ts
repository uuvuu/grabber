

import { ContainerModule, interfaces } from 'inversify';

import { TYPES as NETWORK_TYPES } from '../../../infrastructure/network/api/module';
import { RequestClient } from '../../../infrastructure/network/api/RequestClient';
import { RequestFactory } from '../../../infrastructure/network/api/RequestFactory';
import { RequestThrottler } from '../../../infrastructure/network/api/RequestThrottler';

import { GdaxCandleClient } from '../../../gdax/network/api/candle/__mocks__/GdaxCandleClient';
import { GdaxCandleRequest, GdaxCandleRequestFactory, GdaxCandleResponse } from '../../../gdax/network/api/candle/GdaxCandleRequest';
import { GdaxCandleRequestThrottler } from '../../../gdax/network/api/candle/GdaxCandleRequestThrottler';


const GdaxCandlesModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<RequestFactory<GdaxCandleRequest>>(NETWORK_TYPES.RequestFactory).to(GdaxCandleRequestFactory).inSingletonScope();
    bind<RequestClient<GdaxCandleRequest, GdaxCandleResponse>>(NETWORK_TYPES.RequestClient).to(GdaxCandleClient).inSingletonScope();

    bind<RequestThrottler<GdaxCandleRequest, GdaxCandleResponse, GdaxCandleClient>>(
      NETWORK_TYPES.RequestThrottler).to(GdaxCandleRequestThrottler).inSingletonScope();
  }
);


export { GdaxCandlesModule };
