
import { inject, injectable } from 'inversify';

import { Candle, CandleUtils } from '../../domain/candle/entities/Candle';
import { TYPES as CANDLE_TYPES } from '../../domain/candle/module';
import { CandleRepository } from '../../domain/candle/repositories/CandleRepository';
import { GdaxNetworkConfig } from '../../gdax/network/config/GdaxNetworkConfigOptionsGroup';
import { GdaxCandleNetworkConfig } from '../../gdax/network/api/candle/GdaxCandleNetworkConfigOptionsGroup';
import { GdaxCandleClient } from '../../gdax/network/api/candle/GdaxCandleClient';
import { GdaxCandleRequest, GdaxCandleRequestFactory, GdaxCandleResponse } from '../../gdax/network/api/candle/GdaxCandleRequest';
import { GdaxCandleRequestThrottler } from '../../gdax/network/api/candle/GdaxCandleRequestThrottler';
import { GdaxCandleToCandle } from '../../gdax/network/entities/GdaxCandleToCandle';
import { Component } from '../../infrastructure/component/Component';
import { TYPES as CONFIG_TYPES } from '../../infrastructure/config/module';
import { ProductConfig } from '../../infrastructure/config/ProductConfigOptionsGroup';
import { MongoConfig } from '../../infrastructure/data-access/MongoConfigOptionsGroup';
import { SortOrder } from '../../infrastructure/data-access/Query';
import { Logger } from '../../infrastructure/logger/Logger';
import { LoggerConfig } from '../../infrastructure/logger/LoggerConfigOptionsGroup';
import { TYPES as LOGGER_TYPES } from '../../infrastructure/logger/module';
import { TYPES as NETWORK_TYPES } from '../../infrastructure/network/api/module';
import { RetryableRequester } from '../../infrastructure/network/api/RetryableRequester';
import { DateUtils } from '../../utils/DateUtils';

import { TYPES as MISSING_CANDLES_TYPES } from '../missing-candles/module';
import { MissingCandles } from '../missing-candles/MissingCandles';


@injectable()
class GdaxGetCandles extends RetryableRequester<GdaxCandleRequest, GdaxCandleResponse> implements Component {
  private _receivedCandleCount = 0;
  private _insertedCandleCount = 0;
  private _duplicateCandleCount = 0;
  private _missingCandleCount = 0;
  private _dbErrorCount = 0;

  private _lastFilledCandle: Candle.CandleDocument;

  constructor(
    @inject(CONFIG_TYPES.Config) private readonly _config: GdaxCandleNetworkConfig & GdaxNetworkConfig & ProductConfig & LoggerConfig & MongoConfig,
    @inject(LOGGER_TYPES.Logger) readonly _logger: Logger,
    @inject(CANDLE_TYPES.CandleRepository) private readonly _candleRepository: CandleRepository,
    @inject(NETWORK_TYPES.RequestThrottler) private readonly _requestThrottler: GdaxCandleRequestThrottler<GdaxCandleClient>,
    @inject(NETWORK_TYPES.RequestFactory) private readonly _requestFactory: GdaxCandleRequestFactory,
    @inject(MISSING_CANDLES_TYPES.MissingCandles) private readonly _missingCandles: MissingCandles) {

    super(_logger, _config.requestRetryDelayInMillis, _config.requestRetries, (request) => { return () => this._requestThrottler.request(request); });
  }

  async load() {
  }

  async unload() {
  }

  async processCandles(startDate: Date, endDate?: Date) {
    let processCandlesStartDate = DateUtils.alignDate(new Date(), DateUtils.TimeAlignment.MINUTES);
    let processCandlesEndDate;

    this._requestFactory.dateRange = { startDate: startDate, endDate: endDate ? endDate : processCandlesStartDate };

    while (! processCandlesEndDate || processCandlesEndDate.getTime() != processCandlesStartDate.getTime()) {
      if (processCandlesEndDate) {
        processCandlesStartDate = processCandlesEndDate;
      }

      let request = this._requestFactory.newRequest();

      while (request) {
        this._logger.log('info', `Processing request: ${JSON.stringify(request, undefined, 2)}`);

        this.request(request)
          .then(async (response) => {
            this._logger.log(
              'info', `Processing response for request: ${JSON.stringify(response.request, undefined, 2)}`);

            const candles = GdaxCandleToCandle.newCandleDocumentsFromArray(response.data, this._config.product);

            try {
              await this.saveCandles(candles);
            } catch (error) {
              this._dbErrorCount++;
              this._logger.log('error', this.errorMessage(error, response.request));
            }
          })
          .catch(error => {
            this._requestErrorCount++;
            this._logger.log('error', this.errorMessage(error.error, error.request));
          });

        // iterate to next request
        request = this._requestFactory.newRequest();
      }

      try {
        await this._requestThrottler.waitForRequestsToComplete();
      } catch (error) {
        throw error;
      }

      const dateRange = this._requestFactory.dateRange;

      // the above while loop may take minutes to hours. so rerun it with the current 'now' as the end date
      this._logger.log('info',
        `Finished pass of getting candles. Date range was: ${dateRange.startDate.toISOString()} - ${dateRange.endDate.toISOString()}`);

      // if an explicit end date was passed in to this function, then we are done, so break out of the loop
      if (endDate) {
        break;
      }

      processCandlesEndDate = DateUtils.alignDate(new Date(), DateUtils.TimeAlignment.MINUTES);

      this._requestFactory.reset();
      this._requestFactory.dateRange = { startDate: processCandlesStartDate, endDate: processCandlesEndDate };
    }

    const getCandlesEndDate = DateUtils.alignDate(new Date(processCandlesEndDate, DateUtils.TimeAlignment.MINUTES));
    getCandlesEndDate.setTime(getCandlesEndDate.getTime() - this._config.durationInMillis);

    try {
      await this._missingCandles.processCandles(startDate, getCandlesEndDate, async (date: Date) => {
        await this.createMissingCandle(date);
      });
    } catch (error) {
      throw error;
    }
  }

  // for testing only
  protected resetStats() {
    this._receivedCandleCount = 0;
    this._insertedCandleCount = 0;
    this._duplicateCandleCount = 0;
    this._missingCandleCount = 0;
    this._requestSuccessCount = 0;
    this._requestErrorCount = 0;
    this._dbErrorCount = 0;
  }

  get stats() {
    return {
      receivedCandleCount: this._receivedCandleCount,
      insertedCandleCount: this._insertedCandleCount,
      duplicateCandleCount: this._duplicateCandleCount,
      missingCandleCount: this._missingCandleCount,
      requestSuccessCount: this._requestSuccessCount,
      requestErrorCount: this._requestErrorCount,
      dbErrorCount: this._dbErrorCount
    };
  }

  protected async request(request: GdaxCandleRequest) {
    const response = await super.request(request);

    this._receivedCandleCount += response.data.length;

    return response;
  }

  protected async createMissingCandle(date: Date) {
    this._missingCandleCount++;

    let candle;

    if (this._lastFilledCandle && date.getTime() - this._lastFilledCandle.time.getTime() <= this._config.durationInMillis) {
      candle = this._lastFilledCandle;
    } else {
      const query = {
        filter: {
          time: { $lt: date }
        },
        sort: {
          time: SortOrder.DESCENDING
        },
        limit: 1
      };

      // @ts-ignore
      const candles = await this._candleRepository.findManyByQuery(query);
      candle = candles[0];
    }

    this._logger.log('info', `Missing candle. Interpolating candle for: ${date.toISOString()}`);
    const fillCandle = CandleUtils.interpolateCandle(candle, date);

    this._lastFilledCandle = (await this._candleRepository.create(fillCandle))[0];

    this._insertedCandleCount++;

    this._lastFilledCandle = this._lastFilledCandle[0];
  }

  protected async saveCandles(candles: Candle.CandleDocument[]) {
    const bulkResult = await this._candleRepository.upsertMany(candles, 'time');

    this._insertedCandleCount += bulkResult.nUpserted;
    this._duplicateCandleCount += bulkResult.nModified;
  }

  protected errorMessage(error: any, request: GdaxCandleRequest) {
    let errorStr = JSON.stringify(error);
    if (errorStr === '{}') {
      errorStr = error;
    }

    return `Candles for request #${request.requestNumber} for date range: `
      + `${request.start} - ${request.end} missing because of an error. `
      + `Error was: '${errorStr}'`;
  }
}


export { GdaxGetCandles };

