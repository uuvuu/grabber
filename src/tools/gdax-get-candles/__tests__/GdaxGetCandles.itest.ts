
import * as lolex from 'lolex';

import { AsyncContainerModule, ContainerModule, injectable } from 'inversify';

import { CandleModule, TYPES as CANDLE_TYPES } from '../../../domain/candle/module';
import { CandleRepository } from '../../../domain/candle/repositories/CandleRepository';
import { GdaxNetworkConfig, GdaxNetworkConfigOptionsGroup } from '../../../gdax/network/config/GdaxNetworkConfigOptionsGroup';
import { GdaxCandleNetworkConfig, GdaxCandleNetworkConfigOptionsGroup } from '../../../gdax/network/api/candle/GdaxCandleNetworkConfigOptionsGroup';
import { GdaxCandleClient } from '../../../gdax/network/api/candle/__mocks__/GdaxCandleClient';
import { GdaxCandleRequestFactory } from '../../../gdax/network/api/candle/GdaxCandleRequest';
import { GdaxNetworkModule } from '../../../gdax/network/module';
import { Config } from '../../../infrastructure/config/Config';
import { TYPES as CONFIG_TYPES } from '../../../infrastructure/config/module';
import { ProductConfigOptionsGroup } from '../../../infrastructure/config/ProductConfigOptionsGroup';
import { ModelFactoryModule } from '../../../infrastructure/data-access/factories/module';
import { TYPES as DB_TYPES } from '../../../infrastructure/data-access/module';
import { referenceContainer } from '../../../infrastructure/inversify';
import { Logger } from '../../../infrastructure/logger/Logger';
import { LoggerConfigOptionsGroup } from '../../../infrastructure/logger/LoggerConfigOptionsGroup';
import { LoggerModule, TYPES as LOGGER_TYPES } from '../../../infrastructure/logger/module';
import { NetworkModule, TYPES as NETWORK_TYPES } from '../../../infrastructure/network/api/module';
import { RateLimiter } from '../../../infrastructure/network/api/RateLimiter';
import { ArgvConfig } from '../../../testing/infrastructure/config/ArgvConfig';
import { IntegrationTestingDatabaseClient } from '../../../testing/infrastructure/data-access/integration/IntegrationTestingDatabaseClient';
import { IntegrationTestingDatabaseModule } from '../../../testing/infrastructure/data-access/integration/module';

import { MissingCandlesModule } from '../../missing-candles/module';
import { GdaxCandlesModule } from '../__mocks__/module';

import { GdaxGetCandles } from '../GdaxGetCandles';
import { GdaxGetCandlesModule, TYPES } from '../module';


const DEPENDENCIES: ContainerModule[] = [
  CandleModule,
  GdaxCandlesModule,
  GdaxGetCandlesModule,
  GdaxNetworkModule,
  LoggerModule,
  MissingCandlesModule,
  ModelFactoryModule,
  NetworkModule
];

const ASYNC_DEPENDENCIES: AsyncContainerModule[] = [
  IntegrationTestingDatabaseModule
];

describe('GdaxGetCandles', () => {
  @injectable()
  class TestConfig extends ArgvConfig {
    constructor() {
      super();

      this.configOptionsGroups.push(new GdaxCandleNetworkConfigOptionsGroup());
      this.configOptionsGroups.push(new GdaxNetworkConfigOptionsGroup());
      this.configOptionsGroups.push(new LoggerConfigOptionsGroup('GdaxGetCandlesTest'));
      this.configOptionsGroups.push(new ProductConfigOptionsGroup());
    }
  }

  const startDate = new Date('2018-09-11T06:30:00.000Z');
  const endDate = new Date('2018-09-11T06:38:00.000Z');

  let config;

  let logger: Logger;
  let gdaxGetCandles: GdaxGetCandles;
  let candleRepository: CandleRepository;
  let gdaxCandleClient: GdaxCandleClient;
  let gdaxCandlesRequestFactory: GdaxCandleRequestFactory;

  let databaseClient: IntegrationTestingDatabaseClient;

  let originalArgv;

  let clock;

  beforeAll(async () => {
    clock = lolex.install({ now: endDate, shouldAdvanceTime: true });

    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([`-s ${startDate.toISOString()} -e ${endDate.toISOString()}`]);

    referenceContainer.bind<Config>(CONFIG_TYPES.Config).to(TestConfig).inSingletonScope();

    referenceContainer.load(...DEPENDENCIES);

    try {
      await referenceContainer.loadAsync(...ASYNC_DEPENDENCIES);
    } catch (error) {
      // let this error be handled/logged at the caller level
      throw error;
    }

    config = referenceContainer.get<TestConfig>(CONFIG_TYPES.Config) as TestConfig & GdaxCandleNetworkConfig & GdaxNetworkConfig;
    config.maxDataItems = 4;
    config.publicEndpointRateLimit = { amountPerInterval: 100, intervalLengthInMillis: 1 } as RateLimiter.RateLimit;

    gdaxGetCandles = referenceContainer.get<GdaxGetCandles>(TYPES.GdaxGetCandles);
    candleRepository = referenceContainer.get<CandleRepository>(CANDLE_TYPES.CandleRepository);
    gdaxCandleClient = referenceContainer.get<GdaxCandleClient>(NETWORK_TYPES.RequestClient);
    gdaxCandlesRequestFactory = referenceContainer.get<GdaxCandleRequestFactory>(NETWORK_TYPES.RequestFactory);

    databaseClient = referenceContainer.get<IntegrationTestingDatabaseClient>(DB_TYPES.DatabaseClient);

    logger = referenceContainer.get<Logger>(LOGGER_TYPES.Logger);

    // create no-op implementation for logger to keep test output less noisy
    logger.log = jest.spyOn(logger, 'log').mockImplementation(() => {});
  });

  afterAll(async () => {
    // make sure clock.uninstall() comes first as there are some sleep() calls in infrastructure teardown
    // if lolex.install() is installed without the { shouldAdvanceTime: true } option, the sleep() will
    // never return
    clock.uninstall();

    // @ts-ignore
    logger.log.mockRestore();

    await databaseClient.reset();
    await databaseClient.disconnect();

    referenceContainer.unbindAll();

    process.argv = originalArgv;
  });

  beforeEach(async () => {
    await databaseClient.reset();
  });

  afterEach(async () => {
    // @ts-ignore
    gdaxGetCandles.resetStats();

    // @ts-ignore
    gdaxCandlesRequestFactory.reset();
  });

  test('processCandles(): 2 consecutive requests, no duplicates', async () => {
    expect.assertions(3);

    const expectedStats = {
      receivedCandleCount: 8,
      insertedCandleCount: 8,
      duplicateCandleCount: 0,
      missingCandleCount: 0,
      requestSuccessCount: 2,
      requestErrorCount: 0,
      dbErrorCount: 0
    };

    let candles = await candleRepository.findManyByPage(startDate);

    expect(candles.length).toEqual(0);

    await gdaxCandleClient.loadTestData(__dirname + '/__data__/candles-request-response.json');
    await gdaxGetCandles.processCandles(startDate, endDate);

    candles = await candleRepository.findManyByPage(startDate);

    expect(candles.length).toEqual(8);

    expect(gdaxGetCandles.stats).toEqual(expectedStats);
  });

  test('processCandles(): request with missing candle creates interpolated candle', async () => {
    expect.assertions(11);

    const expectedStats = {
      receivedCandleCount: 7,
      insertedCandleCount: 8,
      duplicateCandleCount: 0,
      missingCandleCount: 1,
      requestSuccessCount: 2,
      requestErrorCount: 0,
      dbErrorCount: 0
    };

    await gdaxCandleClient.loadTestData(__dirname + '/__data__/candles-request-response-missing.json');
    await gdaxGetCandles.processCandles(startDate, endDate);

    const candles = await candleRepository.findManyByPage(startDate);

    const previousCandle = candles[1];
    const filledMissingCandle = candles[2];

    expect(filledMissingCandle.product).toEqual(previousCandle.product);
    expect(filledMissingCandle.time.getTime()).toEqual(previousCandle.time.getTime() + config.durationInMillis);
    expect(filledMissingCandle.low).toEqual(previousCandle.close);
    expect(filledMissingCandle.high).toEqual(previousCandle.close);
    expect(filledMissingCandle.open).toEqual(previousCandle.close);
    expect(filledMissingCandle.close).toEqual(previousCandle.close);
    expect(filledMissingCandle.volume).toEqual('0');
    expect(filledMissingCandle.source).toEqual('cbpro-api');
    expect(filledMissingCandle.interpolated).toEqual(true);

    expect(candles.length).toEqual(8);
    expect(gdaxGetCandles.stats).toEqual(expectedStats);
  });

  test('processCandles(): interpolated candle is correctly overwritten with non-interpolated data', async () => {
    expect.assertions(19);

    const expectedStats = {
      receivedCandleCount: 8,
      insertedCandleCount: 0,
      duplicateCandleCount: 8,
      missingCandleCount: 0,
      requestSuccessCount: 2,
      requestErrorCount: 0,
      dbErrorCount: 0
    };

    await gdaxCandleClient.loadTestData(__dirname + '/__data__/candles-request-response-missing.json');
    await gdaxGetCandles.processCandles(startDate, endDate);

    let candles = await candleRepository.findManyByPage(startDate);
    const previousCandle = candles[1];
    let filledMissingCandle = candles[2];

    expect(filledMissingCandle.product).toEqual(previousCandle.product);
    expect(filledMissingCandle.time.getTime()).toEqual(previousCandle.time.getTime() + config.durationInMillis);
    expect(filledMissingCandle.low).toEqual(previousCandle.close);
    expect(filledMissingCandle.high).toEqual(previousCandle.close);
    expect(filledMissingCandle.open).toEqual(previousCandle.close);
    expect(filledMissingCandle.close).toEqual(previousCandle.close);
    expect(filledMissingCandle.volume).toEqual('0');
    expect(filledMissingCandle.source).toEqual('cbpro-api');
    expect(filledMissingCandle.interpolated).toEqual(true);

    // @ts-ignore
    gdaxCandlesRequestFactory.reset();

    // @ts-ignore
    gdaxGetCandles.resetStats();

    // call processCandles() again to test duplicates and to ensure previously missing candle gets
    // overwritten
    await gdaxCandleClient.loadTestData(__dirname + '/__data__/candles-request-response.json');
    await gdaxGetCandles.processCandles(startDate, endDate);

    candles = await candleRepository.findManyByPage(startDate);
    filledMissingCandle = candles[2];

    expect(filledMissingCandle.product).toEqual(config.product);
    expect(filledMissingCandle.time.getTime()).toEqual(1536647520 * 1000);
    expect(filledMissingCandle.low).toEqual('6333.40');
    expect(filledMissingCandle.high).toEqual('6337.16');
    expect(filledMissingCandle.open).toEqual('6337.14');
    expect(filledMissingCandle.close).toEqual('6333.78');
    expect(filledMissingCandle.volume).toEqual('0.49328889');
    expect(filledMissingCandle.source).toEqual('cbpro-api');
    expect(filledMissingCandle.interpolated).toEqual(false);

    expect(gdaxGetCandles.stats).toEqual(expectedStats);
  });

  test('processCandles(): handles duplicates correctly', async () => {
    expect.assertions(1);

    const expectedStats = {
      receivedCandleCount: 16,
      insertedCandleCount: 8,
      duplicateCandleCount: 8,
      missingCandleCount: 0,
      requestSuccessCount: 4,
      requestErrorCount: 0,
      dbErrorCount: 0
    };

    await gdaxCandleClient.loadTestData(__dirname + '/__data__/candles-request-response.json');
    await gdaxGetCandles.processCandles(startDate, endDate);

    // @ts-ignore
    gdaxCandlesRequestFactory.reset();

    // call processCandles() again to test duplicates
    await gdaxGetCandles.processCandles(startDate, endDate);

    expect(gdaxGetCandles.stats).toEqual(expectedStats);
  });
});

