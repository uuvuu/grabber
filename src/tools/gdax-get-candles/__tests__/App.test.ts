
import { AsyncContainerModule } from 'inversify';

import { bootstrap } from '../../../infrastructure/bootstrap';
import { referenceContainer } from '../../../infrastructure/inversify';
import { TYPES } from '../../../infrastructure/module';
import { ArgvConfig } from '../../../testing/infrastructure/config/ArgvConfig';
import { DatabaseModule } from '../../../testing/infrastructure/data-access/unit/module';
import { argvHelper } from '../../../testing/utils/argv';

import { App } from '../App';
import { Config } from '../Config';
import { DEPENDENCIES } from '../module';


const ASYNC_DEPENDENCIES: AsyncContainerModule[] = [
  DatabaseModule
];


describe('App', () => {
  let app: App;

  let originalArgv;

  beforeAll(() => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([]);
  });

  afterAll(() => {
    process.argv = originalArgv;
  });

  beforeEach(() => {
    // this breaks because when running DatabaseModule, it is getting config and logger from
    // the reference container, instead of them being injected
    referenceContainer.unbindAll();
  });

  test('bootstraps successfully', async () => {
    expect.assertions(1);

    const testArgv = process.argv;
    process.argv = argvHelper(['-n 100']);

    app = await bootstrap<Config, App>(App, TYPES.App, Config, referenceContainer, DEPENDENCIES, ASYNC_DEPENDENCIES);

    expect(app).toBeDefined();

    // we need to 'manually' disconnect in this test as we haven't actually run the
    // app yet
    // @ts-ignore
    await app._databaseClient.disconnect();

    process.argv = testArgv;
  });

  test('bootstraps fails if neither minutesBefore now nor startDate are set', async () => {
    expect.assertions(1);

    await expect(bootstrap<Config, App>(App, TYPES.App, Config, referenceContainer, DEPENDENCIES, ASYNC_DEPENDENCIES)).rejects.toBeDefined();
  });
});
