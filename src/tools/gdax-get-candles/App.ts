
import { inject, injectable } from 'inversify';

import { CandleConfig } from '../../domain/candle/CandleConfigOptionsGroup';
import { CandleReaderConfig } from '../../domain/candle/CandleReaderConfigOptionsGroup';
import { App as BaseApp } from '../../infrastructure/App';
import { TYPES as CONFIG_TYPES } from '../../infrastructure/config/module';
import { DatabaseClient } from '../../infrastructure/data-access/DatabaseClient';
import { TYPES as DB_TYPES } from '../../infrastructure/data-access/module';
import { Logger } from '../../infrastructure/logger/Logger';
import { TYPES as LOGGER_TYPES } from '../../infrastructure/logger/module';

import { Config } from './Config';
import { GdaxGetCandles } from './GdaxGetCandles';
import { TYPES } from './module';


@injectable()
class App extends BaseApp<Config> {
  public constructor(
    @inject(CONFIG_TYPES.Config) readonly _config: Config & CandleConfig & CandleReaderConfig,
    @inject(LOGGER_TYPES.Logger) readonly _logger: Logger,
    @inject(DB_TYPES.DatabaseClient) readonly _databaseClient: DatabaseClient,
    @inject(TYPES.GdaxGetCandles) protected readonly _getCandles: GdaxGetCandles) {

    super(_config, _logger);

    this.registerComponent(this._databaseClient);
    this.registerComponent(this._getCandles);
  }

  protected async _run() {
    await this._getCandles.processCandles(this._config.startDate, this._config.endDate);
  }

  protected async terminateHook() {
    const stats = this._getCandles.stats;

    console.log();
    this._logger.log('info', `receivedCandleCount = ${stats.receivedCandleCount}`);
    this._logger.log('info', `insertedCandleCount = ${stats.insertedCandleCount}`);
    this._logger.log('info', `duplicateCandleCount = ${stats.duplicateCandleCount}`);
    this._logger.log('info', `missingCandleCount = ${stats.missingCandleCount}`);
    this._logger.log('info', `requestSuccessCount = ${stats.requestSuccessCount}`);
    this._logger.log('info', `requestErrorCount = ${stats.requestErrorCount}`);
    this._logger.log('info', `dbErrorCount = ${stats.dbErrorCount}`);
    console.log();
  }
}


export { App };
