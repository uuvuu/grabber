
import 'reflect-metadata';

import { AsyncContainerModule, ContainerModule, interfaces } from 'inversify';

import { CandleModule } from '../../domain/candle/module';
import { GdaxNetworkModule } from '../../gdax/network/module';
import { GdaxCandleNetworkModule } from '../../gdax/network/api/candle/module';
import { CliModule } from '../../infrastructure/cli/module';
import { DatabaseModule } from '../../infrastructure/data-access/module';
import { ModelFactoryModule } from '../../infrastructure/data-access/factories/module';
import { LoggerModule } from '../../infrastructure/logger/module';
import { NetworkModule } from '../../infrastructure/network/api/module';
import { MissingCandlesModule } from '../missing-candles/module';


import { GdaxGetCandles } from './GdaxGetCandles';


const TYPES = {
  GdaxGetCandles: Symbol.for('GdaxGetCandles')
};

const GdaxGetCandlesModule = new ContainerModule(
  (bind: interfaces.Bind) => {

    bind<GdaxGetCandles>(TYPES.GdaxGetCandles).to(GdaxGetCandles).inSingletonScope();
  }
);

const DEPENDENCIES: ContainerModule[] = [
  CandleModule,
  CliModule,
  GdaxCandleNetworkModule,
  GdaxGetCandlesModule,
  GdaxNetworkModule,
  LoggerModule,
  MissingCandlesModule,
  ModelFactoryModule,
  NetworkModule
];

const ASYNC_DEPENDENCIES: AsyncContainerModule[] = [
  DatabaseModule
];


export { ASYNC_DEPENDENCIES, DEPENDENCIES, GdaxGetCandlesModule, TYPES };
