
import { Command } from 'commander';

import { ConfigError, ConfigErrorFactory } from '../../infrastructure/config/Config';
import { ConfigOptionsGroup } from '../../infrastructure/config/ConfigOptionsGroup';


class DEFAULTS {
  static readonly MAX_TRADE_PERCENT = 1;
  static readonly MAX_TICKER_CHANGE_PERCENT = .20;
  static readonly MAX_PROCESSING_DELAY_MILLIS = 2000;
  static readonly INCREMENT_LIMIT_PRICE_CENTS = 1;
  static readonly MARKET_ORDERS = false;

  static readonly TRADE_REQUEST_RETRIES = 5;
  static readonly TRADE_REQUEST_RETRY_DELAY_IN_MILLIS = 1000;
}

class TradeConfigOptionsGroup extends ConfigOptionsGroup {
  static readonly DEFAULTS = DEFAULTS;

  constructor() {
    super();

    this.addOption({
      maxTickerChangePercent: {
        flag: '-C',
        description: `Max ticker change percent between tickers.`,
        coercionFunction: this.parseInt,
        defaultValue: TradeConfigOptionsGroup.DEFAULTS.MAX_TICKER_CHANGE_PERCENT
      }
    });

    this.addOption({
      maxTradePercent: {
        flag: '-T',
        description: `Max trade percent to apply to trade messages.`,
        coercionFunction: this.parseInt,
        defaultValue: TradeConfigOptionsGroup.DEFAULTS.MAX_TRADE_PERCENT
      }
    });

    this.addOption({
      maxProcessingDelayMillis: {
        flag: '-d',
        description: `Maximum processing delay from last candle time to accept in millis.`,
        coercionFunction: this.parseInt,
        defaultValue: TradeConfigOptionsGroup.DEFAULTS.MAX_PROCESSING_DELAY_MILLIS
      }
    });

    this.addOption({
      incrementLimitPriceCents: {
        flag: '-i',
        description: `Amount to offset limit order by, in cents.`,
        coercionFunction: this.parseInt,
        defaultValue: TradeConfigOptionsGroup.DEFAULTS.INCREMENT_LIMIT_PRICE_CENTS
      }
    });

    this.addOption({
      marketOrders: {
        flag: '-m',
        description: `Place market orders. If true, only market orders will be placed, otherwise limit orders are placed.`,
        coercionFunction: this.parseInt,
        defaultValue: TradeConfigOptionsGroup.DEFAULTS.MARKET_ORDERS
      }
    });

    // run against real GDAX, but do no execute orders
    this.addOption({
      fakeMode: {
        flag: '-F',
        description: `Run against real GDAX, but do no execute orders.`,
        defaultValue: false
      }
    });
  }

  addExtraParams(command: Command) {
    this.addExtraParam('tradeRequestRetries', TradeConfigOptionsGroup.DEFAULTS.TRADE_REQUEST_RETRIES);
    this.addExtraParam('tradeRequestRetryDelayInMillis', TradeConfigOptionsGroup.DEFAULTS.TRADE_REQUEST_RETRY_DELAY_IN_MILLIS);
  }

  validateOptions(command: Command, configErrorFactory: ConfigErrorFactory): ConfigError {
    const maxTradePercent = command.maxTradePercent;

    if (! Number.isInteger(maxTradePercent) || maxTradePercent > 1 || maxTradePercent <= 0) {
      return configErrorFactory.newError('maxTradePercent must be an integer larger than 0 and less than or equal to 1.');
    }

    const maxProcessingDelayMillis = command.maxProcessingDelayMillis;

    if (! Number.isInteger(maxProcessingDelayMillis) || maxProcessingDelayMillis < 0) {
      return configErrorFactory.newError('maxProcessingDelayMillis must be an integer larger than or equal to 0.');
    }

    const incrementLimitPriceCents = command.incrementLimitPriceCents;

    if (! Number.isInteger(incrementLimitPriceCents) || incrementLimitPriceCents <= 0) {
      return configErrorFactory.newError('maxProcessingDelayMillis must be an integer larger than 0.');
    }

    return undefined;
  }
}

interface TradeConfig {
  incrementLimitPriceCents: number;
  maxTradePercent: number;
  maxTickerChangePercent: number;
  maxProcessingDelayMillis: number;
  marketOrders: boolean;
  fakeMode: boolean;
  tradeRequestRetries: number;
  tradeRequestRetryDelayInMillis: number;
}


export { TradeConfig, TradeConfigOptionsGroup };
