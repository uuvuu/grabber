
const TYPES = {
  ClearanceLogger: Symbol.for('ClearanceLogger'),
  GdaxGetAccounts: Symbol.for('GdaxGetAccounts'),
  GdaxGetTicker: Symbol.for('GdaxGetTicker'),
  TradeExecutor: Symbol.for('TradeExecutor')
};


export { TYPES };
