
import { inject, injectable } from 'inversify';

import { AuthenticatedClient } from '../../gdax/network/api/AuthenticatedClient';
import { GdaxNetworkConfig, GdaxNetworkConfigOptionsGroup } from '../../gdax/network/config/GdaxNetworkConfigOptionsGroup';
import { TYPES as NETWORK_TYPES } from '../../gdax/network/types';
import { App as BaseApp } from '../../infrastructure/App';
import { TYPES as CONFIG_TYPES } from '../../infrastructure/config/module';
import { ProductConfig } from '../../infrastructure/config/ProductConfigOptionsGroup';
import { DatabaseClient } from '../../infrastructure/data-access/DatabaseClient';
import { TYPES as DB_TYPES } from '../../infrastructure/data-access/module';
import { Logger } from '../../infrastructure/logger/Logger';
import { TYPES as LOGGER_TYPES } from '../../infrastructure/logger/module';
import { Handshakes, ShakeType } from '../../infrastructure/pipes/Handshakes';
import { Message } from '../../infrastructure/pipes/Message';
import { MessageHandler } from '../../infrastructure/pipes/MessageHandler';
import { NamedPipe } from '../../infrastructure/pipes/NamedPipe';
import { PipeConfig } from '../../infrastructure/pipes/PipeConfigOptionsGroup';
import { PipeManager } from '../../infrastructure/pipes/PipeManager';
import { TYPES as PIPES_TYPES } from '../../infrastructure/pipes/types';
import { TYPES as STATE_TYPES } from '../../infrastructure/state/module';
import { State, StateManager } from '../../infrastructure/state/State';
import { sleep } from '../../utils/sleep';

import { Config } from './Config';
import { GdaxGetAccounts } from './GdaxGetAccounts';
import { TYPES } from './types';
import { TradeExecutor } from './TradeExecutor';


@injectable()
class App extends BaseApp<Config> {
  private _authenticated = false;

  public constructor(
    @inject(CONFIG_TYPES.Config) readonly _config: Config & GdaxNetworkConfig & PipeConfig & ProductConfig,
    @inject(LOGGER_TYPES.Logger) readonly _logger: Logger,
    @inject(DB_TYPES.DatabaseClient) readonly _databaseClient: DatabaseClient,
    @inject(NETWORK_TYPES.AuthenticatedClientFactory) readonly _authenticatedClientFactory: AuthenticatedClient.AuthenticatedClientFactory,
    @inject(PIPES_TYPES.Handshakes) readonly _handshakes: Handshakes,
    @inject(PIPES_TYPES.MessageHandler) protected readonly _messageHandler: MessageHandler,
    @inject(PIPES_TYPES.PipeManager) protected readonly _pipeManager: PipeManager,
    @inject(STATE_TYPES.StateManager) protected readonly _stateManager: StateManager,
    @inject(TYPES.GdaxGetAccounts) protected readonly _gdaxGetAccounts: GdaxGetAccounts,
    @inject(TYPES.TradeExecutor) protected readonly _tradeExecutor: TradeExecutor) {

    super(_config, _logger);

    this._config.channels.push('ticker');

    // the following config options should not be set via the CLI for this app, so we add them after
    // the app has been configured
    this._config.configOptionsGroups.push(new GdaxNetworkConfigOptionsGroup());
    this._config.apiUrl = GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_API_URL;

    this._config.outgoingPipes.forEach(outgoingPipe => {
      this._handshakes.addPartner(outgoingPipe);
    });

    this.registerComponent(this._databaseClient);
    this.registerComponent(this._pipeManager);
    this.registerComponent(this._stateManager);
    this.registerComponent(this._messageHandler);
    this.registerComponent(this._gdaxGetAccounts);
    this.registerComponent(this._tradeExecutor);

    this._messageHandler.addAcknowledgeStartupMessageListener((target: NamedPipe) => {
      this.acknowledgeStartupMessageListener(target);
    });

    this._messageHandler.addStartupMessageListener(async (target: NamedPipe) => {
      await this.startupMessageListener(target);
    });

    this._messageHandler.addAuthnMessageListener((message: Message.AuthnMessage) => {
      this.authnMessageListener(message);
    });

    this._messageHandler.addUnauthnMessageListener((message: Message.UnauthnMessage) => {
      this.unauthnMessageListener(message);
    });

    this._messageHandler.addTradeMessageListener((message: Message.TradeMessage) => {
      this.tradeMessageListener(message);
    });
  }

  protected acknowledgeStartupMessageListener(target: NamedPipe) {
    this._handshakes.shake(target, ShakeType.AcknowledgeStartup);

    this._messageHandler.sendSubscribeStateChangeMessage(...this._config.outgoingPipes);
    this._messageHandler.sendGetStateMessage(...this._config.outgoingPipes);
  }

  protected startupMessageListener(target: NamedPipe) {
    this._handshakes.shake(target, ShakeType.Startup);

    this._messageHandler.sendStartupAcknowledgeMessage(target);
    this._messageHandler.sendSubscribeStateChangeMessage(...this._config.outgoingPipes);
    this._messageHandler.sendGetStateMessage(...this._config.outgoingPipes);
  }

  protected authnMessageListener(message: Message.AuthnMessage) {
    if (! this._authenticated) {
      this._authenticatedClientFactory(message.key, message.secret, message.passphrase, this._config.apiUrl);

      this._authenticated = true;
      this._tradeExecutor.authenticated = true;
    } else {
      this._logger.log('warn', 'Received authn message, but already authenticated. Ignoring message.');
    }
  }

  protected unauthnMessageListener(message: Message.UnauthnMessage) {
    if (this._authenticated) {
      AuthenticatedClient.unauthenticate();

      this._authenticated = false;
      this._tradeExecutor.authenticated = false;
    } else {
      this._logger.log('warn', 'Received unauthn message, but not (yet) authenticated. Ignoring message.');
    }
  }

  protected tradeMessageListener(message: Message.TradeMessage) {
    this._tradeExecutor.trade(message).catch(error => {
      this._logger.log('warn', 'Trade not placed due to error: ' + error);
    });
  }

  protected async _run() {
    try {
      this._stateManager.changeState(State.Ready);

      this._messageHandler.sendStartupMessage(...this._config.outgoingPipes);

      // TODO handshaking logic
      // wait for handshakes to complete if they haven't already
      // while (! this._handshakes.complete()) {
      //   await sleep(50);
      // }

      // wait for authentication
      while (! this._authenticated) {
        await sleep(500);
      }

      await this._gdaxGetAccounts.getAccounts(this._config.product.toUpperCase().split('-'));

      // loop forever
      while (true) {
        await sleep(2000);
      }
    } catch (error) {
      this._logger.log('info', `Caught unexpected error: ${error}`);
    }
  }
}


export { App };
