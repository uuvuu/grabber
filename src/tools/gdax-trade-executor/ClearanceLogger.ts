
import * as fs from 'fs';

import { inject, injectable } from 'inversify';
import { TYPES as CONFIG_TYPES } from '../../infrastructure/config/module';
import { ProductConfig } from '../../infrastructure/config/ProductConfigOptionsGroup';

import { Logger } from '../../infrastructure/logger/Logger';
import { LoggerConfig } from '../../infrastructure/logger/LoggerConfigOptionsGroup';
import { TYPES as LOGGER_TYPES } from '../../infrastructure/logger/module';


@injectable()
class ClearanceLogger {
  private static readonly CLEARANCES_FILE = 'order-clearances.csv';
  private static readonly CLEARANCES_FILLS_FILE = 'order-clearances-fills.csv';
  private static readonly CLEARANCES_FULL_FILE = 'order-clearances-full.csv';

  private static readonly HEADER = 'orderStartTime,orderFinishTime,latestCandleTime,gdaxOrderId,side,price,midMarketPrice,size,filledSize,status,fiatAmount,cryptoAmount,totalFiatAmount';

  constructor(
    @inject(CONFIG_TYPES.Config) protected readonly _config: ProductConfig & LoggerConfig,
    @inject(LOGGER_TYPES.Logger) protected readonly _logger: Logger) {}

  logClearance(order, accounts, midMarketPrice) {
    try {
      if (!order.orderId.startsWith('NO-ORDER')
        && !order.orderId.startsWith('ORDER-AMOUNT-TOO-SMALL')
        && order.status !== 'rejected'
        && order.executedValue > 0) {
        this._logClearance(order, accounts, midMarketPrice, this._config.logDirectory + '/' + ClearanceLogger.CLEARANCES_FILLS_FILE);
      }

      if (!order.orderId.startsWith('NO-ORDER')
        && !order.orderId.startsWith('ORDER-AMOUNT-TOO-SMALL')) {
        this._logClearance(order, accounts, midMarketPrice, this._config.logDirectory + '/' + ClearanceLogger.CLEARANCES_FILE);
      }

      this._logClearance(order, accounts, midMarketPrice, this._config.logDirectory + '/' + ClearanceLogger.CLEARANCES_FULL_FILE);
    } catch (error) {
      this._logger.log('error', `Error writing clearances file(s). Error was: ${error}. Order was: ${JSON.stringify(order)}`);
    }
  }

  private _logClearance(order, accounts, midMarketPrice, logFile) {
    if (! fs.existsSync(logFile)) {
      this.writeHeader(logFile);
    }

    let totalUSD = 0;

    Object.values(accounts).forEach(account => {
      // @ts-ignore
      totalUSD += +account.balance * +account.exchangeRateUSD;
    });

    const [ cryptoCurrency, fiatCurrency ] = this._config.product.split('-');

    const line =
      `${order.createdTime.toISOString()},${order.doneTime.toISOString()},${order.candleTime.toISOString()},`
      + `${order.orderId},${order.side},${order.price},${midMarketPrice},${order.size},${order.filledSize},`
      + `${order.clearanceStatus},${accounts[fiatCurrency].balance},${accounts[cryptoCurrency].balance},`
      + `${totalUSD}`;

    this._logger.log('info', `Logging order clearance to ${logFile}: ${line}`);

    fs.appendFileSync(logFile, line + '\n');
  }

  private writeHeader(logFile) {
    this._logger.log('info', `Writing header for order clearances file: ${ClearanceLogger.HEADER}`);
    fs.appendFileSync(logFile, ClearanceLogger.HEADER + '\n');
  }
}


export { ClearanceLogger };
