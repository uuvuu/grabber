
import { inject, injectable, named } from 'inversify';

import { GdaxTickerClient } from '../../gdax/network/api/ticker/GdaxTickerClient';
import { GdaxTickerRequest, GdaxTickerResponse } from '../../gdax/network/api/ticker/GdaxTickerRequest';
import { GdaxTickerRequestThrottler } from '../../gdax/network/api/ticker/GdaxTickerRequestThrottler';
import { GdaxNetworkConfig } from '../../gdax/network/config/GdaxNetworkConfigOptionsGroup';
import { Component } from '../../infrastructure/component/Component';
import { TYPES as CONFIG_TYPES } from '../../infrastructure/config/module';
import { Logger } from '../../infrastructure/logger/Logger';
import { TYPES as LOGGER_TYPES } from '../../infrastructure/logger/module';
import { TYPES as NETWORK_TYPES } from '../../infrastructure/network/api/module';
import { RetryableRequester } from '../../infrastructure/network/api/RetryableRequester';


@injectable()
class GdaxGetTicker extends RetryableRequester<GdaxTickerRequest, GdaxTickerResponse> implements Component {
  constructor(
    @inject(CONFIG_TYPES.Config) readonly _config: GdaxNetworkConfig,
    @inject(LOGGER_TYPES.Logger) readonly _logger: Logger,
    @inject(NETWORK_TYPES.RequestThrottler) @named('Ticker') protected readonly _requestThrottler: GdaxTickerRequestThrottler<GdaxTickerClient>) {

    super(_logger, _config.requestRetryDelayInMillis, _config.requestRetries, (request) => { return () => this._requestThrottler.request(request); });
  }

  async load() {
  }

  async unload() {
  }

  async getTicker(product: string) {
    return await this.request(new GdaxTickerRequest(product));
  }

  get stats() {
    return {
      requestSuccessCount: this._requestSuccessCount,
      requestErrorCount: this._requestErrorCount
    };
  }

  protected errorMessage(error: any, request: GdaxTickerRequest) {
    let errorStr = JSON.stringify(error);
    if (errorStr === '{}') {
      errorStr = error;
    }

    return `Ticker for request #${request.requestNumber} had an error. Error was: '${errorStr}'`;
  }
}


export { GdaxGetTicker };
