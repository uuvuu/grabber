
import { LimitOrder  } from 'gdax';
import { inject, injectable, named } from 'inversify';

import { FILL_TYPES, ORDER_TYPES } from '../../domain/order/module';
import { Order } from '../../domain/order/entities/Order';
import { FillRepository } from '../../domain/order/repositories/FillRepository';
import { OrderRepository } from '../../domain/order/repositories/OrderRepository';
import { GdaxAccountClient } from '../../gdax/network/api/account/GdaxAccountClient';
import { GdaxAccountRequest } from '../../gdax/network/api/account/GdaxAccountRequest';
import { GdaxAccountRequestThrottler } from '../../gdax/network/api/account/GdaxAccountRequestThrottler';
import { GdaxFillClient } from '../../gdax/network/api/order/GdaxFillClient';
import { GdaxFillRequest } from '../../gdax/network/api/order/GdaxFillRequest';
import { GdaxFillRequestThrottler } from '../../gdax/network/api/order/GdaxFillRequestThrottler';
import { GdaxOrderClient } from '../../gdax/network/api/order/GdaxOrderClient';
import { GdaxOrderRequest } from '../../gdax/network/api/order/GdaxOrderRequest';
import { GdaxOrderRequestThrottler } from '../../gdax/network/api/order/GdaxOrderRequestThrottler';
import { GdaxPlaceOrderClient } from '../../gdax/network/api/order/GdaxPlaceOrderClient';
import { GdaxPlaceOrderRequest, GdaxPlaceOrderResponse } from '../../gdax/network/api/order/GdaxPlaceOrderRequest';
import { GdaxPlaceOrderRequestThrottler } from '../../gdax/network/api/order/GdaxPlaceOrderRequestThrottler';
import { GdaxNetworkConfig } from '../../gdax/network/config/GdaxNetworkConfigOptionsGroup';
import { GdaxAccountToAccount } from '../../gdax/network/entities/GdaxAccountToAccount';
import { GdaxFillToFill } from '../../gdax/network/entities/GdaxFillToFill';
import { GdaxOrderToOrder } from '../../gdax/network/entities/GdaxOrderToOrder';
import { Component } from '../../infrastructure/component/Component';
import { TYPES as CONFIG_TYPES } from '../../infrastructure/config/module';
import { ProductConfig } from '../../infrastructure/config/ProductConfigOptionsGroup';
import { Query } from '../../infrastructure/data-access/Query';
import { Logger } from '../../infrastructure/logger/Logger';
import { TYPES as LOGGER_TYPES } from '../../infrastructure/logger/module';
import { TYPES as NETWORK_TYPES } from '../../infrastructure/network/api/module';
import { RetryableRequester } from '../../infrastructure/network/api/RetryableRequester';
import { Message } from '../../infrastructure/pipes/Message';
import { ClearanceLogger } from './ClearanceLogger';

import { GdaxGetTicker } from './GdaxGetTicker';
import { TYPES } from './types';
import { TradeConfig } from './TradeConfigOptionsGroup';


@injectable()
class TradeExecutor extends RetryableRequester<GdaxPlaceOrderRequest, GdaxPlaceOrderResponse> implements Component {
  private static readonly STALE_MESSAGE_TIMEOUT_MILLIS = 75000;

  private _fetchOrderTimeout;

  private _previousTicker;
  private _ticker;
  private _accounts;

  private _orderPlacedMidMarketPrice = {};

  private _authenticated = false;

  constructor(
    @inject(CONFIG_TYPES.Config) protected readonly _config: GdaxNetworkConfig & ProductConfig & TradeConfig,
    @inject(LOGGER_TYPES.Logger) readonly _logger: Logger,
    @inject(FILL_TYPES.FillRepository) protected readonly _fillRepository: FillRepository,
    @inject(ORDER_TYPES.OrderRepository) protected readonly _orderRepository: OrderRepository,
    @inject(TYPES.GdaxGetTicker) protected readonly _gdaxGetTicker: GdaxGetTicker,
    @inject(NETWORK_TYPES.RequestThrottler) @named('PlaceOrder') protected readonly _requestThrottler: GdaxPlaceOrderRequestThrottler<GdaxPlaceOrderClient>,
    @inject(NETWORK_TYPES.RequestThrottler) @named('Order') protected readonly _orderRequestThrottler: GdaxOrderRequestThrottler<GdaxOrderClient>,
    @inject(NETWORK_TYPES.RequestThrottler) @named('Fill') protected readonly _fillRequestThrottler: GdaxFillRequestThrottler<GdaxFillClient>,
    @inject(NETWORK_TYPES.RequestThrottler) @named('Account') private readonly _accountRequestThrottler: GdaxAccountRequestThrottler<GdaxAccountClient>,
    @inject(TYPES.ClearanceLogger) protected readonly _clearanceLogger: ClearanceLogger) {

    super(
      _logger,
      _config.tradeRequestRetryDelayInMillis,
      _config.tradeRequestRetries,
      (request) => { return () => this._requestThrottler.request(request); })
    ;
  }

  async load() {
  }

  async unload() {
    clearTimeout(this._fetchOrderTimeout);
  }

  get authenticated() {
    return this._authenticated;
  }

  set authenticated(_authenticated: boolean) {
    this._authenticated = _authenticated;
  }

  async trade(message: Message.TradeMessage) {
    if (this._config.fakeMode) {
      this._logger.log('info', 'Running in fake-mode. Not placing real orders!');
    }

    const now = new Date();

    if (now.getTime() - message.candleDate.getTime() > TradeExecutor.STALE_MESSAGE_TIMEOUT_MILLIS) {
      this._logger.log('warn', `Received trade message, but message is too old. Ignoring message. ` +
        `Candle date: ${message.candleDate.toISOString()}, Current date: ${now.toISOString()}`);
      return;
    }

    const side = this.calculateSide(message);
    const price = await this.calculatePrice(side);
    const size = await this.calculateSize(side, price);

    const previousTickerMidMarketPrice = this.calculateMidMarketPrice(this._previousTicker);
    const currentTickerMidMarketPrice = this.calculateMidMarketPrice(this._ticker);

    const percentPriceChange = Math.abs((previousTickerMidMarketPrice - currentTickerMidMarketPrice) / previousTickerMidMarketPrice);

    if (percentPriceChange > this._config.maxTickerChangePercent) {
      throw `Exiting trade executor process. The percent difference between old ticker price: ${previousTickerMidMarketPrice} `
      + `and new ticker price: ${currentTickerMidMarketPrice} is greater than the configured tolerance `
      + `of: ${this._config.maxTickerChangePercent * 100}%`;
    }

    const gdaxOrder = {
      product_id: this._config.product,
      type: Order.Type.LIMIT.toString(),
      side: side.toString(),
      price: price,
      size: size,
      time_in_force: Order.TimeInForce.GTT.toString(),
      cancel_after: Order.CancelAfter.MIN.toString(),
      post_only: true
    } as LimitOrder;

    let order;

    if (side !== Order.Side.NONE && ! this._config.fakeMode && this.authenticated) {
      try {
        const response = await this.request(new GdaxPlaceOrderRequest(gdaxOrder));

        if (response && response.data) {
          if (response.data.status === 'rejected') {
            this._logger.log('warn', `Order was rejected. Reason: ${response.data.reject_reason}`);
          } else {
            this._logger.log('info', `Successfully placed order: ${response.data.id}`);

            this._orderPlacedMidMarketPrice[response.data.id] = this.calculateMidMarketPrice(this._ticker);

            this._logger.log('debug', `placedOrder = ${JSON.stringify(response.data, undefined, 2)}`);

            this.updateCompletedOrder(response.data);
          }
        }

        order = GdaxOrderToOrder.newOrderDocumentFromGdaxOrder(response.data, message.candleDate);

        if (response.data.status === 'rejected') {
          order.clearanceStatus = response.data.reject_reason;
          this._clearanceLogger.logClearance(order, this._accounts, this.calculateMidMarketPrice(this._ticker));
        }

      } catch (error) {
        if (error && error.message) {
          if (error.message.match(/size is too small/)) {
            this._logger.log('info', `Order size too small. '${GdaxOrderToOrder.NO_ORDER_SIZE_TOO_SMALL_PREFIX}' order created.`);
            order = GdaxOrderToOrder.newSizeTooSmallOrder(gdaxOrder, message.candleDate);

            this._clearanceLogger.logClearance(order, this._accounts, this.calculateMidMarketPrice(this._ticker));
          } else if (error.message.match(/size is too accurate/)) {
            this._logger.log('warn', `Order size was too accurate. Order rejected.`);
          } else if (error.message.match(/price is too accurate/)) {
            this._logger.log('warn', `Order price was too accurate. Order rejected.`);
          } else if (error.message.match(/Insufficient funds/)) {
            // This should never happen if we are calculating size correctly based on funds. Instead
            // we'll expect to see 'size is too small'.
            this._logger.log('warn', `Insufficient funds. Order rejected.`);
          }
        }
      }
    } else {
      if (this._config.fakeMode) {
        this._logger.log('info', `Running in fake mode. '${GdaxOrderToOrder.NO_ORDER_FAKE_MODE_PREFIX}' order created.`);

        order = GdaxOrderToOrder.newFakeModeOrder(gdaxOrder, message.candleDate);
      } else if (! this.authenticated) {
        this._logger.log('info', `Not authenticated. '${GdaxOrderToOrder.NO_ORDER_UNAUTHENTICATED_PREFIX}' order created.`);

        order = GdaxOrderToOrder.newUnauthenticatedOrder(gdaxOrder, message.candleDate);
      } else {
        this._logger.log('info', `Order size was 0. '${GdaxOrderToOrder.NO_ORDER_NO_OP_PREFIX}' order created.`);

        order = GdaxOrderToOrder.newNoOpOrder(gdaxOrder, message.candleDate, this.authenticated);
      }

      this._clearanceLogger.logClearance(order, this._accounts, this.calculateMidMarketPrice(this._ticker));
    }

    await this._orderRepository.create(order);
  }

  private async getTicker() {
    this._previousTicker = this._ticker;
    this._ticker = await this._gdaxGetTicker.getTicker(this._config.product);

    if (! this._previousTicker) {
      this._previousTicker = this._ticker;
    }
  }

  private async getAccounts() {
    const accounts = await this._accountRequestThrottler.request(new GdaxAccountRequest());

    this._accounts =
      GdaxAccountToAccount.newAccountsMapFromGdaxAccounts(
        accounts.data, this._config.product, '' + this.calculateMidMarketPrice(this._ticker));

    return this._accounts;
  }

  private calculateSide(message: Message.TradeMessage) {
    // in fake mode create NONE side trades that will not execute
    if (this._config.fakeMode) {
      return Order.Side.NONE;
    }

    return +message.amount > 0 ? Order.Side.BUY : +message.amount < 0 ? Order.Side.SELL : Order.Side.NONE;
  }

  private async calculatePrice(side: Order.Side) {
    await this.getTicker();
    const incrementInDollars = this._config.incrementLimitPriceCents / 100;

    const price = this.calculateMidMarketPrice(this._ticker);

    return (price + (Order.Side.BUY === side ? -1 : 1) * incrementInDollars).toFixed(2).toString();
  }

  private async calculateSize(side: Order.Side, price: string) {
    const [ cryptoName, fiatName ] = this._config.product.split('-');

    await this.getAccounts();

    let size = 0;

    if (side === Order.Side.BUY) {
      size = +this._accounts[fiatName].available / +price;
    } else {
      size = +this._accounts[cryptoName].available;
    }

    size *= this._config.maxTradePercent;

    // round *down* to 8 decimal places, toFixed() rounds up and we get insufficient funds issues
    const EIGHT_DECIMALS = 100000000;
    size = Math.floor(size * EIGHT_DECIMALS) / EIGHT_DECIMALS;

    return size.toFixed(8).toString();
  }

  private calculateMidMarketPrice(ticker: any) {
    return (+ticker.data.ask + +ticker.data.bid) / 2;
  }

  private updateCompletedOrder(order: any) {
    const orderId = order.id;
    const query: Query<Order.OrderDocument> = { filter: { orderId: orderId }};

    const now = new Date();
    const expireTime =
      order.expire_time && ! order.expire_time.toUpperCase().endsWith('Z') ? new Date(order.expire_time + 'Z') : undefined;

    // fetch order delay with a 5 second buffer to allow for order settlement and fees in 'hold'
    // to returned
    const fetchDelay = expireTime.getTime() - now.getTime() + 5000;

    this._fetchOrderTimeout = setTimeout(async () => {
      const midMarketPrice = this._orderPlacedMidMarketPrice[order.id];
      delete this._orderPlacedMidMarketPrice[order.id];

      await this.getTicker();
      await this.getAccounts();

      let updatedOrder;

      try {
        const gdaxOrder = await this._orderRequestThrottler.request(new GdaxOrderRequest(order.id));

        this._logger.log('debug', `updatedOrder = ${JSON.stringify(gdaxOrder, undefined, 2)}`);

        const clearanceStatus =
          gdaxOrder.data.size === gdaxOrder.data.filled_size ? gdaxOrder.data.done_reason : 'partially filled';

        updatedOrder = await this._orderRepository.findOneByQueryAndUpdate(query,
          { $set:
              {
                status: gdaxOrder.data.status,
                doneReason: gdaxOrder.data.done_reason,
                doneTime: gdaxOrder.data.done_at,
                clearanceStatus: clearanceStatus,
                executedValue: gdaxOrder.data.executed_value,
                fillFees: gdaxOrder.data.fill_fees,
                filledSize: gdaxOrder.data.filled_size,
                settled: gdaxOrder.data.settled,
                stp: gdaxOrder.data.stp
              }
          });

        const gdaxFills = await this._fillRequestThrottler.request(new GdaxFillRequest(order.id));

        const fills = GdaxFillToFill.newFillDocumentsFromGdaxFills(gdaxFills.data);

        this._logger.log('debug', `fills = ${JSON.stringify(fills, undefined, 2)}`);

        await this._fillRepository.create(...fills);

        this._logger.log('info', `Order ${orderId} was ${clearanceStatus}.`);
      } catch (error) {
        if (error && error.message) {
          if (error.message.match(/HTTP 404 Error: NotFound/)) {
            updatedOrder = await this._orderRepository.findOneByQueryAndUpdate(query,
              { $set:
                  {
                    status: Order.Status.DONE.toString(),
                    doneReason: 'canceled',
                    doneTime: expireTime,
                    clearanceStatus: 'expire time exceeded',
                    settled: true
                  }
              });

            this._logger.log('info', `Order ${orderId} was canceled.`);
          }
        }
      }

      this._clearanceLogger.logClearance(updatedOrder, this._accounts, midMarketPrice);
    }, fetchDelay);
  }
}


export { TradeExecutor };
