
import { inject, injectable, named } from 'inversify';

import { Account } from '../../domain/account/entities/Account';
import { TYPES as ACCOUNT_TYPES } from '../../domain/account/module';
import { AccountRepository } from '../../domain/account/repositories/AccountRepository';
import { GdaxAccountClient } from '../../gdax/network/api/account/GdaxAccountClient';
import { GdaxAccountRequest, GdaxAccountResponse } from '../../gdax/network/api/account/GdaxAccountRequest';
import { GdaxAccountRequestThrottler } from '../../gdax/network/api/account/GdaxAccountRequestThrottler';
import { GdaxTickerResponse } from '../../gdax/network/api/ticker/GdaxTickerRequest';
import { GdaxNetworkConfig } from '../../gdax/network/config/GdaxNetworkConfigOptionsGroup';
import { GdaxAccountToAccount } from '../../gdax/network/entities/GdaxAccountToAccount';
import { Component } from '../../infrastructure/component/Component';
import { TYPES as CONFIG_TYPES } from '../../infrastructure/config/module';
import { ProductConfig } from '../../infrastructure/config/ProductConfigOptionsGroup';
import { MongoConfig } from '../../infrastructure/data-access/MongoConfigOptionsGroup';
import { Logger } from '../../infrastructure/logger/Logger';
import { LoggerConfig } from '../../infrastructure/logger/LoggerConfigOptionsGroup';
import { TYPES as LOGGER_TYPES } from '../../infrastructure/logger/module';
import { TYPES as NETWORK_TYPES } from '../../infrastructure/network/api/module';
import { RetryableRequester } from '../../infrastructure/network/api/RetryableRequester';

import { GdaxGetTicker } from './GdaxGetTicker';
import { TYPES } from './types';


@injectable()
class GdaxGetAccounts extends RetryableRequester<GdaxAccountRequest, GdaxAccountResponse> implements Component {
  private _insertedAccountsCount = 0;
  private _dbErrorCount = 0;

  private _currentAccounts: Account.AccountsDocument;

  private _getAccountsInterval;

  constructor(
    @inject(CONFIG_TYPES.Config) readonly _config: GdaxNetworkConfig & ProductConfig & LoggerConfig & MongoConfig,
    @inject(LOGGER_TYPES.Logger) readonly _logger: Logger,
    @inject(NETWORK_TYPES.RequestThrottler) @named('Account') private readonly _requestThrottler: GdaxAccountRequestThrottler<GdaxAccountClient>,
    @inject(ACCOUNT_TYPES.AccountRepository) private readonly _accountRepository: AccountRepository,
    @inject(TYPES.GdaxGetTicker) private readonly _gdaxGetTicker: GdaxGetTicker) {

    super(_logger, _config.requestRetryDelayInMillis, _config.requestRetries, (request) => { return () => this._requestThrottler.request(request); });
  }

  async load() {
  }

  async unload() {
    clearInterval(this._getAccountsInterval);
  }

  async getAccounts(currencies: string[]) {
    this._currentAccounts = await this._getAccounts();

    this.logAccounts(currencies, true);

    this._getAccountsInterval = setInterval(async () => {
      this._currentAccounts = await this._getAccounts();

      this.logAccounts(currencies);
    }, 60000);
  }

  protected async _getAccounts() {
    return this.request(new GdaxAccountRequest())
      .then(async (response) => {
        const tickerRequests: Promise<GdaxTickerResponse>[] = [];

        for (let i = 0; i < response.data.length; i++) {
          if (response.data[i].currency.toUpperCase() === 'USD') {
            response.data[i].exchangeRateUSD = '1';
          } else {
            tickerRequests.push(this._gdaxGetTicker.getTicker(response.data[i].currency.toUpperCase() + '-USD'));
          }
        }

        const tickerResponses = await Promise.all(tickerRequests);

        for (let i = 0; i < response.data.length; i++) {
          if (response.data[i].currency.toUpperCase() !== 'USD') {
            for (let j = 0; j < tickerResponses.length; j++) {
              if (response.data[i].currency.toUpperCase() === tickerResponses[j].request.product.split('-')[0]) {
                response.data[i].exchangeRateUSD = (+tickerResponses[j].data.ask + +tickerResponses[j].data.bid) / 2;
              }
            }
          }
        }

        // all accounts live under the same profile id
        const accounts = GdaxAccountToAccount.newAccountsDocumentFromGdaxAccounts(response.data, response.data[0].profile_id);

        try {
          await this.saveAccounts(accounts);
        } catch (error) {
          this._dbErrorCount++;
          this._logger.log('error', this.errorMessage(error));
        }

        return accounts;
      })
      .catch(async (error) => {
        let errorStr = JSON.stringify(error);
        if (errorStr === '{}') {
          errorStr = error;
        }

        this._logger.log(
          'error',
          `Request for tickers had an error. Error was: '${JSON.stringify(JSON.parse(errorStr), undefined, 2)}'`);
        this._logger.log('error', 'Using data for last row in accounts table instead.');

        try {
          const lastAccounts = await this._accountRepository.findLatestItem();

          // unset _id so it will be inserted instead of updated
          lastAccounts._id = undefined;
          lastAccounts.accounts.forEach(account => {
            // @ts-ignore
            account._id = undefined;
          });

          await this.saveAccounts(lastAccounts);

          return lastAccounts;

        } catch (error) {
          this._dbErrorCount++;
          this._logger.log('error', this.errorMessage(error));

          throw error;
        }
      });
  }

  protected logAccounts(currencies: string[], firstMessage = false) {
    let accounts;

    if (currencies && currencies.length > 0) {
      accounts = this._currentAccounts.accounts.filter(account => {
        return currencies.includes(account.currency.toUpperCase());
      });
    }

    let totalUSD = 0;

    accounts.forEach(account => {
      if (firstMessage) {
        this._logger.log('info', `Initial ${account.currency} balance since restart:`);
      } else {
        this._logger.log('info', `Current ${account.currency} balance:`);
      }

      this._logger.log('info', `    balance:           ${account.balance}`);
      this._logger.log('info', `    available:         ${account.available}`);
      this._logger.log('info', `    hold:              ${account.hold}`);
      this._logger.log('info', `    exchangeRateUSD:   ${account.exchangeRateUSD}`);

      totalUSD += +account.balance * +account.exchangeRateUSD;
    });

    this._logger.log('info', `Total USD balance : ${totalUSD}`);
  }

  get stats() {
    return {
      requestSuccessCount: this._requestSuccessCount,
      requestErrorCount: this._requestErrorCount,
      insertedAccountsCount: this._insertedAccountsCount,
      dbErrorCount: this._dbErrorCount,
      currentAccounts: this._currentAccounts
    };
  }

  protected async saveAccounts(accounts: Account.AccountsDocument) {
    const results = await this._accountRepository.create(accounts);

    this._insertedAccountsCount += results.length;
  }

  protected errorMessage(error: any) {
    let errorStr = JSON.stringify(error);
    if (errorStr === '{}') {
      errorStr = error;
    }

    return `Request for accounts had an error. Error was: '${JSON.stringify(JSON.parse(errorStr), undefined, 2)}'`;
  }
}


export { GdaxGetAccounts };
