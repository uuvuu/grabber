
import 'reflect-metadata';

import { AsyncContainerModule, ContainerModule, interfaces } from 'inversify';

import { AccountModule } from '../../domain/account/module';
import { OrderModule } from '../../domain/order/module';
import { GdaxAccountNetworkModule } from '../../gdax/network/api/account/module';
import { GdaxOrderNetworkModule } from '../../gdax/network/api/order/module';
import { GdaxTickerNetworkModule } from '../../gdax/network/api/ticker/module';

import { GdaxNetworkModule } from '../../gdax/network/module';
import { CliModule } from '../../infrastructure/cli/module';
import { ModelFactoryModule } from '../../infrastructure/data-access/factories/module';
import { DatabaseModule } from '../../infrastructure/data-access/module';
import { LoggerModule } from '../../infrastructure/logger/module';
import { NetworkModule } from '../../infrastructure/network/api/module';
import { PipeModule } from '../../infrastructure/pipes/module';
import { StateModule } from '../../infrastructure/state/module';

import { ClearanceLogger } from './ClearanceLogger';
import { GdaxGetAccounts } from './GdaxGetAccounts';
import { GdaxGetTicker } from './GdaxGetTicker';
import { TradeExecutor } from './TradeExecutor';
import { TYPES } from './types';


const GdaxTradeExecutorModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<ClearanceLogger>(TYPES.ClearanceLogger).to(ClearanceLogger).inSingletonScope();
    bind<GdaxGetAccounts>(TYPES.GdaxGetAccounts).to(GdaxGetAccounts).inSingletonScope();
    bind<GdaxGetTicker>(TYPES.GdaxGetTicker).to(GdaxGetTicker).inSingletonScope();
    bind<TradeExecutor>(TYPES.TradeExecutor).to(TradeExecutor).inSingletonScope();
  }
);


const DEPENDENCIES: ContainerModule[] = [
  AccountModule,
  CliModule,
  GdaxAccountNetworkModule,
  GdaxTickerNetworkModule,
  GdaxNetworkModule,
  GdaxOrderNetworkModule,
  GdaxTradeExecutorModule,
  LoggerModule,
  ModelFactoryModule,
  NetworkModule,
  OrderModule,
  PipeModule,
  StateModule
];

const ASYNC_DEPENDENCIES: AsyncContainerModule[] = [
  DatabaseModule
];


export { ASYNC_DEPENDENCIES, DEPENDENCIES, GdaxTradeExecutorModule };
