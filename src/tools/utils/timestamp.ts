
import * as program from 'commander';

interface ConfigData {
  programName: string;
  minutes: number;
  numberOnly: boolean;
  dateToNow: Date;
  epoch: number;
}

function parseDate(date: any) {
  return new Date(date);
}

export function processCli(): ConfigData {
  program
    .version('0.0.1')
    .description('A tool for providing a ISO 8601 date string')
    .option('-d, --dateToNow [dateToNow]', 'Display number of minutes from date to now', parseDate)
    .option('-n, --numberOnly [numberOnly]', 'Display number of millis since epoch only', false)
    .option('-m, --minutes [minutes]', 'Minutes to subtract from current time', parseInt)
    .option('-e, --epoch [epoch]', 'Convert epoch timestamp to ISO date', parseInt)
    .parse(process.argv);

  const cliData = <ConfigData>{
    programName: program.name(),
    minutes: program.minutes,
    numberOnly: program.numberOnly,
    dateToNow: program.dateToNow,
    epoch: program.epoch
  };

  if (typeof program.minutes !== 'undefined' && (! Number.isInteger(program.minutes) || cliData.minutes < 0)) {
    console.log(`\n  Minutes must be a positive integer.`);
    console.log(`\n  For more help see: ${cliData.programName} -h`);

    process.exit(1);
  }

  if (typeof program.minutes !== 'undefined' && typeof program.dateToNow !== 'undefined') {
    console.log(`\n  Can only define minutes or dateToNow, but not both.`);
    console.log(`\n  For more help see: ${cliData.programName} -h`);

    process.exit(1);
  }

  if (typeof program.minutes === 'undefined'
      && typeof program.dateToNow === 'undefined'
      && typeof program.epoch === 'undefined') {
    console.log(`\n  Either minutes, dateToNow or epoch must be defined.`);
    console.log(`\n  For more help see: ${cliData.programName} -h`);

    process.exit(1);
  }


  if (typeof program.dateToNow !== 'undefined') {
    if (isNaN(program.dateToNow.getTime())) {
      console.log(`\n  dateToNow is an invalid date. It must be a valid date in ISO8601 format.`);
      console.log(`\n  For more help see: ${cliData.programName} -h`);

      process.exit(1);
    }
  }

  if (typeof program.epoch !== 'undefined') {
    if (! Number.isInteger(program.epoch) || program.epoch < 0) {
      console.log(`\n  epoch is an invalid integer.`);
      console.log(`\n  For more help see: ${cliData.programName} -h`);

      process.exit(1);
    }
  }

  return cliData;
}

const config: ConfigData = processCli();

if (typeof config.minutes !== 'undefined') {
  const date = new Date();
  date.setMinutes(date.getMinutes() - config.minutes);
  date.setSeconds(0);
  date.setMilliseconds(0);

  if (!config.numberOnly) {
    process.stdout.write(date.toISOString() + '     ' + date.getTime());
  } else {
    process.stdout.write(date.getTime() + '');
  }
} else if (typeof config.epoch !== 'undefined') {
  process.stdout.write(new Date(config.epoch * 1000).toISOString());
} else {
  const now = new Date();
  const timeDiffInMins = Math.ceil((now.getTime() - config.dateToNow.getTime()) / 1000 / 60);

  process.stderr.write(`Minutes (ceiling) between ${config.dateToNow.toISOString()} and ${now.toISOString()} = ${timeDiffInMins}`);
}




