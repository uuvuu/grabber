
import { inject, injectable } from 'inversify';

import { CandleConfig } from '../../domain/candle/CandleConfigOptionsGroup';
import { TYPES as CANDLE_TYPES } from '../../domain/candle/module';
import { CandleRepository } from '../../domain/candle/repositories/CandleRepository';
import { GdaxCandleNetworkConfig, GdaxCandleNetworkConfigOptionsGroup } from '../../gdax/network/api/candle/GdaxCandleNetworkConfigOptionsGroup';
import { GdaxNetworkConfig } from '../../gdax/network/config/GdaxNetworkConfigOptionsGroup';
import { TYPES as NETWORK_TYPES } from '../../gdax/network/types';
import { GdaxTickerChannel } from '../../gdax/network/websocket/channels/GdaxTickerChannel';
import { WebsocketFeed } from '../../gdax/network/websocket/WebsocketFeed';
import { App as BaseApp } from '../../infrastructure/App';
import { TYPES as CONFIG_TYPES } from '../../infrastructure/config/module';
import { DatabaseClient } from '../../infrastructure/data-access/DatabaseClient';
import { TYPES as DB_TYPES } from '../../infrastructure/data-access/module';
import { Logger } from '../../infrastructure/logger/Logger';
import { TYPES as LOGGER_TYPES } from '../../infrastructure/logger/module';
import { Handshakes, ShakeType } from '../../infrastructure/pipes/Handshakes';
import { MessageHandler } from '../../infrastructure/pipes/MessageHandler';
import { NamedPipe } from '../../infrastructure/pipes/NamedPipe';
import { PipeConfig } from '../../infrastructure/pipes/PipeConfigOptionsGroup';
import { PipeManager } from '../../infrastructure/pipes/PipeManager';
import { TYPES as PIPES_TYPES } from '../../infrastructure/pipes/types';
import { TYPES as STATE_TYPES } from '../../infrastructure/state/module';
import { State, StateManager } from '../../infrastructure/state/State';
import { sleep } from '../../utils/sleep';

import { GdaxGetCandles } from '../gdax-get-candles/GdaxGetCandles';
import { TYPES as GET_CANDLES_TYPES } from '../gdax-get-candles/module';

import { CandleMaker } from './CandleMaker';
import { Config } from './Config';
import { TYPES } from './module';
import { TimeLastLiveHandler } from './TimeLastLiveHandler';


@injectable()
class App extends BaseApp<Config> {
  // @ts-ignore - typescript's string enum behavior is a bit annoying
  private readonly _busyState: State = State[State.Busy];

  private _timeLastLiveMessage: { target: NamedPipe, timeLastLiveDate: Date };

  private _getCandlesTimeout;

  public constructor(
    @inject(CONFIG_TYPES.Config) readonly _config: Config & CandleConfig & GdaxNetworkConfig & GdaxCandleNetworkConfig & PipeConfig,
    @inject(LOGGER_TYPES.Logger) readonly _logger: Logger,
    @inject(TYPES.CandleMaker) protected readonly _candleMaker: CandleMaker,
    @inject(CANDLE_TYPES.CandleRepository) private readonly _candleRepository: CandleRepository,
    @inject(DB_TYPES.DatabaseClient) readonly _databaseClient: DatabaseClient,
    @inject(PIPES_TYPES.Handshakes) readonly _handshakes: Handshakes,
    @inject(NETWORK_TYPES.GdaxTickerChannel) protected readonly _gdaxTickerChannel: GdaxTickerChannel,
    @inject(GET_CANDLES_TYPES.GdaxGetCandles) protected readonly _getCandles: GdaxGetCandles,
    @inject(PIPES_TYPES.MessageHandler) protected readonly _messageHandler: MessageHandler,
    @inject(PIPES_TYPES.PipeManager) protected readonly _pipeManager: PipeManager,
    @inject(STATE_TYPES.StateManager) protected readonly _stateManager: StateManager,
    @inject(TYPES.TimeLastLiveHandler) protected readonly _timeLastLiveHandler: TimeLastLiveHandler,
    @inject(NETWORK_TYPES.WebsocketFeed) protected readonly _websocketFeed: WebsocketFeed) {

    super(_config, _logger);

    this._config.channels.push('ticker');

    // the following config options should not be set via the CLI for this app, so we add them after
    // the app has been configured
    this._config.configOptionsGroups.push(new GdaxCandleNetworkConfigOptionsGroup());
    this._config.maxDataItems = GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.GDAX_MAX_DATA_ITEMS;

    this._config.outgoingPipes.forEach(outgoingPipe => {
      this._handshakes.addPartner(outgoingPipe);
    });

    this.registerComponent(this._candleMaker);
    this.registerComponent(this._databaseClient);
    this.registerComponent(this._pipeManager);
    this.registerComponent(this._stateManager);
    this.registerComponent(this._messageHandler);
    this.registerComponent(this._timeLastLiveHandler);
    this.registerComponent(this._websocketFeed);

    this._messageHandler.addAcknowledgeStartupMessageListener(async (target: NamedPipe) => {
      await this.acknowledgeStartupMessageListener(target);
    });

    this._messageHandler.addStartupMessageListener(async (target: NamedPipe) => {
      await this.startupMessageListener(target);
    });

    this._messageHandler.addSendStateMessageListener(async (target: NamedPipe, state: State) => {
      await this.sendStateMessageListener(target, state);
    });

    this._messageHandler.addSendStateChangeMessageListener(async (target: NamedPipe, oldState: State, newState: State) => {
      await this.sendStateChangeMessageListener(target, oldState, newState);
    });

    this._messageHandler.addTimeLastLiveMessageListener(async (target: NamedPipe, timeLastLiveDate: Date) => {
      await this.timeLastLiveMessageListener(target, timeLastLiveDate);
    });
  }

  protected acknowledgeStartupMessageListener(target: NamedPipe) {
    this._handshakes.shake(target, ShakeType.AcknowledgeStartup);

    this._messageHandler.sendSubscribeStateChangeMessage(...this._config.outgoingPipes);
    this._messageHandler.sendGetStateMessage(...this._config.outgoingPipes);
  }

  protected startupMessageListener(target: NamedPipe) {
    this._handshakes.shake(target, ShakeType.Startup);

    this._messageHandler.sendStartupAcknowledgeMessage(target);
    this._messageHandler.sendSubscribeStateChangeMessage(...this._config.outgoingPipes);
    this._messageHandler.sendGetStateMessage(...this._config.outgoingPipes);
  }

  protected sendStateMessageListener(target: NamedPipe, state: State) {
    this._candleMaker.targetState = state;
    this._timeLastLiveHandler.targetState = state;
  }

  protected sendStateChangeMessageListener(target: NamedPipe, oldState: State, newState: State) {
    this._candleMaker.targetState = newState;
    this._timeLastLiveHandler.targetState = newState;
  }

  protected async timeLastLiveMessageListener(target: NamedPipe, timeLastLiveDate: Date) {
    if (this._stateManager.state === this._busyState) {
      this._logger.log('info', `Grabber is currently busy. Storing timelastlive message to be handled later.`);

      this._timeLastLiveMessage = { target: target, timeLastLiveDate: timeLastLiveDate };

      return;
    }

    await this.handleTimeLastLiveMessage(target, timeLastLiveDate);
  }

  protected async handleTimeLastLiveMessage(target: NamedPipe, timeLastLiveDate: Date) {
    this._stateManager.changeState(State.Busy);

    try {
      await this._timeLastLiveHandler.processCandles(target, timeLastLiveDate);
    } catch (error) {
      this._logger.log('error', error);
    }

    this._stateManager.changeState(State.Ready);
  }

  protected async _run() {
    this._stateManager.changeState(State.Busy);

    const latestCandle = await this._candleRepository.findLatestItem();

    if (latestCandle) {
      this._config.startDate = latestCandle.time;
    } else {
      // this should only happen the first time a system comes up. for completeness and correctness
      // handle this case anyway and seed that data.
      //
      // the seed start date is: 2016-07-01T00:00:00Z
      //
      this._config.startDate = new Date('2016-07-01T00:00:00Z');
    }

    this._messageHandler.sendStartupMessage(...this._config.outgoingPipes);

    const startDelay = App.calculateStartDelay();

    this._logger.log('info', `Waiting ${startDelay} milliseconds for GDAX candle data and local ticker data to sync...`);

    this._getCandlesTimeout = setTimeout(async () => {
      try {
        this._logger.log('info', `Begin getting candles in range: ${this._config.startDate.toISOString()} - NOW.`);

        await this._getCandles.processCandles(this._config.startDate);

        this._logger.log('info', `End getting candles in range: ${this._config.startDate.toISOString()} - NOW.`);
      } catch (error) {
        const errorMessage = error.message ? error.message : error;

        this._logger.log('error', errorMessage);

        await this.terminate('Could not get candles from GDAX. Candle data corrupt. Aborting...', true);
      }

      // TODO handshaking logic
      // // wait for handshakes to complete if they haven't already
      // while (! this._handshakes.complete()) {
      //   await sleep(50);
      // }

      try {
        this._logger.log('info', `Begin making candles from tickers.`);

        // @ts-ignore - typescript's string enum behavior is a bit annoying
        await this._candleMaker.makeCandles(NamedPipe[NamedPipe.CRYPTO_PREDICTOR_PIPE_NAME]);

        if (this._timeLastLiveMessage) {
          await this.handleTimeLastLiveMessage(this._timeLastLiveMessage.target, this._timeLastLiveMessage.timeLastLiveDate);

          this._timeLastLiveMessage = undefined;
        }

        this._stateManager.changeState(State.Ready);
      } catch (error) {
        const errorMessage = error.message ? error.message : error;

        this._logger.log('error', errorMessage);

        await this.terminate('Could not make candles from tickers. Candle data corrupt. Aborting...', true);
      }
    }, startDelay);

    // loop forever
    while (true) {
      await sleep(2000);
    }
  }

  protected async terminateHook() {
    clearTimeout(this._getCandlesTimeout);
  }

  // wait for at least an entire 1 minute interval to be filled with complete ticker data
  private static calculateStartDelay() {
    const now = new Date();

    const nextMinute = new Date(now);
    nextMinute.setMinutes(nextMinute.getMinutes() + 2);
    nextMinute.setMilliseconds(0);

    // add 2 seconds as a buffer time
    nextMinute.setSeconds(2);

    return nextMinute.getTime() - now.getTime();
  }
}


export { App };
