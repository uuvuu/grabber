import { inject, injectable } from 'inversify';

import { CandleConfig } from '../../domain/candle/CandleConfigOptionsGroup';
import { Candle, CandleUtils } from '../../domain/candle/entities/Candle';
import { TYPES as CANDLE_TYPES } from '../../domain/candle/module';
import { CandleRepository } from '../../domain/candle/repositories/CandleRepository';
import { TYPES as TICKER_TYPES } from '../../domain/ticker/module';
import { TickerRepository } from '../../domain/ticker/repositories/TickerRepository';
import { Component } from '../../infrastructure/component/Component';
import { TYPES as CONFIG_TYPES } from '../../infrastructure/config/module';
import { ProductConfig } from '../../infrastructure/config/ProductConfigOptionsGroup';
import { MongoConfig } from '../../infrastructure/data-access/MongoConfigOptionsGroup';
import { Logger } from '../../infrastructure/logger/Logger';
import { LoggerConfig } from '../../infrastructure/logger/LoggerConfigOptionsGroup';
import { TYPES as LOGGER_TYPES } from '../../infrastructure/logger/module';
import { Message } from '../../infrastructure/pipes/Message';
import { MessageHandler } from '../../infrastructure/pipes/MessageHandler';
import { NamedPipe } from '../../infrastructure/pipes/NamedPipe';
import { TYPES as PIPES_TYPES } from '../../infrastructure/pipes/types';
import { TYPES as STATE_TYPES } from '../../infrastructure/state/module';
import { State, StateManager } from '../../infrastructure/state/State';
import { DateUtils } from '../../utils/DateUtils';
import { sleep } from '../../utils/sleep';


const MONGO_DUPLICATE_KEY_ERROR_CODE = 11000;


@injectable()
class CandleMaker implements Component {
  private _startingTimeout;
  private _makeCandlesInterval;

  // @ts-ignore - typescript's string enum behavior is a bit annoying
  private readonly _busyState: State = State[State.Busy];

  private _target: NamedPipe;
  // @ts-ignore - typescript's string enum behavior is a bit annoying
  private _targetState: State = State[State.Busy];

  private _dataQueue: Message.DataMessage[] = [];

  private _filterDateMatchStage = { $match: { time: { $gte: undefined, $lt: undefined }}};

  private _groupStage = {
    $group: {
      _id: undefined,
      product: { $first: '$product' },
      high: { $max: { $toDouble: '$price' }},
      low: { $min: { $toDouble: '$price' }},
      open: { $first: { $toDouble: '$price' }},
      close: { $last: { $toDouble: '$price' }},
      volume: { $sum: { $toDouble: '$lastSize' }},
      count: { $sum: 1 }
    }
  };

  private _projectStage = {
    $project: {
      product: 1,

      time: { $literal: undefined },

      low: 1,
      high: 1,
      open: 1,
      close: 1,

      volume: 1,

      count: 1
    }
  };

  constructor(
    @inject(CONFIG_TYPES.Config) private readonly _config: CandleConfig & LoggerConfig & MongoConfig & ProductConfig,
    @inject(LOGGER_TYPES.Logger) readonly _logger: Logger,
    @inject(CANDLE_TYPES.CandleRepository) private readonly _candleRepository: CandleRepository,
    @inject(TICKER_TYPES.TickerRepository) private readonly _tickerRepository: TickerRepository,
    @inject(PIPES_TYPES.MessageHandler) protected readonly _messageHandler: MessageHandler,
    @inject(STATE_TYPES.StateManager) protected readonly _stateManager: StateManager) {
  }

  async load() {
  }

  async unload() {
    clearTimeout(this._startingTimeout);
    clearInterval(this._makeCandlesInterval);
  }

  set targetState(state: State) {
    this._targetState = state;
  }

  // wrapper method to schedule makeCandles to run close to the minute boundary (2 seconds after)
  async makeCandles(target: NamedPipe) {
    this._target = target;

    await this.bridgeCandles();
    await this._makeCandles();
  }

  private async bridgeCandles() {
    let latestCandle = await this._candleRepository.findLatestItem();

    const nowOnTheMinute = DateUtils.alignDate(new Date(), DateUtils.TimeAlignment.MINUTES);
    const nextCandleTime = new Date(latestCandle.time);
    const bridgeStartTime = new Date(latestCandle.time);

    this._logger.log('info',
      `Starting bridging candle and ticker data in date range = ${bridgeStartTime.toISOString()} - ${nowOnTheMinute.toISOString()}`);

    while (nextCandleTime.getTime() != nowOnTheMinute.getTime()) {
      const dates = DateUtils.newDatePairWithDiff(new Date(nextCandleTime), this._config.durationInMillis, DateUtils.TimeAlignment.MINUTES);

      const aggregationResults = await this.makeAggregateQuery(dates);

      const candle = this.createCandle(latestCandle, dates.startDate, aggregationResults);

      try {
        const newCandle = await this.saveCandle(candle);

        this._logger.log('info', `New bridging candle created: ${JSON.stringify(newCandle, undefined, 2)}`);
      } catch (error) {
        this.handleSaveError(candle, error);
      }

      latestCandle = candle;

      nextCandleTime.setMilliseconds(nextCandleTime.getMilliseconds() + this._config.durationInMillis);
    }

    this._logger.log('info',
      `Ending bridging candle and ticker data in date range = ${bridgeStartTime.toISOString()} - ${nowOnTheMinute.toISOString()}`);
  }

  private async _makeCandles() {
    await this.waitForNextMinute();

    await this.makeCandle();

    // TODO beware of time drift
    this._makeCandlesInterval = setInterval(async () => {
      this._logger.log('debug', `Executing _makeCandles at time: ${new Date().toISOString()}`);

      await this.makeCandle();
    }, this._config.durationInMillis);
  }

  private async waitForNextMinute() {
    const date = DateUtils.alignDate(new Date(), DateUtils.TimeAlignment.MINUTES);
    date.setMilliseconds(date.getMilliseconds() + this._config.durationInMillis);

    // add 2 seconds as a buffer time
    date.setSeconds(2);

    while (new Date() < date) {
      await sleep(500);
    }
  }

  private async makeCandle() {
    const latestCandle = await this._candleRepository.findLatestItem();

    const dates = DateUtils.newDatePairWithDiff(new Date(), -this._config.durationInMillis, DateUtils.TimeAlignment.MINUTES);

    const aggregationResults = await this.makeAggregateQuery(dates);

    const candle = this.createCandle(latestCandle, dates.startDate, aggregationResults);

    try {
      const newCandle = await this.saveCandle(candle);

      this._logger.log('info', `New candle created: ${JSON.stringify(newCandle, undefined, 2)}`);

      if (this._stateManager.state === this._busyState) {
        this._logger.log('info', `System is busy. Emptying candle queue and not sending candle.`);
        this._dataQueue = [];

        return;
      }

      await this.sendCandleMessage(candle);
    } catch (error) {
      this.handleSaveError(candle, error);
    }
  }

  private async makeAggregateQuery(dates: DateUtils.DatePair) {
    // TODO - clean this up to return new objects instead of altering object state that
    // could affect subsequent requests
    this._filterDateMatchStage.$match.time.$gte = dates.startDate;
    this._filterDateMatchStage.$match.time.$lt = dates.endDate;

    this._groupStage.$group._id = `${dates.startDate.toISOString()} - ${dates.endDate.toISOString()}`;

    this._projectStage.$project.time.$literal = dates.startDate;

    const aggregationResults = await this._tickerRepository.aggregate([
      this._filterDateMatchStage,
      this._groupStage,
      this._projectStage
    ]);

    return aggregationResults;
  }

  private createCandle(latestCandle: Candle.CandleDocument, startDate: Date, aggregationResults: any[]) {
    let candle: Candle.CandleDocument;

    if (! aggregationResults.length) {
      this._logger.log('info', `Empty interval. Interpolating candle for: ${startDate.toISOString()}`);
      candle = CandleUtils.interpolateCandle(latestCandle, startDate);
    } else {
      candle = this.candleFromAggregationResult(aggregationResults[0]);
    }

    return candle;
  }

  private async saveCandle(candle: Candle.CandleDocument) {
    const newCandle = (await this._candleRepository.create(candle))[0];

    return newCandle;
  }

  private candleFromAggregationResult(result: any) {
    return {
      product: result.product,
      time: result.time,

      low: result.low.toFixed(2),
      high: result.high.toFixed(2),
      open: result.open.toFixed(2),
      close: result.close.toFixed(2),

      volume: result.volume.toString(),

      source: 'cbpro-websocket',

      interpolated: false
    } as Candle.CandleDocument;
  }

  private handleSaveError(candle, error) {
    if (error.code && error.code === MONGO_DUPLICATE_KEY_ERROR_CODE) {
      this._logger.log(
        'info', `Skipping creating candle at time '${candle.time.toISOString()}' because candle already exists.`);
    } else {
      throw error;
    }
  }

  private async sendCandleMessage(candle: Candle.CandleDocument) {
    const [cryptoName, currencyName] = this._config.product.split('-');

    const dataMessage = {
      dataType: this._targetState === this._busyState ? 'training' : 'live',
      product: cryptoName,
      currency: currencyName,
      timeInMillis: '' + candle.time.getTime(),
      low: candle.low,
      high: candle.high,
      open: candle.open,
      close: candle.close,
      volume: candle.volume,
      timeDeltaInMillis: '0'
    } as Message.DataMessage;

    if (this._targetState === this._busyState) {
      this._logger.log('info', `Target '${NamedPipe[this._target]}' is busy. Queueing candle as training data to be sent later.`);

      this._dataQueue.push(dataMessage);
    } else {
      while (this._dataQueue.length > 0) {
        this._messageHandler.sendDataMessage(this._dataQueue.shift(), this._target);
        await sleep(10);
      }

      this._messageHandler.sendDataMessage(dataMessage, this._target);
    }
  }
}


export { CandleMaker };
