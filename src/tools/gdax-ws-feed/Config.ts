
import { Command } from 'commander';
import { inject, injectable } from 'inversify';

import { CandleConfigOptionsGroup } from '../../domain/candle/CandleConfigOptionsGroup';
import { GdaxNetworkConfigOptionsGroup } from '../../gdax/network/config/GdaxNetworkConfigOptionsGroup';
import { TYPES } from '../../infrastructure/cli/module';
import { Config as BaseConfig } from '../../infrastructure/config/Config';
import { MongoConfigOptionsGroup } from '../../infrastructure/data-access/MongoConfigOptionsGroup';
import { LoggerConfigOptionsGroup } from '../../infrastructure/logger/LoggerConfigOptionsGroup';
import { ProductConfigOptionsGroup } from '../../infrastructure/config/ProductConfigOptionsGroup';
import { NamedPipe, PipeType } from '../../infrastructure/pipes/NamedPipe';
import { PipeConfigOptionsGroup } from '../../infrastructure/pipes/PipeConfigOptionsGroup';


@injectable()
class Config extends BaseConfig {
  private static readonly VERSION = `0.0.1`;
  private static readonly DESCRIPTION = `A tool for handling  GDAX's WS feed messages.`;

  public constructor(@inject(TYPES.Command) _program: Command, @inject(TYPES.ARGV) _argv: string[]) {
    super(_program, _argv);

    this.configOptionsGroups.push(new CandleConfigOptionsGroup());
    this.configOptionsGroups.push(new GdaxNetworkConfigOptionsGroup());
    this.configOptionsGroups.push(new LoggerConfigOptionsGroup('GdaxWebsocketFeed', 'grabber'));
    this.configOptionsGroups.push(new MongoConfigOptionsGroup());

    // @ts-ignore - typescript's string enum behavior is a bit annoying
    const pipeName: NamedPipe = NamedPipe[NamedPipe.CRYPTO_GRABBER_PIPE_NAME];
    // @ts-ignore - typescript's string enum behavior is a bit annoying
    const outgoingPipes: NamedPipe[] = [ NamedPipe[NamedPipe.CRYPTO_PREDICTOR_PIPE_NAME] ];

    this.configOptionsGroups.push(new PipeConfigOptionsGroup(pipeName, PipeType.READER, outgoingPipes));
    this.configOptionsGroups.push(new ProductConfigOptionsGroup());
  }

  protected get version(): string {
    return Config.VERSION;
  }

  protected get description(): string {
    return Config.DESCRIPTION;
  }
}

export { Config };
