
import 'reflect-metadata';

import { AsyncContainerModule, ContainerModule, interfaces } from 'inversify';

import { CandleModule } from '../../domain/candle/module';
import { TickerModule } from '../../domain/ticker/module';
import { GdaxNetworkModule } from '../../gdax/network/module';
import { GdaxCandleNetworkModule } from '../../gdax/network/api/candle/module';

import { CliModule } from '../../infrastructure/cli/module';
import { DatabaseModule } from '../../infrastructure/data-access/module';
import { ModelFactoryModule } from '../../infrastructure/data-access/factories/module';
import { LoggerModule } from '../../infrastructure/logger/module';
import { NetworkModule } from '../../infrastructure/network/api/module';
import { PipeModule } from '../../infrastructure/pipes/module';
import { StateModule } from '../../infrastructure/state/module';

import { GdaxGetCandlesModule } from '../gdax-get-candles/module';
import { MissingCandlesModule } from '../missing-candles/module';

import { CandleMaker } from './CandleMaker';
import { TimeLastLiveHandler } from './TimeLastLiveHandler';


const TYPES = {
  CandleMaker: Symbol.for('CandleMaker'),
  TimeLastLiveHandler: Symbol.for('TimeLastLiveHandler')
};


const GdaxWebsocketFeedModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<CandleMaker>(TYPES.CandleMaker).to(CandleMaker).inSingletonScope();
    bind<TimeLastLiveHandler>(TYPES.TimeLastLiveHandler).to(TimeLastLiveHandler).inSingletonScope();
  }
);

const DEPENDENCIES: ContainerModule[] = [
  CandleModule,
  CliModule,
  GdaxCandleNetworkModule,
  GdaxGetCandlesModule,
  GdaxNetworkModule,
  GdaxWebsocketFeedModule,
  LoggerModule,
  MissingCandlesModule,
  ModelFactoryModule,
  NetworkModule,
  PipeModule,
  StateModule,
  TickerModule
];

const ASYNC_DEPENDENCIES: AsyncContainerModule[] = [
  DatabaseModule
];


export { ASYNC_DEPENDENCIES, DEPENDENCIES, GdaxWebsocketFeedModule, TYPES };
