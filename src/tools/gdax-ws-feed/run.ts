

import { referenceContainer } from '../../infrastructure/inversify';
import { TYPES } from '../../infrastructure/module';
import { run } from '../../infrastructure/run';

import { App } from './App';
import { Config } from './Config';
import { ASYNC_DEPENDENCIES, DEPENDENCIES } from './module';


(async () => {
  await run(App, TYPES.App, Config, referenceContainer, DEPENDENCIES, ASYNC_DEPENDENCIES);
})();
