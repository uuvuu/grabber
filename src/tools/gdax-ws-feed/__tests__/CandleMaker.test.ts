
import { AsyncContainerModule, injectable } from 'inversify';

import { MongoConfigOptionsGroup } from '../../../infrastructure/data-access/MongoConfigOptionsGroup';
import { NamedPipe, PipeType } from '../../../infrastructure/pipes/NamedPipe';
import { PipeConfigOptionsGroup } from '../../../infrastructure/pipes/PipeConfigOptionsGroup';

import { TYPES as CONFIG_TYPES } from '../../../infrastructure/config/module';
import { ProductConfigOptionsGroup } from '../../../infrastructure/config/ProductConfigOptionsGroup';
import { referenceContainer } from '../../../infrastructure/inversify';
import { LoggerConfigOptionsGroup } from '../../../infrastructure/logger/LoggerConfigOptionsGroup';
import { ArgvConfig } from '../../../testing/infrastructure/config/ArgvConfig';
import { DatabaseModule } from '../../../testing/infrastructure/data-access/unit/module';

import { DEPENDENCIES } from '../module';


const ASYNC_DEPENDENCIES: AsyncContainerModule[] = [
  DatabaseModule
];


describe('CandleFeeder', () => {
  let originalArgv;

  @injectable()
  class TestConfig extends ArgvConfig {
    constructor() {
      super();

      this.configOptionsGroups.push(new LoggerConfigOptionsGroup('CandleFeeder'));
      this.configOptionsGroups.push(new MongoConfigOptionsGroup());
      this.configOptionsGroups.push(
        new PipeConfigOptionsGroup(NamedPipe['crypto_grabber'], PipeType.READER, [ NamedPipe['crypto_predictor'] ]));
      this.configOptionsGroups.push(new ProductConfigOptionsGroup());
    }
  }

  beforeAll(async () => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([]);

    referenceContainer.bind<TestConfig>(CONFIG_TYPES.Config).to(TestConfig).inSingletonScope();

    referenceContainer.load(...DEPENDENCIES);

    try {
      await referenceContainer.loadAsync(...ASYNC_DEPENDENCIES);
    } catch (error) {
      // let this error be handled/logged at the caller level
      throw error;
    }

    console.log = jest.spyOn(console, 'log').mockImplementation(() => {});
  });

  afterAll(() => {
    // @ts-ignore
    console.log.mockRestore();

    referenceContainer.unbindAll();

    process.argv = originalArgv;
  });

  test.skip('do we even want to have a \'CandleFeeder\' separate from gdax-ws-feed?',  async () => {
    fail('WRITE ME');
  });
});
