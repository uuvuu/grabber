
import { inject, injectable } from 'inversify';

import { CandleConfig } from '../../domain/candle/CandleConfigOptionsGroup';
import { TYPES as CANDLE_TYPES } from '../../domain/candle/module';
import { CandleRepository } from '../../domain/candle/repositories/CandleRepository';
import { Component } from '../../infrastructure/component/Component';
import { TYPES as CONFIG_TYPES } from '../../infrastructure/config/module';
import { ProductConfig } from '../../infrastructure/config/ProductConfigOptionsGroup';
import { Logger } from '../../infrastructure/logger/Logger';
import { TYPES as LOGGER_TYPES } from '../../infrastructure/logger/module';
import { Message } from '../../infrastructure/pipes/Message';
import { MessageHandler } from '../../infrastructure/pipes/MessageHandler';
import { NamedPipe } from '../../infrastructure/pipes/NamedPipe';
import { PipeConfig } from '../../infrastructure/pipes/PipeConfigOptionsGroup';
import { TYPES as PIPES_TYPES } from '../../infrastructure/pipes/types';
import { State } from '../../infrastructure/state/State';
import { sleep } from '../../utils/sleep';


@injectable()
class TimeLastLiveHandler implements Component {
  // @ts-ignore - typescript's string enum behavior is a bit annoying
  private readonly _busyState: State = State[State.Busy];

  private _target: NamedPipe;
  // @ts-ignore - typescript's string enum behavior is a bit annoying
  private _targetState: State = State[State.Busy];

  _loggedBusyMessage = false;

  public constructor(
    @inject(CONFIG_TYPES.Config) protected readonly _config: CandleConfig & PipeConfig & ProductConfig,
    @inject(CANDLE_TYPES.CandleRepository) protected readonly _candleRepository: CandleRepository,
    @inject(LOGGER_TYPES.Logger) protected readonly _logger: Logger,
    @inject(PIPES_TYPES.MessageHandler) protected readonly _messageHandler: MessageHandler) {
  }

  async load() {
  }

  async unload() {
  }

  set targetState(state: State) {
    this._targetState = state;
  }

  // date is date from timelastlive message
  async processCandles(target: NamedPipe, date: Date) {
    this._target = target;

    const [cryptoName, currencyName] = this._config.product.split('-');

    this._logger.log('info', `Begin sending timelastlive messages to: '${NamedPipe[this._target]}`);

    while (date < new Date()) {
      const candles = await this._candleRepository.findManyByPage(date);

      if (candles.length === 0) {
        break;
      }

      for (let i = 0; i < candles.length; i++) {
        while (this._targetState === this._busyState) {
          if (! this._loggedBusyMessage) {
            this._logger.log('info', `Target '${NamedPipe[this._target]}' is busy. Waiting to send more data.`);

            this._loggedBusyMessage = true;
          }

          await sleep(50);
        }

        this._loggedBusyMessage = false;

        this._messageHandler.sendDataMessage({
          dataType: 'training',
          product: cryptoName,
          currency: currencyName,
          timeInMillis: '' + candles[i].time.getTime(),
          low: candles[i].low,
          high: candles[i].high,
          open: candles[i].open,
          close: candles[i].close,
          volume: candles[i].volume,
          timeDeltaInMillis: '0'
        } as Message.DataMessage, this._target);

        // throttle messages
        await sleep(10);
      }

      date = new Date(candles[candles.length - 1].time);
      date.setMinutes(date.getMinutes() + 1);
    }

    this._logger.log('info', `End sending timelastlive messages to: '${NamedPipe[this._target]}`);
  }
}


export { TimeLastLiveHandler };
