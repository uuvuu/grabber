import { ContainerModule, injectable } from 'inversify';

import { TYPES as CONFIG_TYPES } from '../../../infrastructure/config/module';
import { ArgvConfig } from '../../../testing/infrastructure/config/ArgvConfig';

import { referenceContainer } from '../../inversify';
import { LoggerConfigOptionsGroup } from '../../logger/LoggerConfigOptionsGroup';
import { LoggerModule } from '../../logger/module';

import { StateModule, TYPES } from '../module';

import { State, StateChange, StateManager } from '../State';


const DEPENDENCIES: ContainerModule[] = [
  LoggerModule,
  StateModule
];


describe('State', () => {
  @injectable()
  class Config extends ArgvConfig {
    public constructor() {
      super();

      this.configOptionsGroups.push(new LoggerConfigOptionsGroup('TestState'));
    }
  }

  let originalArgv;

  let stateManager: StateManager;

  // @ts-ignore - typescript string enum annoyances
  const busyState: State = State[State.Busy];

  // @ts-ignore - typescript string enum annoyances
  const readyState: State = State[State.Ready];

  beforeAll(() => {
  });

  afterAll(() => {
    process.argv = originalArgv;
  });

  beforeEach(() => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([]);

    referenceContainer.bind<Config>(CONFIG_TYPES.Config).to(Config).inSingletonScope();

    referenceContainer.load(...DEPENDENCIES);

    stateManager = referenceContainer.get<StateManager>(TYPES.StateManager);
  });

  afterEach(() => {
    referenceContainer.unbindAll();
  });

  test('state property is initialized correctly', () => {
    expect(stateManager.state).toEqual(busyState);
  });

  test('changeState() changes state and emits \'stateChange\' event', () => {
    // @ts-ignore
    const eventEmitterSpy = jest.spyOn(stateManager._eventEmitter, 'emit');

    expect(stateManager.state).toEqual(busyState);

    stateManager.changeState(State.Ready);

    expect(stateManager.state).toEqual(readyState);

    expect(eventEmitterSpy).toHaveBeenCalledTimes(1);
    expect(eventEmitterSpy).toHaveBeenCalledWith(
      'stateChange', { oldState: busyState, newState: readyState } as StateChange);
  });
});
