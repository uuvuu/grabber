
import { ContainerModule, interfaces } from 'inversify';

import { StateManager } from './State';


const TYPES = {
  StateManager: Symbol.for('StateManager')
};


const StateModule = new ContainerModule(
  (bind: interfaces.Bind) => {

    bind<StateManager>(TYPES.StateManager).to(StateManager).inSingletonScope();
  }
);


export { StateModule, TYPES };
