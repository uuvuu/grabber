
import { EventEmitter } from 'events';
import { inject, injectable } from 'inversify';

import { Component } from '../component/Component';
import { Logger } from '../logger/Logger';
import { TYPES as LOGGER_TYPES } from '../logger/module';


enum State {
  Ready = <any>'ready',
  Busy = <any>'busy',
  Waiting = <any>'waiting'
}

interface StateChange {
  oldState: State;
  newState: State;
}


//
// Using an EventEmitter delegate instead of extending it so tests
// can be written for this class
//
// see bug: https://github.com/inversify/InversifyJS/issues/984
//
// also see line 52 of src/infrastructure/inversify.ts:
//
//     decorate(injectable(), EventEmitter);
//

@injectable()
class StateManager implements Component {
  private _state: State;
  private _eventEmitter: EventEmitter;

  constructor(@inject(LOGGER_TYPES.Logger) readonly _logger: Logger) {
    this._eventEmitter = new EventEmitter();

    // @ts-ignore - typescript's string enum behavior is a bit annoying
    this._state = State[State.Busy];
  }

  on(event: string | symbol, listener: (...args: any[]) => void): this {
    this._eventEmitter.on(event, listener);

    return this;
  }

  off(event: string | symbol, listener: (...args: any[]) => void): this {
    this._eventEmitter.off(event, listener);

    return this;
  }

  async load() {
    // @ts-ignore - typescript's string enum behavior is a bit annoying
    this._state = State[State.Busy];
  }

  async unload() {
    // @ts-ignore - typescript's string enum behavior is a bit annoying
    this._state = State[State.Busy];
  }

  get state() {
    return this._state;
  }

  changeState(newState: State) {
    // @ts-ignore - typescript's string enum behavior is a bit annoying
    const newStateEnum: State = State[newState];

    if (newStateEnum !== this._state) {
      const oldState = this._state;
      this._state = newStateEnum;

      this._logger.log('info', `State change from '${oldState}' to '${this._state}'`);

      this._eventEmitter.emit('stateChange', { oldState: oldState, newState: this._state } as StateChange);
    }
  }
}


export { State, StateChange, StateManager };
