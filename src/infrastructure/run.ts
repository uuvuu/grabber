
import { AsyncContainerModule, Container, ContainerModule } from 'inversify';

import { App as BaseApp } from './App';
import { bootstrap } from './bootstrap';
import { Config } from './config/Config';
import { DatabaseClient } from './data-access/DatabaseClient';
import { TYPES as DB_TYPES } from './data-access/module';


async function run<C extends Config, A extends BaseApp<C>>(
  appConstructor: new (...args: any[]) => A,
  appType: symbol,
  configConstructor: new (...args: any[]) => C,
  referenceContainer: Container,
  modules: ContainerModule[],
  asyncModules: AsyncContainerModule[]
) {

  process.on('unhandledRejection', (reason, promise) => {
    console.log('Unhandled rejection at: Promise', promise, 'reason:', reason);

    console.trace();
    console.log();

    console.log('Exiting due to unexpected and unhandled error...');

    process.exit(1);
  });

  let app;

  try {
    app = await bootstrap<C, A>(
      appConstructor, appType, configConstructor, referenceContainer, modules, asyncModules);
  } catch (error) {
    let cliError = false;

    // TODO - refactor this a bit...presumptuous to assume every app is a DB-based app. Eg:
    // PipeHandlers are not DB-based apps
    // try to close db connection if we can
    try {
      const databaseClient = referenceContainer.get<DatabaseClient>(DB_TYPES.DatabaseClient);

      if (databaseClient) {
        // have we gotten far enough through the bootstrapping process to have a databaseClient
        // and connection?
        if (databaseClient && databaseClient.initialized) {
          await databaseClient.disconnect();
        }
      }
    } catch (error) {
      // no-op, we're exiting anyway, but just trying to clean up
    }

    // yuck, but cli config errors should be user friendly
    const prefixString = '@postConstruct error in class Config: ';

    let errorMessage: string;

    if (error && error.message) {
      errorMessage = error.message;

      if (error.message.match(new RegExp(prefixString))) {
        errorMessage = error.message.replace(prefixString, '');
        cliError = true;
      }
    } else {
      errorMessage = error;
    }

    console.log(`\n  ${errorMessage}`);
    if (cliError) {
      console.log(`\n  For more help see: ${Config.PROGRAM_NAME} -h`);
    }

    process.exit(1);
  }

  if (app) {
    try {
      await app.run();
    } catch (error) {
      console.log('App run failed: ' + error);
      process.exit(1);
    }
  }
}


export { run };
