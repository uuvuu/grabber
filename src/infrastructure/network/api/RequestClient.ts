
import { injectable } from 'inversify';

import { Request, RequestError, Response } from './Request';


@injectable()
abstract class RequestClient<RQ extends Request, RP extends Response<RQ>> {
  protected constructor(protected _client?) { }

  async request(request: RQ): Promise<RP> {
    try {
      const data = await this._request(request);

      return { data: data, request: request } as RP;
    } catch (error) {
      throw new RequestError(request, error);
    }
  }

  protected abstract get _request();
}


export { RequestClient };
