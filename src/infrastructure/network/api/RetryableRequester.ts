
import { injectable, unmanaged } from 'inversify';

import { retry } from '../../../utils/retry';
import { Logger } from '../../logger/Logger';

import { Request, Response } from './Request';


@injectable()
class RetryableRequester<RQ extends Request, RP extends Response<RQ>> {
  protected _requestSuccessCount = 0;
  protected _requestErrorCount = 0;

  constructor(
    @unmanaged() protected readonly _logger: Logger,
    @unmanaged() private readonly _requestRetryDelayInMillis: number,
    @unmanaged() private readonly _requestRetries: number,
    @unmanaged() private readonly _requestFunction: (request: RQ) => void) {
  }

  protected async request(request: RQ) {
    try {
      const _request = this._requestFunction(request);

      const response = await retry<RP>(_request, this._requestRetryDelayInMillis, this._requestRetries, this._logger);

      if (! Array.isArray(response.data) && response.data.message) {
        throw { error: response.data.message, request: response.request };
      }

      this._requestSuccessCount++;

      return response;
    } catch (error) {
      this._requestErrorCount++;
      throw error;
    }
  }
}


export { RetryableRequester };
