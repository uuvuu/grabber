
import { RateLimiter } from 'limiter';

import { RequestThrottler } from '../RequestThrottler';

import { TestRequest, TestResponse } from './TestRequest';
import { TestRequestClient } from './TestRequestClient';


class TestRequestThrottler extends RequestThrottler<TestRequest, TestResponse, TestRequestClient> {
  constructor(readonly requestClient: TestRequestClient, readonly rateLimiter: RateLimiter) {
    super(requestClient, rateLimiter);
  }
}


export { TestRequestThrottler };
