
import { RequestClient } from '../RequestClient';

import { TestRequest, TestResponse } from './TestRequest';


class TestRequestClient extends RequestClient<TestRequest, TestResponse> {
  constructor() {
    super();
  }

  //
  // make methods public so they can be overridden with mock implementation in tests
  //

  requestFunction() {
    const promise = new Promise(resolve => {
      setTimeout(() => { resolve(); }, 5);
    });

    return promise;
  }

  get _request() {
    return this.requestFunction;
  }

  set _request(requestFunction) {
    this.requestFunction = requestFunction;
  }
}


export { TestRequestClient };
