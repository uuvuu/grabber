
import { Request, Response } from '../Request';


class TestRequest extends Request {
}

class TestResponse extends Response<TestRequest> {
}


export { TestRequest, TestResponse };
