
import { ContainerModule, interfaces } from 'inversify';
import { RateLimiter as Limiter } from 'limiter';

import { RateLimiter} from './RateLimiter';


const TYPES = {
  AuthenticatedRequestClient: Symbol.for('AuthenticatedRequestClient'),
  RateLimiterFactory: Symbol.for('RateLimiterFactory'),
  RequestClient: Symbol.for('RequestClient'),
  RequestFactory: Symbol.for('RequestFactory'),
  RequestThrottler: Symbol.for('RequestThrottler')
};

const NetworkModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<interfaces.Factory<Limiter>>(TYPES.RateLimiterFactory).toFactory<Limiter>(RateLimiter.rateLimiterFactory);
  }
);


export { NetworkModule, TYPES };


