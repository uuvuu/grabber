
abstract class Request {
  requestNumber: number;
}

abstract class Response<R extends Request> {
  data: any;
  request: R;
}

class RequestError extends Error {
  constructor(private readonly _request, ...params) {
    super(...params);

    // maintains proper stack trace for where our error was thrown (only available on V8)
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, RequestError);
    }
  }

  public get request() {
    return this._request;
  }
}


export { Request, RequestError, Response };
