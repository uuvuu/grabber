
import { RateLimiter as Limiter } from 'limiter';


namespace RateLimiter {
  const urlToRateLimits = new Map<string, Limiter>();

  export class RateLimit {
    amountPerInterval: number;
    intervalLengthInMillis: number;
    fireImmediately?: boolean;
  }

  export function rateLimiterFactory() {
    return (apiUrl, rateLimit: RateLimit) => {
      const key = `${apiUrl}-${JSON.stringify(rateLimit)}`;

      let rateLimiter = urlToRateLimits.get(key);

      if (! rateLimiter) {
        rateLimiter = new Limiter(
          rateLimit.amountPerInterval,
          rateLimit.intervalLengthInMillis,
          rateLimit.fireImmediately);

        urlToRateLimits.set(key, rateLimiter);
      }

      return rateLimiter;
    };
  }

  export type RateLimiterFactory = (apiUrl: string, rateLimit: RateLimit) => Limiter;
}


export { RateLimiter };
