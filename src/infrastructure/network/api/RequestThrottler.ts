
import { injectable } from 'inversify';
import { RateLimiter } from 'limiter';

import { Request, Response } from './Request';
import { RequestClient } from './RequestClient';


@injectable()
abstract class RequestThrottler<RQ extends Request, RP extends Response<RQ>, C extends RequestClient<RQ, RP>> {
  static readonly REQUESTS_TIMEOUT_ERROR = new Error('waitForRequestsToComplete(): Requests timed out.');

  static readonly DEFAULT_CHECK_REQUESTS_COMPLETED_MILLIS = 1000;

  // if all requests haven't completed 1 minute after the final request was made
  // time the requests out
  static readonly DEFAULT_TIMEOUT_REQUESTS_IN_MILLIS = 300000;

  private _checkRequestsCompletedMillis;
  private _timeoutRequestsInMillis;

  private _requestsStarted = false;
  private _requestsCount = 0;
  private _processedRequestsCount = 0;
  private _erroredRequestsCount = 0;

  private _lastResponseDate = new Date();


  protected constructor(
    private readonly _requestClient: C, private readonly _rateLimiter: RateLimiter) {
  }

  async request(request: RQ): Promise<RP> {
    this._requestsStarted = true;
    this._requestsCount++;

    return new Promise<RP>((resolve, reject) => {
      this._rateLimiter.removeTokens(1, async (error) => {
        // we always remove exactly 1 token in removeTokens() so in theory,
        // the callback will never be called back with an error. see the 'limiter'
        // documentation for more info
        if (error) {
          reject(error);
        }

        try {
          const response = await this._requestClient.request(request);

          this._processedRequestsCount++;

          this._lastResponseDate = new Date();

          resolve(response);
        } catch (error) {
          this._erroredRequestsCount++;

          reject(error);
        }
      });
    });
  }

  public get checkRequestsCompletedMillis() {
    return typeof this._checkRequestsCompletedMillis !== 'undefined'
      ? this._checkRequestsCompletedMillis
      : RequestThrottler.DEFAULT_CHECK_REQUESTS_COMPLETED_MILLIS;
  }

  public set checkRequestsCompletedMillis(checkRequestsCompletedMillis: number) {
    this._checkRequestsCompletedMillis = checkRequestsCompletedMillis;
  }

  public get timeoutRequestsInMillis() {
    return typeof this._timeoutRequestsInMillis !== 'undefined'
      ? this._timeoutRequestsInMillis
      : RequestThrottler.DEFAULT_TIMEOUT_REQUESTS_IN_MILLIS;
  }

  public set timeoutRequestsInMillis(timeoutRequestsInMillis: number) {
    this._timeoutRequestsInMillis = timeoutRequestsInMillis;
  }

  get requestsStarted(): boolean {
    return this._requestsStarted;
  }

  get requestsCount(): number {
    return this._requestsCount;
  }

  get processedRequestsCount(): number {
    return this._processedRequestsCount;
  }

  get erroredRequestsCount(): number {
    return this._erroredRequestsCount;
  }

  async waitForRequestsToComplete(): Promise<any> {
    return new Promise<boolean>((resolve, reject) => {
      const interval = setInterval(async () => {
        const lastResponsePlusTimeout = new Date(this._lastResponseDate);
        lastResponsePlusTimeout.setMilliseconds(lastResponsePlusTimeout.getMilliseconds() + this.timeoutRequestsInMillis);

        if (this.requestsStarted
            && this.requestsCount === this._processedRequestsCount + this._erroredRequestsCount) {
          clearInterval(interval);
          resolve(true);
        } else if (! this._requestsStarted) {
          clearInterval(interval);
          resolve(true);
        } else if (lastResponsePlusTimeout < new Date()) {
          clearInterval(interval);
          // time the requests out
          reject(RequestThrottler.REQUESTS_TIMEOUT_ERROR);
        }
      }, this.checkRequestsCompletedMillis);
    });
  }
}


export { RequestClient, RequestThrottler };
