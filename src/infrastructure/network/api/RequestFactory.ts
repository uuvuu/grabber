
import { injectable } from 'inversify';

import { Request } from './Request';


@injectable()
abstract class RequestFactory<R extends Request> {
  abstract newRequest(): R;
}


export { RequestFactory };
