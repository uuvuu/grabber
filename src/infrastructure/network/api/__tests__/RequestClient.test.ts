
import 'reflect-metadata';

import { TestRequest } from '../__mocks__/TestRequest';
import { TestRequestClient } from '../__mocks__/TestRequestClient';
import { RequestError } from '../Request';

describe('RequestThrottler', () => {
  test('request() returns response on successful request', async () => {
    expect.assertions(5);

    const request = new TestRequest();
    const data = {
      foo: 'bar'
    };

    const testRequestClient = new TestRequestClient();

    const requestGetterSpy = jest.spyOn(testRequestClient, '_request', 'get');

    testRequestClient.requestFunction = jest.spyOn(testRequestClient, 'requestFunction').mockImplementation(() => {
      return new Promise(resolve => {
        setTimeout(() => { resolve(data); }, 5);
      });
    });

    const response = await testRequestClient.request(request);

    expect(requestGetterSpy).toHaveBeenCalledTimes(1);
    expect(testRequestClient.requestFunction).toHaveBeenCalledWith(request);

    expect(response).toBeDefined();
    expect(response.request).toEqual(request);
    expect(response.data).toEqual(data);

    // @ts-ignore
    testRequestClient.requestFunction.mockRestore();
  });

  test('request() throws error on unsuccessful request', async () => {
    expect.assertions(3);

    const errorMessage = 'request error';

    const request = {
      requestNumber: 1,
      start: '2018-07-27T00:00:00:000Z',
      end: '2018-07-27T05:00:00.000Z',
      granularity: '60'
    };

    const error = new RequestError(request, errorMessage);

    const testRequestClient = new TestRequestClient();

    const requestGetterSpy = jest.spyOn(testRequestClient, '_request', 'get');

    testRequestClient.requestFunction = jest.spyOn(testRequestClient, 'requestFunction').mockImplementation(() => {
      return new Promise((resolve, reject) => {
        setTimeout(() => { reject(errorMessage); }, 5);
      });
    });

    await expect(testRequestClient.request(request)).rejects.toThrow(error);

    expect(requestGetterSpy).toHaveBeenCalledTimes(1);
    expect(testRequestClient.requestFunction).toHaveBeenCalledWith(request);

    // @ts-ignore
    testRequestClient.requestFunction.mockRestore();
  });
});
