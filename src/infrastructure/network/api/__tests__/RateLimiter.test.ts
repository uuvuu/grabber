
import 'reflect-metadata';

import { ContainerModule } from 'inversify';

import { referenceContainer } from '../../../inversify';

import { NetworkModule, TYPES } from '../module';
import { RateLimiter } from '../RateLimiter';


const DEPENDENCIES: ContainerModule[] = [
  NetworkModule
];


describe('RateLimiter', () => {
  const testUrl = 'test_url';
  const testUrl2 = 'test_url2';

  const rateLimit = {
    amountPerInterval: 1,
    intervalLengthInMillis: 1000
  } as RateLimiter.RateLimit;

  const rateLimit2 = {
    amountPerInterval: 2,
    intervalLengthInMillis: 1000
  } as RateLimiter.RateLimit;

  beforeAll(() => {
    referenceContainer.load(...DEPENDENCIES);
  });

  afterAll(() => {
    referenceContainer.unbindAll();
  });


  test('RateLimiterFactory creates RateLimiter as singleton', () => {
    const rateLimiterFactory = referenceContainer.get<RateLimiter.RateLimiterFactory>(TYPES.RateLimiterFactory);

    const rateLimiter = rateLimiterFactory(testUrl, rateLimit);
    const rateLimiter2 = rateLimiterFactory(testUrl, rateLimit);

    expect(rateLimiter === rateLimiter2).toBeTruthy();
  });

  test('RateLimiterFactory creates RateLimiter as singleton per api and ratelimit configuration', () => {
    const rateLimiterFactory = referenceContainer.get<RateLimiter.RateLimiterFactory>(TYPES.RateLimiterFactory);

    let rateLimiter = rateLimiterFactory(testUrl, rateLimit);
    let rateLimiter2 = rateLimiterFactory(testUrl2, rateLimit);

    expect(rateLimiter !== rateLimiter2).toBeTruthy();

    rateLimiter = rateLimiterFactory(testUrl, rateLimit);
    rateLimiter2 = rateLimiterFactory(testUrl, rateLimit2);

    expect(rateLimiter !== rateLimiter2).toBeTruthy();
  });
});
