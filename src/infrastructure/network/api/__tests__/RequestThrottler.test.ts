
import 'reflect-metadata';

import * as lolex from 'lolex';

import { RateLimiter } from 'limiter';

import { TestRequest } from '../__mocks__/TestRequest';
import { TestRequestClient } from '../__mocks__/TestRequestClient';
import { TestRequestThrottler } from '../__mocks__/TestRequestThrottler';
import { RequestError } from '../Request';
import { RequestThrottler } from '../RequestThrottler';


describe('RequestThrottler', () => {
  let clock;
  let clearIntervalSpy;

  const testRequestClient = new TestRequestClient();

  const checkRequestsCompletedMillis = 10;
  const timeoutRequestsInMillis = 50;

  let requestThrottler;

  beforeAll(() => {
    clock = lolex.install({ shouldAdvanceTime: true });
    clearIntervalSpy = jest.spyOn(clock, 'clearInterval');
  });

  afterAll(() => {
    clock.uninstall();
  });

  beforeEach(() => {
    clearIntervalSpy.mockClear();

    requestThrottler = new TestRequestThrottler(testRequestClient, new RateLimiter(1, 10));

    requestThrottler.checkRequestsCompletedMillis = checkRequestsCompletedMillis;
    requestThrottler.timeoutRequestsInMillis = timeoutRequestsInMillis;
  });

  test('requestThrottler is initialized correctly', () => {
    expect(requestThrottler).toBeDefined();
    expect(requestThrottler.requestsStarted).toEqual(false);
    expect(requestThrottler.requestsCount).toEqual(0);
    expect(requestThrottler.processedRequestsCount).toEqual(0);
    expect(requestThrottler.erroredRequestsCount).toEqual(0);

    expect(requestThrottler.checkRequestsCompletedMillis).toEqual(checkRequestsCompletedMillis);
    expect(requestThrottler.timeoutRequestsInMillis).toEqual(timeoutRequestsInMillis);
  });

  test('successfully queues and makes a request', async () => {
    expect.assertions(4);

    await requestThrottler.request(new TestRequest()).then(() => {}).catch(() => {});

    expect(requestThrottler.requestsStarted).toEqual(true);
    expect(requestThrottler.requestsCount).toEqual(1);
    expect(requestThrottler.processedRequestsCount).toEqual(1);
    expect(requestThrottler.erroredRequestsCount).toEqual(0);
  });

  test('throws error on unsuccessful request', async () => {
    expect.assertions(5);

    const errorMessage = 'request error';

    const request = {
      requestNumber: 1,
      start: '2018-07-27T00:00:00:000Z',
      end: '2018-07-27T05:00:00.000Z',
      granularity: '60'
    };

    const error = new RequestError(request, errorMessage);

    testRequestClient.requestFunction = jest.spyOn(testRequestClient, 'requestFunction').mockImplementation(() => {
      return new Promise((resolve, reject) => {
        setTimeout(() => { reject(errorMessage); }, 5);
      });
    });

    await expect(requestThrottler.request(request)).rejects.toThrow(error);

    expect(requestThrottler.requestsStarted).toEqual(true);
    expect(requestThrottler.requestsCount).toEqual(1);
    expect(requestThrottler.processedRequestsCount).toEqual(0);
    expect(requestThrottler.erroredRequestsCount).toEqual(1);

    // @ts-ignore
    testRequestClient.requestFunction.mockRestore();
  });

  test('waitForRequestsToComplete() waits for request to complete', async () => {
    expect.assertions(5);

    requestThrottler.request(new TestRequest()).then(() => {}).catch(() => {});

    await requestThrottler.waitForRequestsToComplete();

    expect(requestThrottler.requestsStarted).toEqual(true);
    expect(requestThrottler.requestsCount).toEqual(1);
    expect(requestThrottler.processedRequestsCount).toEqual(1);
    expect(requestThrottler.erroredRequestsCount).toEqual(0);

    expect(clearIntervalSpy).toHaveBeenCalledTimes(1);
  });

  test('waitForRequestsToComplete() returns correctly if no requests have been started', async () => {
    expect.assertions(5);

    await requestThrottler.waitForRequestsToComplete();

    expect(requestThrottler.requestsStarted).toEqual(false);
    expect(requestThrottler.requestsCount).toEqual(0);
    expect(requestThrottler.processedRequestsCount).toEqual(0);
    expect(requestThrottler.erroredRequestsCount).toEqual(0);

    expect(clearIntervalSpy).toHaveBeenCalledTimes(1);
  });

  test('handles successful and unsuccessful requests correctly', async () => {
    expect.assertions(5);

    testRequestClient.requestFunction = jest.spyOn(testRequestClient, 'requestFunction')
      .mockImplementationOnce(() => {
        return new Promise(resolve => {
          setTimeout(() => resolve(), 5);
        });
      })
      .mockImplementationOnce(() => {
        return new Promise((resolve, reject) => {
          setTimeout(() => reject(), 5);
        });
      });

    requestThrottler.request(new TestRequest()).then(() => {}).catch(() => {});
    requestThrottler.request(new TestRequest()).then(() => {}).catch(() => {});

    await requestThrottler.waitForRequestsToComplete();

    expect(requestThrottler.requestsStarted).toEqual(true);
    expect(requestThrottler.requestsCount).toEqual(2);
    expect(requestThrottler.processedRequestsCount).toEqual(1);
    expect(requestThrottler.erroredRequestsCount).toEqual(1);

    expect(clearIntervalSpy).toHaveBeenCalledTimes(1);

    // @ts-ignore
    testRequestClient.requestFunction.mockRestore();
  });

  test('waitForRequestsToComplete() rejects if timeout occurs after last request', async() => {
    expect.assertions(2);

    testRequestClient.requestFunction = jest.spyOn(testRequestClient, 'requestFunction').mockImplementation(() => {
      return new Promise(resolve => {
        setTimeout(() => {
          resolve();
        }, requestThrottler.timeoutRequestsInMillis + 50);
      });
    });

    requestThrottler.request(new TestRequest()).then(() => {}).catch(() => {});

    await expect(requestThrottler.waitForRequestsToComplete()).rejects.toEqual(RequestThrottler.REQUESTS_TIMEOUT_ERROR);
    expect(clearIntervalSpy).toHaveBeenCalledTimes(1);

    // @ts-ignore
    testRequestClient.requestFunction.mockRestore();
  });

  test('waitForRequestsToComplete() rejects if timeout occurs after last request with '
      + 'mixed successful and unsuccessful requests', async() => {
    expect.assertions(6);

    testRequestClient.requestFunction = jest.spyOn(testRequestClient, 'requestFunction')
      .mockImplementationOnce(() => {
        return new Promise(resolve => {
          setTimeout(() => resolve(), requestThrottler.timeoutRequestsInMillis + 50);
        });
      })
      .mockImplementationOnce(() => {
        return new Promise((resolve, reject) => {
          setTimeout(() => reject(), 5);
        });
      })
      .mockImplementationOnce(() => {
        return new Promise(resolve => {
          setTimeout(() => resolve(), 5);
        });
      });

    requestThrottler.request(new TestRequest()).then(() => {}).catch(() => {});
    requestThrottler.request(new TestRequest()).then(() => {}).catch(() => {});
    requestThrottler.request(new TestRequest()).then(() => {}).catch(() => {});

    await expect(requestThrottler.waitForRequestsToComplete()).rejects.toEqual(RequestThrottler.REQUESTS_TIMEOUT_ERROR);

    expect(requestThrottler.requestsStarted).toEqual(true);
    expect(requestThrottler.requestsCount).toEqual(3);
    expect(requestThrottler.processedRequestsCount).toEqual(1);
    expect(requestThrottler.erroredRequestsCount).toEqual(1);

    expect(clearIntervalSpy).toHaveBeenCalledTimes(1);

    // @ts-ignore
    testRequestClient.requestFunction.mockRestore();
  });
});
