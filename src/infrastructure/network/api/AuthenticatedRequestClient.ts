
import { injectable } from 'inversify';
import { AuthenticatedClient } from '../../../gdax/network/api/AuthenticatedClient';

import { Request, Response } from './Request';
import { RequestClient } from './RequestClient';


@injectable()
abstract class AuthenticatedRequestClient<RQ extends Request, RP extends Response<RQ>> extends RequestClient<RQ, RP> {
  protected constructor(private readonly _authenticatedClientFactory: AuthenticatedClient.AuthenticatedClientFactory) {
    super();
  }

  async request(request: RQ): Promise<RP> {
    // delayed initialization because we don't have authn credentials during the bootstrapping phase
    if (! this._client) {
      this._client = this._authenticatedClientFactory();
    }

    return super.request(request);
  }
}


export { AuthenticatedRequestClient };
