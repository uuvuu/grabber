
import { ConfigOptionsGroup } from './ConfigOptionsGroup';


class TestModeConfigOptionsGroup extends ConfigOptionsGroup {
  constructor() {
    super();

    // run against sandbox GDAX
    this.addOption({
      testMode: {
        flag: '-t',
        description: `Run in test mode against GDAX sandbox URLs.`,
        defaultValue: false
      }
    });
  }
}


export { TestModeConfigOptionsGroup };
