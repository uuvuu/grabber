
import { Command } from 'commander';

import { ConfigError, ConfigErrorFactory } from './config';


type ConfigOption = {
  [name: string]: {
    flag: string;
    description: string;
    coercionFunction?: ((arg1: any, arg2: any) => void) | RegExp;
    defaultValue?: any;
  }
};


abstract class ConfigOptionsGroup {
  private _options = {};
  private _extraParams = {};

  private _flagsList = [] as string[];

  protected constructor() {
  }

  get options() {
    return this._options;
  }

  protected addOption(option: ConfigOption) {
    const optionName = Object.keys(option)[0];
    const optionFlag = option[optionName].flag;

    if (this._options.hasOwnProperty(optionName)) {
      throw `Attempting to redefine a configuration option that already exists: ${optionName}.`;
    }

    if (this._extraParams.hasOwnProperty(optionName)) {
      throw `Attempting to define an option that already exists as an extra param: ${optionName}.`;
    }

    if (this._flagsList.includes(optionFlag)) {
      throw `Attempting to redefine a configuration flag that already exists in group. option name: ${optionName}, flag: ${optionFlag}.`;
    }

    this._options[optionName] = option[optionName];

    this._flagsList.push(optionFlag);
  }

  // allow non-cli params to be added to the config and/or allow a set of config params to be
  // grouped on the config as a single field
  addExtraParams(command: Command) {
    // default behavior is no-op
  }

  protected addExtraParam(name: string, value: any) {
    let paramName: string;
    let paramValue: any;

    if (typeof value !== undefined) {
      paramName = name;
      paramValue = value;
    } else {
      paramName = Object.keys(name)[0];
      paramValue = name[paramName];
    }

    if (this._options.hasOwnProperty(paramName)) {
      throw `Attempting to define a param group that already exists as an option: ${paramName}.`;
    }

    this._extraParams[paramName] = paramValue;
  }

  // we need to wrap the actual parseInt because of a bug in commander. see:
  // https://github.com/tj/commander.js/issues/834
  protected parseInt(num: any) {
    return parseInt(num);
  }

  // Date.parse() used directly returns number of millis in epoch, but we want a real
  // date so use this function
  protected parseDate(date: any) {
    let d;

    try {
      d = new Date(date);
    } catch (error) {
      d = error;
    }

    return d;
  }

  protected parseArray(arrayStr: any) {
    return arrayStr ? arrayStr.split(',') : [];
  }

  // any extra config info that doesn't need to be an option on the command-line
  get extraParams() {
    return this._extraParams;
  }

  preProcessOptions(command: Command) {
    // default behavior is no-op
  }

  validateOptions(command: Command, configErrorFactory: ConfigErrorFactory): ConfigError {
    // default behavior is no-op
    return undefined;
  }
}


export { ConfigOptionsGroup, ConfigOption };
