
import 'reflect-metadata';

import * as Case from 'case';

import { Command } from 'commander';
import { injectable, postConstruct, unmanaged } from 'inversify';
import { basename } from 'path';

import { ConfigOptionsGroup } from './ConfigOptionsGroup';
import { TestModeConfigOptionsGroup } from './TestModeConfigOptionsGroup';


class ConfigError extends Error {
  name = 'ConfigError';
  programName: string;

  constructor(programName: string, message: string) {
    super(message);
  }
}

class ConfigErrorFactory {
  constructor(private _programName: string) {
  }

  newError(message: string) {
    return new ConfigError(this._programName, message);
  }
}


@injectable()
abstract class Config {
  public static PROGRAM_NAME = basename(process.argv[1]);

  protected _errorFactory: ConfigErrorFactory;
  protected _configOptionsGroups = [] as ConfigOptionsGroup[];

  private _flagsList = [] as string[];

  protected constructor(@unmanaged() protected _program: Command, @unmanaged() protected _argv: string[]) {
    // TODO: jrhite consider whether we should be adding TestModeConfigOptionsGroup by default
    // like this...particularly in the constructor
    this._configOptionsGroups.push(new TestModeConfigOptionsGroup());
  }

  @postConstruct()
  protected initialize() {
    this.buildCli();

    this._program.parse(this._argv);

    this.preProcessOptions();

    this._errorFactory = new ConfigErrorFactory(Config.PROGRAM_NAME);

    const error = this.validateOptions();

    if (error) {
      throw error;
    }

    this.addExtraParams();
  }

  public get configOptionsGroups() {
    return this._configOptionsGroups;
  }

  protected buildCli() {
    this._program
      .version(this.version)
      .description(this.description);

    for (const configOptions of this.configOptionsGroups) {
      for (const optionName of Object.keys(configOptions.options)) {
        if (this.hasOwnProperty(optionName)) {
          throw `Attempting to redefine a configuration option that already exists: ${optionName}.`;
        }

        const option = configOptions.options[optionName];

        if (this._flagsList.includes(option.flag)) {
          throw `Attempting to redefine a configuration flag that already exists. option name: ${optionName}, flag: ${option.flag}.`;
        }

        this._flagsList.push(option.flag);

        const flags = `${option.flag}, --${Case.kebab(optionName)} [${optionName}]`;

        if (option.coercionFunction) {
          this._program.option(flags, option.description, option.coercionFunction, option.defaultValue);
        } else {
          this._program.option(flags, option.description, option.defaultValue);
        }

        Object.defineProperty(this, optionName, {
          get: () => { return this._program[optionName]; },

          // TODO better safety checking on extra params
          set: (value: any) => {
            if (typeof this._program[optionName] !== 'undefined') {
              this._program[optionName] = value;
            }
          }
        });
      }
    }
  }

  protected preProcessOptions() {
    for (const configOptions of this.configOptionsGroups) {
      configOptions.preProcessOptions(this._program);
    }
  }

  protected validateOptions() {
    let error: ConfigError;

    for (const configOptions of this.configOptionsGroups) {
      error = configOptions.validateOptions(this._program, this._errorFactory);

      if (error) {
        return error;
      }
    }

    return undefined;
  }

  protected addExtraParams() {
    for (const configOptions of this.configOptionsGroups) {
      configOptions.addExtraParams(this._program);

      for (const extraParamName of Object.keys(configOptions.extraParams)) {
        if (this.hasOwnProperty(extraParamName)) {
          throw `Attempting to an extra param that already exists: ${extraParamName}`;
        }

        Object.defineProperty(this, extraParamName, {
          get: () => { return configOptions.extraParams[extraParamName]; },

          // TODO better safety checking on extra params
          set: (value: any) => {
            if (typeof configOptions.extraParams[extraParamName] !== 'undefined') {
              configOptions.extraParams[extraParamName] = value;
            }
          }
        });
      }
    }
  }

  protected get version() {
    return '0.0.1';
  }

  protected get description() {
    return '';
  }
}


export { Config, ConfigError, ConfigErrorFactory };
