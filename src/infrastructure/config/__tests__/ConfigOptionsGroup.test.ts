
import { ConfigOptionsGroup as BaseConfigOptionsGroup } from '../ConfigOptionsGroup';


describe('ConfigOptionsGroup', () => {
  test('conflicting options names in same options groups fails', () => {
    class ConfigOptionsGroup extends BaseConfigOptionsGroup {
      constructor() {
        super();

        this.addOption({ testOption: { flag: '-a', description: '', defaultValue: 0 } });
        this.addOption({ testOption: { flag: '-b', description: '', defaultValue: 0 } });
      }
    }

    expect(() => { new ConfigOptionsGroup(); }).toThrow();
  });

  test('conflicting options flags in same options group fails', () => {
    class ConfigOptionsGroup extends BaseConfigOptionsGroup {
      constructor() {
        super();

        this.addOption({ testOption: { flag: '-a', description: '', defaultValue: 0 } });
        this.addOption({ testOption2: { flag: '-a', description: '', defaultValue: 0 } });
      }
    }

    expect(() => { new ConfigOptionsGroup(); }).toThrow();
  });

  test('conflicting extra params in same options groups does not throw', () => {
    const overwrittenValue = 2;

    class ConfigOptionsGroup extends BaseConfigOptionsGroup {
      constructor() {
        super();
      }

      addExtraParams() {
        this.addExtraParam('testParam', 1);
        this.addExtraParam('testParam', overwrittenValue);
      }
    }

    const config = new ConfigOptionsGroup();

    expect(() => { config.addExtraParams(); }).not.toThrow();
  });

  test('conflicting option and extra params in same options groups fails', () => {
    class ConfigOptionsGroup extends BaseConfigOptionsGroup {
      constructor() {
        super();

        this.addOption({ testOption: { flag: '-a', description: '', defaultValue: 0 } });
      }

      addExtraParams() {
        this.addExtraParam('testOption', {});
      }
    }

    const config = new ConfigOptionsGroup();

    expect(() => { config.addExtraParams(); }).toThrow();
  });
});
