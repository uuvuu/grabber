
import { injectable } from 'inversify';

import { ArgvConfig } from '../../../testing/infrastructure/config/ArgvConfig';

import { TYPES as CONFIG_TYPES } from '../../config/module';
import { referenceContainer } from '../../inversify';
import { Config } from '../Config';

import { ProductConfig, ProductConfigOptionsGroup } from '../ProductConfigOptionsGroup';


describe('ProductConfigOptionsGroup', () => {
  @injectable()
  class TestConfig extends ArgvConfig {
    public constructor() {
      super();

      this.configOptionsGroups.push(new ProductConfigOptionsGroup());
    }
  }

  let originalArgv;

  beforeAll(() => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([]);
  });

  afterAll(() => {
    process.argv = originalArgv;
  });

  beforeEach(() => {
    referenceContainer.bind<Config>(CONFIG_TYPES.Config).to(TestConfig).inSingletonScope();
  });

  afterEach(() => {
    referenceContainer.unbindAll();
  });

  test('config of product has correct config', () => {
    // @ts-ignore
    const config = referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config) as ProductConfig;

    expect(config.product).toEqual(ProductConfigOptionsGroup.DEFAULTS.PRODUCT);
  });

  test('validateOptions() fails if product is invalid', () => {
    const originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper(['-p foo']);

    expect(() => { referenceContainer.get<Config>(CONFIG_TYPES.Config); }).toThrow();

    process.argv = originalArgv;
  });
});
