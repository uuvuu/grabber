
import 'reflect-metadata';

import { Command } from 'commander';
import { injectable } from 'inversify';

import { ArgvConfig } from '../../../testing/infrastructure/config/ArgvConfig';

import { referenceContainer } from '../../inversify';

import { ConfigError, ConfigErrorFactory } from '../config';
import { ConfigOptionsGroup } from '../ConfigOptionsGroup';
import { TYPES as CONFIG_TYPES } from '../module';


describe('Config', () => {
  let originalArgv;

  beforeAll(() => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([]);
  });

  afterAll(() => {
    process.argv = originalArgv;
  });

  afterEach(() => {
    referenceContainer.unbindAll();
  });

  test('handles invalid argument',  () => {
    class InvalidConfigOptionsGroup extends ConfigOptionsGroup {
      constructor() {
        super();
      }

      validateOptions(command: Command, configErrorFactory: ConfigErrorFactory): ConfigError {
        return configErrorFactory.newError('Invalid config');
      }
    }

    @injectable()
    class InvalidConfig extends ArgvConfig {
      public constructor() {
        super();

        this._configOptionsGroups.push(new InvalidConfigOptionsGroup());
      }
    }

    referenceContainer.bind<ArgvConfig>(CONFIG_TYPES.Config).to(InvalidConfig).inSingletonScope();
    expect(() => { referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config); }).toThrow();
  });

  class ConfigOptionsGroupA extends ConfigOptionsGroup {
    constructor() {
      super();

      this.addOption({
        testOption: {
          flag: '-a',
          description: 'test option',
          defaultValue: 0
        }
      });
    }
  }

  class ConfigOptionsGroupB extends ConfigOptionsGroup {
    constructor() {
      super();

      this.addOption({
        testOption: {
          flag: '-b',
          description: 'test option',
          defaultValue: 0
        }
      });
    }
  }

  class ConfigOptionsGroupC extends ConfigOptionsGroup {
    constructor() {
      super();

      this.addOption({
        testOptionC: {
          flag: '-a',
          description: 'test option',
          defaultValue: 0
        }
      });
    }
  }

  class ConfigOptionsGroupD extends ConfigOptionsGroup {
    constructor() {
      super();
    }

    addExtraParams(command: Command) {
      this.addExtraParam('testOption', {});
    }
  }

  class ConfigOptionsGroupE extends ConfigOptionsGroup {
    constructor() {
      super();
    }

    addExtraParams(command: Command) {
      this.addExtraParam('testOption', {});
    }
  }

  test('conflicting options names in different options groups fails', () => {
    class TestConfig extends ArgvConfig {
      constructor() {
        super();

        this._configOptionsGroups.push(new ConfigOptionsGroupA());
        this._configOptionsGroups.push(new ConfigOptionsGroupB());
      }
    }

    referenceContainer.bind<ArgvConfig>(CONFIG_TYPES.Config).to(TestConfig).inSingletonScope();
    expect(() => { referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config); }).toThrow();
  });

  test('conflicting options flags in different options group fails', () => {
    class TestConfig extends ArgvConfig {
      constructor() {
        super();

        this._configOptionsGroups.push(new ConfigOptionsGroupA());
        this._configOptionsGroups.push(new ConfigOptionsGroupC());
      }
    }

    referenceContainer.bind<ArgvConfig>(CONFIG_TYPES.Config).to(TestConfig).inSingletonScope();
    expect(() => { referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config); }).toThrow();
  });

  test('conflicting extra params in different options groups fails', () => {
    class TestConfig extends ArgvConfig {
      constructor() {
        super();

        this._configOptionsGroups.push(new ConfigOptionsGroupD());
        this._configOptionsGroups.push(new ConfigOptionsGroupE());
      }
    }

    referenceContainer.bind<ArgvConfig>(CONFIG_TYPES.Config).to(TestConfig).inSingletonScope();
    expect(() => { referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config); }).toThrow();
  });

  test('conflicting option and extra params in different options groups fails', () => {
    class TestConfig extends ArgvConfig {
      constructor() {
        super();

        this._configOptionsGroups.push(new ConfigOptionsGroupA());
        this._configOptionsGroups.push(new ConfigOptionsGroupD());
      }
    }

    referenceContainer.bind<ArgvConfig>(CONFIG_TYPES.Config).to(TestConfig).inSingletonScope();
    expect(() => { referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config); }).toThrow();
  });
});
