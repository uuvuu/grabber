
import { Command } from 'commander';

import { ConfigError, ConfigErrorFactory} from './config';
import { ConfigOptionsGroup } from './ConfigOptionsGroup';

class DEFAULTS {
  static readonly PRODUCT = 'BTC-USD';
  static readonly ALLOWED_PRODUCTS = [ 'BTC-USD', 'ETH-USD', 'LTC-USD', 'BCH-USD' ];
}

class ProductConfigOptionsGroup extends ConfigOptionsGroup {
  static readonly DEFAULTS = DEFAULTS;

  constructor() {
    super();

    this.addOption({
      product: {
        flag: '-p',
        description: `Cryptocurrency-Fiat pair to use. One of: ${ProductConfigOptionsGroup.DEFAULTS.ALLOWED_PRODUCTS}`,
        defaultValue: ProductConfigOptionsGroup.DEFAULTS.PRODUCT
      }
    });
  }

  validateOptions(command: Command, configErrorFactory: ConfigErrorFactory): ConfigError {
    if (! ProductConfigOptionsGroup.DEFAULTS.ALLOWED_PRODUCTS.includes(command.product)) {
      return configErrorFactory.newError(`product invalid. product must be one of: ${ProductConfigOptionsGroup.DEFAULTS.ALLOWED_PRODUCTS}.`);
    }

    return undefined;
  }
}

interface ProductConfig {
  product: string;
}


export { ProductConfig, ProductConfigOptionsGroup };
