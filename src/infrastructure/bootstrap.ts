
import { AsyncContainerModule, Container, ContainerModule } from 'inversify';
import { buildProviderModule } from 'inversify-binding-decorators';

import { App } from './App';
import { Config } from './config/Config';
import { TYPES as CONFIG_TYPES } from './config/module';


async function bootstrap<C extends Config, A extends App<C>>(
  appConstructor: new (...args: any[]) => A,
  appType: symbol,
  configConstructor: new (...args: any[]) => C,
  referenceContainer: Container,
  modules: ContainerModule[],
  asyncModules: AsyncContainerModule[]): Promise<A> {

  if (referenceContainer.isBound(appType) === false) {
    referenceContainer.bind<Config>(CONFIG_TYPES.Config).to(configConstructor).inSingletonScope();

    try {
      referenceContainer.load(buildProviderModule(), ...modules);

      await referenceContainer.loadAsync(...asyncModules);
    } catch (error) {
      // let this error be handled/logged at the caller level
      throw error;
    }

    referenceContainer.bind<A>(appType).to(appConstructor).inSingletonScope();
  }

  return referenceContainer.get<A>(appType);
}


export { bootstrap };
