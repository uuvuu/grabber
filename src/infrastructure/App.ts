
import { injectable } from 'inversify';

import { Component } from './component/Component';
import { Config } from './config/Config';
import { Logger } from './logger/Logger';


@injectable()
abstract class App<T extends Config> {
  _components: Component[] = [];

  protected constructor(protected readonly _config: T, protected readonly _logger: Logger) {
    process.on('SIGINT', async () => {
      await this.terminate('User terminated. Exiting....', false);
    });
  }

  public async run() {
    let hadError = false;

    try {
      this._components.forEach(component => {
        component.load();
      });

      await this._run();
    } catch (error) {
      hadError = true;

      this._logger.log('error', error);

      await this.terminate('Exiting due to unexpected error...', true);
    } finally {
      if (! hadError) {
        await this.terminate('Processing finished. Exiting...', false);
      }
    }
  }

  protected registerComponent(component: Component) {
    this._components.push(component);
  }

  protected abstract _run(): any;

  protected async terminate(message?: string, isError = false) {
    await this.terminateHook();

    this._components.forEach(component => {
      component.unload();
    });

    const logLevel = isError ? 'error' : 'info';
    const exitCode = isError ? 1 : 0;

    if (message) {
      this._logger.log(logLevel, message);
    }

    process.exit(exitCode);
  }

  // overridable callback to call during program termination. default is no-op
  protected async terminateHook() {
  }
}


export { App };
