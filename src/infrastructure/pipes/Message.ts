
import { inject, injectable } from 'inversify';

import { TYPES as CONFIG_TYPES } from '../config/module';
import { LoggerConfig } from '../logger/LoggerConfigOptionsGroup';
import { State } from '../state/State';

import { NamedPipe } from './NamedPipe';
import { PipeConfig } from './PipeConfigOptionsGroup';


namespace Message {
  export interface Message {
    type: string;
    serialize(): string;
  }

  export interface TargetableMessage extends Message {
    target: NamedPipe;
  }

  abstract class TargetableMessageImpl implements TargetableMessage {
    protected constructor(private _target: NamedPipe) {
      if (! this._target) {
        throw `unknown target: ${this._target}`;
      }
    }

    abstract get type();
    abstract serialize();

    get target() {
      return this._target;
    }

    protected get targetStr() {
      return NamedPipe[this._target];
    }
  }

  export interface SubTypeableMessage extends Message {
    subType: string;
  }

  export class AuthnMessage implements Message {
    constructor(private _key: string, private _secret: string, private _passphrase: string) {}

    get type() {
      return 'authn';
    }

    serialize() {
      return `${this.type} ${this.key} ${this.secret} ${this.passphrase}`;
    }

    get key() {
      return this._key;
    }

    get secret() {
      return this._secret;
    }

    get passphrase() {
      return this._passphrase;
    }
  }

  export class UnauthnMessage implements Message {
    constructor() {}

    get type() {
      return 'unauthn';
    }

    serialize() {
      return `${this.type}`;
    }
  }

  export interface DataMessage {
    // [dataType: string (eg: live, training)]
    // [product: string (eg: BTC, LTC, …)]
    // [currency: string USD, YEN, …]
    // [timeinMillis: long]   -- deltas based off of this time
    // [low: real]
    // [high: real]
    // [open: real]
    // [close: real]
    // [volume: real]
    // [timeDeltaInMillis: long (delta from time)]
    readonly dataType: 'live' | 'training';
    readonly product: string;
    readonly currency: string;
    readonly timeInMillis: string;
    readonly low: string;
    readonly high: string;
    readonly open: string;
    readonly close: string;
    readonly volume: string;
    readonly timeDeltaInMillis: string;
  }

  export class DataMessageImpl implements DataMessage, Message {
    constructor(private _message: DataMessage) {}

    get type() {
      return 'data';
    }

    serialize() {
      return `${this.type} ${this.dataType} ${this.product} ${this.currency} ${this.timeInMillis} ${this.low} `
        + `${this.high} ${this.open} ${this.close} ${this.volume} ${this.timeDeltaInMillis}`;
    }

    get dataType() {
      return this._message.dataType;
    }

    get product() {
      return this._message.product;
    }

    get currency() {
      return this._message.currency;
    }

    get timeInMillis() {
      return this._message.timeInMillis;
    }

    get low() {
      return this._message.low;
    }

    get high() {
      return this._message.high;
    }

    get open() {
      return this._message.open;
    }

    get close() {
      return this._message.close;
    }

    get volume() {
      return this._message.volume;
    }

    get timeDeltaInMillis() {
      return this._message.timeDeltaInMillis;
    }
  }

  export class GetStateMessage extends TargetableMessageImpl {
    constructor(_target: NamedPipe) {
      super(_target);
    }

    get type() {
      return 'getstate';
    }

    serialize() {
      return `${this.type} ${this.targetStr}`;
    }
  }

  export abstract class HeartbeatMessage extends TargetableMessageImpl implements SubTypeableMessage {
    protected constructor(_target: NamedPipe, private _subType: string) {
      super(_target);
    }

    get type() {
      return 'heartbeat';
    }

    serialize() {
      return `${this.type} ${this.subType} ${this.targetStr}`;
    }

    get subType() {
      return this._subType;
    }
  }

  export class AcknowledgeStartupMessage extends HeartbeatMessage {
    constructor(_target: NamedPipe) {
      super(_target, 'acknowledgestartup');
    }
  }

  export class PingMessage extends HeartbeatMessage {
    constructor(_target: NamedPipe) {
      super(_target, 'ping');
    }
  }

  export class PongMessage extends HeartbeatMessage {
    constructor(_target: NamedPipe) {
      super(_target, 'pong');
    }
  }

  export class StartupMessage extends HeartbeatMessage {
    constructor(_target: NamedPipe) {
      super(_target, 'startup');
    }
  }

  export class SendStateChangeMessage extends TargetableMessageImpl {
    private _newState: State;
    private _oldState: State;

    constructor(_target: NamedPipe, oldState: State, newState: State) {
      super(_target);

      this._newState = newState;
      this._oldState = oldState;

      if (! this._newState) {
        throw `unknown new state: ${this._newState}`;
      }

      if (! this._oldState) {
        throw `unknown old state: ${this._oldState}`;
      }
    }

    get type() {
      return 'sendstatechange';
    }

    serialize() {
      return `${this.type} ${State[this.oldState]} ${State[this.newState]} ${this.targetStr}`;
    }

    get newState() {
      return this._newState;
    }

    get oldState() {
      return this._oldState;
    }
  }

  export class SendStateMessage extends TargetableMessageImpl {
    private readonly _state: State;

    constructor(_target: NamedPipe, state: State) {
      super(_target);

      this._state = state;

      if (! this._state) {
        throw `unknown state: ${this._state}`;
      }
    }

    get type() {
      return 'sendstate';
    }

    serialize() {
      return `${this.type} ${State[this.state]} ${this.targetStr}`;
    }

    get state() {
      return this._state;
    }
  }

  export class ShutdownMessage implements Message {
    constructor() {
    }

    get type() {
      return 'shutdown';
    }

    serialize() {
      return `${this.type}`;
    }
  }

  export class SubscribeStateChangeMessage extends TargetableMessageImpl {
    constructor(_target: NamedPipe) {
      super(_target);
    }

    get type() {
      return 'subscribestatechange';
    }

    serialize() {
      return `${this.type} ${this.targetStr}`;
    }
  }

  export abstract class TimeLastLiveMessage extends TargetableMessageImpl {
    abstract get timeLastLiveDate(): Date;
  }

  export class TimeLastLiveMessageImpl extends TimeLastLiveMessage {
    private readonly _timeLastLiveDate: Date;

    constructor(_target: NamedPipe, timeLastLiveInMillis: string) {
      super(_target);

      this._timeLastLiveDate = new Date(+timeLastLiveInMillis);
    }

    get type() {
      return 'timelastlive';
    }

    serialize() {
      return `${this.type} ${this.timeLastLiveDate.getTime()} ${this.targetStr}`;
    }

    get timeLastLiveDate() {
      return this._timeLastLiveDate;
    }
  }

  export class TradeMessage implements Message {
    // [amount: real (range: -1 - 1, -1 = max sell, +1 = max buy)]
    // [candleDate: long (in millis, indicates the time of the grabbed datum that resulted in this trade)
    // [timeDeltaInMillis: long (delta from time)]
    readonly _amount: string;
    readonly _candleDate: Date;
    readonly _timeDeltaInMillis: number;

    constructor(amount: string, candleDate: string, timeDeltaInMillis: string) {
      this._amount = amount;
      this._candleDate = new Date(+candleDate);
      this._timeDeltaInMillis = +timeDeltaInMillis;
    }

    get type() {
      return 'trade';
    }

    serialize() {
      return `${this.type} ${this.amount} ${this.candleDate.getTime()} ${this.timeDeltaInMillis}`;
    }

    get amount() {
      return this._amount;
    }

    get candleDate() {
      return this._candleDate;
    }

    get timeDeltaInMillis() {
      return this._timeDeltaInMillis;
    }
  }

  @injectable()
  export class MessageFactory {
    constructor(@inject(CONFIG_TYPES.Config) readonly _config: PipeConfig & LoggerConfig) { }

    newAcknowledgeStartupMessage(target: NamedPipe) {
      return new AcknowledgeStartupMessage(target);
    }

    newAuthnMessage(key: string, secret: string, passphrase: string) {
      return new AuthnMessage(key, secret, passphrase);
    }

    newUnauthnMessage() {
      return new UnauthnMessage();
    }

    newDataMessage(message: DataMessage) {
      return new DataMessageImpl(message);
    }

    newGetStateMessage(target: NamedPipe) {
      return new GetStateMessage(target);
    }

    newPingMessage(target: NamedPipe) {
      return new PingMessage(target);
    }

    newPongMessage(target: NamedPipe) {
      return new PongMessage(target);
    }

    newSendStateChangeMessage(target: NamedPipe, oldState: State, newState: State) {
      return new SendStateChangeMessage(target, oldState, newState);
    }

    newSendStateMessage(target: NamedPipe, state: State) {
      return new SendStateMessage(target, state);
    }

    newShutdownMessage() {
      return new ShutdownMessage();
    }

    newStartupMessage(target: NamedPipe) {
      return new StartupMessage(target);
    }

    newSubscribeStateChangeMessage(target: NamedPipe) {
      return new SubscribeStateChangeMessage(target);
    }

    newTimeLastLiveMessage(target: NamedPipe, timeLastLive: string) {
      return new TimeLastLiveMessageImpl(target, timeLastLive);
    }

    newTradeMessage(amount: string, timestamp: string, timeDeltaInMillis: string) {
      return new TradeMessage(amount, timestamp, timeDeltaInMillis);
    }

    deserialize(message: string): Message {
      let messageType = '<unknown>';

      if (message) {
        let messageComponents = message.split(' ');

        messageComponents = messageComponents.map(component => component.trim());

        if (messageComponents) {
          messageType = messageComponents[0].toLowerCase();
          let messageDetails = messageComponents.slice(1, messageComponents.length);

          switch (messageType) {
            case 'authn': {
              return this.newAuthnMessage(messageDetails[0], messageDetails[1], messageDetails[2]);
            }

            case 'unauthn': {
              return this.newUnauthnMessage();
            }

            case 'data': {
              return this.newDataMessage({
                dataType: messageDetails[0].toLowerCase(),
                product: messageDetails[1].toLowerCase(),
                currency: messageDetails[2].toLowerCase(),
                timeInMillis: messageDetails[3].toLowerCase(),
                low: messageDetails[4].toLowerCase(),
                high: messageDetails[5].toLowerCase(),
                open: messageDetails[6].toLowerCase(),
                close: messageDetails[7].toLowerCase(),
                volume: messageDetails[8].toLowerCase(),
                timeDeltaInMillis: messageDetails[9].toLowerCase()
              } as Message.DataMessage);
            }

            case 'getstate': {
              return this.newGetStateMessage(this.stringToPipe(messageDetails[0].toLowerCase()));
            }

            case 'heartbeat': {
              if (messageDetails) {
                const messageSubType = messageDetails[0].toLowerCase();
                messageDetails = messageDetails.slice(1, messageComponents.length);

                switch (messageSubType) {
                  case 'acknowledgestartup': {
                    return this.newAcknowledgeStartupMessage(this.stringToPipe(messageDetails[0].toLowerCase()));
                  }

                  case 'ping': {
                    return this.newPingMessage(this.stringToPipe(messageDetails[0].toLowerCase()));
                  }

                  case 'pong': {
                    return this.newPongMessage(this.stringToPipe(messageDetails[0].toLowerCase()));
                  }

                  case 'startup': {
                    return this.newStartupMessage(this.stringToPipe(messageDetails[0].toLowerCase()));
                  }

                  default: {
                    throw Error(`Heartbeat message of subtype '${messageSubType}' unknown. Could not deserialize message.`);
                  }
                }
              }
            }

            case 'sendstatechange': {
              return this.newSendStateChangeMessage(
                this.stringToPipe(messageDetails[2].toLowerCase()), State[messageDetails[0].toLowerCase()], State[messageDetails[1].toLowerCase()]);
            }

            case 'sendstate': {
              return this.newSendStateMessage(this.stringToPipe(messageDetails[1].toLowerCase()), State[messageDetails[0].toLowerCase()]);
            }

            case 'shutdown': {
              return this.newShutdownMessage();
            }

            case 'subscribestatechange': {
              return this.newSubscribeStateChangeMessage(this.stringToPipe(messageDetails[0].toLowerCase()));
            }

            case 'timelastlive': {
              return this.newTimeLastLiveMessage(this.stringToPipe(messageDetails[1].toLowerCase()), messageDetails[0].toLowerCase());
            }

            case 'trade': {
              return this.newTradeMessage(messageDetails[0].toLowerCase(), messageDetails[1].toLowerCase(), messageDetails[2].toLowerCase());
            }
          }
        }
      }

      throw `Message type ${messageType} unknown. Could not deserialize message.`;
    }

    private stringToPipe(pipeString: string): NamedPipe {
      const namedPipe = NamedPipe[pipeString];

      if (! namedPipe) {
        throw `Named pipe '${pipeString}' unknown. Could not deserialize message.`;
      }

      return namedPipe;
    }
  }
}


export { Message };
