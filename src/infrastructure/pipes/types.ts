
const TYPES = {
  Handshakes: Symbol.for('Handshakes'),
  MessageHandler: Symbol.for('MessageHandler'),
  MessageFactory: Symbol.for('MessageFactory'),
  PipeManager: Symbol.for('PipeManager')
};


export { TYPES };
