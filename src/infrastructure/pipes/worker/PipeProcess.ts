
import 'reflect-metadata';

import { CliModule } from '../../cli/module';

import { referenceContainer } from '../../inversify';
import { LoggerModule } from '../../logger/module';
import { TYPES } from '../../module';
import { run } from '../../run';

import { Config } from './Config';
import { PipeWorkerModule } from './module';
import { PipeHandler } from './PipeHandler';


const DEPENDENCIES = [
  CliModule,
  LoggerModule,
  PipeWorkerModule
];

const ASYNC_DEPENDENCIES = [];


(async () => {
  await run(PipeHandler, TYPES.App, Config, referenceContainer, DEPENDENCIES, ASYNC_DEPENDENCIES);
})();
