
import { ContainerModule, injectable } from 'inversify';

import { ArgvConfig } from '../../../../testing/infrastructure/config/ArgvConfig';

import { TYPES as CONFIG_TYPES } from '../../../config/module';
import { referenceContainer } from '../../../inversify';
import { LoggerConfigOptionsGroup } from '../../../logger/LoggerConfigOptionsGroup';
import { LoggerModule } from '../../../logger/module';
import { TYPES } from '../../../module';

import { PipeModule } from '../../module';
import { NamedPipe, PipeType } from '../../NamedPipe';
import { PipeConfigOptionsGroup } from '../../PipeConfigOptionsGroup';

import { PipeWorkerModule } from '../module';
import { PipeHandler } from '../PipeHandler';


const DEPENDENCIES: ContainerModule[] = [
  LoggerModule,
  PipeModule,
  PipeWorkerModule
];


describe('PipeHandler', () => {
  let originalArgv;

  let pipeHandler;

  @injectable()
  class TestConfig extends ArgvConfig {
    constructor() {
      super();

      this.configOptionsGroups.push(new LoggerConfigOptionsGroup('PipeHandlerTest'));
      this.configOptionsGroups.push(
        new PipeConfigOptionsGroup(NamedPipe['crypto_grabber'], PipeType.READER, [ NamedPipe['crypto_predictor'] ]));
    }
  }

  beforeAll(() => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([]);

    referenceContainer.bind<PipeHandler>(TYPES.App).to(PipeHandler).inSingletonScope();
    referenceContainer.bind<TestConfig>(CONFIG_TYPES.Config).to(TestConfig).inSingletonScope();

    referenceContainer.load(...DEPENDENCIES);

    pipeHandler = referenceContainer.get<PipeHandler>(TYPES.App);
  });

  afterAll(async () => {
    // need to do manual clean up in test environment
    // @ts-ignore
    await pipeHandler.terminateHook();

    referenceContainer.unbindAll();

    process.argv = originalArgv;
  });

  test.skip('setupReader() creates new reader on \'close\' event', async () => {
    fail('WRITE ME');
  });
});

