
import { ArgvConfig } from '../../../../testing/infrastructure/config/ArgvConfig';

import { bootstrap } from '../../../bootstrap';
import { CliModule } from '../../../cli/module';
import { referenceContainer } from '../../../inversify';
import { TYPES as CONFIG_TYPES } from '../../../config/module';
import { LoggerConfig } from '../../../logger/LoggerConfigOptionsGroup';
import { LoggerModule } from '../../../logger/module';
import { TYPES as APP_TYPES } from '../../../module';
import { PipeType } from '../../NamedPipe';

import { PipeConfig } from '../../PipeConfigOptionsGroup';

import { Config } from '../Config';
import { PipeWorkerModule } from '../module';
import { PipeHandler } from '../PipeHandler';


const DEPENDENCIES = [
  CliModule,
  LoggerModule,
  PipeWorkerModule
];

const ASYNC_DEPENDENCIES = [];


describe('PipeProcess', () => {
  let pipeHandler: PipeHandler;

  let originalArgv;

  const pipeProcessConfig = {
    awaitPipeDelayMillis: 500,
    logLevel: 'info',
    loggerPrefix: 'PipeProcessDriver',
    logDirectory: '/log',
    pipeBasePath: '/var/tmp',
    pipeName: 'crypto_grabber',
    pipeType: PipeType[PipeType.READER]
  };

  beforeAll(() => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([ JSON.stringify(pipeProcessConfig) ]);
  });

  afterAll(() => {
    process.argv = originalArgv;
  });

  beforeEach(() => {
    // this breaks because when running DatabaseModule, it is getting config and logger from
    // the reference container, instead of them being injected
    referenceContainer.unbindAll();
  });

  test('bootstraps successfully', async () => {
    expect.assertions(8);

    pipeHandler = await bootstrap<Config, PipeHandler>(PipeHandler, APP_TYPES.App, Config, referenceContainer, DEPENDENCIES, ASYNC_DEPENDENCIES);

    // @ts-ignore
    const config = referenceContainer.get<Config>(CONFIG_TYPES.Config) as LoggerConfig & PipeConfig;

    expect(pipeHandler).toBeDefined();

    expect(config.awaitPipeDelayMillis).toEqual(pipeProcessConfig.awaitPipeDelayMillis);
    expect(config.pipeBasePath).toEqual(pipeProcessConfig.pipeBasePath);
    expect(config.pipeName).toEqual(pipeProcessConfig.pipeName);
    expect(config.pipeType).toEqual(pipeProcessConfig.pipeType);

    expect(config.logLevel).toEqual(pipeProcessConfig.logLevel);
    expect(config.loggerOptions.level).toEqual(pipeProcessConfig.logLevel);
    expect(config.loggerOptions.prefix).toEqual(pipeProcessConfig.loggerPrefix);

    // need to do manual clean up in test environment
    // @ts-ignore
    await pipeHandler.terminateHook();
  });
});

