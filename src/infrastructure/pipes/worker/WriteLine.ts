
import { appendFileSync } from 'fs';


class WriteLine {
  constructor(protected readonly _filePath: string) { }

  write(str: string) {
    appendFileSync(this._filePath, `${str}\n`);
  }
}


export { WriteLine };
