
import { Command } from 'commander';
import { inject, injectable } from 'inversify';

import { TYPES } from '../../cli/module';
import { Config as BaseConfig } from '../../config/Config';
import { LoggerConfigOptionsGroup } from '../../logger/LoggerConfigOptionsGroup';
import { PipeConfigOptionsGroup } from '../PipeConfigOptionsGroup';


@injectable()
class Config extends BaseConfig {
  private static readonly VERSION = `0.0.1`;
  private static readonly DESCRIPTION = `A worker for handling an IPC pipe.`;

  constructor(@inject(TYPES.Command) _program: Command, @inject(TYPES.ARGV) _argv: string[]) {
    super(_program, _argv);

    const cliConfig = this.setupCliArgs();

    this.configOptionsGroups.push(new LoggerConfigOptionsGroup(cliConfig.loggerPrefix, cliConfig.logFileName, false));
    this.configOptionsGroups.push(new PipeConfigOptionsGroup(cliConfig.pipeName, cliConfig.pipeType));
  }

  protected setupCliArgs() {
    // remove last item of array (process.argv[2]) which we've already parsed the JSON from. then add
    // the JSON items as CLI args
    const cliConfig = JSON.parse(process.argv[2]);
    process.argv.pop();

    `-l ${cliConfig.logLevel}`.split(' ').forEach(token => process.argv.push(token));
    `-L ${cliConfig.logDirectory}`.split(' ').forEach(token => process.argv.push(token));
    `-f ${cliConfig.logFileName}`.split(' ').forEach(token => process.argv.push(token));
    `-P ${cliConfig.pipeBasePath}`.split(' ').forEach(token => process.argv.push(token));

    return cliConfig;
  }

  protected get version(): string {
    return Config.VERSION;
  }

  protected get description(): string {
    return Config.DESCRIPTION;
  }
}


export { Config };
