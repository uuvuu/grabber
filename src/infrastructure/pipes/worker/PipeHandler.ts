
import { inject, injectable } from 'inversify';

import { sleep } from '../../../utils/sleep';

import { App as BaseApp } from '../../App';
import { TYPES as CONFIG_TYPES } from '../../config/module';
import { Logger } from '../../logger/Logger';
import { LoggerConfig } from '../../logger/LoggerConfigOptionsGroup';
import { TYPES as LOGGER_TYPES } from '../../logger/module';
import { NamedPipe, PipeType } from '../NamedPipe';
import { PipeConfig } from '../PipeConfigOptionsGroup';

import { Config } from './Config';
import { TYPES } from './module';
import { ReadLineFactory } from './ReadLineFactory';
import { WriteLine } from './WriteLine';


@injectable()
class PipeHandler extends BaseApp<Config> {
  private _exiting = false;
  private _pipeReader;

  constructor(
    @inject(CONFIG_TYPES.Config) readonly _config: Config & LoggerConfig & PipeConfig,
    @inject(LOGGER_TYPES.Logger) readonly _logger: Logger,
    @inject(TYPES.PipeReaderFactory) protected readonly _pipeReaderFactory: ReadLineFactory,
    @inject(TYPES.PipeWriter) protected readonly _pipeWriter: WriteLine) {

    super(_config, _logger);
  }

  protected async _run() {
    // typescript's string enum behavior is a bit annoying, thus the quirky code below
    // @ts-ignore
    const isReader = PipeType[this._config.pipeType] === PipeType.READER;

    if (isReader) {
      this.setupReader();
    } else {
      this.setupWriter();
    }

    // loop forever
    while (true) {
      await sleep(50);
    }
  }

  // memory leak? don't think should be, as this is not a recursive call. we're just
  // registering a call to the same method on the 'close' event
  protected setupReader() {
    this._pipeReader = this._pipeReaderFactory(`${this._config.pipeBasePath}/${NamedPipe[this._config.pipeName]}`);

    this._pipeReader.on('line', (message) => { this.handleReadLine(message); });

    // pipeReader will close after first read as it will hit EOF. so every time we read, create
    // a new pipeReader. wish there was a better way to do this, but could find none.
    // node package node-tail does not work because it relies on fs.stat and checking file size
    // changes, but pipe sizes are always 0.
    this._pipeReader.on('close', () => {
      if (! this._exiting) {
        this._pipeReader = this.setupReader();
      }
    });
  }

  protected handleReadLine(message: string) {
    this._logger.log('info', `Received message: ${JSON.stringify(message)}`);

    try {
      process.send(message);
    } catch (error) {
      const errorMessage = error.message ? error.message : error;
      this._logger.log('error', `Error sending message to parent process. Error was: ${errorMessage}`);
    }
  }

  protected setupWriter() {
    process.on('message', (message) => { this.handleWriteLine(message); });
  }

  protected handleWriteLine(message: string) {
    this._logger.log('info',
      `Sending message to ${this._config.pipeBasePath}/${NamedPipe[this._config.pipeName]}: ${JSON.stringify(message)}`);

    this._pipeWriter.write(`${message}`);
  }

  protected async terminateHook() {
    this._exiting = true;

    this._logger.log('info', 'Received SIGTERM from parent. Shutting down.');

    if (this._pipeReader) {
      await this._pipeReader.close();

      // TODO need to call destroy?
    }
  }
}


export { PipeHandler };
