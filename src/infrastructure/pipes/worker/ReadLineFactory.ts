
import { createReadStream } from 'fs';
import { createInterface, ReadLine } from 'readline';


function readLineFactory() {
  return (filePath: string) => {
    const readStream = createReadStream(filePath);
    const lineReader = createInterface({ input: readStream });
    
    return lineReader;
  };
}

type ReadLineFactory = (filePath: string) => ReadLine;


export { readLineFactory, ReadLineFactory };
