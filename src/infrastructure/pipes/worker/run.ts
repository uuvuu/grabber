
//
// Simple driver script for PipeProcess and PipeHandler. Should not be used other than for
// tinkering/testing.
//
// For real app code use PipeProcess.ts
//

import { argvHelper } from '../../../testing/utils/argv';
import { CliModule } from '../../cli/module';
import { referenceContainer } from '../../inversify';
import { LoggerModule } from '../../logger/module';
import { TYPES } from '../../module';
import { run } from '../../run';

import { Config } from './Config';
import { PipeWorkerModule } from './module';
import { NamedPipe } from '../NamedPipe';
import { PipeHandler } from './PipeHandler';


const DEPENDENCIES = [
  CliModule,
  LoggerModule,
  PipeWorkerModule
];

const ASYNC_DEPENDENCIES = [];

const pipeProcessConfig = {
  awaitPipeDelayMillis: 500,
  logLevel: 'info',
  loggerPrefix: 'PipeProcessDriver',
  logDirectory: '/log',
  logFileName: 'worker-test.log',
  pipeBasePath: '/var/tmp',
  pipeName: NamedPipe.CRYPTO_GRABBER_PIPE_NAME,
};

process.argv = argvHelper([ JSON.stringify(pipeProcessConfig) ]);


(async () => {
  await run(PipeHandler, TYPES.App, Config, referenceContainer, DEPENDENCIES, ASYNC_DEPENDENCIES);
})();

