
import { ContainerModule, interfaces } from 'inversify';
import { ReadLine } from 'readline';

import { TYPES as CONFIG_TYPES } from '../../config/module';
import { referenceContainer } from '../../inversify';
import { NamedPipe } from '../NamedPipe';

import { PipeConfig } from '../PipeConfigOptionsGroup';

import { Config } from './Config';
import { readLineFactory } from './ReadLineFactory';
import { WriteLine } from './WriteLine';


const TYPES = {
  PipeReaderFactory: Symbol.for('PipeReaderFactory'),
  PipeWriter: Symbol.for('PipeWriter')
};


const PipeWorkerModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    // @ts-ignore
    const config = referenceContainer.get<Config>(CONFIG_TYPES.Config) as PipeConfig;

    try {
      bind<interfaces.Factory<ReadLine>>(TYPES.PipeReaderFactory).toFactory<ReadLine>(readLineFactory);

      const lineWriter = new WriteLine(`${config.pipeBasePath}/${NamedPipe[config.pipeName]}`);
      bind<WriteLine>(TYPES.PipeWriter).toConstantValue(lineWriter);
    } catch (error) {
      // let this error be handled/logged at the caller level
      throw error;
    }
  }
);


export { PipeWorkerModule, TYPES };
