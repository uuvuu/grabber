
import { ContainerModule, interfaces } from 'inversify';
import { Handshakes } from './Handshakes';

import { Message } from './Message';
import { MessageHandler } from './MessageHandler';
import { PipeManager } from './PipeManager';

import { TYPES } from './types';


const PipeModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<Handshakes>(TYPES.Handshakes).to(Handshakes).inSingletonScope();
    bind<MessageHandler>(TYPES.MessageHandler).to(MessageHandler).inSingletonScope();
    bind<Message.MessageFactory>(TYPES.MessageFactory).to(Message.MessageFactory).inSingletonScope();
    bind<PipeManager>(TYPES.PipeManager).to(PipeManager).inSingletonScope();
  }
);


export { PipeModule };
