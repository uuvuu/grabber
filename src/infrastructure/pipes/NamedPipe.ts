
enum NamedPipe {
  CRYPTO_GRABBER_PIPE_NAME = <any>'crypto_grabber',
  CRYPTO_PREDICTOR_PIPE_NAME = <any>'crypto_predictor',
  CRYPTO_TRADER_PIPE_NAME = <any>'crypto_trader',
  CRYPTO_EXECUTOR_PIPE_NAME = <any>'crypto_executor',
}

enum PipeType {
  READER = <any>'reader',
  WRITER = <any>'writer'
}


export { NamedPipe, PipeType };
