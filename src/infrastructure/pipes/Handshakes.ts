
import { injectable } from 'inversify';

import { NamedPipe } from './NamedPipe';


enum ShakeType {
  AcknowledgeStartup,
  Startup
}


@injectable()
class Handshakes {
  private _handshakes: Map<NamedPipe, Array<ShakeType>> = new Map<NamedPipe, Array<ShakeType>>();

  addPartner(partner: NamedPipe) {
    this._handshakes.set(partner, []);
  }

  shake(partner: NamedPipe, shakeType: ShakeType) {
    const shakes = this._handshakes.get(partner);

    if (! shakes.includes(shakeType)) {
      shakes.push(shakeType);
    }
  }

  complete() {
    if (! this._handshakes.size) {
      return false;
    }

    for (const shakes of this._handshakes.values()) {
      if (shakes.length < 2) {
        return false;
      }
    }

    return true;
  }
}


export { Handshakes, ShakeType };
