
import { injectable } from 'inversify';

import { ArgvConfig } from '../../../testing/infrastructure/config/ArgvConfig';
import { TYPES as CONFIG_TYPES } from '../../config/module';
import { referenceContainer } from '../../inversify';
import { LoggerConfigOptionsGroup } from '../../logger/LoggerConfigOptionsGroup';
import { LoggerModule } from '../../logger/module';

// import { MessageHandler } from '../MessageHandler';
import { PipeModule } from '../module';
// import { TYPES } from '../types';
import { NamedPipe, PipeType } from '../NamedPipe';
import { PipeConfigOptionsGroup } from '../PipeConfigOptionsGroup';


const DEPENDENCIES = [
  LoggerModule,
  PipeModule
];

describe.skip('MessageHandler', () => {
  @injectable()
  class TestConfig extends ArgvConfig {
    public constructor() {
      super();

      this.configOptionsGroups.push(new LoggerConfigOptionsGroup('MessageHandlerTest'));
      this.configOptionsGroups.push(
        new PipeConfigOptionsGroup(NamedPipe['crypto_grabber'], PipeType.READER, [NamedPipe['crypto_predictor']]));
    }
  }

  let originalArgv;

  // let messageHandler;

  beforeAll(() => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([]);

    referenceContainer.bind<TestConfig>(CONFIG_TYPES.Config).to(TestConfig).inSingletonScope();

    referenceContainer.load(...DEPENDENCIES);

    // messageHandler = referenceContainer.get<MessageHandler>(TYPES.MessageHandler);
  });

  afterAll(() => {
    process.argv = originalArgv;
  });

  beforeEach(() => {
    referenceContainer.bind<ArgvConfig>(CONFIG_TYPES.Config).to(TestConfig).inSingletonScope();
  });

  afterEach(() => {
    referenceContainer.unbindAll();
  });

  test('sanity check', () => {
    fail('write me');
  });
});
