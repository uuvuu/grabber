
import { injectable } from 'inversify';

import { ArgvConfig } from '../../../testing/infrastructure/config/ArgvConfig';

import { TYPES as CONFIG_TYPES } from '../../config/module';
import { referenceContainer } from '../../inversify';
import { NamedPipe, PipeType } from '../NamedPipe';

import { PipeConfig, PipeConfigOptionsGroup } from '../PipeConfigOptionsGroup';


describe('PipeConfigOptionsGroup', () => {
  @injectable()
  class TestConfig extends ArgvConfig {
    public constructor() {
      super();

      this.configOptionsGroups.push(
        new PipeConfigOptionsGroup(NamedPipe['crypto_grabber'], PipeType.READER, [ NamedPipe['crypto_predictor'] ]));
    }
  }

  let originalArgv;

  beforeAll(() => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([]);
  });

  afterAll(() => {
    process.argv = originalArgv;
  });

  beforeEach(() => {
    referenceContainer.bind<ArgvConfig>(CONFIG_TYPES.Config).to(TestConfig).inSingletonScope();
  });

  afterEach(() => {
    referenceContainer.unbindAll();
  });

  test('config of pipes has correct config', () => {
    // @ts-ignore
    const config = referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config) as PipeConfig;

    expect(config.pipeBasePath).toEqual(PipeConfigOptionsGroup.DEFAULTS.PIPE_BASE_PATH);
    expect(config.awaitPipeDelayMillis).toEqual(PipeConfigOptionsGroup.DEFAULTS.AWAIT_PIPE_DELAY_MILLIS);
  });

  test('validateOptions() fails if pipeBasePath is invalid', () => {
    const originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper(['-P /foo/bar']);

    expect(() => { referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config); }).toThrow();

    process.argv = originalArgv;
  });

  test('validateOptions() fails if awaitPipeDelayMillis is not an integer', () => {
    const originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper(['-w a']);

    expect(() => { referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config); }).toThrow();

    process.argv = originalArgv;
  });

  test('validateOptions() fails if awaitPipeDelayMillis is less than 1', () => {
    const originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper(['-w 0']);

    expect(() => { referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config); }).toThrow();

    process.argv = originalArgv;
  });
});
