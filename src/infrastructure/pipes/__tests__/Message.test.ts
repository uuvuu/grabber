import { ContainerModule, injectable } from 'inversify';

import { ArgvConfig } from '../../../testing/infrastructure/config/ArgvConfig';
import { Config } from '../../config/Config';

import { TYPES as CONFIG_TYPES } from '../../config/module';
import { referenceContainer } from '../../inversify';
import { State } from '../../state/State';

import { Message } from '../Message';
import { PipeModule } from '../module';
import { NamedPipe, PipeType } from '../NamedPipe';
import { PipeConfigOptionsGroup } from '../PipeConfigOptionsGroup';
import { TYPES } from '../types';


const DEPENDENCIES: ContainerModule[] = [
  PipeModule
];


describe('Message', () => {
  @injectable()
  class TestConfig extends ArgvConfig {
    public constructor() {
      super();

      this.configOptionsGroups.push(
        new PipeConfigOptionsGroup(NamedPipe['crypto_grabber'], PipeType.READER, [ NamedPipe['crypto_predictor'] ]));
    }
  }

  let originalArgv;
  let messageFactory;

  beforeAll(() => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([ `-P ${PipeConfigOptionsGroup.DEFAULTS.PIPE_BASE_PATH}`]);

    referenceContainer.bind<Config>(CONFIG_TYPES.Config).to(TestConfig).inSingletonScope();

    referenceContainer.load(...DEPENDENCIES);

    messageFactory = referenceContainer.get<Message.MessageFactory>(TYPES.MessageFactory);
  });

  afterAll(() => {
    referenceContainer.unbindAll();

    process.argv = originalArgv;
  });

  test('deserialize() on undefined throws', () => {
    expect(() => { messageFactory.deserialize(); }).toThrow();
  });

  test('deserialize() on empty message throws', () => {
    expect(() => { messageFactory.deserialize(''); }).toThrow();
  });

  test('deserialize() on unknown message type throws', () => {
    expect(() => { messageFactory.deserialize('unknown-message-type'); }).toThrow();
  });

  describe('AcknowledgeStartupMessage', () => {
    let deserializedMessage;
    let serializedMessage;

    beforeAll(() => {
      deserializedMessage = new Message.AcknowledgeStartupMessage(NamedPipe[`crypto_predictor`]);
      serializedMessage = `heartbeat acknowledgestartup crypto_predictor`;
    });

    test('deserialize()', () => {
      const message = messageFactory.deserialize(serializedMessage);

      expect(message.type).toEqual('heartbeat');
      expect(message.subType).toEqual('acknowledgestartup');
      expect(message).toEqual(deserializedMessage);
    });

    test('serialize()', () => {
      expect(deserializedMessage.serialize()).toEqual(serializedMessage);
    });
  });

  describe('AuthnMessage', () => {
    // test strings with capitalization and make sure capitalization is preserved
    const deserializedMessage = new Message.AuthnMessage('testKey', 'tEstsecret', 'testPassphrase');

    const serializedMessage =
      `authn ${deserializedMessage.key} ${deserializedMessage.secret} ${deserializedMessage.passphrase}`;

    test('deserialize()', () => {
      const message = messageFactory.deserialize(serializedMessage);

      expect(message.type).toEqual('authn');
      expect(message).toEqual(deserializedMessage);
    });

    test('serialize()', () => {
      expect(deserializedMessage.serialize()).toEqual(serializedMessage);
    });
  });

  describe('DataMessage', () => {
    const deserializedMessage = new Message.DataMessageImpl({
      dataType: 'live',
      product: 'btc',
      currency: 'usd',
      timeInMillis: '1538109360000',
      low: '998.35',
      high: '1001.82',
      open: '999.44',
      close: '1000.26',
      volume: '2.1593809',
      timeDeltaInMillis: '10'
    } as Message.DataMessage);

    const serializedMessage =
      `data ${deserializedMessage.dataType} ${deserializedMessage.product} ${deserializedMessage.currency} `
      + `${deserializedMessage.timeInMillis} ${deserializedMessage.low} ${deserializedMessage.high} `
      + `${deserializedMessage.open} ${deserializedMessage.close} ${deserializedMessage.volume} `
      + `${deserializedMessage.timeDeltaInMillis}`;

    test('deserialize()', () => {
      const message = messageFactory.deserialize(serializedMessage);

      expect(message.type).toEqual('data');
      expect(message).toEqual(deserializedMessage);
    });

    test('serialize()', () => {
      expect(deserializedMessage.serialize()).toEqual(serializedMessage);
    });
  });

  describe('GetStateMessage', () => {
    let deserializedMessage;
    let serializedMessage;

    beforeAll(() => {
      deserializedMessage = new Message.GetStateMessage(NamedPipe[`crypto_predictor`]);
      serializedMessage = `getstate crypto_predictor`;
    });

    test('deserialize()', () => {
      const message = messageFactory.deserialize(serializedMessage);

      expect(message.type).toEqual('getstate');
      expect(message).toEqual(deserializedMessage);
    });

    test('serialize()', () => {
      expect(deserializedMessage.serialize()).toEqual(serializedMessage);
    });
  });

  describe('HeartbeatMessage', () => {
    test('deserialization of heartbeat message with undefined subTypeThrows', () => {
      const serializedMessage = `heartbeat`;

      expect(() => { messageFactory.deserialize(serializedMessage); }).toThrow();
    });

    test('deserialization of heartbeat message with invalid subTypeThrows', () => {
      const serializedMessage = `heartbeat unknown-subtype`;

      expect(() => { messageFactory.deserialize(serializedMessage); }).toThrow();
    });
  });

  describe('PingMessage', () => {
    let deserializedMessage;
    let serializedMessage;

    beforeAll(() => {
      deserializedMessage = new Message.PingMessage(NamedPipe[`crypto_predictor`]);
      serializedMessage = `heartbeat ping crypto_predictor`;
    });

    test('deserialize()', () => {
      const message = messageFactory.deserialize(serializedMessage);

      expect(message.type).toEqual('heartbeat');
      expect(message.subType).toEqual('ping');
      expect(message).toEqual(deserializedMessage);
    });

    test('serialize()', () => {
      expect(deserializedMessage.serialize()).toEqual(serializedMessage);
    });
  });

  describe('PongMessage', () => {
    let deserializedMessage;
    let serializedMessage;

    beforeAll(() => {
      deserializedMessage = new Message.PongMessage(NamedPipe[`crypto_predictor`]);
      serializedMessage = `heartbeat pong crypto_predictor`;
    });

    test('deserialize()', () => {
      const message = messageFactory.deserialize(serializedMessage);

      expect(message.type).toEqual('heartbeat');
      expect(message.subType).toEqual('pong');
      expect(message).toEqual(deserializedMessage);
    });

    test('serialize()', () => {
      expect(deserializedMessage.serialize()).toEqual(serializedMessage);
    });
  });

  describe('SendStateChangeMessage', () => {
    let deserializedMessage;
    let serializedMessage;

    beforeAll(() => {
      deserializedMessage = new Message.SendStateChangeMessage(NamedPipe[`crypto_predictor`], State[`ready`], State[`busy`]);
      serializedMessage = `sendstatechange ready busy crypto_predictor`;
    });

    test('deserialize()', () => {
      const message = messageFactory.deserialize(serializedMessage);

      expect(message.type).toEqual('sendstatechange');
      expect(message).toEqual(deserializedMessage);
    });

    test('deserialize() is case insensitive', () => {
      const caseInsensitiveSerializedMessage = `sendstatechange reAdy BuSY crypto_predictor`;
      const message = messageFactory.deserialize(caseInsensitiveSerializedMessage);

      expect(message.type).toEqual('sendstatechange');
      expect(message).toEqual(deserializedMessage);
    });

    test('serialize()', () => {
      expect(deserializedMessage.serialize()).toEqual(serializedMessage);
    });

    test('deserialization of message with invalid old state throws', () => {
      const serializedMessage = `sendstatechange unknown_state ready crypto_predictor`;

      expect(() => { messageFactory.deserialize(serializedMessage); }).toThrow();
    });

    test('deserialization of message with invalid new state throws', () => {
      const serializedMessage = `sendstatechange ready unknown_state crypto_predictor`;

      expect(() => { messageFactory.deserialize(serializedMessage); }).toThrow();
    });
  });

  describe('SendStateMessage', () => {
    let deserializedMessage;
    let serializedMessage;

    beforeAll(() => {
      deserializedMessage = new Message.SendStateMessage(NamedPipe[`crypto_predictor`], State['ready']);
      serializedMessage = `sendstate ready crypto_predictor`;
    });

    test('deserialize()', () => {
      const message = messageFactory.deserialize(serializedMessage);

      expect(message.type).toEqual('sendstate');
      expect(message).toEqual(deserializedMessage);
    });

    test('deserialize() is case insensitive', () => {
      const caseInsensitiveSerializedMessage = `sendstate reAdy crypto_predictor`;
      const message = messageFactory.deserialize(caseInsensitiveSerializedMessage);

      expect(message.type).toEqual('sendstate');
      expect(message).toEqual(deserializedMessage);
    });

    test('serialize()', () => {
      expect(deserializedMessage.serialize()).toEqual(serializedMessage);
    });

    test('deserialization of message with invalid state throws', () => {
      const serializedMessage = `sendstate unknown_state crypto_predictor`;

      expect(() => { messageFactory.deserialize(serializedMessage); }).toThrow();
    });
  });

  describe('ShutdownMessage', () => {
    let deserializedMessage;
    let serializedMessage;

    beforeAll(() => {
      deserializedMessage = new Message.ShutdownMessage();
      serializedMessage = `shutdown`;
    });

    test('deserialize()', () => {
      const message = messageFactory.deserialize(serializedMessage);

      expect(message.type).toEqual('shutdown');
      expect(message).toEqual(deserializedMessage);
    });

    test('serialize()', () => {
      expect(deserializedMessage.serialize()).toEqual(serializedMessage);
    });
  });

  describe('StartupMessage', () => {
    let deserializedMessage;
    let serializedMessage;

    beforeAll(() => {
      deserializedMessage = new Message.StartupMessage(NamedPipe[`crypto_predictor`]);
      serializedMessage = `heartbeat startup crypto_predictor`;
    });

    test('deserialize()', () => {
      const message = messageFactory.deserialize(serializedMessage);

      expect(message.type).toEqual('heartbeat');
      expect(message.subType).toEqual('startup');
      expect(message).toEqual(deserializedMessage);
    });

    test('serialize()', () => {
      expect(deserializedMessage.serialize()).toEqual(serializedMessage);
    });
  });

  describe('SubscribeStateChangeMessage', () => {
    let deserializedMessage;
    let serializedMessage;

    beforeAll(() => {
      deserializedMessage = new Message.SubscribeStateChangeMessage(NamedPipe[`crypto_predictor`]);
      serializedMessage = `subscribestatechange crypto_predictor`;
    });

    test('deserialize()', () => {
      const message = messageFactory.deserialize(serializedMessage);

      expect(message.type).toEqual('subscribestatechange');
      expect(message).toEqual(deserializedMessage);
    });

    test('serialize()', () => {
      expect(deserializedMessage.serialize()).toEqual(serializedMessage);
    });
  });

  describe('TimeLastLiveMessage', () => {
    let deserializedMessage;
    let serializedMessage;

    beforeAll(() => {
      deserializedMessage = new Message.TimeLastLiveMessageImpl(NamedPipe[`crypto_predictor`], '1538109360000');
      serializedMessage = `timelastlive 1538109360000 crypto_predictor`;
    });

    test('deserialize()', () => {
      const message = messageFactory.deserialize(serializedMessage);

      expect(message.type).toEqual('timelastlive');
      expect(message).toEqual(deserializedMessage);
    });

    test('serialize()', () => {
      expect(deserializedMessage.serialize()).toEqual(serializedMessage);
    });
  });

  describe('TradeMessage', () => {
    const deserializedMessage = new Message.TradeMessage('1', '1538109360000', '10');

    const serializedMessage =
      `trade ${deserializedMessage.amount} ${deserializedMessage.candleDate.getTime()} ${deserializedMessage.timeDeltaInMillis}`;

    test('deserialize()', () => {
      const message = messageFactory.deserialize(serializedMessage);

      expect(message.type).toEqual('trade');
      expect(message).toEqual(deserializedMessage);
    });

    test('serialize()', () => {
      expect(deserializedMessage.serialize()).toEqual(serializedMessage);
    });
  });
});

