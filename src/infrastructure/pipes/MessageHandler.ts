
import { inject, injectable } from 'inversify';

import { Component } from '../component/Component';
import { TYPES as CONFIG_TYPES } from '../config/module';
import { Logger } from '../logger/Logger';
import { LoggerConfig } from '../logger/LoggerConfigOptionsGroup';
import { TYPES as LOGGER_TYPES } from '../logger/module';
import { TYPES as STATE_TYPES } from '../state/module';
import { State, StateChange, StateManager } from '../state/State';

import { Message } from './Message';
import { NamedPipe } from './NamedPipe';
import { PipeConfig } from './PipeConfigOptionsGroup';
import { PipeManager } from './PipeManager';
import { TYPES as PIPES_TYPES, TYPES } from './types';


@injectable()
class MessageHandler implements Component {
  protected _stateSubscribers: NamedPipe[] = [];

  protected _acknowledgeStartupMessageListeners: ((target: NamedPipe) => void)[] = [];
  protected _authnMessageListeners: ((message: Message.AuthnMessage) => void)[] = [];
  protected _unauthnMessageListeners: ((message: Message.UnauthnMessage) => void)[] = [];
  protected _startupMessageListeners: ((target: NamedPipe) => void)[] = [];
  protected _sendStateMessageListeners: ((target: NamedPipe, state: State) => void)[] = [];
  protected _sendStateChangeMessageListeners: ((target: NamedPipe, oldState: State, newState: State) => void)[] = [];
  protected _timeLastLiveMessageListeners: ((target: NamedPipe, timeLastLiveDate: Date) => void)[] = [];
  protected _tradeMessageListeners: ((message: Message.TradeMessage) => void)[] = [];

  constructor(
    @inject(CONFIG_TYPES.Config) protected readonly _config: LoggerConfig & PipeConfig,
    @inject(LOGGER_TYPES.Logger) protected readonly _logger: Logger,
    @inject(TYPES.MessageFactory) protected readonly _messageFactory: Message.MessageFactory,
    @inject(PIPES_TYPES.PipeManager) protected readonly _pipeManager: PipeManager,
    @inject(STATE_TYPES.StateManager) protected readonly _stateManager: StateManager) {
  }

  async load() {
    this._pipeManager.addMessageHandler((message: Message.Message) => { this.receiveMessage(message); });
    this._stateManager.on('stateChange', this.messageStateChangeSubscribers);
  }

  async unload() {
    this._stateManager.off('stateChange', this.messageStateChangeSubscribers);
  }

  sendStartupMessage(...targets: NamedPipe[]) {
    for (const target of targets) {
      const message = this._messageFactory.newStartupMessage(this._pipeManager.incomingPipe);

      this._logger.log('debug',
      `Sending message to ${this._config.pipeBasePath}/${NamedPipe[target]}: ${message.serialize()}`);

      this._pipeManager.sendMessage(message, target);
    }
  }

  sendStartupAcknowledgeMessage(...targets: NamedPipe[]) {
    for (const target of targets) {
      const message =
        this._messageFactory.newAcknowledgeStartupMessage(this._pipeManager.incomingPipe);

      this._logger.log('debug',
      `Sending message to ${this._config.pipeBasePath}/${NamedPipe[target]}: ${message.serialize()}`);

      this._pipeManager.sendMessage(message, target);
    }
  }

  sendDataMessage(data: Message.DataMessage, target: NamedPipe) {
    const message = this._messageFactory.newDataMessage(data);

    this._logger.log('debug',
      `Sending message to ${this._config.pipeBasePath}/${NamedPipe[target]}: ${message.serialize()}`);

    this._pipeManager.sendMessage(message, target);
  }

  sendGetStateMessage(...targets: NamedPipe[]) {
    for (const target of targets) {
      const message = this._messageFactory.newGetStateMessage(this._pipeManager.incomingPipe);

      this._logger.log('debug',
      `Sending message to ${this._config.pipeBasePath}/${NamedPipe[target]}: ${message.serialize()}`);

      this._pipeManager.sendMessage(message, target);
    }
  }

  sendSubscribeStateChangeMessage(...targets: NamedPipe[]) {
    for (const target of targets) {
      const message = this._messageFactory.newSubscribeStateChangeMessage(this._pipeManager.incomingPipe);

      this._logger.log('debug',
      `Sending message to ${this._config.pipeBasePath}/${NamedPipe[target]}: ${message.serialize()}`);

      this._pipeManager.sendMessage(message, target);
    }
  }

  addAcknowledgeStartupMessageListener(listener: (target: NamedPipe) => void) {
    this._acknowledgeStartupMessageListeners.push(listener);
  }

  addAuthnMessageListener(listener: (message: Message.AuthnMessage) => void) {
    this._authnMessageListeners.push(listener);
  }

  addUnauthnMessageListener(listener: (message: Message.UnauthnMessage) => void) {
    this._unauthnMessageListeners.push(listener);
  }

  addSendStateMessageListener(listener: (target: NamedPipe, state: State) => void) {
    this._sendStateMessageListeners.push(listener);
  }

  addSendStateChangeMessageListener(listener: (target: NamedPipe, oldState: State, newState: State) => void) {
    this._sendStateChangeMessageListeners.push(listener);
  }

  addStartupMessageListener(listener: (target: NamedPipe) => void) {
    this._startupMessageListeners.push(listener);
  }

  addTimeLastLiveMessageListener(listener: (target: NamedPipe, timeLastLiveDate: Date) => void) {
    this._timeLastLiveMessageListeners.push(listener);
  }

  addTradeMessageListener(listener: (message: Message.TradeMessage) => void) {
    this._tradeMessageListeners.push(listener);
  }

  private receiveMessage(message: Message.Message) {
    if (message instanceof Message.AuthnMessage) {
      this._authnMessageListeners.forEach(async (listener) => {
        await listener(message);
      });
    } else if (message instanceof Message.UnauthnMessage) {
      this._unauthnMessageListeners.forEach(async (listener) => {
        await listener(message);
      });
    } else if (message instanceof Message.HeartbeatMessage) {
      if (message instanceof Message.AcknowledgeStartupMessage) {
        this._acknowledgeStartupMessageListeners.forEach(async (listener) => {
          await listener(message.target);
        });
      } else if (message instanceof Message.PingMessage) {
        const outgoingMessage =
          this._messageFactory.newPongMessage(this._pipeManager.incomingPipe);

        this._logger.log('debug',
          `Sending message to ${this._config.pipeBasePath}/${NamedPipe[message.target]}: ${outgoingMessage.serialize()}`);

        this._pipeManager.sendMessage(outgoingMessage, message.target);
      } else if (message instanceof Message.PongMessage) {
        // TODO
        this._logger.log('info', `Received 'pong' message from: ${message.target}`);
      } else if (message instanceof Message.StartupMessage) {
        this._startupMessageListeners.forEach(async (listener) => {
          await listener(message.target);
        });
      }
    } else if (message instanceof Message.GetStateMessage) {
      const outgoingMessage =
        this._messageFactory.newSendStateMessage(this._pipeManager.incomingPipe, this._stateManager.state);

      this._logger.log('debug',
      `Sending message to ${this._config.pipeBasePath}/${NamedPipe[message.target]}: ${message.serialize()}`);

      this._pipeManager.sendMessage(outgoingMessage, message.target);
    }  else if (message instanceof Message.SendStateChangeMessage) {
      this._sendStateChangeMessageListeners.forEach(async (listener) => {
        await listener(message.target, message.oldState, message.newState);
      });
    } else if (message instanceof Message.SendStateMessage) {
      this._sendStateMessageListeners.forEach(async (listener) => {
        await listener(message.target, message.state);
      });
    } else if (message instanceof Message.SubscribeStateChangeMessage) {
      this.addStateChangeSubscriber(message.target);
    } else if (message instanceof Message.ShutdownMessage) {
      process.kill(process.pid, 'SIGINT');
    } else if (message instanceof Message.TimeLastLiveMessage) {
      this._timeLastLiveMessageListeners.forEach(async (listener) => {
        await listener(message.target, message.timeLastLiveDate);
      });
    } else if (message instanceof Message.TradeMessage) {
      this._tradeMessageListeners.forEach(async (listener) => {
        await listener(message);
      });
    }
  }

  private addStateChangeSubscriber(target: NamedPipe) {
    if (! this._stateSubscribers.includes(target)) {
       this._stateSubscribers.push(target);
    }
  }

  private messageStateChangeSubscribers = (stateChange: StateChange) => {
    const message =
      this._messageFactory.newSendStateChangeMessage(this._pipeManager.incomingPipe, stateChange.oldState, stateChange.newState);

    this._stateSubscribers.forEach(stateSubscriber => {
      this._logger.log('debug',
      `Sending message to ${this._config.pipeBasePath}/${NamedPipe[stateSubscriber]}: ${message.serialize()}`);
    });

    this._pipeManager.sendMessage(message, ...this._stateSubscribers);
  };
}


export { MessageHandler };
