
import * as fs from 'fs';

import { Command } from 'commander';

import { ConfigError, ConfigErrorFactory } from '../config/Config';
import { ConfigOptionsGroup } from '../config/ConfigOptionsGroup';

import { NamedPipe, PipeType } from './NamedPipe';


class DEFAULTS {
  static readonly PIPE_BASE_PATH = '/var/tmp';
  static readonly AWAIT_PIPE_DELAY_MILLIS = 500;
}

class PipeConfigOptionsGroup extends ConfigOptionsGroup {
  static readonly DEFAULTS = DEFAULTS;

  constructor(protected _pipeName: NamedPipe, protected _pipeType?: PipeType, protected _outgoingPipes?: NamedPipe[]) {
    super();

    this.addOption({
      pipeBasePath: {
        flag: '-P',
        description: 'Base path of the IPC pipes.',
        defaultValue: PipeConfigOptionsGroup.DEFAULTS.PIPE_BASE_PATH
      }
    });

    this.addOption({
      awaitPipeDelayMillis: {
        flag: '-w',
        description: 'How long to wait for the pipes.',
        coercionFunction: this.parseInt,
        defaultValue: PipeConfigOptionsGroup.DEFAULTS.AWAIT_PIPE_DELAY_MILLIS
      }
    });
  }

  addExtraParams(command: Command) {
    this.addExtraParam('pipeName', this._pipeName);
    this.addExtraParam('pipeType', this._pipeType);
    this.addExtraParam('outgoingPipes', this._outgoingPipes);
  }

  validateOptions(command: Command, configErrorFactory: ConfigErrorFactory): ConfigError {
    if (! fs.existsSync(command.pipeBasePath)) {
      return configErrorFactory.newError('pipeBasePath does not exist.');
    }

    const awaitPipeDelayMillis = command.awaitPipeDelayMillis;
    if (! Number.isInteger(awaitPipeDelayMillis) || awaitPipeDelayMillis < 1) {
      return configErrorFactory.newError('awaitPipeDelayMillis must be an integer larger than 0.');
    }

    return undefined;
  }
}

interface PipeConfig {
  awaitPipeDelayMillis: number;
  pipeBasePath: string;
  pipeName: NamedPipe;
  pipeType: PipeType;
  outgoingPipes: NamedPipe[];
}


export { PipeConfig, PipeConfigOptionsGroup };
