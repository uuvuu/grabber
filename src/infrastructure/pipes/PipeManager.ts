
import * as Case from 'case';
import * as child_process from 'child_process';

import { inject, injectable } from 'inversify';

import { Component } from '../component/Component';
import { TYPES as CONFIG_TYPES } from '../config/module';
import { Logger } from '../logger/Logger';
import { LoggerConfig } from '../logger/LoggerConfigOptionsGroup';
import { TYPES as LOGGER_TYPES } from '../logger/module';
import { Message } from './Message';

import { NamedPipe, PipeType } from './NamedPipe';
import { PipeConfig } from './PipeConfigOptionsGroup';
import { TYPES } from './types';


@injectable()
class PipeManager implements Component {
  private static readonly PIPE_PROCESS_MODULE_PATH = `${__dirname}/../../infrastructure/pipes/worker/PipeProcess.ts`;

  protected _incomingPipe: NamedPipe;
  protected _incomingPipeProcess: child_process.ChildProcess;
  protected _outgoingPipes: Map<NamedPipe, child_process.ChildProcess>;

  protected _messageHandlers: ((message: Message.Message) => void)[] = [];

  constructor(
    @inject(CONFIG_TYPES.Config) protected readonly _config: LoggerConfig & PipeConfig,
    @inject(LOGGER_TYPES.Logger) protected readonly _logger: Logger,
    @inject(TYPES.MessageFactory) protected readonly _messageFactory: Message.MessageFactory) {

    this._incomingPipe = this._config.pipeName;
  }

  async load() {
    this._incomingPipeProcess = this.createPipeReader();
    this._outgoingPipes = this.createPipeWriters();
  }

  async unload() {
    this._incomingPipeProcess.kill('SIGKILL');

    this._outgoingPipes.forEach((pipeProcess) => {
      pipeProcess.kill('SIGKILL');
    });
  }

  get incomingPipe() {
    return this._incomingPipe;
  }

  sendMessage(message: Message.Message, ...targets: NamedPipe[]) {
    targets.forEach(target => {
      this._outgoingPipes[target].send(message.serialize());
    });
  }

  addMessageHandler(handler: (message: Message.Message) => void) {
    this._messageHandlers.push(handler);
  }

  private createPipeReader() {
    const namedPipe = NamedPipe[this._incomingPipe];

    const loggerPrefix = `${this._config.loggerOptions.prefix}_${Case.pascal(namedPipe)}_Reader`;
    const readerPath = `${this._config.pipeBasePath.toLowerCase()}/${namedPipe}`;

    this._logger.log('info', `Creating pipe reader process '${loggerPrefix}' for: ${readerPath}`);

    const pipe = child_process.fork(
      PipeManager.PIPE_PROCESS_MODULE_PATH,
      this.createChildProcessConfig(loggerPrefix, this._incomingPipe, PipeType.READER)
    );

    pipe.on('message', (message: string) => {
      this._messageHandlers.forEach(messageHandler => {
        messageHandler(this._messageFactory.deserialize(message));
      });
    });

    return pipe;
  }

  private createPipeWriters() {
    const pipes = new Map<NamedPipe, child_process.ChildProcess>();

    this._config.outgoingPipes.forEach(outgoingPipe => {
      pipes[outgoingPipe] = this.createPipeWriter(outgoingPipe);
    });

    return pipes;
  }

  private createPipeWriter(pipe: NamedPipe) {
    const namedPipe = NamedPipe[pipe];

    const loggerPrefix = `${this._config.loggerOptions.prefix}_${Case.pascal(namedPipe)}_Writer`;
    const writerPath = `${this._config.pipeBasePath.toLowerCase()}/${namedPipe}`;

    this._logger.log('info', `Creating pipe writer process '${loggerPrefix}' for: ${writerPath}`);

    return child_process.fork(
      PipeManager.PIPE_PROCESS_MODULE_PATH,
      this.createChildProcessConfig(loggerPrefix, pipe, PipeType.WRITER)
    );
  }

  private createChildProcessConfig(loggerPrefix: string, pipe: NamedPipe, pipeType: PipeType) {
    return [
      JSON.stringify({
        awaitPipeDelayMillis: this._config.awaitPipeDelayMillis,
        logLevel: this._config.loggerOptions.level,
        loggerPrefix: loggerPrefix,
        logDirectory: this._config.logDirectory,
        logFileName:  this._config.logFileName,
        pipeBasePath: this._config.pipeBasePath.toLowerCase(),
        pipeName: pipe,
        pipeType: PipeType[pipeType]
      })
    ] as ReadonlyArray<string>;
  }
}


export { PipeManager };
