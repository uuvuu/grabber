

interface Component {
  load();
  unload();
}


export { Component };

