
import 'reflect-metadata';

import { ContainerModule, interfaces } from 'inversify';

import { Command } from 'commander';


const TYPES = {
  ARGV: Symbol.for('ARGV'),
  Command: Symbol.for('Command')
};

const CliModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<string[]>(TYPES.ARGV).toConstantValue(process.argv);
    bind<Command>(TYPES.Command).toConstantValue(new Command());
  }
);


export { CliModule, TYPES };
