
import { ContainerModule, injectable } from 'inversify';

import { ArgvConfig } from '../../../testing/infrastructure/config/ArgvConfig';

import { TYPES as CONFIG_TYPES } from '../../config/module';
import { referenceContainer } from '../../inversify';
import { Logger } from '../Logger';

import { LoggerConfigOptionsGroup } from '../LoggerConfigOptionsGroup';
import { LoggerModule, TYPES } from '../module';


const DEPENDENCIES: ContainerModule[] = [
  LoggerModule
];


describe('Logger', () => {
  let originalArgv;
  let logger;
  let replaceSecretsSpy;
  let winstonLoggerSpy;

  const loggerPrefix = 'TestLoggerPrefix';

  @injectable()
  class TestConfig extends ArgvConfig {
    public constructor() {
      super();

      this.configOptionsGroups.push(new LoggerConfigOptionsGroup(loggerPrefix));
    }
  }

  beforeAll(() => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([]);

    referenceContainer.bind<ArgvConfig>(CONFIG_TYPES.Config).to(TestConfig).inSingletonScope();

    referenceContainer.load(...DEPENDENCIES);

    // @ts-ignore
    logger = referenceContainer.get<Logger>(TYPES.Logger);

    // @ts-ignore
    replaceSecretsSpy = jest.spyOn(logger, 'replaceSecrets');

    // @ts-ignore
    winstonLoggerSpy = jest.spyOn(logger._logger, 'log');

    // quiet console messages
    // @ts-ignore
    console._stdout.write = jest.spyOn(console._stdout, 'write').mockImplementation(() => {});

    console.log = jest.spyOn(console, 'log').mockImplementation(() => {});
    console.trace = jest.spyOn(console, 'trace').mockImplementation(() => {});
  });

  afterAll(() => {
    // @ts-ignore
    console.trace.mockRestore();
    // @ts-ignore
    console.log.mockRestore();
    // @ts-ignore
    console._stdout.write.mockRestore();

    winstonLoggerSpy.mockRestore();

    replaceSecretsSpy.mockRestore();

    referenceContainer.unbindAll();

    process.argv = originalArgv;
  });

  afterEach(() => {
    replaceSecretsSpy.mockClear();
    winstonLoggerSpy.mockClear();

    // @ts-ignore
    console.trace.mockClear();
    // @ts-ignore
    console.log.mockClear();
  });

  test('log() logs message', () => {
    const message = 'test message';

    logger.log('info', message);

    expect(winstonLoggerSpy).toHaveBeenCalledTimes(1);
    expect(winstonLoggerSpy).toHaveBeenCalledWith('info', message, undefined);

    expect(replaceSecretsSpy).toHaveBeenCalledTimes(1);

    expect(replaceSecretsSpy).toHaveBeenCalledWith('    ' + message);
    expect(replaceSecretsSpy).toHaveReturnedWith('    ' + message);
  });

  test('log() handles undefined messages', () => {
    const message = undefined;

    logger.log('info', message);

    expect(winstonLoggerSpy).not.toHaveBeenCalled();
    expect(console.log).toHaveBeenCalledTimes(1);
    expect(console.trace).toHaveBeenCalledTimes(1);
  });

  test('log() handles empty messages', () => {
    const message = '';

    logger.log('info', message);

    expect(winstonLoggerSpy).not.toHaveBeenCalled();
    expect(console.log).toHaveBeenCalledTimes(1);
    expect(console.trace).toHaveBeenCalledTimes(1);
  });

  test('log() filters authn info', () => {
    const message = 'authn key secret passphrase"';
    const filteredMesssage = 'authn **** **** ****"';

    logger.log('error', message);

    expect(winstonLoggerSpy).toHaveBeenCalledTimes(1);

    expect(replaceSecretsSpy).toHaveBeenCalledTimes(1);
    expect(replaceSecretsSpy).toHaveBeenCalledWith('   ' + message);
    expect(replaceSecretsSpy).toHaveReturnedWith('   ' + filteredMesssage);
  });
});
