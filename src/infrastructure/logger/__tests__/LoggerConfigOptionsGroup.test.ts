
import { injectable } from 'inversify';

import { ArgvConfig } from '../../../testing/infrastructure/config/ArgvConfig';

import { TYPES as CONFIG_TYPES } from '../../config/module';
import { referenceContainer } from '../../inversify';

import { LoggerConfig, LoggerConfigOptionsGroup } from '../LoggerConfigOptionsGroup';


describe('LoggerConfigOptionsGroup', () => {
  let originalArgv;

  const loggerPrefix = 'TestLoggerPrefix';

  @injectable()
  class TestConfig extends ArgvConfig {
    public constructor() {
      super();

      this.configOptionsGroups.push(new LoggerConfigOptionsGroup(loggerPrefix));
    }
  }

  beforeAll(() => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([]);
  });

  afterAll(() => {
    process.argv = originalArgv;
  });

  beforeEach(() => {
    referenceContainer.bind<ArgvConfig>(CONFIG_TYPES.Config).to(TestConfig).inSingletonScope();
  });

  afterEach(() => {
    referenceContainer.unbindAll();
  });

  test('config of prefixed logger has correct config', () => {
    // @ts-ignore
    const config = referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config) as LoggerConfig;

    const loggerOptions = config.loggerOptions;

    expect(loggerOptions.level).toEqual(LoggerConfigOptionsGroup.DEFAULTS.LOG_LEVEL);
    expect(loggerOptions.prefix).toEqual(loggerPrefix);
  });

  test('validateOptions fails if logLevel is invalid', () => {
    const originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper(['-l invalidLoglevel']);

    expect(() => { referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config); }).toThrow();

    process.argv = originalArgv;
  });
});
