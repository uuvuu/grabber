
import * as fs from 'fs';

import { Command } from 'commander';

import { PrefixedLoggerOptions } from './Logger';

import { ConfigError, ConfigErrorFactory } from '../config/Config';
import { ConfigOptionsGroup } from '../config/ConfigOptionsGroup';


class DEFAULTS {
  static readonly LOG_LEVEL = 'info';
  // https://github.com/winstonjs/winston#using-logging-levels
  static readonly ALLOWED_LOG_LEVELS = [ 'error', 'warn', 'info', 'verbose', 'debug', 'silly' ];

  static readonly LOG_FILE_DIRECTORY = '/log';
}

class LoggerConfigOptionsGroup extends ConfigOptionsGroup {
  static readonly DEFAULTS = DEFAULTS;

  constructor(protected _loggerPrefix?: string, protected _logFileName?: string, protected _logFileOwner = true) {
    super();

    this.addOption({
      logLevel: {
        flag: '-l',
        description: `Level to log at. One of: ${LoggerConfigOptionsGroup.DEFAULTS.ALLOWED_LOG_LEVELS}`,
        defaultValue: LoggerConfigOptionsGroup.DEFAULTS.LOG_LEVEL
      }
    });

    this.addOption({
      logDirectory: {
        flag: '-L',
        description: `Log file directory.`,
        defaultValue: LoggerConfigOptionsGroup.DEFAULTS.LOG_FILE_DIRECTORY
      }
    });

    this.addOption({
      logFileName: {
        flag: '-f',
        description: `Log file name.`,
        defaultValue: _logFileName
      }
    });
  }

  addExtraParams(command: Command) {
    this.addExtraParam('loggerOptions', {
      level: command.logLevel,
      prefix: this._loggerPrefix
    } as PrefixedLoggerOptions);

    this.addExtraParam('logFileOwner', this._logFileOwner);
  }

  validateOptions(command: Command, configErrorFactory: ConfigErrorFactory): ConfigError {
    if (! LoggerConfigOptionsGroup.DEFAULTS.ALLOWED_LOG_LEVELS.includes(command.logLevel)) {
      return configErrorFactory.newError(`logLevel invalid. logLevel must be one of: ${LoggerConfigOptionsGroup.DEFAULTS.ALLOWED_LOG_LEVELS}.`);
    }

    if (! fs.existsSync(command.logDirectory)) {
      return configErrorFactory.newError(`Log file directory '${command.logDirectory}' does not exist.`);
    }

    return undefined;
  }
}

interface LoggerConfig {
  logLevel: string;
  loggerOptions: PrefixedLoggerOptions;
  logDirectory: string;
  logFileName: string;
  logFileOwner: boolean;
}


export { LoggerConfig, LoggerConfigOptionsGroup };
