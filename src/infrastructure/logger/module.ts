
import 'reflect-metadata';

import { ContainerModule, interfaces } from 'inversify';

import { Logger } from './Logger';


const TYPES = {
  Logger: Symbol.for('Logger')
};


const LoggerModule = new ContainerModule((bind: interfaces.Bind) => {
  bind<Logger>(TYPES.Logger).to(Logger).inSingletonScope();
});


export { LoggerModule, TYPES };
