
// import * as dateformat from 'dateformat';
// import * as fs from 'fs';
// import * as DailyRotateFile from 'winston-daily-rotate-file';

import { inject, injectable } from 'inversify';
import { createLogger, format, Logger as WinstonLogger, LoggerOptions, transports } from 'winston';

import { TYPES } from '../config/module';

import { LoggerConfig } from './LoggerConfigOptionsGroup';


@injectable()
class Logger {
  private _logger: WinstonLogger;

  constructor(@inject(TYPES.Config) protected readonly _config: LoggerConfig) {
    // const dateSuffix = dateformat(new Date(), 'yyyy-mm-dd');
    const fileName = `${this._config.logDirectory}/${this._config.logFileName}.log`;
    // const fileNameWithDateSuffix = `${fileName}.${dateSuffix}`;

    // if (this._config.logFileOwner) {
    //   if (! fs.existsSync(fileNameWithDateSuffix)) {
    //     fs.closeSync(fs.openSync(fileNameWithDateSuffix, 'w'));
    //
    //     try {
    //       if (fs.existsSync(fileName)) {
    //         fs.unlinkSync(fileName);
    //       }
    //       fs.symlinkSync(fileNameWithDateSuffix, `${this._config.logDirectory}/${this._config.logFileName}.log`);
    //     } catch (error) {
    //       console.log(`${new Date().toISOString()} [${this._config.loggerOptions.prefix}] - warn:     Symlink of ${fileName} to ${fileNameWithDateSuffix} failed. Error was: ${error}`);
    //     }
    //   }
    // }

    const loggerOptions = _config.loggerOptions || {};

    loggerOptions.format = loggerOptions.format || format.combine(
      format.timestamp(),
      format.padLevels(),
      format.label({ label: `[${_config.loggerOptions.prefix}]` }),
      this.filterSecrets(),
      format.printf(info => `${info.timestamp} ${info.label} - ${info.level}: ${info.message}`),
    );

    if (this._config.logFileOwner && this._config.logFileName) {
      // const dailyRotationFileTransport = new DailyRotateFile({
      //   filename: fileName,
      //   datePattern: 'YYYY-MM-DD',
      //   zippedArchive: true,
      //   maxFiles: '32d'
      // });
      //
      // dailyRotationFileTransport.on('rotate', (oldFilename, newFilename) => {
      //   // don't prefix symlink to new filename with dir (eg: /log). This will work inside the docker container itself
      //   // as there is actually a /log dir. But on the host system /log is just a link to a different directory on the host
      //   // see the Dockerfile for where /log points to on the host
      //   newFilename = newFilename.split('/')[2];
      //
      //   try {
      //     this._logger.log('info', `Changing log file symlink '${fileName}' from '${oldFilename}' to '${newFilename}'.`);
      //     if (fs.existsSync(fileName)) {
      //       fs.unlinkSync(fileName);
      //     }
      //     fs.symlinkSync(newFilename, fileName);
      //   } catch (error) {
      //     this._logger.log('warn', `Symlink of '${fileName}' to '${newFilename}' failed. Error was: ${error}`);
      //   }
      // });

      loggerOptions.transports = loggerOptions.transports || [
        new transports.Console(),
        // dailyRotationFileTransport
        new (transports.File)({ filename: fileName })
      ];
    } else {
      loggerOptions.transports = loggerOptions.transports || [
        new transports.Console()
      ];
    }

    this._logger = createLogger(loggerOptions);
  }

  log(level: string, message: string, meta?: any) {
    message = this.preProcessMessage(message);

    if (message) {
      this._logger.log(level, message, meta);
    }
  }

  protected preProcessMessage(message: any) {
    if (! message) {
      console.log('Logger.log() called with undefined message. Trace is below...');
      console.trace();
    }

    if (message && message.message) {
      message = message.message;
    }

    return message;
  }

  protected filterSecrets = format((info, opts) => {
    info.message = this.replaceSecrets(info.message);
    return info;
  });

  protected replaceSecrets(secretMessage: string) {
    return secretMessage.replace(/(.*)(authn\s+\S+\s+\S+\s+\S+)("+.*)/, '$1authn **** **** ****$3');
  }
}

interface PrefixedLoggerOptions extends LoggerOptions {
  prefix?: string;
}


export { Logger, PrefixedLoggerOptions };
