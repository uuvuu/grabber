
import 'reflect-metadata';

import { Container } from 'inversify';
import { fluentProvide } from 'inversify-binding-decorators';
import { makeLoggerMiddleware } from 'inversify-logger-middleware';


class ReferenceContainer {
  private static _instance: Container;

  public static get Instance() {
    if (! this._instance) {
      this._instance = new Container();

      if (process.env.NODE_ENV === 'development') {
        this._instance.applyMiddleware(makeLoggerMiddleware());
      }
    }

    return this._instance;
  }
}


function provideSingleton(identifier: any) {
  return fluentProvide(identifier)
    .inSingletonScope()
    .done();
}

function provideNamed(identifier: any, name: string) {
  return fluentProvide(identifier)
    .inSingletonScope()
    .whenTargetNamed(name)
    .done();
}

function injectFunctionDependencies(func, dependencies) {
  const injections = dependencies.map(dependency => {
    return referenceContainer.get(dependency);
  });

  return func.bind(func, ...injections);
}


const referenceContainer = ReferenceContainer.Instance;


export { injectFunctionDependencies, referenceContainer, provideNamed, provideSingleton };
