
import { injectable } from 'inversify';
import { BulkWriteResult, MongoError } from 'mongodb';
import { Document, Model as MongooseModel } from 'mongoose';

import { FieldFilterType, Pageable, Query, UnionIntersectionFilterType } from '../../Query';

import { BaseRepository } from '../BaseRepository';


@injectable()
abstract class BaseRepositoryImpl<
    PageOffsetType,
    EntityType,
    DocumentType extends EntityType & Document,
    ModelType extends MongooseModel<DocumentType> & Pageable<PageOffsetType>>
  implements BaseRepository<PageOffsetType, DocumentType> {

  static readonly OFFICIAL_START_DATE = new Date('2018-11-28T16:38:00Z');

  protected _model: ModelType;

  protected constructor(protected readonly _config, protected readonly _modelFactory) {
    this.init();
  }

  protected abstract init();

  async findById(id: string): Promise<DocumentType> {
    return new Promise<DocumentType>((resolve, reject) => {
      this._model.findById(id, (error: MongoError, result: DocumentType) => {
        if (error) {
          reject(error);
          return;
        }

        if (! result) {
          reject();
        } else {
          resolve(result);
        }
      });
    });
  }

  async findManyByQuery(query: Query<DocumentType>): Promise<DocumentType[]> {
    return new Promise<DocumentType[]>((resolve, reject) => {
      const documentQuery = this._model
        .find(query.filter)
        .limit(query.limit)
        .select(query.projection)
        .sort(query.sort);

      return documentQuery.exec((error: MongoError, results: DocumentType[]) => {
          if (error) {
            reject(error);
            return;
          }

          resolve(results);
        });
    });
  }

  async findManyByPage(pageOffset: PageOffsetType): Promise<DocumentType[]> {
    return new Promise<DocumentType[]>((resolve, reject) => {
      const pageQuery = this._model.pageQuery(pageOffset);

      const documentQuery = this._model.find(pageQuery.filter);

      if (typeof pageQuery.limit !== 'undefined') {
        documentQuery.limit(pageQuery.limit);
      }

      documentQuery.sort(pageQuery.sort);

      return documentQuery.exec((error: MongoError, results: DocumentType[]) => {
        if (error) {
          reject(error);
          return;
        }

        resolve(results);
      });
    });
  }

  async aggregate(aggregations: any[]): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      const aggregate = this._model.aggregate(aggregations).allowDiskUse(true);

      return aggregate.exec((error: MongoError, results: any[]) => {
        if (error) {
          reject(error);
          return;
        }

        resolve(results);
      });
    });
  }

  async create(...docs): Promise<DocumentType[]> {
    return new Promise<DocumentType[]>((resolve, reject) => {
      return this._model.create(docs, (error: MongoError, results: DocumentType[]) => {
        if (error) {
          reject(error);
        }

        resolve(results);
      });
    });
  }

  async findOneByQueryAndUpdate(query: Query<DocumentType>, update: any): Promise<DocumentType> {
    return new Promise<DocumentType>((resolve, reject) => {
      return this._model.findOneAndUpdate(query.filter, update, { new: true }, (error: MongoError, result: DocumentType) => {
        if (error) {
          reject(error);
        }

        resolve(result);
      });
    });
  }

  async insertMany(docs: DocumentType[], options?: { ordered?: boolean, rawResult?: boolean }): Promise<DocumentType[]> {
    return new Promise<DocumentType[]>((resolve, reject) => {
      return this._model.insertMany(docs, options, (error: MongoError, results: DocumentType[]) => {
        if (error) {
          reject(error);
          return;
        }

        resolve(results);
      });
    });
  }

  async upsertMany(docs: DocumentType[], uniqueField: string): Promise<BulkWriteResult> {
    return new Promise<BulkWriteResult>((resolve, reject) => {
      const bulk = this._model.collection.initializeUnorderedBulkOp();

      // no-op case
      if (docs.length === 0) {
        return;
      }

      docs.forEach(doc => {
        bulk.find({ [ uniqueField ]: doc[uniqueField] }).upsert().updateOne(doc);
      });

      return bulk.execute((error: MongoError, result: BulkWriteResult) => {
        if (error) {
          reject(error);
          return;
        }

        resolve(result);
      });
    });
  }

  async count(filter?: FieldFilterType<DocumentType> | UnionIntersectionFilterType<DocumentType>): Promise<number> {
    if (! filter) {
      filter = {};
    }

    return new Promise<number>((resolve, reject) => {
      this._model.countDocuments(filter, (error, result) => {
        if (error) {
          reject(error);
          return;
        }

        resolve(result);
      });
    });
  }

  async pages(): Promise<number> {
    return new Promise<number>((resolve, reject) => {
      this._model.countDocuments({}, (error, result) => {
        if (error) {
          reject(error);
          return;
        }

        resolve(Math.ceil(result / (this._model.limit)));
      });
    });
  }
}


export { BaseRepositoryImpl };

