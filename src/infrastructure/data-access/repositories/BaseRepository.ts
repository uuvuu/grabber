import { BulkWriteResult } from 'mongodb';
import { FieldFilterType, Query, UnionIntersectionFilterType } from '../Query';


interface Read<PageOffset, T> {
  findById(id: string): Promise<T>;
  findManyByQuery(query?: Query<T>): Promise<T[]>;
  findManyByPage(pageOffset: PageOffset): Promise<T[]>;

  aggregate(aggregations: any[]): Promise<any>;

  pages(): Promise<number>;
  count(filter?: FieldFilterType<DocumentType> | UnionIntersectionFilterType<DocumentType>): Promise<number>;
}

interface Write<T> {
  create(...docs): Promise<T[]>;
  findOneByQueryAndUpdate(query: Query<T>, update: any): Promise<T>;

  insertMany(docs: T[], options?: { ordered?: boolean, rawResult?: boolean }): Promise<T[]>;
  upsertMany(docs: T[], uniqueField: string): Promise<BulkWriteResult>;
}


interface BaseRepository<PageOffset, T> extends Read<PageOffset, T>, Write<T> {
}


export { BaseRepository };



