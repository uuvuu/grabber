
import * as mongoose from 'mongoose';

import { inject, injectable } from 'inversify';

import { sleep } from '../../utils/sleep';
import { Component } from '../component/Component';

import { TYPES as CONFIG_TYPES } from '../config/module';
import { Logger } from '../logger/Logger';
import { TYPES as LOGGER_TYPES } from '../logger/module';

import { MongoConfig } from './MongoConfigOptionsGroup';


type DbClient = {
  model: <DocumentType, ModelType>(collectionName: string, schema: mongoose.Schema) => ModelType;
  disconnect: () => Promise<void>;
};


@injectable()
class DatabaseClient implements Component {
  protected _initialized = false;
  protected _dbClient: DbClient;
  protected _connectionString;

  constructor(
    @inject(CONFIG_TYPES.Config) protected readonly _config: MongoConfig,
    @inject(LOGGER_TYPES.Logger) protected readonly _logger: Logger) {

    this._connectionString = `mongodb://${this._config.hostname}/${this._config.dbName}`;
  }

  async init() {
    try {
      await this.connect(this._connectionString, this._config.dbOptions);

      this._logger.log('info', `Connected to DB: ${this._connectionString}`);
    } catch (error) {
      // let this error be handled/logged at the caller level
      error.message = `Error connecting to DB: ${error}`;

      throw error;
    }

    // mongoose.set('debug', (collectionName, method, query, doc) => {
    //   this._logger.log('info', `${collectionName}.${method} => ${JSON.stringify(query)}, ${JSON.stringify(doc)}`);
    // });

    this._dbClient = mongoose;

    this._initialized = true;
  }

  async load() {}

  async unload() {
    await this.disconnect();
  }

  get dbClient(): DbClient {
    if (! this.initialized) {
      throw new Error('Cannot get dbClient before DatabaseClient has been initialized.');
    }

    return this._dbClient;
  }

  async connect(uris: string, options?: mongoose.ConnectionOptions) {
    await mongoose.connect(uris, options);

    mongoose.set('useCreateIndex', true);
    mongoose.set('useFindAndModify', false);
  }

  async disconnect() {
    try {
      // wait a sec for various cleanup
      await sleep(100);

      await this._dbClient.disconnect();
    } catch (error) {
      this._logger.log('info', `Error disconnecting from DB: ${error}`);

      throw error;
    }

    this._logger.log('info', `Disconnected from DB.`);
  }

  get initialized() {
    return this._initialized;
  }
}


export { DatabaseClient, DbClient };
