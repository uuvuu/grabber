
import { inject, injectable } from 'inversify';
import { Schema } from 'mongoose';

import { DbClient } from '../DatabaseClient';
import { TYPES } from '../module';


@injectable()
class ModelFactory {
  private _models = new Map<Symbol, any>();

  constructor(@inject(TYPES.DbClient) protected readonly _dbClient: DbClient) {
  }

  public newModel<DocumentType, ModelType>(
    collectionName: string, sym: symbol, schema: Schema): ModelType {

    if (this._models.get(sym)) {
      return this._models.get(sym) as ModelType;
    }

    const model = this._dbClient.model<DocumentType, ModelType>(collectionName, schema);
    this._models.set(sym, model);

    return model;
  }

  public newProductModel<DocumentType, ModelType>(
    product: string, collectionName: string, sym: symbol, schema: Schema): ModelType {

    if (this._models.get(sym)) {
      return this._models.get(sym) as ModelType;
    }

    const productParts = product.split('-').map(part => {
      return part.toLowerCase();
    });

    collectionName = `${productParts[0].toLowerCase()}_${productParts[1].toLowerCase()}_${collectionName}`;

    const model = this._dbClient.model<DocumentType, ModelType>(collectionName, schema);
    this._models.set(sym, model);

    return model;
  }
}


export { ModelFactory };
