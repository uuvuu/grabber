
import { AsyncContainerModule, ContainerModule, injectable } from 'inversify';

import { CandleSchema, TYPES as CANDLE_TYPES } from '../../../../domain/candle/repositories/impl/CandleRepositoryImpl';
import { ArgvConfig } from '../../../../testing/infrastructure/config/ArgvConfig';
import { DatabaseModule } from '../../../../testing/infrastructure/data-access/unit/module';

import { LoggerConfigOptionsGroup } from '../../../logger/LoggerConfigOptionsGroup';
import { TYPES as CONFIG_TYPES } from '../../../config/module';
import { ProductConfig, ProductConfigOptionsGroup } from '../../../config/ProductConfigOptionsGroup';
import { LoggerModule } from '../../../logger/module';

import { DbClient } from '../../DatabaseClient';
import { TYPES as DB_TYPES } from '../../module';

import { referenceContainer } from '../../../inversify';

import { ModelFactory } from '../ModelFactory';
import { ModelFactoryModule, TYPES as FACTORY_TYPES } from '../module';


const DEPENDENCIES: ContainerModule[] = [
  LoggerModule,
  ModelFactoryModule
];

const ASYNC_DEPENDENCIES: AsyncContainerModule[] = [
  DatabaseModule
];

@injectable()
class Config extends ArgvConfig {
  constructor() {
    super();

    this.configOptionsGroups.push(new LoggerConfigOptionsGroup('TestModelFactory'));
    this.configOptionsGroups.push(new ProductConfigOptionsGroup());
  }
}

describe('model factory', () => {
  let originalArgv;

  beforeAll(async () => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([]);

    referenceContainer.bind<Config>(CONFIG_TYPES.Config).to(Config).inSingletonScope();

    referenceContainer.load(...DEPENDENCIES);

    try {
      await referenceContainer.loadAsync(...ASYNC_DEPENDENCIES);
    } catch (error) {
      // let this error be handled/logged at the caller level
      throw error;
    }
  });

  afterAll(async() => {
    referenceContainer.unbindAll();

    process.argv = originalArgv;
  });

  test('creates singleton models', () => {
    // @ts-ignore
    const config = referenceContainer.get<Config>(CONFIG_TYPES.Config) as ProductConfig;

    const modelFactory = referenceContainer.get<ModelFactory>(FACTORY_TYPES.ModelFactory);
    const dbClient = referenceContainer.get<DbClient>(DB_TYPES.DbClient);

    const dbClientModelSpy = jest.spyOn(dbClient, 'model');

    const model = modelFactory.newProductModel(config.product, 'candle', CANDLE_TYPES.CandleModel, CandleSchema);

    expect(model).toBeDefined();
    expect(dbClientModelSpy).toHaveBeenCalledTimes(1);

    const model2 = modelFactory.newProductModel(config.product, 'candle', CANDLE_TYPES.CandleModel, CandleSchema);

    expect(model2).toEqual(model);

    expect(dbClientModelSpy).toHaveBeenCalledTimes(1);
  });
});
