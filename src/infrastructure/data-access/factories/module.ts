
import 'reflect-metadata';

import { ContainerModule, interfaces } from 'inversify';

import { ModelFactory } from './ModelFactory';


const TYPES = {
  ModelFactory: Symbol.for('ModelFactory')
};

const ModelFactoryModule = new ContainerModule((bind: interfaces.Bind) => {
  bind<ModelFactory>(TYPES.ModelFactory).to(ModelFactory).inSingletonScope();
});


export { ModelFactoryModule, TYPES };
