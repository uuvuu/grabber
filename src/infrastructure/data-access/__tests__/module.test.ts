
jest.mock('../DatabaseClient');

import { AsyncContainerModule, ContainerModule, injectable } from 'inversify';


import { ArgvConfig } from '../../../testing/infrastructure/config/ArgvConfig';

import { Config } from '../../config/Config';
import { TYPES as CONFIG_TYPES } from '../../config/module';
import { referenceContainer } from '../../inversify';
import { LoggerConfigOptionsGroup } from '../../logger/LoggerConfigOptionsGroup';
import { LoggerModule } from '../../logger/module';

import { DatabaseModule, TYPES } from '../module';
import { MongoConfigOptionsGroup } from '../MongoConfigOptionsGroup';

import { DatabaseClient, DbClient } from '../DatabaseClient';


const DEPENDENCIES: ContainerModule[] = [
  LoggerModule
];

const ASYNC_DEPENDENCIES: AsyncContainerModule[] = [
  DatabaseModule
];


describe('DatabaseModule', () => {
  @injectable()
  class TestConfig extends ArgvConfig {
    public constructor() {
      super();

      this.configOptionsGroups.push(new LoggerConfigOptionsGroup('TestCandleRepository'));
      this.configOptionsGroups.push(new MongoConfigOptionsGroup());
    }
  }

  let originalArgv;

  beforeAll(() => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([]);
  });

  afterAll(() => {
    process.argv = originalArgv;
  });

  test('write me', async () => {
    expect.assertions(2);

    referenceContainer.bind<Config>(CONFIG_TYPES.Config).to(TestConfig).inSingletonScope();

    referenceContainer.load(...DEPENDENCIES);

    try {
      await referenceContainer.loadAsync(...ASYNC_DEPENDENCIES);
    } catch (error) {
      // let this error be handled/logged at the caller level
      throw error;
    }

    const databaseClient = referenceContainer.get<DatabaseClient>(TYPES.DatabaseClient);
    const dbClient = referenceContainer.get<DbClient>(TYPES.DbClient);

    expect(databaseClient).toBeDefined();
    expect(dbClient).toBeDefined();
  });
});
