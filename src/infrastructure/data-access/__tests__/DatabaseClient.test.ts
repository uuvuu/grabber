
import 'reflect-metadata';

import * as mongoose from 'mongoose';

import { injectable } from 'inversify';

import { ArgvConfig } from '../../../testing/infrastructure/config/ArgvConfig';

import { Logger } from '../../logger/Logger';

import { DatabaseClient } from '../DatabaseClient';
import { MongoConfig, MongoConfigOptionsGroup } from '../MongoConfigOptionsGroup';


describe('DbClient', () => {
  @injectable()
  class TestConfig extends ArgvConfig {
    public constructor() {
      super();

      this.configOptionsGroups.push(new MongoConfigOptionsGroup());
    }
  }
  let config: TestConfig & MongoConfig;
  let logger: Logger;

  beforeAll(() => {
    config = new TestConfig() as TestConfig & MongoConfig;

    // @ts-ignore
    logger = new class implements Logger {
      log(level: string, message: string, meta?: any): void {}
    };
  });

  test('initializes successfully', async () => {
    expect.assertions(5);

    const databaseClient = new DatabaseClient(config, logger);

    const testMongoose = new mongoose.Mongoose();
    databaseClient.connect = jest.spyOn(databaseClient, 'connect').mockImplementation(() => {
      return testMongoose;
    });

    await expect(databaseClient.init()).resolves.toBeUndefined();

    expect(databaseClient.connect).toHaveBeenCalledTimes(1);
    expect(databaseClient.connect).toHaveBeenCalledWith(`mongodb://${config.hostname}/${config.dbName}`, config.dbOptions);
    expect(databaseClient.connect).toHaveReturnedWith(testMongoose);

    const dbClient = databaseClient.dbClient;

    expect(dbClient).toBeDefined();

    await dbClient.disconnect();

    // @ts-ignore
    databaseClient.connect.mockRestore();
  });

  test('throws exception if dbClient is accessed before init() is called',  () => {
    expect.assertions(1);

    const databaseClient = new DatabaseClient(config, logger);

    expect(() => { databaseClient.dbClient; }).toThrow();
  });

  test('fails gracefully', async () => {
    expect.assertions(1);

    const databaseClient = new DatabaseClient(config, logger);

    const errorMessage = new Error('connect failed');
    databaseClient.connect = jest.spyOn(databaseClient, 'connect').mockImplementation(() => {
      throw errorMessage;
    });

    await expect(databaseClient.init()).rejects.toEqual(errorMessage);

    // @ts-ignore
    databaseClient.connect.mockRestore();
  });
});
