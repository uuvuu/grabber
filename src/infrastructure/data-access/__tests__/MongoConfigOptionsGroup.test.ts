
import { injectable } from 'inversify';

import { ArgvConfig } from '../../../testing/infrastructure/config/ArgvConfig';

import { TYPES as CONFIG_TYPES } from '../../config/module';
import { referenceContainer } from '../../inversify';

import { MongoConfig, MongoConfigOptionsGroup } from '../MongoConfigOptionsGroup';


describe('MongoConfigOptionsGroup', () => {
  @injectable()
  class TestConfig extends ArgvConfig {
    public constructor() {
      super();

      this.configOptionsGroups.push(new MongoConfigOptionsGroup());
    }
  }

  let originalArgv;

  beforeAll(() => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([]);
  });

  afterAll(() => {
    process.argv = originalArgv;
  });

  beforeEach(() => {
    referenceContainer.bind<ArgvConfig>(CONFIG_TYPES.Config).to(TestConfig).inSingletonScope();
  });

  afterEach(() => {
    referenceContainer.unbindAll();
  });

  test('config of database has correct config', () => {
    // @ts-ignore
    const config = referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config) as MongoConfig;

    expect(config.dbName).toEqual(MongoConfigOptionsGroup.DEFAULTS.DATABASE_NAME);

    const dbOptions = config.dbOptions;

    expect(dbOptions.keepAlive).toEqual(MongoConfigOptionsGroup.DEFAULTS.MONGO_KEEP_ALIVE_MILLIS);
    expect(dbOptions.connectTimeoutMS).toEqual(MongoConfigOptionsGroup.DEFAULTS.MONGO_CONNECT_TIMEOUT_MILLIS);
    expect(dbOptions.reconnectTries).toEqual(MongoConfigOptionsGroup.DEFAULTS.MONGO_RECONNECT_RETRIES);
    expect(dbOptions.reconnectInterval).toEqual(MongoConfigOptionsGroup.DEFAULTS.MONGO_RECONNECT_INTERVAL_MILLIS);
  });

  test('testMode uses sandbox test db settings', () => {
    const originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper(['-t']);

    // @ts-ignore
    const config = referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config) as MongoConfig;

    expect(config.dbName).toEqual(
      MongoConfigOptionsGroup.DEFAULTS.DATABASE_NAME + MongoConfigOptionsGroup.DEFAULTS.TEST_DATABASE_NAME_SUFFIX);

    process.argv = originalArgv;
  });

  test('validateOptions() fails if dbName is illegal', () => {
    const originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper(['-D test$db']);

    expect(() => { referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config); }).toThrow();

    process.argv = originalArgv;
  });
});
