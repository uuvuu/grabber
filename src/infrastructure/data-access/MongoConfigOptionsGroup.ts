
import { Command } from 'commander';
import { ConnectionOptions } from 'mongoose';

import { ConfigError, ConfigErrorFactory} from '../config/Config';
import { ConfigOptionsGroup } from '../config/ConfigOptionsGroup';


class DEFAULTS {
  static readonly HOST_NAME = 'mongo';   // run locally host 'mongo' will default to localhost
  static readonly DATABASE_NAME = 'gdax_database';
  static readonly TEST_DATABASE_NAME_SUFFIX = '_sandbox';

  // https://docs.mongodb.com/manual/reference/limits/#naming-restrictions
  static readonly DATABASE_NAME_RESTRICTED_CHARACTERS = [ '/', '\\', '.', ' ', '"', '$' ];

  static readonly MONGO_KEEP_ALIVE_MILLIS = 120;
  static readonly MONGO_CONNECT_TIMEOUT_MILLIS = 30000;
  static readonly MONGO_RECONNECT_RETRIES = Number.MAX_VALUE;
  static readonly MONGO_RECONNECT_INTERVAL_MILLIS = 10000;

  static readonly MONGO_USE_NEW_URL_PARSER = true;
}

class MongoConfigOptionsGroup extends ConfigOptionsGroup {
  static readonly DEFAULTS = DEFAULTS;

  constructor() {
    super();

    this.addOption({
      hostname: {
        flag: '-H',
        description: 'Hostname of mongo server.',
        defaultValue: MongoConfigOptionsGroup.DEFAULTS.HOST_NAME
      }
    });

    this.addOption({
      dbName: {
        flag: '-D',
        description: 'Name of database.',
        defaultValue: MongoConfigOptionsGroup.DEFAULTS.DATABASE_NAME
      }
    });
  }

  addExtraParams(command: Command) {
    this.addExtraParam('dbOptions', {
      keepAlive: MongoConfigOptionsGroup.DEFAULTS.MONGO_KEEP_ALIVE_MILLIS,
      connectTimeoutMS: MongoConfigOptionsGroup.DEFAULTS.MONGO_CONNECT_TIMEOUT_MILLIS,
      reconnectTries: MongoConfigOptionsGroup.DEFAULTS.MONGO_RECONNECT_RETRIES,
      reconnectInterval: MongoConfigOptionsGroup.DEFAULTS.MONGO_RECONNECT_INTERVAL_MILLIS,
      useNewUrlParser: MongoConfigOptionsGroup.DEFAULTS.MONGO_USE_NEW_URL_PARSER
    } as ConnectionOptions);
  }

  validateOptions(command: Command, configErrorFactory: ConfigErrorFactory): ConfigError {
    for (const character of MongoConfigOptionsGroup.DEFAULTS.DATABASE_NAME_RESTRICTED_CHARACTERS) {
      if (command.dbName.includes(character)) {
        return configErrorFactory.newError(
          `dbName invalid. dbName must not include the following characters: ${MongoConfigOptionsGroup.DEFAULTS.DATABASE_NAME_RESTRICTED_CHARACTERS}.`);
      }
    }

    return undefined;
  }

  preProcessOptions(command: Command) {
    if (command.testMode) {
      command.dbName = command.dbName + MongoConfigOptionsGroup.DEFAULTS.TEST_DATABASE_NAME_SUFFIX;
    }
  }
}

interface MongoConfig {
  hostname: string;
  dbName: string;
  dbOptions: any;
}


export { MongoConfig, MongoConfigOptionsGroup };
