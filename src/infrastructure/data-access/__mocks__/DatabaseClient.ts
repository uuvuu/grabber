
import * as mongoose from 'mongoose';

import { inject, injectable } from 'inversify';

import { QueryFactory } from '../../../testing/infrastructure/data-access/QueryFactory';
import { Component } from '../../component/Component';

import { TYPES as CONFIG_TYPES } from '../../config/module';
import { Logger } from '../../logger/Logger';
import { TYPES as LOGGER_TYPES } from '../../logger/module';

import { DbClient } from '../DatabaseClient';
import { MongoConfig } from '../MongoConfigOptionsGroup';


// ideally it'd be nice to have the mock just extend the original DatabaseClient
// as base class, but due to the may jest.mock('..db-client') works and how jest
// will resolve DatabaseClient we end up with an infinite call stack problem

@injectable()
class DatabaseClient implements Component {
  protected _initialized = false;
  protected _dbClient: DbClient;
  protected _connectionString;

  constructor(
    @inject(CONFIG_TYPES.Config) protected readonly _config: MongoConfig,
    @inject(LOGGER_TYPES.Logger) protected readonly _logger: Logger) {

    this._connectionString = `mongodb://${this._config.hostname}/${this._config.dbName}`;
  }

  public async init() {
    this._dbClient = {
      model: jest.fn().mockImplementation(<DocumentType, ModelType>(collectionName, schema) => {
        const _model = {
          findById: jest.fn().mockImplementation((id, callback) => callback()),
          find: jest.fn().mockImplementation(() => QueryFactory.newQuery()),

          create: jest.fn().mockImplementation((...docs: any[]) => {
            let callback;

            if (docs && docs.length) {
              if (typeof docs[docs.length - 1] === 'function') {
                callback = docs.pop();
              }
            }

            return callback ? callback(undefined, docs[0]) : Promise.resolve(docs[0]);
          }),

          insertMany: jest.fn().mockImplementation((docs, options, callback) => callback()),

          aggregate: jest.fn().mockImplementation((aggregations: any[], callback: Function): Promise<any> => callback()),

          countDocuments: jest.fn(),
          pages: jest.fn()
        };

        for (const [key, value] of Object.entries(schema.statics)) {
          _model[key] = value;
        }

        for (const [key, value] of Object.entries(schema.methods)) {
          _model[key] = value;
        }

        return _model;
      }),

      disconnect: jest.fn().mockImplementation(() => Promise.resolve()),
    };

    this._initialized = true;
  }

  async load() {}

  async unload() {
    await this.disconnect();
  }

  public get dbClient(): DbClient {
    if (! this.initialized) {
      throw new Error('Cannot get dbClient before DatabaseClient has been initialized.');
    }

    return this._dbClient;
  }

  public async connect(uris: string, options?: mongoose.ConnectionOptions) {
    return Promise.resolve(undefined);
  }

  public async disconnect() {
    return Promise.resolve(undefined);
  }

  public get initialized() {
    return this._initialized;
  }
}


export { DatabaseClient };

