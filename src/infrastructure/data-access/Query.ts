
enum SortOrder {
  ASCENDING = 1,
  DESCENDING = -1
}

type FieldFilterType<T> = {
  [P in keyof T]?:
  T[P] |
  { $eq: any } |
  { $ne: any } |
  { $gt: any } |
  { $gte: any } |
  { $lt: any } |
  { $lte: any } |
  { $in: [any] } |
  { $nin: [any] } |
  { $text: string } |
  { $toDouble: any } |
  { $regex: RegExp }
};

type UnionIntersectionFilterType<T> = {
  $or?: Array<any>,
  $and?: Array<any>
};

type Query<T> = {
  filter?: FieldFilterType<T> | UnionIntersectionFilterType<T>,
  projection?: {
    [P in keyof T]?: boolean
  },
  sort?: {
    [P in keyof T]?: SortOrder
  },
  limit?: number;
};

type Pageable<T> = {
  limit: number;
  offset: (value: T) => Query<T>;
  pageQuery: (value: T) => Query<T>;
};


export { FieldFilterType, Pageable, Query, SortOrder, UnionIntersectionFilterType };
