
import gql from 'graphql-tag';

//  type Mutation {
//    createOrder(type: String!): Order
//    updateOrder(_id: ID!, type: String): Order
//    deleteOrder(_id: ID!): Order
//  }

const TypeDefs = gql`
  scalar DateTime
  scalar JSON
  scalar MongoObjectId

  type Query {
    dashboard: Dashboard!
    orders: [ Order ]!
    accounts: [ Accounts ]!
    cards: [ Card ]!
  }
  
  type Subscription {
    orderCreated: Order!
    accountsUpdated: [ Accounts ]!
    statusCardsUpdated: [ Card ]!
  }

  type Dashboard {
    orders: [ Order ]!,
    accounts: [ Accounts ]!,
    cards: [ Card ]!
  }

  type Order {
    _id: MongoObjectId!
    
    orderId: ID!
    
    product: String!
    
    price: String!
    size:  String!

    side:  String!

    stp:  String

    funds:  String
    specifiedFunds: String

    type: String!

    timeInForce: String!

    postOnly: Boolean!

    createdTime: DateTime!
    expireTime: DateTime
    doneTime: DateTime
    candleTime: DateTime!

    doneReason: String
    rejectedReason: String

    fillFees: String!
    filledSize: String!

    executedValue: String!

    status: String!

    settled: Boolean!

    clearanceStatus: String
  }
  
  type Accounts {
    _id: MongoObjectId!
    
    time: DateTime!
    
    profileId: String!
    
    totalUSD: String!
    totalCrypto: String!
    exchangeRateCrypto: String!
    
    accounts: [ Account ]
  }
  
  type Account {
    accountId: String!
    
    currency: String!
    
    balance: String!
    available: String!
    hold: String!
    
    exchangeRateUSD: String!
    exchangeRateCrypto: String!
  }
  
  type Card {
    name: String!
    
    type: String!
    
    data: JSON!
  }
`;


export { TypeDefs };
