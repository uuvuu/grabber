
import { ObjectId } from 'bson';
import { GraphQLScalarType, StringValueNode } from 'graphql';


const objectIdPattern = /^[0-9a-fA-F]{24}$/;

function isObjectId(str) {
  return objectIdPattern.test(str);
}

function parseObjectId(id) {
  if (isObjectId(id)) {
    return new ObjectId(id);
  }

  throw new Error('ObjectId must be a string of 24 hex characters');
}

const GraphQLMongoObjectId = new GraphQLScalarType({
  name: 'MongoObjectId',
  description: 'The `ObjectId` scalar type represents a mongodb unique ID',
  serialize: String,
  parseValue: parseObjectId,
  parseLiteral: (ast: StringValueNode) => parseObjectId(ast.value)
});


export { GraphQLMongoObjectId };
