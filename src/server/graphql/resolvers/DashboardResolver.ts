
import { inject, injectable } from 'inversify';

import { Account } from '../../../domain/account/entities/Account';
import { AccountRepository } from '../../../domain/account/repositories/AccountRepository';
import { TYPES as ACCOUNT_TYPES } from '../../../domain/account/repositories/impl/AccountRepositoryImpl';
import { Order } from '../../../domain/order/entities/Order';
import { TYPES as ORDER_TYPES } from '../../../domain/order/repositories/impl/OrderRepositoryImpl';
import { OrderRepository } from '../../../domain/order/repositories/OrderRepository';
import { Environment } from '../../../environment/Environment';
import { Query, SortOrder } from '../../../infrastructure/data-access/Query';
import { Logger } from '../../../infrastructure/logger/Logger';
import { TYPES as LOGGER_TYPES } from '../../../infrastructure/logger/module';


@injectable()
class DashboardResolver {
  private _environment;

  constructor(
    @inject(LOGGER_TYPES.Logger) private readonly _logger: Logger,
    @inject(ACCOUNT_TYPES.AccountRepository) private readonly _accountRepository: AccountRepository,
    @inject(ORDER_TYPES.OrderRepository) private readonly _orderRepository: OrderRepository) {

    this._environment = Environment.environment;
  }

  async dashboard() {
    const startTime = this.startTime();
    const startTimePerformance = new Date();

    const accountsPromise = this.accounts();
    const ordersPromise = this.orders(startTime);
    const cardsPromise = this.cards();

    const promises = Promise.all([ accountsPromise, ordersPromise, cardsPromise ]);

    promises.then(data => {
      const endTimePerformance = new Date();
      this._logger.log(
        'info', `Processing request for dashboard took ${endTimePerformance.getTime() - startTimePerformance.getTime()} millis.`);

      return data;
    });

    const [ accounts, orders, cards ] = await promises;

    return {
      accounts: accounts,
      orders: orders,
      cards: cards
    };
  }

  async orders(startTime?: Date) {
    const startTimePerformance = new Date();

    startTime = this.startTime(startTime);

    const orderQuery: Query<Order.Order> = {
      filter: { createdTime: { $gte: startTime }},
      sort: { createdTime: SortOrder.ASCENDING },
      limit: this._environment.dashboardNumberOfPointsOrders
    };

    // @ts-ignore
    const ordersPromise = this._orderRepository.findManyByQuery(orderQuery);

    ordersPromise.then(orders => {
      const endTimePerformance = new Date();
      this._logger.log(
        'info', `Processing request for orders took ${endTimePerformance.getTime() - startTimePerformance.getTime()} millis.`);

      return orders;
    });

    return ordersPromise;
  }

  async accounts() {
    const startTimePerformance = new Date();

    const accountsFirstAndLast = await this._accountRepository.findFirstAndLastAccount();

    // this may not be the true number of accounts (maybe some accounts are missing due to downtime), however this
    // number will provide even spacing between the first account and the last (current) account row
    const numberOfAccountRows = (accountsFirstAndLast[1].time.getTime() - accountsFirstAndLast[0].time.getTime()) / 60000;

    const accountsPerPoint = numberOfAccountRows / this._environment.dashboardNumberOfPointsAccounts;

    const dates = [];

    const startTime = accountsFirstAndLast[0].time.getTime();
    let nextDate = new Date(startTime);    // new Date(startTime + accountsPerPoint * 60000 * 0)
    for (let i = 1; i < this._environment.dashboardNumberOfPointsAccounts && nextDate < new Date(); i++) {
      dates.push(nextDate);
      nextDate = new Date(startTime + accountsPerPoint * 60000 * i);

      // round up or down by 1 minute, keeping seconds and milliseconds aligned at 0.
      if (nextDate.getSeconds() < 30) {
        nextDate.setSeconds(0);
      } else {
        nextDate.setSeconds(0);
        nextDate.setMinutes(nextDate.getMinutes() + 1);
      }

      nextDate.setMilliseconds(0);
    }

    // push the final date to make sure the latest account data is displayed (technically 2001 data points will
    // be returned
    dates.push(accountsFirstAndLast[1].time);

    // @ts-ignore TODO fix the typescript error
    const accountsListQuery: Query<Account.Accounts> = {
      filter: { time: { $in: dates }},
      sort: { time: SortOrder.ASCENDING }
    };

    // @ts-ignore
    const accountsListPromise = this._accountRepository.findManyByQuery(accountsListQuery) as Promise<any[]>;

    accountsListPromise.then(accountsList => {
      const endTimePerformance = new Date();
      this._logger.log(
        'info', `Processing request for accounts took ${endTimePerformance.getTime() - startTimePerformance.getTime()} millis.`);

      return accountsList;
    });

    return accountsListPromise;
  }

  async cards() {
    const startTimePerformance = new Date();

    const firstBalanceCurrentBalancePromise = this._accountRepository.findFirstAndLastAccount();
    const minAccountBalancePromise = this._accountRepository.findMinAccount();
    const maxAccountBalancePromise = this._accountRepository.findMaxAccount();

    const lastFilledOrderPromise = this._orderRepository.findLatestFilledOrder();
    const orderSuccessRatePromise = this._orderRepository.findOrderSuccessRate();
    const order30DayVolumePromise = this._orderRepository.findOrder30DayVolume();

    const promises = Promise.all([
      firstBalanceCurrentBalancePromise, minAccountBalancePromise, maxAccountBalancePromise,
      lastFilledOrderPromise, orderSuccessRatePromise, order30DayVolumePromise
    ]);

    return new Promise<any>((resolve, reject) => {
      return promises
        .then(([
            firstBalanceCurrentBalances, minAccountBalance, maxAccountBalance, lastFilledOrder,
            orderSuccessRate, order30DayVolume ]) => {

          const cards = [];

          cards.push({
            name: 'Starting Account',
            type: 'starting-account-value',
            data: firstBalanceCurrentBalances[0]
          });

          cards.push({
            name: 'Current Account',
            type: 'current-account-value',
            data: firstBalanceCurrentBalances[1]
          });

          cards.push({
            name: 'Min Account',
            type: 'min-account-value',
            data: minAccountBalance
          });

          cards.push({
            name: 'Max Account',
            type: 'max-account-value',
            data: maxAccountBalance
          });

          cards.push({
            name: 'Last Filled Order',
            type: 'last-filled-order',
            data: lastFilledOrder
          });

          cards.push({
            name: 'Order Success Rate',
            type: 'order-success-rate',
            data: orderSuccessRate
          });

          cards.push({
            name: 'Order 30 Day Volume',
            type: 'order-30day-volume',
            data: order30DayVolume
          });

          const endTimePerformance = new Date();
          this._logger.log(
            'info', `Processing request for cards took ${endTimePerformance.getTime() - startTimePerformance.getTime()} millis.`);

          resolve(cards);
        })
        .catch((error) => {
          this._logger.log('error', `Error retrieving cards: ${error}`);
          reject(error);
        });
    });
  }

  private startTime(startTime?: Date) {
    if (! startTime) {
      startTime = new Date();
      startTime.setMinutes(startTime.getMinutes() - this._environment.dashboardNumberOfPointsOrders);
    }

    return startTime;
  }
}


export { DashboardResolver };

