
import { inject, injectable } from 'inversify';

import { Accounts } from '../../../domain/account/entities/Account';
import { Query, SortOrder } from '../../../infrastructure/data-access/Query';
import { Logger } from '../../../infrastructure/logger/Logger';
import { TYPES as LOGGER_TYPES } from '../../../infrastructure/logger/module';


@injectable()
class PerformanceResolver {
  constructor(
    @inject(LOGGER_TYPES.Logger) private readonly _logger: Logger,
    ) {
  }

  async performance() {
    const startTime = new Date();

    const twelveHoursInMinutes = 720;
    const twelveHoursAgo = new Date();
    twelveHoursAgo.setMinutes(twelveHoursAgo.getMinutes() - twelveHoursInMinutes);

    const query: Query<Accounts> = {
      filter: { time: { $gte: twelveHoursAgo }},
      sort: { time: SortOrder.ASCENDING },
      limit: twelveHoursInMinutes
    };

    // TODO fix the ts-node compilation error
    // @ts-ignore
    const accounts = await this._accountRepository.findManyByQuery(query);



    const endTime = new Date();
    this._logger.log('info', `Processing request for accounts took ${endTime.getTime() - startTime.getTime()} millis.`);

    return {
      performance: accounts
    };
  }
}


export { PerformanceResolver };



/*
  initialAccountDollarValue = x
  numBitcoinsforX * exchangeRate = initialAccountDollarValue

  currentAccountDollarValue
  numBitcoinsforX * exchcangeRate = buyAndHoldAccountDollarValue
*/
