
import 'reflect-metadata';

import { ContainerModule, interfaces } from 'inversify';

import { DashboardResolver } from './resolvers/DashboardResolver';
import { TypeDefs } from './typedefs/TypeDefs';
import { TYPES } from './types';


const GraphQLHttpModule = new ContainerModule((bind: interfaces.Bind) => {
  bind<any>(TYPES.TypeDefs).toConstantValue(TypeDefs);
  bind<DashboardResolver>(TYPES.Resolver).to(DashboardResolver).inSingletonScope().whenTargetNamed('dashboard');
});


export { GraphQLHttpModule };
