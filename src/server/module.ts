
import 'reflect-metadata';

import * as express from 'express';

import { ApolloServer as ApolloServerExpress } from 'apollo-server-express';
import { PubSub } from 'graphql-subscriptions';
import { AsyncContainerModule, ContainerModule, interfaces } from 'inversify';
import { AccountModule } from '../domain/account/module';

import { OrderModule } from '../domain/order/module';
import { CliModule } from '../infrastructure/cli/module';
import { ModelFactoryModule } from '../infrastructure/data-access/factories/module';
import { DatabaseModule } from '../infrastructure/data-access/module';
import { LoggerModule } from '../infrastructure/logger/module';

import { ApolloServer } from './ApolloServer';
import { ExpressServer } from './ExpressServer';
import { GraphQLHttpModule } from './graphql/module';
import { pubSub } from './PubSub';


const TYPES = {
  ApolloServerFactory: Symbol.for('ApolloServerFactory'),
  ExpressServerFactory: Symbol.for('ExpressServerFactory'),
  GraphQLSubscriptionWebSocketServerFactory: Symbol.for('GraphQLSubscriptionWebSocketServerFactory'),
  PubSub: Symbol.for('PubSub')
};


const ServerModule = new ContainerModule(
  (bind: interfaces.Bind) => {

    bind<interfaces.Factory<ApolloServerExpress>>(TYPES.ApolloServerFactory)
      .toFactory<ApolloServerExpress>(ApolloServer.apolloServerFactory);

    bind<interfaces.Factory<express.Application>>(TYPES.ExpressServerFactory)
      .toFactory<express.Application>(ExpressServer.expressServerFactory);

    bind<PubSub>(TYPES.PubSub).toConstantValue(pubSub);
  }
);

const DEPENDENCIES: ContainerModule[] = [
  AccountModule,
  CliModule,
  LoggerModule,
  ModelFactoryModule,
  OrderModule,
  GraphQLHttpModule,
  ServerModule
];

const ASYNC_DEPENDENCIES: AsyncContainerModule[] = [
  DatabaseModule
];


export { ASYNC_DEPENDENCIES, DEPENDENCIES, ServerModule, TYPES };
