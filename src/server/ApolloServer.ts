
import * as express from 'express';

import { ApolloServer as ApolloServerExpress } from 'apollo-server-express';


namespace ApolloServer {
  const apolloServerMap: Map<number, ApolloServerExpress> = new Map<number, ApolloServerExpress>();

  export function apolloServerFactory() {
    return (app: express.Application, port: number, resolvers: any, typeDefs: any[]): ApolloServerExpress => {
      if (! apolloServerMap.get(port)) {
        const apolloServer = new ApolloServerExpress({
          typeDefs,
          resolvers,
          // playground: {
          //   endpoint: '/graphiql',
          //   settings: {
          //     'editor.theme': 'light'
          //   }
          // },
          subscriptions: {}
        });

        apolloServer.applyMiddleware({ app });

        apolloServerMap.set(port, apolloServer);
      }

      return apolloServerMap.get(port);
    };
  }

  export type ApolloServerFactory = (app: express.Application, port: number, resolvers: any, typeDefs: any[]) => ApolloServerExpress;
}


export { ApolloServer };
