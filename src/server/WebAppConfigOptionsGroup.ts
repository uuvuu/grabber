

import { Command } from 'commander';

import { ConfigOptionsGroup } from '../infrastructure/config/ConfigOptionsGroup';


class DEFAULTS {
  static readonly WEB_APP_PORT = 8080;
  static readonly WEB_SOCKET_PORT = 8081;
  // static readonly LIVE_UPDATE_INTERVAL_IN_MILLIS = 3000;
}

class WebAppConfigOptionsGroup extends ConfigOptionsGroup {
  static readonly DEFAULTS = DEFAULTS;

  constructor() {
    super();
  }

  addExtraParams(command: Command) {
    this.addExtraParam('webappPort', WebAppConfigOptionsGroup.DEFAULTS.WEB_APP_PORT);
    this.addExtraParam('websocketPort', WebAppConfigOptionsGroup.DEFAULTS.WEB_SOCKET_PORT);
    // this.addExtraParam('liveUpdateIntervalInMillis', WebAppConfigOptionsGroup.DEFAULTS.LIVE_UPDATE_INTERVAL_IN_MILLIS);
  }
}

interface WebAppConfig {
  webappPort: number;
  websocketPort: number;
  liveUpdateIntervalInMillis: number;
}


export { WebAppConfig, WebAppConfigOptionsGroup };
