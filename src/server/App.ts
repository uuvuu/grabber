
import 'reflect-metadata';

import * as express from 'express';
import * as GraphQLJSON from 'graphql-type-json';

import { ApolloServer as ApolloServerExpress } from 'apollo-server-express';
import { GraphQLDateTime } from 'graphql-iso-date';
import { PubSub } from 'graphql-subscriptions';
import { createServer, Server } from 'http';
import { inject, injectable, named } from 'inversify';

import { CandleConfig } from '../domain/candle/CandleConfigOptionsGroup';
import { ORDER_TYPES } from '../domain/order/module';
import { OrderRepository } from '../domain/order/repositories/OrderRepository';
import { App as BaseApp } from '../infrastructure/App';
import { TYPES as CONFIG_TYPES } from '../infrastructure/config/module';
import { TYPES as LOGGER_TYPES } from '../infrastructure/logger/module';
import { Logger } from '../infrastructure/logger/Logger';
import { sleep } from '../utils/sleep';

import { DashboardResolver } from './graphql/resolvers/DashboardResolver';
import { GraphQLMongoObjectId } from './graphql/resolvers/GraphQLMongoObjectId';
import { TYPES as GRAPHQL_TYPES } from './graphql/types';

import { ApolloServer } from './ApolloServer';
import { Config } from './Config';
import { ExpressServer } from './ExpressServer';
import { TYPES } from './module';
import { WebAppConfig } from './WebAppConfigOptionsGroup';


const ACCOUNTS_UPDATED = 'accounts_updated';
const STATUS_CARDS_UPDATED = 'status_cards_updated';
const ORDER_CREATED = 'order_created';


@injectable()
class App extends BaseApp<Config> {
  private readonly _expressServer: express.Application;
  private readonly _apolloServer: ApolloServerExpress;

  private readonly _expressHttpServer: Server;
  private readonly _webSocketServer: Server;

  private _accountsUpdatedUpdatesInterval;
  private _statusCardsUpdatedUpdatesInterval;
  private _orderCreatedUpdatesInterval;

  public constructor(
    @inject(CONFIG_TYPES.Config) readonly _config: Config & WebAppConfig & CandleConfig,
    @inject(LOGGER_TYPES.Logger) readonly _logger: Logger,
    @inject(TYPES.ExpressServerFactory) readonly expressServerFactory: ExpressServer.ExpressServerFactory,
    @inject(TYPES.ApolloServerFactory) readonly apolloServerFactory: ApolloServer.ApolloServerFactory,
    @inject(TYPES.PubSub) readonly pubSub: PubSub,
    @inject(GRAPHQL_TYPES.Resolver) @named('dashboard') readonly resolver: DashboardResolver,
    @inject(GRAPHQL_TYPES.TypeDefs) readonly typeDefs: any,
    @inject(ORDER_TYPES.OrderRepository) private readonly _orderRepository: OrderRepository) {

    super(_config, _logger);

    const resolvers = {
      Query: {
        dashboard: async () => { return await this.resolver.dashboard(); },

        orders: async () => { return await this.resolver.orders(); },
        accounts: async () => { await this.resolver.accounts(); },
        cards: async () => { return await this.resolver.cards(); }
      },

      Subscription: {
        accountsUpdated: {
          // resolve: (payload, args, context, info) => {
          //   // manipulate and return the new value
          //   return payload.somethingChanged;
          // },

          subscribe: async () => {
            return await pubSub.asyncIterator(ACCOUNTS_UPDATED);
          },
        },
        statusCardsUpdated: {
          // resolve: (payload, args, context, info) => {
          //   // manipulate and return the new value
          //   return payload.somethingChanged;
          // },

          subscribe: async () => {
            // TODO - figure out if this is really a memory leak or not...this code creates
            // the following warning message:
            //
            // (node:23626) MaxListenersExceededWarning: Possible EventEmitter memory leak detected.
            // 11 status_cards_updated listeners added. Use emitter.setMaxListeners() to increase limit
            //
            return await pubSub.asyncIterator(STATUS_CARDS_UPDATED);
          },
        },
        orderCreated: {
          // resolve: (payload, args, context, info) => {
          //   // manipulate and return the new value
          //   return payload.somethingChanged;
          // },

          // subscribe: async () => { return await pubSub.asyncIterator([ ORDER_CREATED, ORDER_UPDATED ORDER_DELETED ]); },
          subscribe: async () => {
            return await pubSub.asyncIterator(ORDER_CREATED);
          },
        }
      },

      DateTime: GraphQLDateTime,
      MongoObjectId: GraphQLMongoObjectId,
      JSON: GraphQLJSON
    };

    this._expressServer = expressServerFactory(this._config.webappPort);
    this._apolloServer = apolloServerFactory(this._expressServer, this._config.webappPort, resolvers, [ typeDefs ]);

    this._webSocketServer = createServer((request, response) => {
      response.writeHead(404);
      response.end();
    });

    this._apolloServer.installSubscriptionHandlers(this._webSocketServer);

    this._expressHttpServer = this._expressServer.listen(this._config.webappPort, () => {
      this._logger.log('info', `Http server listening on port *.${this._config.webappPort}`);
    });

    this._webSocketServer.listen(this._config.websocketPort, () => {
      this._logger.log('info', `WebSocket server listening on port *.${this._config.websocketPort}`);
    });
  }

  protected async _run() {
    await this.waitForNextMinuteMiddle();

    await this.accountsUpdatedUpdates();
    await this.statusCardsUpdatedUpdates();
    await this.orderCreatedUpdates();

    // loop forever
    while (true) {
      await sleep(2000);
    }
  }

  private async waitForNextMinuteMiddle() {
    const date = new Date();

    if (date.getSeconds() > 25) {
      date.setSeconds(date.getSeconds() + 90);
    }

    while (new Date() < date) {
      await sleep(500);
    }
  }

  private accountsUpdatedUpdates() {
    this._accountsUpdatedUpdatesInterval = setInterval(async () => {
      const accounts = await this.resolver.accounts();

      await this.pubSub.publish(ACCOUNTS_UPDATED, { accountsUpdated: accounts });
    }, this._config.durationInMillis);
  }

  private statusCardsUpdatedUpdates() {
    this._statusCardsUpdatedUpdatesInterval = setInterval(async () => {
      const statusCards = await this.resolver.cards();

      await this.pubSub.publish(STATUS_CARDS_UPDATED, { statusCardsUpdated: statusCards });
    }, this._config.durationInMillis);
  }

  private orderCreatedUpdates() {
    this._orderCreatedUpdatesInterval = setInterval(async () => {
      const order = await this._orderRepository.findLatestItem();

      await this.pubSub.publish(ORDER_CREATED, { orderCreated: order });
    }, this._config.durationInMillis);
  }

  protected async terminateHook() {
    this._expressHttpServer.close();
    this._webSocketServer.close();

    clearInterval(this._accountsUpdatedUpdatesInterval);
    clearInterval(this._statusCardsUpdatedUpdatesInterval);
    clearInterval(this._orderCreatedUpdatesInterval);
  }
}


export { App };
