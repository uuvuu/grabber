
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as express from 'express';
import * as helmet from 'helmet';


namespace ExpressServer {
  const expressServerMap: Map<number, express.Application> = new Map<number, express.Application>();

  export function expressServerFactory() {
    return (port: number) => {
      if (! expressServerMap.get(port)) {
        const app = express();

        app.use(bodyParser.urlencoded({
          extended: true
        }));

        app.use(bodyParser.json());
        app.use(helmet());

        app.use((req, res, next) => {
          res.header('Access-Control-Allow-Origin', '*');
          res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
          next();
        });

        app.use('*', cors({ credentials: true, origin: true }));

        expressServerMap.set(port, app);
      }

      return expressServerMap.get(port);
    };
  }

  export type ExpressServerFactory = (port: number) => express.Application;
}


export { ExpressServer };
