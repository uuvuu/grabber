
import { Command } from 'commander';
import { inject, injectable } from 'inversify';
import { CandleConfigOptionsGroup } from '../domain/candle/CandleConfigOptionsGroup';

import { TYPES } from '../infrastructure/cli/module';
import { Config as BaseConfig } from '../infrastructure/config/Config';
import { ProductConfigOptionsGroup } from '../infrastructure/config/ProductConfigOptionsGroup';
import { MongoConfigOptionsGroup } from '../infrastructure/data-access/MongoConfigOptionsGroup';
import { LoggerConfigOptionsGroup } from '../infrastructure/logger/LoggerConfigOptionsGroup';

import { WebAppConfigOptionsGroup } from './WebAppConfigOptionsGroup';


@injectable()
class Config extends BaseConfig {
  private static readonly VERSION = `0.0.1`;
  private static readonly DESCRIPTION = `A webserver for responding to GraphQL and SocketIO requests.`;

  public constructor(@inject(TYPES.Command) _program: Command, @inject(TYPES.ARGV) _argv: string[]) {
    super(_program, _argv);

    this.configOptionsGroups.push(new CandleConfigOptionsGroup());
    this.configOptionsGroups.push(new LoggerConfigOptionsGroup('WebApp', 'webapp'));
    this.configOptionsGroups.push(new MongoConfigOptionsGroup());
    this.configOptionsGroups.push(new ProductConfigOptionsGroup());

    this.configOptionsGroups.push(new WebAppConfigOptionsGroup());
  }

  protected get version(): string {
    return Config.VERSION;
  }

  protected get description(): string {
    return Config.DESCRIPTION;
  }
}

export { Config };
