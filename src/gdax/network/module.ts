
import {
  AuthenticatedClient as GdaxAuthenticatedClient, PublicClient as GdaxPublicClient,
  WebsocketClient as GdaxWebsocketClient } from 'gdax';
import { ContainerModule, interfaces } from 'inversify';

import { AuthenticatedClient } from './api/AuthenticatedClient';
import { PublicClient } from './api/PublicClient';
import { GdaxTickerChannel } from './websocket/channels/GdaxTickerChannel';
import { WebsocketClient } from './websocket/WebsocketClient';
import { WebsocketFeed } from './websocket/WebsocketFeed';
import { TYPES } from './types';


const GdaxNetworkModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<interfaces.Factory<GdaxAuthenticatedClient>>(TYPES.AuthenticatedClientFactory)
      .toFactory<GdaxAuthenticatedClient>(AuthenticatedClient.authenticatedClientFactory);

    bind<interfaces.Factory<GdaxPublicClient>>(TYPES.PublicClientFactory)
      .toFactory<GdaxPublicClient>(PublicClient.publicClientFactory);

    bind<interfaces.Factory<GdaxWebsocketClient>>(TYPES.WebsocketClientFactory)
      .toFactory<GdaxWebsocketClient>(WebsocketClient.websocketClientFactory);

    bind<WebsocketFeed>(TYPES.WebsocketFeed).to(WebsocketFeed).inSingletonScope();

    bind<GdaxTickerChannel>(TYPES.GdaxTickerChannel).to(GdaxTickerChannel).inSingletonScope();
  }
);


export { GdaxNetworkModule, TYPES };
