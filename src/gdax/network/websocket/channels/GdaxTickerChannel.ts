
import { inject, injectable } from 'inversify';

import { TYPES as TICKER_TYPES } from '../../../../domain/ticker/module';
import { TickerRepository } from '../../../../domain/ticker/repositories/TickerRepository';

import { Logger } from '../../../../infrastructure/logger/Logger';
import { TYPES as LOGGER_TYPES } from '../../../../infrastructure/logger/module';

import { GdaxTickerToTicker } from '../../entities/GdaxTickerToTicker';
import { TYPES } from '../../types';

import { WebsocketFeed } from '../WebsocketFeed';

import { GdaxChannel } from './GdaxChannel';


@injectable()
class GdaxTickerChannel extends GdaxChannel {
  constructor(
    @inject(LOGGER_TYPES.Logger) readonly _logger: Logger,
    @inject(TYPES.WebsocketFeed) readonly _websocketFeed: WebsocketFeed,
    @inject(TICKER_TYPES.TickerRepository) private readonly _tickerRepository: TickerRepository) {

    super('ticker', _logger, _websocketFeed);
  }

  protected handleMessage(message: any) {
    const ticker = GdaxTickerToTicker.newTickerDocumentFromGdaxTicker(message, 'cbpro-api');

    this._logger.log('debug', `Ticker received: ${JSON.stringify(ticker)}`);

    this._tickerRepository.create(ticker).catch(error => {
      this._logger.log('error', `Error saving ticker: ${JSON.stringify(ticker)}. Error was: ${error.message}`);
    });
  }
}


export { GdaxTickerChannel };
