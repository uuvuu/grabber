

import { injectable, unmanaged } from 'inversify';

import { Logger } from '../../../../infrastructure/logger/Logger';


@injectable()
abstract class GdaxChannel {
  protected constructor(
    @unmanaged() readonly channelName,
    @unmanaged() protected readonly _logger: Logger,
    @unmanaged() protected readonly _websocketFeed) {

    // pass handler method in via => to preserve 'this'
    this._websocketFeed.on(channelName, (message: any) => { this.handleMessage(message); });
  }

  protected abstract handleMessage(message: any);
}


export { GdaxChannel };
