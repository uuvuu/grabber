
import { ContainerModule, injectable } from 'inversify';

import { TYPES as CONFIG_TYPES } from '../../../../infrastructure/config/module';
import { referenceContainer } from '../../../../infrastructure/inversify';
import { LoggerConfigOptionsGroup } from '../../../../infrastructure/logger/LoggerConfigOptionsGroup';
import { ArgvConfig } from '../../../../testing/infrastructure/config/ArgvConfig';

// import { GdaxNetworkModule, TYPES } from '../../module';

// import { WebsocketFeed } from '../WebsocketFeed';


const DEPENDENCIES: ContainerModule[] = [
  // GdaxNetworkModule
];


describe.skip('WebsocketFeed', () => {
  @injectable()
  class Config extends ArgvConfig {
    public constructor() {
      super();

      this.configOptionsGroups.push(new LoggerConfigOptionsGroup('TestWebsocketFeed'));
    }
  }

  let originalArgv;

  // let websocketFeed: WebsocketFeed;

  beforeAll(() => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([]);

    referenceContainer.bind<Config>(CONFIG_TYPES.Config).to(Config).inSingletonScope();

    referenceContainer.load(...DEPENDENCIES);

    // websocketFeed = referenceContainer.get<WebsocketFeed>(TYPES.WebsocketFeed);
  });

  afterAll(() => {
    process.argv = originalArgv;
  });

  beforeEach(() => {
  });

  afterEach(() => {
    referenceContainer.unbindAll();
  });

  test('write me', () => {

  });
});
