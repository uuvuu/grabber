
import { EventEmitter } from 'events';
import { WebsocketClient as GdaxWebsocketClient, WebsocketMessage } from 'gdax';
import { inject, injectable } from 'inversify';

import { Component } from '../../../infrastructure/component/Component';
import { TYPES as CONFIG_TYPES } from '../../../infrastructure/config/module';
import { ProductConfig } from '../../../infrastructure/config/ProductConfigOptionsGroup';
import { Logger } from '../../../infrastructure/logger/Logger';
import { TYPES as LOGGER_TYPES } from '../../../infrastructure/logger/module';

import { sleep } from '../../../utils/sleep';

import { GdaxNetworkConfig } from '../config/GdaxNetworkConfigOptionsGroup';
import { TYPES } from '../types';

import { WebsocketClient } from './WebsocketClient';


//
// Using an EventEmitter delegate instead of extending it so tests
// can be written for this class
//
// see bug: https://github.com/inversify/InversifyJS/issues/984
//
// also see line 52 of src/infrastructure/inversify.ts:
//
//     decorate(injectable(), EventEmitter);
//

@injectable()
class WebsocketFeed implements Component {
  private _websocketClient: GdaxWebsocketClient;
  private _lastHeartbeat: WebsocketMessage.Heartbeat;
  private _eventEmitter: EventEmitter;

  private _connected = false;
  private _disconnecting = false;

  constructor(
    @inject(CONFIG_TYPES.Config) private readonly _config: GdaxNetworkConfig & ProductConfig,
    @inject(LOGGER_TYPES.Logger) private readonly _logger: Logger,
    @inject(TYPES.WebsocketClientFactory) private readonly _websocketClientFactory: WebsocketClient.WebsocketClientFactory) {

    this._eventEmitter = new EventEmitter();

    this._eventEmitter.on(
      'heartbeat', (heartbeat: WebsocketMessage.Heartbeat) => this.handleHeartbeat(heartbeat));
    this._eventEmitter.on('error', (error: any) => this.handleError(error));
  }

  on(event: string | symbol, listener: (...args: any[]) => void): this {
    this._eventEmitter.on(event, listener);

    return this;
  }

  off(event: string | symbol, listener: (...args: any[]) => void): this {
    this._eventEmitter.off(event, listener);

    return this;
  }

  async load() {
    await this.initializeFeed();
  }

  async unload() {
    await this.disconnect();
  }

  async initializeFeed() {
    this._websocketClient = this._websocketClientFactory([ this._config.product ], this._config.wsUrl, this._config.channels);

    this._websocketClient.on('open', () => {
      this._logger.log('info', `Connected to web socket: ${this._config.wsUrl}`);
      this._connected = true;
    });

    this._websocketClient.on('message', data => {
      this.handleMessage(data);
    });

    this._websocketClient.on('error', error => {
      this._logger.log('error', `data = ${JSON.stringify(error, undefined, 2)}`);
    });

    this._websocketClient.on('close', () => {
      this._connected = false;

      if (! this._disconnecting) {
        this._logger.log('info', `Disconnected from web socket prematurely. Reconnecting...`);
        this._websocketClient.connect();
      } else {
        this._logger.log('info', `Disconnected from web socket.`);
      }
    });
  }

  get lastHeartbeat() {
    return JSON.parse(JSON.stringify(this._lastHeartbeat));
  }

  protected handleMessage(data: any) {
    const messageType = data.type;

    delete data.type;

    if (this._config.channels.includes(messageType)) {
      this._eventEmitter.emit(messageType, data);
    }
  }

  protected handleHeartbeat(heartbeat: WebsocketMessage.Heartbeat) {
    this._lastHeartbeat = heartbeat;

    this._logger.log('debug', `heartbeat: ${JSON.stringify(this._lastHeartbeat)}`);
  }

  protected handleError(error: any) {
    const errorMessage = error.message ? error.message : error;

    this._logger.log('error', errorMessage);
  }

  async disconnect() {
    if (this._connected) {
      this._disconnecting = true;
      await this._websocketClient.disconnect();

      // set false here too in case disconnect gets called before the 'close' event is
      // received
      this._connected = false;

      // wait for close event
      await sleep(2000);
    }
  }
}


export { WebsocketFeed };
