
import { WebsocketClient as GdaxWebsocketClient } from 'gdax';


namespace WebsocketClient {
  let websocketClient;

  export function websocketClientFactory() {
    return (products: string[], wsUrl: string, channels?: string[]) => {
      if (!websocketClient) {
        websocketClient = new GdaxWebsocketClient(products, wsUrl, undefined, {channels: channels});
      }

      return websocketClient;
    };
  }

  export type WebsocketClientFactory = (products: string[], wsUrl: string, channels?: string[]) => GdaxWebsocketClient;
}


export { WebsocketClient };

