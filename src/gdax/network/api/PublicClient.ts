
import { PublicClient as GdaxPublicClient } from 'gdax';


namespace PublicClient {
  let publicClient;

  export function publicClientFactory() {
    return (apiUrl: string) => {
      if (! publicClient) {
        publicClient = new GdaxPublicClient(apiUrl);
      }

      return publicClient;
    };
  }

  export type PublicClientFactory = (apiUrl: string) => GdaxPublicClient;
}

export { PublicClient };
