
import { Command } from 'commander';

import { CandleReaderConfig, CandleReaderConfigOptionsGroup } from '../../../../domain/candle/CandleReaderConfigOptionsGroup';
import { ConfigError, ConfigErrorFactory } from '../../../../infrastructure/config/Config';


class DEFAULTS extends CandleReaderConfigOptionsGroup.DEFAULTS {
  static readonly ALLOWED_DURATIONS_IN_MILLIS = [ 60000, 300000, 900000, 3600000, 21600000, 86400000 ];

  // max set by GDAX: https://docs.gdax.com/#get-historic-rates
  static readonly GDAX_MAX_DATA_ITEMS = 300;
}

class GdaxCandleNetworkConfigOptionsGroup extends CandleReaderConfigOptionsGroup {
  static readonly DEFAULTS = DEFAULTS;

  constructor() {
    super();

    // @ts-ignore
    this.options.durationInMillis.description += ` One of: ${GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.ALLOWED_DURATIONS_IN_MILLIS}`;
  }

  addExtraParams(command: Command) {
    this.addExtraParam('maxDataItems', GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.GDAX_MAX_DATA_ITEMS);
  }

  validateOptions(command: Command, configErrorFactory: ConfigErrorFactory): ConfigError {
    const configError = super.validateOptions(command, configErrorFactory);

    if (configError) {
      return configError;
    }

    if (! GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.ALLOWED_DURATIONS_IN_MILLIS.includes(command.durationInMillis)) {
      return configErrorFactory.newError(`duration invalid. duration must be one of: ${GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.ALLOWED_DURATIONS_IN_MILLIS}`);
    }

    return undefined;
  }
}

interface GdaxCandleNetworkConfig extends CandleReaderConfig {
  maxDataItems: number;
}


export { GdaxCandleNetworkConfig, GdaxCandleNetworkConfigOptionsGroup };

