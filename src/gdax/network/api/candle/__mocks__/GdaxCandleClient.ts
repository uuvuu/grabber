
import { inject, injectable } from 'inversify';

import { TYPES as CONFIG_TYPES } from '../../../../../infrastructure/config/module';
import { ProductConfig } from '../../../../../infrastructure/config/ProductConfigOptionsGroup';

import { GdaxNetworkConfig } from '../../../config/GdaxNetworkConfigOptionsGroup';
import { TYPES } from '../../../types';

import { PublicClient } from '../../PublicClient';

import { GdaxCandleClient as BaseGdaxCandleClient } from '../GdaxCandleClient';


@injectable()
class GdaxCandleClient extends BaseGdaxCandleClient {
  private _testData: any;

  constructor(
    @inject(CONFIG_TYPES.Config) readonly config: GdaxNetworkConfig & ProductConfig,
    @inject(TYPES.PublicClientFactory) readonly publicClientFactory: PublicClient.PublicClientFactory) {

    super(config, publicClientFactory);
  }

  public async loadTestData(fileName: string) {
    this._testData = await import(fileName);
  }

  protected get _request() {
    return (request: any) => {
      for (const testDatum of this._testData) {
        if (testDatum.request.includes(`start=${request.start}`)
            && testDatum.request.includes(`end=${request.end}`)) {
          return Promise.resolve(testDatum.response);
        }
      }

      return Promise.resolve([]);
    };
  }
}


export { GdaxCandleClient };
