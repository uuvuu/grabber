
import { inject, injectable } from 'inversify';

import { TYPES as CONFIG_TYPES } from '../../../../infrastructure/config/module';
import { ProductConfig } from '../../../../infrastructure/config/ProductConfigOptionsGroup';
import { RequestClient } from '../../../../infrastructure/network/api/RequestClient';

import { GdaxNetworkConfig } from '../../config/GdaxNetworkConfigOptionsGroup';
import { TYPES } from '../../types';

import { PublicClient } from '../PublicClient';

import { GdaxCandleRequest, GdaxCandleResponse } from './GdaxCandleRequest';


@injectable()
class GdaxCandleClient extends RequestClient<GdaxCandleRequest, GdaxCandleResponse> {
  constructor(
    @inject(CONFIG_TYPES.Config) private readonly _config: GdaxNetworkConfig & ProductConfig,
    @inject(TYPES.PublicClientFactory) readonly publicClientFactory: PublicClient.PublicClientFactory) {

    super(publicClientFactory(_config.apiUrl));
  }

  protected get _request() {
    // careful! don't simply return:
    //
    //   this._client.getProductHistoricRates
    //
    // as the function to call because the 'this' variable will get mixed up. that's
    // why we wrap it in a function here using => to preserve the 'this' variable

    return (args: any) => this._client.getProductHistoricRates(this._config.product, args);
  }
}


export { GdaxCandleClient };
