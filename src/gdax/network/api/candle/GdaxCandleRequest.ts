
import { inject, injectable } from 'inversify';

import { TYPES as CONFIG_TYPES } from '../../../../infrastructure/config/module';
import { Request, Response } from '../../../../infrastructure/network/api/Request';
import { RequestFactory } from '../../../../infrastructure/network/api/RequestFactory';
import { DateUtils } from '../../../../utils/DateUtils';

import { GdaxCandleNetworkConfig } from './GdaxCandleNetworkConfigOptionsGroup';


class GdaxCandleRequest extends Request {
  start: string;
  end: string;
  granularity: number;
}

class GdaxCandleResponse extends Response<GdaxCandleRequest> {
}


@injectable()
class GdaxCandleRequestFactory extends RequestFactory<GdaxCandleRequest> {
  private _requestNumber = 1;

  private _granularityInSecs: number;
  private _maxDataItems: number;

  private _originalDates: DateUtils.DatePair;
  private _nextDates: DateUtils.DatePair;

  constructor(@inject(CONFIG_TYPES.Config) protected readonly _config: GdaxCandleNetworkConfig) {
    super();
  }

  get dateRange(): DateUtils.DatePair {
    return { startDate: new Date(this._originalDates.startDate), endDate: new Date(this._originalDates.endDate) };
  }

  set dateRange(dates: DateUtils.DatePair) {
    this._originalDates = {
      startDate: new Date(dates.startDate),
      endDate: new Date(dates.endDate)
    };
  }

  // TODO - consider reapproaching this...it's handy, but because it relies on config data
  // rather than data being passed in, it can't be reused so well. consider the case of
  // GdaxGetCandles usage of this vs gdax-ws-feed usage
  newRequest(): GdaxCandleRequest {
    if (! this._originalDates) {
      throw 'Date range must be set on GdaxCandleRequestFactory before newRequest() can be called';
    }

    if (this.isFirstRequest()) {
      this._granularityInSecs = this._config.durationInMillis / 1000;
      this._maxDataItems = this._config.maxDataItems;

      this._nextDates =
        DateUtils.newDatePairWithDiff(
          this._originalDates.startDate,
          this._config.maxDataItems * this._config.durationInMillis, DateUtils.TimeAlignment.MINUTES);
    } else {
      this._nextDates =
        DateUtils.incrementDatePair(this._nextDates, this._maxDataItems * this._config.durationInMillis);
    }

    // we've reached the end of the date range
    if (this._nextDates.startDate >= this._originalDates.endDate) {
      return undefined;
    }

    if (this._nextDates.endDate > this._originalDates.endDate) {
      this._nextDates.endDate = new Date(this._originalDates.endDate);
    }

    const request = new GdaxCandleRequest();

    request.requestNumber = this._requestNumber;
    request.start = this._nextDates.startDate.toISOString();
    request.end = this._nextDates.endDate.toISOString();
    request.granularity = this._granularityInSecs;

    this._requestNumber++;

    return request;
  }

  reset() {
    this._originalDates = undefined;
    this._nextDates = undefined;
  }

  private isFirstRequest() {
    return typeof this._nextDates === 'undefined';
  }
}


export { GdaxCandleRequest, GdaxCandleRequestFactory, GdaxCandleResponse };
