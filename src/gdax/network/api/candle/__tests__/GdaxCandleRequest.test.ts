
import * as lolex from 'lolex';

import { ContainerModule, injectable } from 'inversify';

import { Config } from '../../../../../infrastructure/config/Config';
import { TYPES as CONFIG_TYPES } from '../../../../../infrastructure/config/module';
import { referenceContainer } from '../../../../../infrastructure/inversify';
import { TYPES as NETWORK_TYPES } from '../../../../../infrastructure/network/api/module';
import { ArgvConfig } from '../../../../../testing/infrastructure/config/ArgvConfig';

import { GdaxCandleNetworkConfigOptionsGroup } from '../GdaxCandleNetworkConfigOptionsGroup';
import { GdaxCandleRequestFactory } from '../GdaxCandleRequest';
import { GdaxCandleNetworkModule } from '../module';


const DEPENDENCIES: ContainerModule[] = [
  GdaxCandleNetworkModule
];

describe('GetCandlesRequest', () => {
  @injectable()
  class TestConfig extends ArgvConfig {
    public constructor() {
      super();

      this.configOptionsGroups.push(new GdaxCandleNetworkConfigOptionsGroup());
    }
  }

  let clock;
  let originalArgv;

  beforeAll(() => {
    clock = lolex.install({ shouldAdvanceTime: true });
  });

  afterAll(() => {
    clock.uninstall();
  });

  beforeEach(() => {
    originalArgv = process.argv;

    referenceContainer.bind<Config>(CONFIG_TYPES.Config).to(TestConfig).inSingletonScope();

    referenceContainer.load(...DEPENDENCIES);
  });

  afterEach(() => {
    referenceContainer.unbindAll();

    process.argv = originalArgv;
  });

  test('numberOfCandles: 1', () => {
    process.argv = ArgvConfig.argvHelper(['-n 1']);

    const expectedStartDate = GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.NOW();
    expectedStartDate.setMinutes(expectedStartDate.getMinutes() - 1);

    const expectedEndDate = GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.NOW();

    const candlesRequestFactory =
      referenceContainer.get<GdaxCandleRequestFactory>(NETWORK_TYPES.RequestFactory);

    candlesRequestFactory.dateRange = { startDate: expectedStartDate, endDate: expectedEndDate };
    const request = candlesRequestFactory.newRequest();

    expect(request).toBeDefined();
    expect(request.requestNumber).toEqual(1);
    expect(request.start).toEqual(expectedStartDate.toISOString());
    expect(request.end).toEqual(expectedEndDate.toISOString());
    expect(request.granularity).toEqual(GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.DURATION_IN_MILLIS / 1000);
  });

  test('final newRequest() call returns undefined', () => {
    process.argv = ArgvConfig.argvHelper(['-n 1']);

    const expectedStartDate = GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.NOW();
    expectedStartDate.setMinutes(expectedStartDate.getMinutes() - 1);

    const expectedEndDate = GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.NOW();

    const candlesRequestFactory =
      referenceContainer.get<GdaxCandleRequestFactory>(NETWORK_TYPES.RequestFactory);

    candlesRequestFactory.dateRange = { startDate: expectedStartDate, endDate: expectedEndDate };

    let request = candlesRequestFactory.newRequest();
    expect(request).toBeDefined();

    request = candlesRequestFactory.newRequest();

    expect(request).toBeUndefined();
  });

  test('newRequest() increments requestCount and dates correctly', () => {
    const numberOfCandles = GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.GDAX_MAX_DATA_ITEMS + 1;

    process.argv = ArgvConfig.argvHelper([`-n ${numberOfCandles}`]);

    const expectedStartDate = GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.NOW();
    expectedStartDate.setMinutes(
      expectedStartDate.getMinutes() - numberOfCandles);

    const expectedEndDate = new Date(expectedStartDate);
    expectedEndDate.setMinutes(expectedEndDate.getMinutes() + GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.GDAX_MAX_DATA_ITEMS);

    const candlesRequestFactory =
      referenceContainer.get<GdaxCandleRequestFactory>(NETWORK_TYPES.RequestFactory);

    candlesRequestFactory.dateRange = { startDate: expectedStartDate, endDate: GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.NOW() };

    let request = candlesRequestFactory.newRequest();

    expect(request).toBeDefined();
    expect(request.requestNumber).toEqual(1);
    expect(request.granularity).toEqual(GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.DURATION_IN_MILLIS / 1000);

    expect(request.start).toEqual(expectedStartDate.toISOString());
    expect(request.end).toEqual(expectedEndDate.toISOString());

    const requestStartDate = new Date(request.start);
    const requestEndDate = new Date(request.end);

    expect(requestEndDate.getTime() - requestStartDate.getTime())
      .toEqual(GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.GDAX_MAX_DATA_ITEMS * 60 * 1000);

    request = candlesRequestFactory.newRequest();

    expectedStartDate.setMinutes(
      expectedStartDate.getMinutes() + GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.GDAX_MAX_DATA_ITEMS);
    expectedEndDate.setMinutes(expectedEndDate.getMinutes() + 1);

    expect(request).toBeDefined();
    expect(request.requestNumber).toEqual(2);
    expect(request.start).toEqual(expectedStartDate.toISOString());
    expect(request.end).toEqual(expectedEndDate.toISOString());
    expect(request.granularity).toEqual(GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.DURATION_IN_MILLIS / 1000);
  });

  test('newRequest() increments requestCount and dates correctly for 3 requests', () => {
    const numberOfCandles = GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.GDAX_MAX_DATA_ITEMS * 2 + 1;

    process.argv = ArgvConfig.argvHelper([`-n ${numberOfCandles}`]);

    const expectedStartDate = GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.NOW();
    expectedStartDate.setMinutes(
      expectedStartDate.getMinutes() - numberOfCandles);

    const expectedEndDate = new Date(expectedStartDate);
    expectedEndDate.setMinutes(expectedEndDate.getMinutes() + GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.GDAX_MAX_DATA_ITEMS);

    const candlesRequestFactory =
      referenceContainer.get<GdaxCandleRequestFactory>(NETWORK_TYPES.RequestFactory);

    candlesRequestFactory.dateRange = { startDate: expectedStartDate, endDate: GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.NOW() };

    let request = candlesRequestFactory.newRequest();

    expect(request).toBeDefined();
    expect(request.requestNumber).toEqual(1);
    expect(request.granularity).toEqual(GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.DURATION_IN_MILLIS / 1000);

    expect(request.start).toEqual(expectedStartDate.toISOString());
    expect(request.end).toEqual(expectedEndDate.toISOString());

    let requestStartDate = new Date(request.start);
    let requestEndDate = new Date(request.end);

    expect(requestEndDate.getTime() - requestStartDate.getTime())
      .toEqual(GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.GDAX_MAX_DATA_ITEMS * 60 * 1000);

    request = candlesRequestFactory.newRequest();

    expectedStartDate.setMinutes(
      expectedStartDate.getMinutes() + GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.GDAX_MAX_DATA_ITEMS);
    expectedEndDate.setMinutes(expectedEndDate.getMinutes() + GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.GDAX_MAX_DATA_ITEMS);

    expect(request).toBeDefined();
    expect(request.requestNumber).toEqual(2);
    expect(request.granularity).toEqual(GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.DURATION_IN_MILLIS / 1000);

    expect(request.start).toEqual(expectedStartDate.toISOString());
    expect(request.end).toEqual(expectedEndDate.toISOString());

    requestStartDate = new Date(request.start);
    requestEndDate = new Date(request.end);

    expect(requestEndDate.getTime() - requestStartDate.getTime())
      .toEqual(GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.GDAX_MAX_DATA_ITEMS * 60 * 1000);

    request = candlesRequestFactory.newRequest();

    // only add 1 minute to the end date because we've hit the end of the dates and it is capped
    // by this
    expectedStartDate.setMinutes(
      expectedStartDate.getMinutes() + GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.GDAX_MAX_DATA_ITEMS);
    expectedEndDate.setMinutes(expectedEndDate.getMinutes() + 1);

    expect(request).toBeDefined();
    expect(request.requestNumber).toEqual(3);
    expect(request.granularity).toEqual(GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.DURATION_IN_MILLIS / 1000);

    expect(request.start).toEqual(expectedStartDate.toISOString());
    expect(request.end).toEqual(expectedEndDate.toISOString());

    request = candlesRequestFactory.newRequest();

    // we've hit the last request, so now calls to newRequest() should return undefined
    expect(request).toBeUndefined();
  });

  test('numberOfCandles: 1, using startDate', () => {
    const startDate = GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.NOW();
    startDate.setMinutes(startDate.getMinutes() - 1);

    process.argv = ArgvConfig.argvHelper([`-s ${startDate.toISOString()}`]);

    const expectedStartDate = GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.NOW();
    expectedStartDate.setMinutes(expectedStartDate.getMinutes() - 1);

    const expectedEndDate = GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.NOW();

    const candlesRequestFactory =
      referenceContainer.get<GdaxCandleRequestFactory>(NETWORK_TYPES.RequestFactory);

    candlesRequestFactory.dateRange = { startDate: expectedStartDate, endDate: GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.NOW() };

    const request = candlesRequestFactory.newRequest();

    expect(request).toBeDefined();
    expect(request.requestNumber).toEqual(1);
    expect(request.start).toEqual(expectedStartDate.toISOString());
    expect(request.end).toEqual(expectedEndDate.toISOString());
    expect(request.granularity).toEqual(GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.DURATION_IN_MILLIS / 1000);
  });

  test('startDate and endDate in past produces expected requests', () => {
    const startMinutesBefore = Math.ceil(GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.GDAX_MAX_DATA_ITEMS * 3.7);
    const endMinutesBefore = Math.ceil(GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.GDAX_MAX_DATA_ITEMS * 2.2);

    const startDate = GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.NOW();
    startDate.setMinutes(startDate.getMinutes() - startMinutesBefore);

    const endDate = GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.NOW();
    endDate.setMinutes(endDate.getMinutes() - endMinutesBefore);

    process.argv = ArgvConfig.argvHelper([`-s ${startDate.toISOString()}`, `-e ${endDate.toISOString()}`]);

    const expectedStartDate = startDate;
    const expectedEndDate = new Date(startDate);
    expectedEndDate.setMinutes(
      expectedEndDate.getMinutes() + GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.GDAX_MAX_DATA_ITEMS);

    const candlesRequestFactory =
      referenceContainer.get<GdaxCandleRequestFactory>(NETWORK_TYPES.RequestFactory);

    candlesRequestFactory.dateRange = { startDate: expectedStartDate, endDate: endDate };

    let request = candlesRequestFactory.newRequest();

    expect(request).toBeDefined();
    expect(request.requestNumber).toEqual(1);
    expect(request.start).toEqual(expectedStartDate.toISOString());
    expect(request.end).toEqual(expectedEndDate.toISOString());
    expect(request.granularity).toEqual(GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.DURATION_IN_MILLIS / 1000);

    request = candlesRequestFactory.newRequest();

    expectedStartDate.setMinutes(
      expectedStartDate.getMinutes() + GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.GDAX_MAX_DATA_ITEMS);
    expectedEndDate.setTime(endDate.getTime());

    expect(request).toBeDefined();
    expect(request.requestNumber).toEqual(2);
    expect(request.start).toEqual(expectedStartDate.toISOString());
    expect(request.end).toEqual(expectedEndDate.toISOString());
    expect(request.granularity).toEqual(GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.DURATION_IN_MILLIS / 1000);

    request = candlesRequestFactory.newRequest();

    expect(request).toBeUndefined();
  });

  test('startDate and endDate that are equal returns undefined request', () => {
    const date = GdaxCandleNetworkConfigOptionsGroup.DEFAULTS.NOW();
    date.setMinutes(date.getMinutes() - 5);

    process.argv = ArgvConfig.argvHelper([`-s ${date.toISOString()}`, `-e ${date.toISOString()}`]);

    const candlesRequestFactory =
      referenceContainer.get<GdaxCandleRequestFactory>(NETWORK_TYPES.RequestFactory);

    candlesRequestFactory.dateRange = { startDate: date, endDate: date };

    const request = candlesRequestFactory.newRequest();

    expect(request).toBeUndefined();
  });
});
