
import * as lolex from 'lolex';

import { injectable } from 'inversify';

import { TYPES as CONFIG_TYPES } from '../../../../../infrastructure/config/module';
import { referenceContainer } from '../../../../../infrastructure/inversify';
import { ArgvConfig } from '../../../../../testing/infrastructure/config/ArgvConfig';

import { GdaxCandleNetworkConfigOptionsGroup } from '../GdaxCandleNetworkConfigOptionsGroup';


describe('GdaxCandleNetworkConfig', () => {
  @injectable()
  class TestConfig extends ArgvConfig {
    public constructor() {
      super();

      this.configOptionsGroups.push(new GdaxCandleNetworkConfigOptionsGroup());
    }
  }

  let clock;
  let originalArgv;

  beforeAll(() => {
    clock = lolex.install({ shouldAdvanceTime: true });
  });

  beforeEach(() => {
    referenceContainer.bind<ArgvConfig>(CONFIG_TYPES.Config).to(TestConfig).inSingletonScope();

    originalArgv = process.argv;
  });

  afterEach(() => {
    process.argv = originalArgv;

    referenceContainer.unbindAll();
  });

  afterAll(() => {
    clock.uninstall();
  });

  test('valid config throws no errors', () => {
    process.argv = ArgvConfig.argvHelper([ '-n 1 -d 60000' ]);

    const config = referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config);

    expect(config).toBeDefined();
  });

  test('config fails if duration not in allowed duration', () => {
    process.argv = ArgvConfig.argvHelper([ `-n 1`, `-d 10` ]);

    expect(() => { referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config); }).toThrow();
  });
});
