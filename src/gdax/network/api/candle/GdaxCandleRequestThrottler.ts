
import { inject, injectable } from 'inversify';

import { TYPES as CONFIG_TYPES } from '../../../../infrastructure/config/module';
import { TYPES as NETWORK_TYPES } from '../../../../infrastructure/network/api/module';
import { RateLimiter } from '../../../../infrastructure/network/api/RateLimiter';

import { GdaxNetworkConfig } from '../../config/GdaxNetworkConfigOptionsGroup';

import { GdaxPublicEndpointRequestThrottler } from '../GdaxPublicEndpointRequestThrottler';

import { GdaxCandleClient } from './GdaxCandleClient';
import { GdaxCandleRequest, GdaxCandleResponse } from './GdaxCandleRequest';


@injectable()
class GdaxCandleRequestThrottler<C extends GdaxCandleClient>
  extends GdaxPublicEndpointRequestThrottler<GdaxCandleRequest, GdaxCandleResponse, C> {

  constructor(
    @inject(CONFIG_TYPES.Config) readonly config: GdaxNetworkConfig,
    @inject(NETWORK_TYPES.RequestClient) readonly requestClient: C,
    @inject(NETWORK_TYPES.RateLimiterFactory) readonly raterLimiterFactory: RateLimiter.RateLimiterFactory) {

    super(config, requestClient, raterLimiterFactory);
  }
}


export { GdaxCandleRequestThrottler };
