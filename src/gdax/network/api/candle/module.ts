
import { ContainerModule, interfaces } from 'inversify';

import { TYPES } from '../../../../infrastructure/network/api/module';
import { RequestClient } from '../../../../infrastructure/network/api/RequestClient';
import { RequestFactory } from '../../../../infrastructure/network/api/RequestFactory';
import { RequestThrottler } from '../../../../infrastructure/network/api/RequestThrottler';

import { GdaxCandleClient } from './GdaxCandleClient';
import { GdaxCandleRequest, GdaxCandleRequestFactory, GdaxCandleResponse } from './GdaxCandleRequest';
import { GdaxCandleRequestThrottler } from './GdaxCandleRequestThrottler';


const GdaxCandleNetworkModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<RequestFactory<GdaxCandleRequest>>(TYPES.RequestFactory).to(GdaxCandleRequestFactory).inSingletonScope();
    bind<RequestClient<GdaxCandleRequest, GdaxCandleResponse>>(TYPES.RequestClient).to(GdaxCandleClient).inSingletonScope();

    bind<RequestThrottler<GdaxCandleRequest, GdaxCandleResponse, GdaxCandleClient>>(
      TYPES.RequestThrottler).to(GdaxCandleRequestThrottler).inSingletonScope();
  }
);


export { GdaxCandleNetworkModule };
