
import { inject, injectable } from 'inversify';

import { TYPES as CONFIG_TYPES } from '../../../../infrastructure/config/module';
import { AuthenticatedRequestClient } from '../../../../infrastructure/network/api/AuthenticatedRequestClient';

import { GdaxNetworkConfig } from '../../config/GdaxNetworkConfigOptionsGroup';
import { TYPES } from '../../types';

import { AuthenticatedClient } from '../AuthenticatedClient';

import { GdaxPlaceOrderRequest, GdaxPlaceOrderResponse } from './GdaxPlaceOrderRequest';


@injectable()
class GdaxPlaceOrderClient extends AuthenticatedRequestClient<GdaxPlaceOrderRequest, GdaxPlaceOrderResponse> {
  constructor(
    @inject(CONFIG_TYPES.Config) readonly config: GdaxNetworkConfig,
    @inject(TYPES.AuthenticatedClientFactory) readonly authenticatedClientFactory: AuthenticatedClient.AuthenticatedClientFactory) {

    super(authenticatedClientFactory);
  }

  protected get _request() {
    return (request: GdaxPlaceOrderRequest) => this._client.placeOrder(request.order);
  }
}


export { GdaxPlaceOrderClient };

