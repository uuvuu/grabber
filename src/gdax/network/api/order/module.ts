
import { ContainerModule, interfaces } from 'inversify';

import { TYPES } from '../../../../infrastructure/network/api/module';
import { RequestClient } from '../../../../infrastructure/network/api/RequestClient';
import { RequestThrottler } from '../../../../infrastructure/network/api/RequestThrottler';

import { GdaxFillClient } from './GdaxFillClient';
import { GdaxFillRequest, GdaxFillResponse } from './GdaxFillRequest';
import { GdaxFillRequestThrottler } from './GdaxFillRequestThrottler';
import { GdaxOrderClient } from './GdaxOrderClient';
import { GdaxOrderRequest, GdaxOrderResponse } from './GdaxOrderRequest';
import { GdaxOrderRequestThrottler } from './GdaxOrderRequestThrottler';
import { GdaxPlaceOrderClient } from './GdaxPlaceOrderClient';
import { GdaxPlaceOrderRequest, GdaxPlaceOrderResponse } from './GdaxPlaceOrderRequest';
import { GdaxPlaceOrderRequestThrottler } from './GdaxPlaceOrderRequestThrottler';


const GdaxOrderNetworkModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<RequestClient<GdaxFillRequest, GdaxFillResponse>>(TYPES.RequestClient)
      .to(GdaxFillClient).inSingletonScope().whenTargetNamed('Fill');

    bind<RequestThrottler<GdaxFillRequest, GdaxFillResponse, GdaxFillClient>>(TYPES.RequestThrottler)
      .to(GdaxFillRequestThrottler).inSingletonScope().whenTargetNamed('Fill');

    bind<RequestClient<GdaxOrderRequest, GdaxOrderResponse>>(TYPES.RequestClient)
      .to(GdaxOrderClient).inSingletonScope().whenTargetNamed('Order');

    bind<RequestThrottler<GdaxOrderRequest, GdaxOrderResponse, GdaxOrderClient>>(TYPES.RequestThrottler)
      .to(GdaxOrderRequestThrottler).inSingletonScope().whenTargetNamed('Order');

    bind<RequestClient<GdaxPlaceOrderRequest, GdaxPlaceOrderResponse>>(TYPES.RequestClient)
      .to(GdaxPlaceOrderClient).inSingletonScope().whenTargetNamed('PlaceOrder');

    bind<RequestThrottler<GdaxPlaceOrderRequest, GdaxPlaceOrderResponse, GdaxPlaceOrderClient>>(TYPES.RequestThrottler)
      .to(GdaxPlaceOrderRequestThrottler).inSingletonScope().whenTargetNamed('PlaceOrder');
  }
);


export { GdaxOrderNetworkModule };
