
import { Request, Response } from '../../../../infrastructure/network/api/Request';


class GdaxFillRequest extends Request {
  constructor(public orderId: string) {
    super();
  }

}

class GdaxFillResponse extends Response<GdaxFillRequest> {
}


export { GdaxFillRequest, GdaxFillResponse };
