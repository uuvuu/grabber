
import { inject, injectable } from 'inversify';

import { TYPES as CONFIG_TYPES } from '../../../../infrastructure/config/module';
import { AuthenticatedRequestClient } from '../../../../infrastructure/network/api/AuthenticatedRequestClient';

import { GdaxNetworkConfig } from '../../config/GdaxNetworkConfigOptionsGroup';
import { TYPES } from '../../types';

import { AuthenticatedClient } from '../AuthenticatedClient';

import { GdaxOrderRequest, GdaxOrderResponse } from './GdaxOrderRequest';


@injectable()
class GdaxOrderClient extends AuthenticatedRequestClient<GdaxOrderRequest, GdaxOrderResponse> {
  constructor(
    @inject(CONFIG_TYPES.Config) readonly config: GdaxNetworkConfig,
    @inject(TYPES.AuthenticatedClientFactory) readonly authenticatedClientFactory: AuthenticatedClient.AuthenticatedClientFactory) {

    super(authenticatedClientFactory);
  }

  protected get _request() {
    return (request: GdaxOrderRequest) => this._client.getOrder(request.orderId);
  }
}


export { GdaxOrderClient };

