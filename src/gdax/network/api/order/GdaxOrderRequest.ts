
import { Request, Response } from '../../../../infrastructure/network/api/Request';


class GdaxOrderRequest extends Request {
  constructor(public orderId: string) {
    super();
  }

}

class GdaxOrderResponse extends Response<GdaxOrderRequest> {
}


export { GdaxOrderRequest, GdaxOrderResponse };
