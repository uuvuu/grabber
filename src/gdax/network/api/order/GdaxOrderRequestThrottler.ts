
import { inject, injectable, named } from 'inversify';

import { TYPES as CONFIG_TYPES } from '../../../../infrastructure/config/module';
import { TYPES as NETWORK_TYPES } from '../../../../infrastructure/network/api/module';
import { RateLimiter } from '../../../../infrastructure/network/api/RateLimiter';

import { GdaxNetworkConfig } from '../../config/GdaxNetworkConfigOptionsGroup';

import { GdaxPrivateEndpointRequestThrottler } from '../GdaxPrivateEndpointRequestThrottler';

import { GdaxOrderClient } from './GdaxOrderClient';
import { GdaxOrderRequest, GdaxOrderResponse } from './GdaxOrderRequest';


@injectable()
class GdaxOrderRequestThrottler<C extends GdaxOrderClient>
  extends GdaxPrivateEndpointRequestThrottler<GdaxOrderRequest, GdaxOrderResponse, C> {

  constructor(
    @inject(CONFIG_TYPES.Config) readonly config: GdaxNetworkConfig,
    @inject(NETWORK_TYPES.RequestClient) @named('Order') readonly requestClient: C,
    @inject(NETWORK_TYPES.RateLimiterFactory) readonly raterLimiterFactory: RateLimiter.RateLimiterFactory) {

    super(config, requestClient, raterLimiterFactory);
  }
}


export { GdaxOrderRequestThrottler };
