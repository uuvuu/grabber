
import { LimitOrder, MarketOrder } from 'gdax';

import { Request, Response } from '../../../../infrastructure/network/api/Request';


class GdaxPlaceOrderRequest extends Request {
  constructor(public order: LimitOrder | MarketOrder) {
    super();
  }
}

class GdaxPlaceOrderResponse extends Response<GdaxPlaceOrderRequest> {
}


export { GdaxPlaceOrderRequest, GdaxPlaceOrderResponse };
