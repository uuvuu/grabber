
import { injectable } from 'inversify';

import { RateLimiter } from '../../../infrastructure/network/api/RateLimiter';
import { Request, Response } from '../../../infrastructure/network/api/Request';
import { RequestClient } from '../../../infrastructure/network/api/RequestClient';
import { RequestThrottler } from '../../../infrastructure/network/api/RequestThrottler';

import { GdaxNetworkConfig } from '../config/GdaxNetworkConfigOptionsGroup';


@injectable()
abstract class GdaxPrivateEndpointRequestThrottler<RQ extends Request, RP extends Response<RQ>, C extends RequestClient<RQ, RP>>
   extends RequestThrottler<RQ, RP, C> {

  protected constructor(
    readonly config: GdaxNetworkConfig,
    readonly requestClient: C,
    readonly rateLimiterFactory: RateLimiter.RateLimiterFactory) {

    super(requestClient, rateLimiterFactory(config.apiUrl, config.privateEndpointRateLimit));
  }
}


export { GdaxPrivateEndpointRequestThrottler };
