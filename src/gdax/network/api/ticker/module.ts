
import { ContainerModule, interfaces } from 'inversify';

import { TYPES } from '../../../../infrastructure/network/api/module';
import { RequestClient } from '../../../../infrastructure/network/api/RequestClient';
import { RequestThrottler } from '../../../../infrastructure/network/api/RequestThrottler';

import { GdaxTickerClient } from './GdaxTickerClient';
import { GdaxTickerRequest, GdaxTickerResponse } from './GdaxTickerRequest';
import { GdaxTickerRequestThrottler } from './GdaxTickerRequestThrottler';


const GdaxTickerNetworkModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<RequestClient<GdaxTickerRequest, GdaxTickerResponse>>(TYPES.RequestClient)
      .to(GdaxTickerClient).inSingletonScope().whenTargetNamed('Ticker');

    bind<RequestThrottler<GdaxTickerRequest, GdaxTickerResponse, GdaxTickerClient>>(TYPES.RequestThrottler)
      .to(GdaxTickerRequestThrottler).inSingletonScope().whenTargetNamed('Ticker');
  }
);


export { GdaxTickerNetworkModule };
