
import { inject, injectable, named } from 'inversify';

import { TYPES as CONFIG_TYPES } from '../../../../infrastructure/config/module';
import { TYPES as NETWORK_TYPES } from '../../../../infrastructure/network/api/module';
import { RateLimiter } from '../../../../infrastructure/network/api/RateLimiter';

import { GdaxNetworkConfig } from '../../config/GdaxNetworkConfigOptionsGroup';

import { GdaxPublicEndpointRequestThrottler } from '../GdaxPublicEndpointRequestThrottler';

import { GdaxTickerClient } from './GdaxTickerClient';
import { GdaxTickerRequest, GdaxTickerResponse } from './GdaxTickerRequest';


@injectable()
class GdaxTickerRequestThrottler<C extends GdaxTickerClient>
  extends GdaxPublicEndpointRequestThrottler<GdaxTickerRequest, GdaxTickerResponse, C> {

  constructor(
    @inject(CONFIG_TYPES.Config) readonly config: GdaxNetworkConfig,
    @inject(NETWORK_TYPES.RequestClient) @named('Ticker') readonly requestClient: C,
    @inject(NETWORK_TYPES.RateLimiterFactory) readonly raterLimiterFactory: RateLimiter.RateLimiterFactory) {

    super(config, requestClient, raterLimiterFactory);
  }
}


export { GdaxTickerRequestThrottler };
