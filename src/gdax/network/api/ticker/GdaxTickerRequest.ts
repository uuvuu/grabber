
import { Request, Response } from '../../../../infrastructure/network/api/Request';


class GdaxTickerRequest extends Request {
  constructor(public product: string) {
    super();
  }

}

class GdaxTickerResponse extends Response<GdaxTickerRequest> {
}


export { GdaxTickerRequest, GdaxTickerResponse };
