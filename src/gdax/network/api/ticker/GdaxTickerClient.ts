
import { inject, injectable } from 'inversify';
import { TYPES as CONFIG_TYPES } from '../../../../infrastructure/config/module';

import { RequestClient } from '../../../../infrastructure/network/api/RequestClient';
import { GdaxNetworkConfig } from '../../config/GdaxNetworkConfigOptionsGroup';

import { TYPES } from '../../types';

import { PublicClient } from '../PublicClient';

import { GdaxTickerRequest, GdaxTickerResponse } from './GdaxTickerRequest';


@injectable()
class GdaxTickerClient extends RequestClient<GdaxTickerRequest, GdaxTickerResponse> {
  constructor(
    @inject(CONFIG_TYPES.Config) readonly config: GdaxNetworkConfig,
    @inject(TYPES.PublicClientFactory) readonly publicClientFactory: PublicClient.PublicClientFactory) {

    super(publicClientFactory(config.apiUrl));
  }

  protected get _request() {
    return (request: GdaxTickerRequest) => this._client.getProductTicker(request.product);
  }
}


export { GdaxTickerClient };
