
import { AuthenticatedClient as GdaxAuthenticatedClient } from 'gdax';


namespace AuthenticatedClient {
  let authenticatedClient;

  export function authenticatedClientFactory() {
    return (key?: string, secret?: string, passphrase?: string, apiUrl?: string) => {
      if (! authenticatedClient && (! key || ! secret || ! passphrase || ! apiUrl)) {
        throw 'Authentication parameters must be passed in to factory if client is not yet authenticated';
      }

      if (! authenticatedClient) {
        authenticatedClient = new GdaxAuthenticatedClient(key, secret, passphrase, apiUrl);
      }

      return authenticatedClient;
    };
  }

  export function unauthenticate() {
    authenticatedClient = undefined;
  }

  export type AuthenticatedClientFactory =
    (key?: string, secret?: string, passphrase?: string, apiUrl?: string) => GdaxAuthenticatedClient;
}


export { AuthenticatedClient };
