
import { ContainerModule, interfaces } from 'inversify';

import { TYPES } from '../../../../infrastructure/network/api/module';
import { RequestClient } from '../../../../infrastructure/network/api/RequestClient';
import { RequestThrottler } from '../../../../infrastructure/network/api/RequestThrottler';

import { GdaxAccountClient } from './GdaxAccountClient';
import { GdaxAccountRequest, GdaxAccountResponse } from './GdaxAccountRequest';
import { GdaxAccountRequestThrottler } from './GdaxAccountRequestThrottler';


const GdaxAccountNetworkModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<RequestClient<GdaxAccountRequest, GdaxAccountResponse>>(TYPES.RequestClient)
      .to(GdaxAccountClient).inSingletonScope().whenTargetNamed('Account');

    bind<RequestThrottler<GdaxAccountRequest, GdaxAccountResponse, GdaxAccountClient>>(TYPES.RequestThrottler)
      .to(GdaxAccountRequestThrottler).inSingletonScope().whenTargetNamed('Account');
  }
);


export { GdaxAccountNetworkModule };
