
import { inject, injectable } from 'inversify';

import { AuthenticatedRequestClient } from '../../../../infrastructure/network/api/AuthenticatedRequestClient';

import { TYPES } from '../../types';

import { AuthenticatedClient } from '../AuthenticatedClient';

import { GdaxAccountRequest, GdaxAccountResponse } from './GdaxAccountRequest';


@injectable()
class GdaxAccountClient extends AuthenticatedRequestClient<GdaxAccountRequest, GdaxAccountResponse> {
  constructor(
    @inject(TYPES.AuthenticatedClientFactory) readonly authenticatedClientFactory: AuthenticatedClient.AuthenticatedClientFactory) {

    super(authenticatedClientFactory);
  }

  protected get _request() {
    return () => this._client.getAccounts();
  }
}


export { GdaxAccountClient };
