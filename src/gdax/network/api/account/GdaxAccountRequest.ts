
import { Request, Response } from '../../../../infrastructure/network/api/Request';


class GdaxAccountRequest extends Request {
}

class GdaxAccountResponse extends Response<GdaxAccountRequest> {
}


export { GdaxAccountRequest, GdaxAccountResponse };
