
import { inject, injectable, named } from 'inversify';

import { TYPES as CONFIG_TYPES } from '../../../../infrastructure/config/module';
import { TYPES as NETWORK_TYPES } from '../../../../infrastructure/network/api/module';
import { RateLimiter } from '../../../../infrastructure/network/api/RateLimiter';

import { GdaxNetworkConfig } from '../../config/GdaxNetworkConfigOptionsGroup';

import { GdaxPrivateEndpointRequestThrottler } from '../GdaxPrivateEndpointRequestThrottler';


import { GdaxAccountClient } from './GdaxAccountClient';
import { GdaxAccountRequest, GdaxAccountResponse } from './GdaxAccountRequest';


@injectable()
class GdaxAccountRequestThrottler<C extends GdaxAccountClient>
  extends GdaxPrivateEndpointRequestThrottler<GdaxAccountRequest, GdaxAccountResponse, C> {

  constructor(
    @inject(CONFIG_TYPES.Config) readonly config: GdaxNetworkConfig,
    @inject(NETWORK_TYPES.RequestClient) @named('Account') readonly requestClient: C,
    @inject(NETWORK_TYPES.RateLimiterFactory) readonly raterLimiterFactory: RateLimiter.RateLimiterFactory) {

    super(config, requestClient, raterLimiterFactory);
  }
}


export { GdaxAccountRequestThrottler };
