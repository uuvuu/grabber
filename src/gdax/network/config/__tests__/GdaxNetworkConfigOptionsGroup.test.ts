
import { injectable } from 'inversify';

import { TYPES as CONFIG_TYPES } from '../../../../infrastructure/config/module';
import { referenceContainer } from '../../../../infrastructure/inversify';
import { ArgvConfig } from '../../../../testing/infrastructure/config/ArgvConfig';

import { GdaxNetworkConfig, GdaxNetworkConfigOptionsGroup } from '../GdaxNetworkConfigOptionsGroup';


describe('GdaxNetworkConfigOptionsGroup', () => {
  @injectable()
  class TestModeConfig extends ArgvConfig {
    public constructor() {
      super();

      this.configOptionsGroups.push(new GdaxNetworkConfigOptionsGroup());
    }
  }

  let originalArgv;

  beforeAll(() => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([]);
  });

  afterAll(() => {
    process.argv = originalArgv;
  });

  beforeEach(() => {
    referenceContainer.bind<ArgvConfig>(CONFIG_TYPES.Config).to(TestModeConfig).inSingletonScope();
  });

  afterEach(() => {
    referenceContainer.unbindAll();
  });

  test('config of gdax has correct config', () => {
    // @ts-ignore
    const config = referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config) as GdaxNetworkConfig;

    expect(config.url).toEqual(GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_API_URL);
    expect(config.apiUrl).toEqual(GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_API_URL);
    expect(config.wsUrl).toEqual(GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_WS_URL);

    const publicEndpointRateLimit = config.publicEndpointRateLimit;
    expect(publicEndpointRateLimit.amountPerInterval)
      .toEqual(GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_PUBLIC_ENDPOINT_RATE_LIMIT.amountPerInterval);
    expect(publicEndpointRateLimit.intervalLengthInMillis)
      .toEqual(GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_PUBLIC_ENDPOINT_RATE_LIMIT.intervalLengthInMillis);

    const privateEndpointRateLimit = config.publicEndpointRateLimit;
    expect(privateEndpointRateLimit.amountPerInterval)
      .toEqual(GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_PUBLIC_ENDPOINT_RATE_LIMIT.amountPerInterval);
    expect(privateEndpointRateLimit.intervalLengthInMillis)
      .toEqual(GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_PUBLIC_ENDPOINT_RATE_LIMIT.intervalLengthInMillis);
  });

  test('testMode uses gdax sandbox url settings', () => {
    const originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper(['-t']);

    // @ts-ignore
    const config = referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config) as GdaxNetworkConfig;

    expect(config.url).toEqual(GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_API_URL_SANDBOX);
    expect(config.apiUrl).toEqual(GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_API_URL_SANDBOX);
    expect(config.wsUrl).toEqual(GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_WS_URL_SANDBOX);

    const publicEndpointRateLimit = config.publicEndpointRateLimit;
    expect(publicEndpointRateLimit.amountPerInterval)
      .toEqual(GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_PUBLIC_ENDPOINT_RATE_LIMIT.amountPerInterval);
    expect(publicEndpointRateLimit.intervalLengthInMillis)
      .toEqual(GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_PUBLIC_ENDPOINT_RATE_LIMIT.intervalLengthInMillis);

    const privateEndpointRateLimit = config.publicEndpointRateLimit;
    expect(privateEndpointRateLimit.amountPerInterval)
      .toEqual(GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_PUBLIC_ENDPOINT_RATE_LIMIT.amountPerInterval);
    expect(privateEndpointRateLimit.intervalLengthInMillis)
      .toEqual(GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_PUBLIC_ENDPOINT_RATE_LIMIT.intervalLengthInMillis);

    process.argv = originalArgv;
  });
});
