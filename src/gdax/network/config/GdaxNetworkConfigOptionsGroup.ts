
import { Command } from 'commander';

import { ConfigOptionsGroup } from '../../../infrastructure/config/ConfigOptionsGroup';
import { RateLimiter } from '../../../infrastructure/network/api/RateLimiter';


class DEFAULTS {
  static readonly GDAX_API_URL = 'https://api.pro.coinbase.com';
  static readonly GDAX_API_URL_SANDBOX = 'https://api-public.sandbox.pro.coinbase.com';

  static readonly GDAX_WS_URL = 'wss://ws-feed.pro.coinbase.com';
  static readonly GDAX_WS_URL_SANDBOX = 'wss://ws-feed-public.sandbox.gdax.com';

  static readonly GDAX_REQUEST_RETRIES = 20;
  static readonly GDAX_REQUEST_RETRY_DELAY_IN_MILLIS = 5000;

  //
  // https://docs.gdax.com/#rate-limits
  //
  // Public Endpoints
  //   by IP: 3 requests per second, up to 6 requests per second in bursts
  //
  // Private Endpoints
  //   by user ID: 5 requests per second, up to 10 requests per second in bursts.
  //
  // GDAX doesn't seem to respect their documentated rates. So configuring slower
  // rates below
  //
  static readonly GDAX_PUBLIC_ENDPOINT_RATE_LIMIT: RateLimiter.RateLimit = {
    amountPerInterval: 1,
    intervalLengthInMillis: 2000
  };

  static readonly GDAX_PRIVATE_ENDPOINT_RATE_LIMIT: RateLimiter.RateLimit = {
    amountPerInterval: 2,
    intervalLengthInMillis: 500
  };

  static readonly GDAX_WS_CHANNELS = [
    // heartbeat channel is automatically subscribed to if a websocket connection is made
    // 'heartbeat', // include sequence numbers and last trade ids/ can be used to verify no messages were missed
    'ticker',    // for real-time price updates every time a match happens
    'level2',    // maintain snapshot of order book (messages guaranteed)
    'matches',   // match messages only (messages can be dropped)
    'full',      // for maintaining level 3 order book (messages can be dropped)
    'user',      // version of the full channel that only contains messages that include the
                 // authenticated user
  ];
}

class GdaxNetworkConfigOptionsGroup extends ConfigOptionsGroup {
  static readonly DEFAULTS = DEFAULTS;

  constructor() {
    super();

    this.addOption({
      channels: {
        flag: '-c',
        description: `Channels to subscribe to in the GDAX websocket feed. Valid feeds are: ${GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_WS_CHANNELS}`,
        coercionFunction: this.parseArray,
        defaultValue: []     // subscribe only to heartbeat channel by default
      }
    });
  }

  addExtraParams(command: Command) {
    if (command.testMode) {
      this.addExtraParam('url', GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_API_URL_SANDBOX);
      this.addExtraParam('apiUrl', GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_API_URL_SANDBOX);

      this.addExtraParam('wsUrl', GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_WS_URL_SANDBOX);
    } else {
      this.addExtraParam('url', GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_API_URL);
      this.addExtraParam('apiUrl', GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_API_URL);

      this.addExtraParam('wsUrl', GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_WS_URL);
    }

    this.addExtraParam('requestRetries', GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_REQUEST_RETRIES);
    this.addExtraParam('requestRetryDelayInMillis', GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_REQUEST_RETRY_DELAY_IN_MILLIS);

    this.addExtraParam('publicEndpointRateLimit', GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_PUBLIC_ENDPOINT_RATE_LIMIT);
    this.addExtraParam('privateEndpointRateLimit', GdaxNetworkConfigOptionsGroup.DEFAULTS.GDAX_PRIVATE_ENDPOINT_RATE_LIMIT);
  }
}

interface GdaxNetworkConfig {
  url: string;
  apiUrl: string;
  wsUrl: string;

  channels: string[];

  requestRetries: number;
  requestRetryDelayInMillis: number;

  publicEndpointRateLimit: RateLimiter.RateLimit;
  privateEndpointRateLimit: RateLimiter.RateLimit;
}


export { GdaxNetworkConfig, GdaxNetworkConfigOptionsGroup };
