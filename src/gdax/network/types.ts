

/*
 A separate types.ts file is needed to store TYPES for this module to avoid a cyclic dependency
 between module.ts and WebsocketFeed.ts when TYPES are stored in module.ts.
 */
const TYPES = {
  AuthenticatedClientFactory: Symbol.for('AuthenticatedClientFactory'),
  PublicClientFactory: Symbol.for('PublicClientFactory'),
  WebsocketClientFactory: Symbol.for('WebsocketClientFactory'),
  WebsocketFeed: Symbol.for('WebsocketFeed'),

  GdaxHeartbeatChannel: Symbol.for('GdaxHeartbeatChannel'),
  GdaxTickerChannel: Symbol.for('GdaxTickerChannel')
};


export { TYPES };
