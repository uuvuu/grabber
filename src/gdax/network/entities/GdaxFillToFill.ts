
import { FillDocument } from '../../../domain/order/entities/Fill';


namespace GdaxFillToFill {
  export function newFillDocumentFromGdaxFill(fill: any) {
    const fillDocument = {
      tradeId: fill.trade_id,
      orderId: fill.order_id,

      product: fill.product_id,

      price: fill.price,
      size: fill.size,

      createdTime: new Date(fill.created_at),

      liquidity: fill.liquidity,

      fee: fill.fee,
      settled: fill.settled,

      side: fill.side,

      usdVolume: fill.usd_volume
    };

    return fillDocument as FillDocument;
  }

  export function newFillDocumentsFromGdaxFills(fills: any[]) {
    return fills.map(fill => {
      return newFillDocumentFromGdaxFill(fill);
    });
  }
}


export { GdaxFillToFill };

