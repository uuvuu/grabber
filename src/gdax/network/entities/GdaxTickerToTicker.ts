
import { Ticker } from '../../../domain/ticker/entities/Ticker';


namespace GdaxTickerToTicker {
  export function newTickerDocumentFromGdaxTicker(ticker: any, source = 'cbpro-api') {
    const tickerDocument = {
      product: ticker.product_id,

      time: new Date(ticker.time),

      tradeId: ticker.trade_id,
      sequence: ticker.sequence,

      price: ticker.price,
      side: ticker.side,
      lastSize: ticker.last_size,
      bestBid: ticker.best_bid,
      bestAsk: ticker.best_ask,

      open24h: ticker.open_24h,
      low24h: ticker.low_24h,
      high24h: ticker.high_24h,

      volume24h: ticker.volume_24h,
      volume30d: ticker.volume_30d,

      source: source
    };

    return tickerDocument as Ticker.TickerDocument;
  }

  export function newTickerDocumentsFromGdaxTickers(tickers: any[], source = 'cbpro-api') {
    return tickers.map(ticker => {
      return newTickerDocumentFromGdaxTicker(ticker, source);
    });
  }
}


export { GdaxTickerToTicker };
