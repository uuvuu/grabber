
import { Account } from '../../../domain/account/entities/Account';
import { DateUtils } from '../../../utils/DateUtils';


namespace GdaxAccountToAccount {
  import TimeAlignment = DateUtils.TimeAlignment;

  export function newAccountDocumentFromGdaxAccount(account: any) {
    return {
      accountId: account.id,

      currency: account.currency.toUpperCase(),

      balance: account.balance,
      available: account.available,
      hold: account.hold,

      exchangeRateUSD: account.exchangeRateUSD
    } as Account.Account;
  }

  export function newAccountsDocumentFromGdaxAccounts(accounts: any[], profileId: string) {
    const time = DateUtils.alignDate(new Date(), TimeAlignment.MINUTES);

    let totalUSD = 0;

    accounts = accounts.map(account => {
      totalUSD += +account.balance * +account.exchangeRateUSD;

      return newAccountDocumentFromGdaxAccount(account);
    });

    return {
      time: time,

      profileId: profileId,

      accounts: accounts,

      totalUSD: '' + totalUSD
    } as Account.AccountsDocument;
  }

  export function newAccountsMapFromGdaxAccounts(accounts: any[], product: string, price: string) {
    const accountsMap = {};

    const currencies = product.split('-');

    accounts.forEach(account => {
      if (currencies.includes(account.currency)) {
        accountsMap[account.currency.toUpperCase()] = {
          accountId: account.id,
          balance: account.balance,
          available: account.available,
          hold: account.hold,
        };

        if (account.currency.toUpperCase() == 'USD') {
          accountsMap[account.currency.toUpperCase()].exchangeRateUSD = '1';
        } else {
          accountsMap[account.currency.toUpperCase()].exchangeRateUSD = price;
        }
      }
    });

    return accountsMap;
  }
}


export { GdaxAccountToAccount };

