
import { Candle } from '../../../domain/candle/entities/Candle';


namespace GdaxCandleToCandle {
  export function newCandleDocumentFromArray(candle: any[], product, source = 'cbpro-api', interpolated = false) {
    // time, low, high, open, close, volume
    const candleDocument = {
      product: product,

      // convert epoch time to millis
      time: new Date(candle[0] * 1000),

      low: `${candle[1].toFixed(2)}`,
      high: `${candle[2].toFixed(2)}`,
      open: `${candle[3].toFixed(2)}`,
      close: `${candle[4].toFixed(2)}`,

      volume: `${candle[5]}`,

      source: source,
      interpolated: interpolated
    };

    return candleDocument as Candle.CandleDocument;
  }

  export function newCandleDocumentsFromArray(candles: any[][], product, source = 'cbpro-api', interpolated = false) {
    return candles.map(candle => {
      return newCandleDocumentFromArray(candle, product, source, interpolated);
    });
  }

  export function candleReplacer(key, value) {
    if (key === 'data') {
      return value.map(candle => {
        return [
          new Date(candle[0] * 1000).toISOString(),

          `${candle[1].toFixed(2)}`,
          `${candle[2].toFixed(2)}`,
          `${candle[3].toFixed(2)}`,
          `${candle[4].toFixed(2)}`,

          `${candle[5]}`
        ];
      });
    }

    return value;
  }
}

export { GdaxCandleToCandle };

