
import { Ticker } from '../../../../domain/ticker/entities/Ticker';

import { GdaxTickerToTicker } from '../GdaxTickerToTicker';


describe('GdaxTickerToTicker', () => {
  const tickers = [
    {
      'sequence': 7031467891,
      'product_id': 'BTC-USD',
      'price': '6692.50000000',
      'open_24h': '6744.28000000',
      'volume_24h': '4168.7455136',
      'low_24h': '6628.01000000',
      'high_24h': '6754.49000000',
      'volume_30d': '240246.32838006',
      'best_bid': '6692.49',
      'best_ask': '6692.5',
      'side': 'buy',
      'time': '2018-09-23T02:44:19.136Z',
      'trade_id': 51334645,
      'last_size': '0.01287286'
    },
    {
      'sequence': 7030720012,
      'product_id': 'BTC-USD',
      'price': '6706.48000000',
      'open_24h': '6805.18000000',
      'volume_24h': '4736.55494811',
      'low_24h': '6628.01000000',
      'high_24h': '6810.00000000',
      'volume_30d': '240337.43275619',
      'best_bid': '6706.47',
      'best_ask': '6706.48',
      'side': 'buy',
      'time': '2018-09-23T00:24:01.334000Z',
      'trade_id': 51329423,
      'last_size': '0.00133679'
    },
  ];

  test('newTickerDocumentFromGdaxTicker() creates new TickerDocument', () => {
    const tickerDocument = GdaxTickerToTicker.newTickerDocumentFromGdaxTicker(tickers[0], 'cbpro-api');

    tickerDocumentsEqualsGdaxTickers([tickers[0]], [tickerDocument], 'cbpro-api');
  });

  test('newTickerDocumentsFromGdaxTickers() creates new TickerDocuments', () => {
    const tickerDocuments = GdaxTickerToTicker.newTickerDocumentsFromGdaxTickers(tickers, 'cbpro-api');

    tickerDocumentsEqualsGdaxTickers(tickers, tickerDocuments, 'cbpro-api');
  });

  function tickerDocumentsEqualsGdaxTickers(tickers: any, tickerDocuments: Ticker.TickerDocument[], source) {
    expect(tickerDocuments.length).toEqual(tickers.length);

    for (let i = 0; i < tickers.length; i++) {
      expect(tickerDocuments[i].product).toEqual(tickers[i].product_id);
      expect(tickerDocuments[i].time).toEqual(new Date(tickers[i].time));
      expect(tickerDocuments[i].tradeId).toEqual(tickers[i].trade_id);
      expect(tickerDocuments[i].sequence).toEqual(tickers[i].sequence);
      expect(tickerDocuments[i].price).toEqual(tickers[i].price);
      expect(tickerDocuments[i].side).toEqual(tickers[i].side);
      expect(tickerDocuments[i].lastSize).toEqual(tickers[i].last_size);
      expect(tickerDocuments[i].bestBid).toEqual(tickers[i].best_bid);
      expect(tickerDocuments[i].bestAsk).toEqual(tickers[i].best_ask);
      expect(tickerDocuments[i].open24h).toEqual(tickers[i].open_24h);
      expect(tickerDocuments[i].low24h).toEqual(tickers[i].low_24h);
      expect(tickerDocuments[i].high24h).toEqual(tickers[i].high_24h);
      expect(tickerDocuments[i].volume24h).toEqual(tickers[i].volume_24h);
      expect(tickerDocuments[i].volume30d).toEqual(tickers[i].volume_30d);
      expect(tickerDocuments[i].source).toEqual(source);
    }
  }
});
