
import { Candle } from '../../../../domain/candle/entities/Candle';

import { GdaxCandleToCandle } from '../GdaxCandleToCandle';


describe('GdaxCandleToCandle', () => {
  const product = 'BTC-USD';

  const dates = [
    1535691729,
    1535692197
  ];

  const candles = [
    [
      dates[0],
      112.13,
      189.48,
      134.19,
      157.65,
      14.49038
    ],
    [
      dates[1],
      938.58,
      1014.80,
      1200.00,
      1039.17,
      8.7705
    ],
  ];


  test('newCandleDocumentFromArray() creates new CandleDocument', () => {
    const candleDocument = GdaxCandleToCandle.newCandleDocumentFromArray(candles[0], product, 'cbpro-api', false);

    candleDocumentsEqualsCandles([candles[0]], [candleDocument], product, 'cbpro-api', false);
  });

  test('newCandleDocumentsFromArray() creates new CandleDocuments array', () => {
    const candleDocuments = GdaxCandleToCandle.newCandleDocumentsFromArray(candles, product);

    candleDocumentsEqualsCandles(candles, candleDocuments, product, 'cbpro-api', false);
  });

  test('candleReplacer()', () => {
    const obj = JSON.parse(JSON.stringify({ data: candles }, GdaxCandleToCandle.candleReplacer));
    const replacedCandles = obj.data;

    for (let i = 0; i < candles.length; i++) {
      expect(replacedCandles[i][0]).toEqual(new Date(candles[i][0] * 1000).toISOString());
    }
  });

  function candleDocumentsEqualsCandles(
    candles: any[][], candleDocuments: Candle.CandleDocument[], product: string, source: string, interpolated: boolean) {

    expect(candleDocuments.length).toEqual(candles.length);

    for (let i = 0; i < candles.length; i++) {
      expect(candleDocuments[i].product).toEqual(product);

      // timestamps from GDAX are given in epoch seconds. test that we convert it to millis
      expect(candleDocuments[i].time).toEqual(new Date(dates[i] * 1000));

      expect(candleDocuments[i].low).toEqual(`${candles[i][1].toFixed(2)}`);
      expect(candleDocuments[i].high).toEqual(`${candles[i][2].toFixed(2)}`);
      expect(candleDocuments[i].open).toEqual(`${candles[i][3].toFixed(2)}`);
      expect(candleDocuments[i].close).toEqual(`${candles[i][4].toFixed(2)}`);

      expect(candleDocuments[i].volume).toEqual(`${candles[i][5]}`);

      expect(candleDocuments[i].source).toEqual(source);
      expect(candleDocuments[i].interpolated).toEqual(interpolated);
    }
  }
});
