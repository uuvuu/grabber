
import { LimitOrder } from 'gdax';

import { Order } from '../../../domain/order/entities/Order';
import { RandomUtils } from '../../../utils/RandomUtils';


namespace GdaxOrderToOrder {
  export const NO_ORDER_FAKE_MODE_PREFIX = 'NO-ORDER-FAKE-MODE-ORDER';
  export const NO_ORDER_NO_OP_PREFIX = 'NO-ORDER-NO-OP';
  export const NO_ORDER_SIZE_TOO_SMALL_PREFIX = 'NO-ORDER-SIZE-TOO-SMALL';
  export const NO_ORDER_UNAUTHENTICATED_PREFIX = 'NO-ORDER-UNAUTHENTICATED';

  export function newOrderDocumentFromGdaxOrder(order: any, candleDate: Date) {
    // order.expire_time is missing millis and the 'Z' in the time string
    const expireTime =
      order.expire_time && ! order.expire_time.toUpperCase().endsWith('Z') ? new Date(order.expire_time + 'Z') : undefined;

    return {
      orderId: order.id,

      product: order.product_id,

      price: order.price,
      size: order.size,

      side: order.side,

      stp: order.stp,

      funds: order.funds,
      specifiedFunds: order.specified_funds,

      type: order.type,

      timeInForce: order.time_in_force,

      postOnly: order.post_only,

      createdTime: new Date(order.created_at),
      expireTime: expireTime,
      doneTime: order.done_at ? new Date(order.done_at) : undefined,
      candleTime: candleDate,

      doneReason: order.done_reason,
      rejectedReason: order.reject_reason,

      fillFees: order.fill_fees,
      filledSize: order.filled_size,

      executedValue: order.executed_value,

      status: order.status,

      settled: order.settled
    } as Order.OrderDocument;
  }

  export function newFakeModeOrder(order: LimitOrder, candleDate: Date) {
    const o = newFakeOrder(NO_ORDER_FAKE_MODE_PREFIX + '-' + RandomUtils.guid(), order, candleDate);

    o.status = Order.Status.DONE;
    o.doneReason = 'fake-mode order';
    o.clearanceStatus = 'fake-mode order';

    return o;
  }

  export function newUnauthenticatedOrder(order: LimitOrder, candleDate: Date) {
    const o = newFakeOrder(NO_ORDER_UNAUTHENTICATED_PREFIX + '-' + RandomUtils.guid(), order, candleDate);

    o.status = Order.Status.DONE;
    o.doneReason = 'unauthenticated order';
    o.clearanceStatus = o.doneReason;

    return o;
  }

  export function newNoOpOrder(order: LimitOrder, candleDate: Date, authenticated: boolean) {
    const o = newFakeOrder(NO_ORDER_NO_OP_PREFIX + '-' + RandomUtils.guid(), order, candleDate);

    o.status = Order.Status.DONE;
    o.doneReason = 'no-op order' + (! authenticated ? ' (unauthenticated)' : '');
    o.clearanceStatus = o.doneReason;

    return o;
  }

  export function newSizeTooSmallOrder(order: LimitOrder, candleDate: Date) {
    const o = newFakeOrder(NO_ORDER_SIZE_TOO_SMALL_PREFIX + '-' + RandomUtils.guid(), order, candleDate);

    o.status = Order.Status.REJECTED;
    o.rejectedReason = 'order size too small';
    o.clearanceStatus = 'order size too small';

    return o;
  }

  function newFakeOrder(orderId: string, order: LimitOrder, candleDate: Date) {
    const now = new Date();

    const expireTime = new Date(now);
    expireTime.setMinutes(expireTime.getMinutes() + 1);
    expireTime.setMilliseconds(0);

    // @ts-ignore
    return {
      orderId: orderId,

      product: order.product_id,

      price: order.price,
      size: order.size,

      side: order.side,

      stp: undefined,

      funds: undefined,
      specifiedFunds: undefined,

      type: order.type,

      timeInForce: order.time_in_force,

      postOnly: order.post_only,

      createdTime: now,
      expireTime: expireTime,
      doneTime: now,
      candleTime: candleDate,

      doneReason: undefined,
      rejectedReason: undefined,

      fillFees: '0.0000000000000000',
      filledSize: '0.00000000',

      executedValue: '0.0000000000000000',

      status: undefined,

      settled: true
    } as Order.OrderDocument;
  }
}


export { GdaxOrderToOrder };

