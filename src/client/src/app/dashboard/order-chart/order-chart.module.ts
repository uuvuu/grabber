
import { NgModule } from '@angular/core';

import { NgxChartsModule } from '@swimlane/ngx-charts';

import { SharedModule } from '../../shared/shared.module';

import { OrderChartComponent } from './order-chart.component';


@NgModule({
  declarations: [
    OrderChartComponent
  ],
  exports: [ OrderChartComponent ],
  // providers: [ ],
  imports: [
    NgxChartsModule,
    SharedModule
  ]
})
export class OrderChartModule { }
