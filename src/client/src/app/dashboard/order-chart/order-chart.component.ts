
import * as dateformat from 'dateformat';
import * as shape from 'd3-shape';

import { Component, AfterViewInit, Injector } from '@angular/core';
import { AbstractDashboardCard } from '../abstract-dashboard-card';
import { DashboardCard } from '../dashboard-card';
import { DashboardServiceGQL } from '../dashboard.service';

import { OrderCreatedServiceGQL } from '../order-created.service';

import { Order, OrderUtils } from '../order';


interface ItemSeries {
  name: string;
  series: Item[];
}

interface Item {
  name: number;
  value: number;
  min: number;
  max: number;
}

@Component({
  selector: 'order-chart',
  templateUrl: './order-chart.component.html',
  styleUrls: [ './order-chart.component.scss' ]
})
export class OrderChartComponent extends AbstractDashboardCard implements AfterViewInit {
  colorScheme = 'vivid';
  schemeType = 'ordinal';
  animationsEnabled = true;
  showLegend = false;
  gradientEnabled = false;

  showXAxis = true;
  showXAxisLabel = true;
  showYAxis = true;
  showYAxisLabel = true;

  xAxisLabel = 'Time';
  yAxisLabel = 'Price';

  enableAutoScale = true;

  enableTimelineControl = true;

  showGridLines = true;

  curve = shape.curveLinear;

  // opacity of the shadow around the line indication the (optional) min and max values.
  // The range shadow is only displayed if min and max values are provided with the data.
  // The color of the shadow is always the color of the central line.
  rangeFillOpacity = 0.40;

  roundDomains = true;

  tooltipDisabled = false;

  private readonly startTimeMinutesBefore = 5760;

  private maxValue: number;
  private minValue: number;
  private scale: number;

  readonly orderMap: Map<number, Order.Order> = new Map<number, Order.Order>();
  orderSeries: ItemSeries[];

  readonly Side = Order.Side;
  readonly Status = Order.Status;

  // @ts-ignore
  readonly noneSide: Order.Side = this.Side[this.Side.NONE];

  // @ts-ignore
  readonly buySide: Order.Side = this.Side[this.Side.BUY];

  // @ts-ignore
  readonly sellSide: Order.Side = this.Side[this.Side.SELL];

  // @ts-ignore
  readonly rejectedStatus: Order.Status = this.Status[this.Status.REJECTED];

  constructor(
    private injector: Injector,
    private dashboardServiceGQL: DashboardServiceGQL,
    private orderCreatedServiceGQL: OrderCreatedServiceGQL) {

    super(
      injector.get(DashboardCard.metadata.NAME),
      injector.get(DashboardCard.metadata.TITLE),
      injector.get(DashboardCard.metadata.ROWS),
      injector.get(DashboardCard.metadata.COLUMNS)
    );

    this.dashboardServiceGQL
      .fetch()
      .subscribe(result => {
        this.initializeOrders(OrderUtils.jsonOrdersToOrders(result.data.dashboard.orders));
      });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      if (this.orderSeries && this.orderSeries.length) {
        this.orderSeries = [ ...this.orderSeries ];
      }
    }, 30000);
  }

  initializeOrders(orders: Order.Order[]) {
    this.initializeOrderMap(orders);
    this.orderSeries = this.mapOrdersToItemSeriesArray(orders);

    this.subscribeToOrderCreation();
  }

  private subscribeToOrderCreation() {
    this.orderCreatedServiceGQL.subscribe().subscribe(result => this.addNewOrder(result));
  }

  addNewOrder(order: any) {
    const newOrder = OrderUtils.jsonOrderToOrder(order.data['orderCreated']);

    // let orderSeries = this.orderSeries[0].series;
    // console.log(`BEGIN: orderSeries.length = ${orderSeries.length}`);
    // console.log(`BEGIN: orderSeries[0].time = ${new Date(orderSeries[0].name).toISOString()}`);
    // console.log(`BEGIN: orderSeries[1].time = ${new Date(orderSeries[1].name).toISOString()}`);
    // console.log(`BEGIN: orderSeries[len - 2].time = ${new Date(orderSeries[orderSeries.length - 2].name).toISOString()}`);
    // console.log(`BEGIN: orderSeries[len - 1].time = ${new Date(orderSeries[orderSeries.length - 1].name).toISOString()}`);

    this.addOrderToMap(newOrder);

    // if we have a new min or max set the new value and recalculate the series to adjust for hte
    // new scale
    if (newOrder.price > this.maxValue) {
      this.maxValue = newOrder.price;
      this.orderSeries = this.mapOrdersToItemSeriesArray(Array.from(this.orderMap.values()));
    } else if (newOrder.price < this.minValue) {
      this.minValue = newOrder.price;
      this.orderSeries = this.mapOrdersToItemSeriesArray(Array.from(this.orderMap.values()));
    }

    this.orderSeries[0].series.push(this.mapOrderToItem(newOrder));

    if (this.orderSeries[0].series.length > this.startTimeMinutesBefore) {
      const order = this.orderSeries[0].series.shift();
      this.orderMap.delete(order.name);
    }

    this.orderSeries = [ ...this.orderSeries ];

    // orderSeries = this.orderSeries[0].series;
    // console.log(`END: orderSeries.length = ${orderSeries.length}`);
    // console.log(`END: orderSeries[0].time = ${new Date(orderSeries[0].name).toISOString()}`);
    // console.log(`END: orderSeries[1].time = ${new Date(orderSeries[1].name).toISOString()}`);
    // console.log(`END: orderSeries[len - 2].time = ${new Date(orderSeries[orderSeries.length - 2].name).toISOString()}`);
    // console.log(`END: orderSeries[len - 1].time = ${new Date(orderSeries[orderSeries.length - 1].name).toISOString()}`);
  }

  onLegendLabelClick(event) {
    console.log(`onLegendLabelClick(): event = ${JSON.stringify(event)}`);
  }

  select(event) {
    console.log(`select(): event = ${JSON.stringify(event)}`);
  }

  activate(event) {
    console.log(`activate(): event = ${JSON.stringify(event)}`);
  }

  deactivate(event) {
    console.log(`deactivate(): event = ${JSON.stringify(event)}`);
  }

  xAxisFormat(label) {
    // const [ , minutes ] = label.split(':');
    //
    // return +minutes % 10 ? ''  : label;

    const date = new Date(label);

    return dateformat(date, 'UTC:mm-dd HH:MM');
  }

  orderDetails(model) {
    return this.orderMap.get(model.name);
  }

  columnsToDisplay = [ 'name', 'value' ];

  orderToRows(model) {
    if (! model) {
      return;
    }

    const order = this.orderMap.get(model.name);

    const rows = [
      { name: [ order.status === this.rejectedStatus ? 'Rejected Reason' : 'Status' ], value: order.clearanceStatus },
      { name: 'Order Id', value: OrderUtils.shortenOrderId(order.orderId) },
      { name: 'Time', value: order.createdTime.toISOString() },
      { name: 'Price', value: order.price.toFixed(6) },
    ];

    if (order.side !== this.noneSide && order.doneReason !== 'unauthenticated order') {
      rows.push({ name: 'Size', value: order.size.toFixed(6) });
      rows.push({ name: 'Filled Size', value: order.filledSize.toFixed(6) });
      rows.push({ name: 'Executed Value', value: order.executedValue.toFixed(6) });
    }

    return rows;
  }

  private initializeOrderMap(orders: Order.Order[]) {
    orders.forEach(order => this.addOrderToMap(order));
  }

  private orderToOrderChartItemName(order: Order.Order) {
    return order.createdTime.getTime();
  }

  private addOrderToMap(order: Order.Order) {
    if (typeof this.minValue === 'undefined' && typeof this.maxValue === 'undefined') {
      this.minValue = order.price;
      this.maxValue = order.price;
    } else {
      if (order.price > this.maxValue) {
        this.maxValue = order.price;
      } else if (order.price < this.minValue) {
        this.minValue = order.price;
      }
    }

    this.scale = this.maxValue - this.minValue;

    this.orderMap.set(this.orderToOrderChartItemName(order), order);
  }

  private mapOrderToItem(order: Order.Order): Item {
    let min = order.price;
    let max = order.price;

    if (order.side === this.buySide) {
      max = max + this.scale * 0.033;
    } else if (order.side === this.sellSide) {
      min = min + this.scale * -0.033;
    }

    return { name: this.orderToOrderChartItemName(order), value: order.price, max: max, min: min };
  }

  private mapOrdersToItemSeriesArray(orders: Order.Order[]): ItemSeries[] {
    const series = orders.map(order => this.mapOrderToItem(order));

    return [
      { name: 'Price', series: series },
    ];
  }
}


