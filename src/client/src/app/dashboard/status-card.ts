
// TODO this file is almost copied from the server side in its entirety. Figure out a way
// to reuse the server-side entity definitions

namespace StatusCard {
  export interface StatusCard {
    name: string;
    type: string;
    data: any;
  }
}

namespace StatusCardUtils {
  export function jsonStatusCardToStatusCard(jsonStatusCard: any) {
    return {
      name: jsonStatusCard.name,
      type: jsonStatusCard.type,
      data: jsonStatusCard.data
    } as StatusCard.StatusCard;
  }

  export function jsonStatusCardsToStatusCards(jsonStatusCards: any) {
    return jsonStatusCards.map(jsonOrder => jsonStatusCardToStatusCard(jsonOrder));
  }
}


export { StatusCard, StatusCardUtils };

