
import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable } from 'rxjs';

import { DashboardCard } from './dashboard-card';


@Injectable()
export class DashboardCardsService {
  constructor() { }

  private _statusCards: BehaviorSubject<DashboardCard[]> = new BehaviorSubject<DashboardCard[]>([]);
  private _chartCards: BehaviorSubject<DashboardCard[]> = new BehaviorSubject<DashboardCard[]>([]);

  addStatusCard(card: DashboardCard): void {
    this._statusCards.next(this._statusCards.getValue().concat(card));
  }

  get statusCards(): Observable<DashboardCard[]> {
    return this._statusCards.asObservable();
  }

  addChartCard(card: DashboardCard): void {
    this._chartCards.next(this._chartCards.getValue().concat(card));
  }

  get chartCards(): Observable<DashboardCard[]> {
    return this._chartCards.asObservable();
  }
}

