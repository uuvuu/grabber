
import gql from 'graphql-tag';

import { Injectable } from '@angular/core';

import { Subscription } from 'apollo-angular';

import { Account } from './account';
import { StatusCard } from './status-card';


@Injectable({
  providedIn: 'root',
})
export class StatusCardsUpdatedServiceGQL extends Subscription<StatusCard.StatusCard> {
  document = gql`
    subscription statusCardsUpdated {
      statusCardsUpdated {
        name
        
        type
        
        data
      }
    }
  `;
}
