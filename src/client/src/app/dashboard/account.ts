

// TODO this file is almost copied from the server side in its entirety. Figure out a way
// to reuse the server-side entity definitions
namespace Account {
  export interface Account {
    currency: string;

    accountId: string;

    balance: number;
    available: number;
    hold: number;

    exchangeRateUSD: number;
    exchangeRateCrypto: number;
  }

  export interface Accounts {
    time: Date;

    profileId: string;

    accounts: Account[];

    totalUSD: number;
    totalCrypto: number;

    exchangeRateCrypto: number;
  }
}

namespace AccountUtils {
  export function jsonAccountToAccount(jsonAccount: any) {
    return {
      currency: jsonAccount.currency,

      accountId: jsonAccount.accountId,

      balance: +jsonAccount.balance,
      available: +jsonAccount.available,
      hold: +jsonAccount.hold,

      exchangeRateUSD: +jsonAccount.exchangeRateUSD,
      exchangeRateCrypto: +jsonAccount.exchangeRateCrypto,
    } as Account.Account;
  }

  export function jsonAccountListToAccountList(jsonAccountList: any) {
    return jsonAccountList.map(jsonAccount => jsonAccountToAccount(jsonAccount));
  }

  export function jsonAccountsToAccounts(jsonAccounts: any) {
    return {
      time: new Date(jsonAccounts.time),

      profileId: jsonAccounts.profileId,

      accounts: jsonAccountListToAccountList(jsonAccounts.accounts),

      totalUSD: +jsonAccounts.totalUSD,
      totalCrypto: +jsonAccounts.totalCrypto,

      exchangeRateCrypto: +jsonAccounts.exchangeRateCrypto
    } as Account.Accounts;
  }

  export function jsonAccountsListToAccountsList(jsonAccountsList: any) {
    return jsonAccountsList.map(jsonAccounts => jsonAccountsToAccounts(jsonAccounts));
  }
}


export { Account, AccountUtils };
