

// TODO this file is almost copied from the server side in its entirety. Figure out a way
// to reuse the server-side entity definitions

namespace Order {
  export enum CancelAfter {
    MIN = <any>'min',
    HOUR = <any>'hour',
    DAY = <any>'day'
  }

  export enum Side {
    BUY = <any>'buy',
    NONE = <any>'none',
    SELL = <any>'sell'
  }

  // self-trade prevention flag
  export enum STP {
    DC = <any>'dc',
    CO = <any>'co',
    CN = <any>'cn',
    CB = <any>'cb'
  }

  export enum Type {
    LIMIT = <any>'limit',
    MARKET = <any>'market'
  }

  export enum TimeInForce {
    GTC = <any>'GTC',
    GTT = <any>'GTT',
    IOC = <any>'IOC',
    FOK = <any>'FOK'
  }

  export enum Status {
    OPEN = <any>'open',
    PENDING = <any>'pending',
    ACTIVE = <any>'active',
    DONE = <any>'done',
    SETTLED = <any>'settled',
    REJECTED = <any>'rejected'
  }

  export interface Order {
    orderId: string;

    product: string;

    price: number;
    size: number;

    side: Side;

    stp: STP;    // self-trade prevention flag (optional)

    funds: number;
    specifiedFunds: number;

    type: Type;    // (default is limit)

    timeInForce: TimeInForce;    // (optional, default is GTC)

    postOnly: boolean;

    createdTime: Date;
    expireTime: Date;
    doneTime: Date;
    candleTime: Date;       // the latest candle time that this trade was based on

    doneReason: string;
    rejectedReason: string;

    fillFees: number;
    filledSize: number;

    executedValue: number;

    status: Status;

    settled: boolean;

    clearanceStatus: string;
  }
}

namespace OrderUtils {
  export function jsonOrderToOrder(jsonOrder: any) {
    // @ts-ignore
    const side: Order.Side = Order.Side[jsonOrder.side];

    // @ts-ignore
    const stp: Order.STP = jsonOrder.stp ? Order.STP[jsonOrder.stp] : undefined;

    // @ts-ignore
    const type: Order.Type = Order.Type[jsonOrder.type];

    // @ts-ignore
    const timeInForce: Order.TimeInForce = Order.TimeInForce[jsonOrder.timeInForce];

    // @ts-ignore
    const status: Order.Status = Order.Status[jsonOrder.status];

    return {
      orderId: jsonOrder.orderId,
      product: jsonOrder.product,
      price: +jsonOrder.price,
      size: +jsonOrder.size,
      side: side,
      stp: stp,
      funds: +jsonOrder.funds,
      specifiedFunds: +jsonOrder.specifiedFunds,
      type: type,
      timeInForce: timeInForce,
      postOnly: jsonOrder.postOnly,
      createdTime: new Date(jsonOrder.createdTime),
      expireTime: new Date(jsonOrder.expireTime),
      doneTime: new Date(jsonOrder.doneTime),
      candleTime: new Date(jsonOrder.candleTime),
      doneReason: jsonOrder.doneReason,
      rejectedReason: jsonOrder.rejectedReason,
      fillFees: +jsonOrder.fillFees,
      filledSize: +jsonOrder.filledSize,
      executedValue: +jsonOrder.executedValue,
      status: status,
      settled: jsonOrder.settled,
      clearanceStatus: jsonOrder.clearanceStatus
    } as Order.Order;
  }

  export function jsonOrdersToOrders(jsonOrders: any) {
    return jsonOrders.map(jsonOrder => jsonOrderToOrder(jsonOrder));
  }

  export function shortenOrderId(orderId: string) {
    const parts = orderId.split('-');

    return '... ' + parts[parts.length - 2] + '-' + parts[parts.length - 1];
  }
}


export { Order, OrderUtils };
