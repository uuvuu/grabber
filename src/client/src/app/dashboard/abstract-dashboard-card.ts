

export abstract class AbstractDashboardCard {
  protected constructor(private _name: string, private _title: string, private _columns: string, private _rows: string) { }

  get name(): string {
    return this._name;
  }

  get title(): string {
    return this._title;
  }

  get rows(): string {
    return this._rows;
  }

  get columns(): string {
    return this._columns;
  }
}
