
import { LineChartComponent } from '@swimlane/ngx-charts';

import * as dateformat from 'dateformat';
import * as shape from 'd3-shape';

import { Component, AfterViewInit, Injector, ViewChild } from '@angular/core';
import { AbstractDashboardCard } from '../abstract-dashboard-card';

import { Account, AccountUtils } from '../account';
import { AccountsUpdatedServiceGQL } from '../accounts-updated.service';
import { DashboardCard } from '../dashboard-card';
import { DashboardServiceGQL } from '../dashboard.service';


interface ItemSeries {
  name: string;
  series: Item[];
}

interface Item {
  name: number;
  value: number;
}

@Component({
  selector: 'accounts-performance-chart',
  templateUrl: './accounts-performance-chart.component.html',
  styleUrls: [ './accounts-performance-chart.component.scss' ]
})
export class AccountsPerformanceChartComponent extends AbstractDashboardCard implements AfterViewInit {

  @ViewChild('accountPerformanceLineChart') accountPerformanceLineChart: LineChartComponent;

  colorScheme = 'vivid';
  schemeType = 'ordinal';
  animationsEnabled = true;
  showLegend = true;
  gradientEnabled = false;

  showXAxis = true;
  showXAxisLabel = true;
  showYAxis = true;
  showYAxisLabel = true;

  xAxisLabel = 'Time';
  yAxisLabel = '% Gain/Loss';

  enableAutoScale = true;

  enableTimelineControl = true;

  showGridLines = true;

  curve = shape.curveLinear;

  // opacity of the shadow around the line indication the (optional) min and max values.
  // The range shadow is only displayed if min and max values are provided with the data.
  // The color of the shadow is always the color of the central line.
  rangeFillOpacity = 0.40;

  roundDomains = true;

  tooltipDisabled = false;

  private initialBalance: { totalUSD: number, totalCrypto: number, exchangeRateCrypto: number };

  accountsSeries: ItemSeries[];

  private subscribedToAccountUpdates = false;

  constructor(
    private injector: Injector,
    private dashboardServiceGQL: DashboardServiceGQL,
    private accountsUpdatedServiceGQL: AccountsUpdatedServiceGQL
    ) {
    super(
      injector.get(DashboardCard.metadata.NAME),
      injector.get(DashboardCard.metadata.TITLE),
      injector.get(DashboardCard.metadata.ROWS),
      injector.get(DashboardCard.metadata.COLUMNS)
    );

    this.dashboardServiceGQL
      .fetch()
      .subscribe(result => {
        this.initializeAccounts(AccountUtils.jsonAccountsListToAccountsList(result.data.dashboard.accounts));
      });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      if (this.accountsSeries && this.accountsSeries.length) {
        this.accountsSeries = [ ...this.accountsSeries ];
      }
    }, 30000);
  }

  initializeAccounts(accounts: Account.Accounts[]) {
    this.initialBalance = {
      totalUSD: accounts[0].totalUSD,
      totalCrypto: accounts[0].totalCrypto,
      exchangeRateCrypto: accounts[0].exchangeRateCrypto
    };

    this.accountsSeries = this.mapAccountsListToItemSeriesArray(accounts);

    if (! this.subscribedToAccountUpdates) {
      this.subscribeToAccountUpdates();
      this.subscribedToAccountUpdates = true;
    }
  }

  private subscribeToAccountUpdates() {
    this.accountsUpdatedServiceGQL.subscribe().subscribe(result => this.updateAccounts(result));
  }

  updateAccounts(accounts: any) {
    this.initializeAccounts(AccountUtils.jsonAccountsListToAccountsList(accounts.data['accountsUpdated']));
  }

  onLegendLabelClick(event) {
    console.log(`onLegendLabelClick(): event = ${JSON.stringify(event)}`);
  }

  select(event) {
    console.log(`select(): event = ${JSON.stringify(event)}`);
  }

  activate(event) {
    console.log(`activate(): event = ${JSON.stringify(event)}`);
  }

  deactivate(event) {
    console.log(`deactivate(): event = ${JSON.stringify(event)}`);
  }

  xAxisFormat(label) {
    // const [ , minutes ] = label.split(':');
    //
    // return +minutes % 10 ? ''  : label;

    const date = new Date(label);

    return dateformat(date, 'UTC:mm-dd HH:MM');
  }

  private accountsToAccountsChartItemName(accounts: Account.Accounts) {
    return accounts.time.getTime();
  }

  private mapAccountsToAccountItem(accounts: Account.Accounts): Item {
    const performancePercent = ((accounts.totalUSD / this.initialBalance.totalUSD) - 1) * 100;

    return { name: this.accountsToAccountsChartItemName(accounts), value: performancePercent };
  }

  private mapAccountsToVsBuyHoldItem(accounts: Account.Accounts): Item {
    const buyHoldPercent = ((accounts.exchangeRateCrypto / this.initialBalance.exchangeRateCrypto) - 1) * 100;
    const buyHoldDollar = this.initialBalance.totalUSD * (1 + buyHoldPercent / 100);

    const performanceVsBuyHold = ((accounts.totalUSD / buyHoldDollar) - 1) * 100;

    return { name: this.accountsToAccountsChartItemName(accounts), value: performanceVsBuyHold };
  }

  private mapAccountsListToItemSeriesArray(accountsList: Account.Accounts[]): ItemSeries[] {
    const accountPerformanceSeries = accountsList.map(accounts => this.mapAccountsToAccountItem(accounts));
    const accountVsBuyHoldSeries = accountsList.map(accounts => this.mapAccountsToVsBuyHoldItem(accounts));

    const zeroSeries = [];

    for (let i = 0; i < accountPerformanceSeries.length; i++) {
      zeroSeries.push({ name: accountPerformanceSeries[i].name, value: 0 });
    }

    return [
      { name: 'Account', series: accountPerformanceSeries },
      { name: 'vs Buy & Hold', series: accountVsBuyHoldSeries },
      { name: 'Zero', series: zeroSeries }
    ];
  }

  formatDate(dateInMillis: any) {
    const dateStr = dateformat(new Date(dateInMillis), 'UTC:mm-dd HH:MM');
    return dateStr + ' UTC';
  }

  stringToFixedNumber(value: string, digits: number) {
    const numericValue = '' + parseFloat((+value).toFixed(digits));

    return numericValue.includes('e') ? 0 : numericValue;
  }
}


