
import { NgModule } from '@angular/core';

import { NgxChartsModule } from '@swimlane/ngx-charts';

import { SharedModule } from '../../shared/shared.module';

import { AccountsPerformanceChartComponent } from './accounts-performance-chart.component';


@NgModule({
  declarations: [
    AccountsPerformanceChartComponent
  ],
  exports: [ AccountsPerformanceChartComponent ],
  // providers: [ ],
  imports: [
    NgxChartsModule,
    SharedModule
  ]
})
export class AccountsPerformanceChartModule { }
