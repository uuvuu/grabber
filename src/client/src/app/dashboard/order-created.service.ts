
import gql from 'graphql-tag';

import { Injectable } from '@angular/core';

import { Subscription } from 'apollo-angular';

import { Order } from './order';


@Injectable({
  providedIn: 'root',
})
export class OrderCreatedServiceGQL extends Subscription<Order.Order> {
  document = gql`
    subscription orderCreated {
      orderCreated {
          _id
          
          orderId
      
          product
        
          price
          size
      
          side
      
          stp
      
          funds
          specifiedFunds
          
          type
      
          timeInForce
      
          postOnly
      
          createdTime
          expireTime
          doneTime
          candleTime
      
          doneReason
          rejectedReason
      
          fillFees
          filledSize
      
          executedValue
      
          status
      
          settled
      
          clearanceStatus
      }
    }
  `;
}
