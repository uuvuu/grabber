
import { NgModule } from '@angular/core';

import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../shared/shared.module';

import {
  CurrentAccountCardComponent,
  LastFilledOrderCardComponent,
  MaxAccountCardComponent,
  MinAccountCardComponent,
  OrderSuccessRateCardComponent,
  StartingAccountCardComponent,
  Order30DVolumeCardComponent
} from './status-card.component';


@NgModule({
  declarations: [
    StartingAccountCardComponent, CurrentAccountCardComponent, MinAccountCardComponent, MaxAccountCardComponent,
    LastFilledOrderCardComponent, OrderSuccessRateCardComponent, Order30DVolumeCardComponent
  ],
  exports: [
    StartingAccountCardComponent, CurrentAccountCardComponent, MinAccountCardComponent, MaxAccountCardComponent,
    LastFilledOrderCardComponent, OrderSuccessRateCardComponent, Order30DVolumeCardComponent
  ],
  // providers: [ ],
  imports: [
    NgxChartsModule,
    SharedModule
  ]
})
export class StatusCardModule { }
