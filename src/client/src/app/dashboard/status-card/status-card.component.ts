
import * as dateformat from 'dateformat';

import { Component, Injector } from '@angular/core';
import { AbstractDashboardCard } from '../abstract-dashboard-card';
import { AccountUtils } from '../account';
import { AccountsUpdatedServiceGQL } from '../accounts-updated.service';
import { DashboardCard } from '../dashboard-card';

import { DashboardServiceGQL } from '../dashboard.service';
import { OrderUtils } from '../order';
import { StatusCard, StatusCardUtils } from '../status-card';
import { StatusCardsUpdatedServiceGQL } from '../status-cards-updated.service';


abstract class StatusCardComponent extends AbstractDashboardCard {
  protected statusCards: StatusCard.StatusCard[];

  initialized = false;

  columnsToDisplay = [ 'name', 'value' ];

  private subscribedToStatusCardUpdates = false;

  protected constructor(
    private injector: Injector,
    private dashboardServiceGQL: DashboardServiceGQL,
    private statusCardsUpdatedServiceGQL: StatusCardsUpdatedServiceGQL) {

    super(
      injector.get(DashboardCard.metadata.NAME),
      injector.get(DashboardCard.metadata.TITLE),
      injector.get(DashboardCard.metadata.ROWS),
      injector.get(DashboardCard.metadata.COLUMNS)
    );

    this.dashboardServiceGQL
      .fetch()
      .subscribe(result => {
        this.initializeCardData(StatusCardUtils.jsonStatusCardsToStatusCards(result.data.dashboard.cards));

        this.initialized = true;
      });
  }

  initializeCardData(statusCards: StatusCard.StatusCard[]) {
    this.statusCards = statusCards;

    if (! this.subscribedToStatusCardUpdates) {
      this.subscribeToStatusCardUpdates();
      this.subscribedToStatusCardUpdates = true;
    }
  }

  private subscribeToStatusCardUpdates() {
    this.statusCardsUpdatedServiceGQL.subscribe().subscribe(result => this.updateStatusCards(result));
  }

  protected updateStatusCards(statusCards: any) {
    this.initializeCardData(StatusCardUtils.jsonStatusCardsToStatusCards(statusCards.data['statusCardsUpdated']));
  }

  protected abstract get cardIndex(): number;

  abstract statusCardToRows();


  protected stringToFixedNumber(value: string) {
    const numericValue = '' + parseFloat((+value).toFixed(12));

    return numericValue.includes('e') ? 0 : numericValue;
  }
}

abstract class AccountCardComponent extends StatusCardComponent {
  protected constructor(
    injector: Injector,
    dashboardServiceGQL: DashboardServiceGQL,
    statusCardsUpdatedServiceGQL: StatusCardsUpdatedServiceGQL) {

    super(injector, dashboardServiceGQL, statusCardsUpdatedServiceGQL);
  }

  protected getAccountForCurrency(currencyName: string) {
    const accounts = this.statusCards[this.cardIndex].data.accounts.filter(account => account.currency.toUpperCase() === currencyName.toUpperCase());

    return accounts[0];
  }

  statusCardToRows() {
    if (! this.initialized) {
      return;
    }

    const cardData = this.statusCards[this.cardIndex];

    const date = new Date(cardData.data.time);

    return [
      { name: 'Date', value: dateformat(date, 'isoUtcDateTime') },
      { name: 'BTC Price', value: this.stringToFixedNumber(cardData.data.exchangeRateCrypto) },
      { name: 'USD', value: this.stringToFixedNumber(this.getAccountForCurrency('USD').balance) },
      { name: 'BTC', value: this.stringToFixedNumber(this.getAccountForCurrency('BTC').balance) },
      { name: 'Total USD', value: this.stringToFixedNumber(cardData.data.totalUSD) }
    ];
  }
}

@Component({
  selector: 'status-card',
  templateUrl: './status-card.component.html',
  styleUrls: ['./status-card.component.scss']
})
export class StartingAccountCardComponent extends AccountCardComponent {
  constructor(
    injector: Injector,
    dashboardServiceGQL: DashboardServiceGQL,
    statusCardsUpdatedServiceGQL: StatusCardsUpdatedServiceGQL) {

    super(injector, dashboardServiceGQL, statusCardsUpdatedServiceGQL);
  }

  get cardIndex() {
    return 0;
  }
}

@Component({
  selector: 'status-card',
  templateUrl: './status-card.component.html',
  styleUrls: ['./status-card.component.scss']
})
export class CurrentAccountCardComponent extends AccountCardComponent {
  constructor(
    injector: Injector,
    dashboardServiceGQL: DashboardServiceGQL,
    statusCardsUpdatedServiceGQL: StatusCardsUpdatedServiceGQL) {

    super(injector, dashboardServiceGQL, statusCardsUpdatedServiceGQL);
  }

  get cardIndex() {
    return 1;
  }
}

@Component({
  selector: 'status-card',
  templateUrl: './status-card.component.html',
  styleUrls: ['./status-card.component.scss']
})
export class MinAccountCardComponent extends AccountCardComponent {
  constructor(
    injector: Injector,
    dashboardServiceGQL: DashboardServiceGQL,
    statusCardsUpdatedServiceGQL: StatusCardsUpdatedServiceGQL) {

    super(injector, dashboardServiceGQL, statusCardsUpdatedServiceGQL);
  }

  get cardIndex() {
    return 2;
  }
}

@Component({
  selector: 'status-card',
  templateUrl: './status-card.component.html',
  styleUrls: ['./status-card.component.scss']
})
export class MaxAccountCardComponent extends AccountCardComponent {
  constructor(
    injector: Injector,
    dashboardServiceGQL: DashboardServiceGQL,
    statusCardsUpdatedServiceGQL: StatusCardsUpdatedServiceGQL) {

    super(injector, dashboardServiceGQL, statusCardsUpdatedServiceGQL);
  }

  get cardIndex() {
    return 3;
  }
}

@Component({
  selector: 'status-card',
  templateUrl: './status-card.component.html',
  styleUrls: ['./status-card.component.scss']
})
export class LastFilledOrderCardComponent extends StatusCardComponent {
  constructor(
    injector: Injector,
    dashboardServiceGQL: DashboardServiceGQL,
    statusCardsUpdatedServiceGQL: StatusCardsUpdatedServiceGQL) {

    super(injector, dashboardServiceGQL, statusCardsUpdatedServiceGQL);
  }

  get cardIndex() {
    return 4;
  }

  statusCardToRows() {
    if (! this.initialized) {
      return;
    }

    const cardData = this.statusCards[this.cardIndex];

    const date = new Date(cardData.data.createdTime);

    return [
      { name: 'Date', value: dateformat(date, 'isoUtcDateTime') },
      { name: 'Order Id', value: OrderUtils.shortenOrderId(cardData.data.orderId) },
      { name: 'Side', value: cardData.data.side },
      { name: 'Price', value: this.stringToFixedNumber(cardData.data.price) },
      { name: 'Size', value: this.stringToFixedNumber(cardData.data.size) },
      { name: 'Filled Size', value: this.stringToFixedNumber(cardData.data.filledSize) },
      { name: 'Filled Value', value: this.stringToFixedNumber(cardData.data.executedValue) }
    ];
  }
}

@Component({
  selector: 'status-card',
  templateUrl: './status-card.component.html',
  styleUrls: ['./status-card.component.scss']
})
export class OrderSuccessRateCardComponent extends StatusCardComponent {
  constructor(
    injector: Injector,
    dashboardServiceGQL: DashboardServiceGQL,
    statusCardsUpdatedServiceGQL: StatusCardsUpdatedServiceGQL) {

    super(injector, dashboardServiceGQL, statusCardsUpdatedServiceGQL);
  }

  get cardIndex() {
    return 5;
  }

  statusCardToRows() {
    if (! this.initialized) {
      return;
    }

    const cardData = this.statusCards[this.cardIndex];

    return [
      { name: 'Accepted Orders', value: cardData.data.numAcceptedOrders },
      { name: 'Filled Orders', value: cardData.data.numFilledOrders },
      { name: 'Success Rate', value: this.stringToFixedNumber(cardData.data.successRate) }
    ];
  }
}

@Component({
  selector: 'status-card',
  templateUrl: './status-card.component.html',
  styleUrls: ['./status-card.component.scss']
})
export class Order30DVolumeCardComponent extends StatusCardComponent {
  constructor(
    injector: Injector,
    dashboardServiceGQL: DashboardServiceGQL,
    statusCardsUpdatedServiceGQL: StatusCardsUpdatedServiceGQL) {

    super(injector, dashboardServiceGQL, statusCardsUpdatedServiceGQL);
  }

  get cardIndex() {
    return 6;
  }

  statusCardToRows() {
    if (! this.initialized) {
      return;
    }

    const cardData = this.statusCards[this.cardIndex];

    return [
      { name: 'USD Volume', value: this.stringToFixedNumber(cardData.data.usdVolume) },
      { name: 'BTC Volume', value: this.stringToFixedNumber(cardData.data.btcVolume) },
      { name: 'Filled Orders', value: cardData.data.numFilledOrders }
    ];
  }
}
