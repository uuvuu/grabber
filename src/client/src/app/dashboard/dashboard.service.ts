
import gql from 'graphql-tag';

import { Injectable } from '@angular/core';

import { Query } from 'apollo-angular';

import { Account } from './account';
import { Order } from './order';
import { StatusCard } from './status-card';


export interface DashboardResponse {
  dashboard: Dashboard;
}

export interface Dashboard {
  cards: StatusCard.StatusCard[];
  accounts: Account.Accounts[];
  orders: Order.Order[];
}


@Injectable({
  providedIn: 'root',
})
export class DashboardServiceGQL extends Query<DashboardResponse> {
  document = gql`
    query dashboard {
      dashboard {
        cards {
          name
          
          type
          
          data
        }
        
        accounts {
          _id
          
          time
          
          profileId
          
          totalUSD
          totalCrypto
          
          exchangeRateCrypto
          
          accounts {
            accountId
            
            currency
            
            balance
            available
            hold
            
            exchangeRateUSD
            exchangeRateCrypto
          }
        }
        
        orders {
          _id
            
          orderId
        
          #product
          
          price
          size
        
          side
        
          #stp
        
          #funds
          #specifiedFunds
            
          #type
        
          #timeInForce
        
          #postOnly
      
          createdTime
          #expireTime
          #doneTime
          #candleTime
      
          #doneReason
          #rejectedReason
      
          #fillFees
          filledSize
      
          executedValue
      
          status
      
          #settled
      
          clearanceStatus
        }
      }
    }
  `;
}

