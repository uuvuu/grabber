
import { NgModule } from '@angular/core';

import { FlexLayoutModule } from '@angular/flex-layout';

import { SharedModule } from '../shared/shared.module';
import { AccountValuesChartComponent } from './account-values/account-values-chart.component';
import { AccountValuesChartModule } from './account-values/account-values-chart.module';

import {
  CurrentAccountCardComponent,
  LastFilledOrderCardComponent,
  MaxAccountCardComponent,
  MinAccountCardComponent,
  OrderSuccessRateCardComponent,
  StartingAccountCardComponent,
  Order30DVolumeCardComponent
} from './status-card/status-card.component';
import { StatusCardModule } from './status-card/status-card.module';

import { AccountsPerformanceChartComponent } from './accounts-performance/accounts-performance-chart.component';
import { AccountsPerformanceChartModule } from './accounts-performance/accounts-performance-chart.module';

import { OrderChartComponent } from './order-chart/order-chart.component';
import { OrderChartModule } from './order-chart/order-chart.module';

import { DashboardCardSpawnerComponent } from './dashboard-card-spawner.component';
import { DashboardCardsService } from './dashboard-cards.service';

import { DashboardComponent } from './dashboard.component';


@NgModule({
  declarations: [
    DashboardComponent,
    DashboardCardSpawnerComponent
  ],
  exports: [ DashboardComponent, DashboardCardSpawnerComponent ],
  providers: [ DashboardCardsService ],
  imports: [
    FlexLayoutModule,

    SharedModule,

    AccountValuesChartModule,
    AccountsPerformanceChartModule,
    OrderChartModule,

    StatusCardModule,
  ],
  entryComponents: [
    AccountValuesChartComponent,
    AccountsPerformanceChartComponent,
    OrderChartComponent,

    StartingAccountCardComponent,
    CurrentAccountCardComponent,
    MinAccountCardComponent,
    MaxAccountCardComponent,
    LastFilledOrderCardComponent,
    OrderSuccessRateCardComponent,
    Order30DVolumeCardComponent
  ],
})
export class DashboardModule { }
