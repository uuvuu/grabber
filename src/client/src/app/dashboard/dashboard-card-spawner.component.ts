
import { Component, ComponentFactoryResolver, Injector, Input, SkipSelf, ViewChild, ViewContainerRef } from '@angular/core';

import { DashboardCard } from './dashboard-card';


@Component({
  selector: 'app-dashboard-card-spawner',
  templateUrl: './dashboard-card-spawner.component.html',
  styleUrls: [ './dashboard-card-spawner.component.scss' ]
})
export class DashboardCardSpawnerComponent {

  @ViewChild('spawn', { read: ViewContainerRef }) container: ViewContainerRef;

  constructor(@SkipSelf() private parentInjector: Injector, private resolver: ComponentFactoryResolver) {
  }

  @Input()
  set card(data: DashboardCard) {
    if (! data) {
      return;
    }

    const inputProviders = Object.keys(data.inputs).map((inputName) => {
      return {
        provide: data.inputs[inputName].key,
        useValue: data.inputs[inputName].value,
        deps: []
      };
    });

    // inputProviders.concat(data.services);

    const injector = Injector.create({
        providers: inputProviders,
        parent: this.parentInjector
    });

    const factory = this.resolver.resolveComponentFactory(data.component);
    const component = factory.create(injector);

    this.container.insert(component.hostView);
  }
}
