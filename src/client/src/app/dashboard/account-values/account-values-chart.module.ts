
import { NgModule } from '@angular/core';

import { NgxChartsModule } from '@swimlane/ngx-charts';

import { SharedModule } from '../../shared/shared.module';

import { AccountValuesChartComponent } from './account-values-chart.component';


@NgModule({
  declarations: [
    AccountValuesChartComponent
  ],
  exports: [ AccountValuesChartComponent ],
  // providers: [ ],
  imports: [
    NgxChartsModule,
    SharedModule
  ]
})
export class AccountValuesChartModule { }
