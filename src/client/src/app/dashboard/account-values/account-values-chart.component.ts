

import { Component, AfterViewInit, Injector, Input } from '@angular/core';
import { AbstractDashboardCard } from '../abstract-dashboard-card';

import { DashboardCard } from '../dashboard-card';
import { DashboardServiceGQL } from '../dashboard.service';
import { OrderUtils } from '../order';
import { StatusCard, StatusCardUtils } from '../status-card';


interface ItemSeries {
  name: string;
  series: Item[];
}

interface Item {
  name: string;
  value: number;
}

@Component({
  selector: 'account-values-chart',
  templateUrl: './account-values-chart.component.html',
  styleUrls: [ './account-values-chart.component.scss' ]
})
export class AccountValuesChartComponent extends AbstractDashboardCard implements AfterViewInit {
  colorScheme = 'vivid';
  schemeType = 'ordinal';
  animationsEnabled = true;
  showLegend = true;
  gradientEnabled = false;
  showDataLabel = true;
  barPadding = 10;

  showXAxis = true;
  showXAxisLabel = false;
  showYAxis = true;
  showYAxisLabel = false;

  xAxisLabel = 'Account';
  yAxisLabel = 'Value';

  showGridLines = true;

  roundDomains = true;

  tooltipDisabled = false;

  accountSeries: ItemSeries[];
  readonly accountMap: Map<string, StatusCard.StatusCard> = new Map<string, StatusCard.StatusCard>();

  constructor(private injector: Injector, private dashboardServiceGQL: DashboardServiceGQL) {
    super(
      injector.get(DashboardCard.metadata.NAME),
      injector.get(DashboardCard.metadata.TITLE),
      injector.get(DashboardCard.metadata.ROWS),
      injector.get(DashboardCard.metadata.COLUMNS)
    );

    this.dashboardServiceGQL
      .fetch()
      .subscribe(result => {
        this.initializeAccounts(StatusCardUtils.jsonStatusCardsToStatusCards(result.data.dashboard.cards));
      });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      if (this.accountSeries && this.accountSeries.length) {
        this.accountSeries = [ ...this.accountSeries ];
      }
    }, 30000);
  }

  initializeAccounts(accounts: StatusCard.StatusCard[]) {
    accounts = accounts.filter(account => account.type.endsWith('account-value'));

    this.initializeAccountMap(accounts);
    this.accountSeries = this.mapAccountsToItemSeriesArray(accounts);
  }

  initializeAccountMap(accounts: StatusCard.StatusCard[]) {
    accounts.forEach(account => this.addAccountToMap(account));
  }

  private addAccountToMap(account: StatusCard.StatusCard) {
    this.accountMap.set(account.name, account);
  }

    // add to zeroSeries as well

    // const newAccounts = AccountUtils.jsonAccountsToAccounts(accounts.data['accountsCreated']);
    //
    // // let orderSeries = this.orderSeries[0].series;
    // // console.log(`BEGIN: orderSeries.length = ${orderSeries.length}`);
    // // console.log(`BEGIN: orderSeries[0].time = ${new Date(orderSeries[0].name).toISOString()}`);
    // // console.log(`BEGIN: orderSeries[1].time = ${new Date(orderSeries[1].name).toISOString()}`);
    // // console.log(`BEGIN: orderSeries[len - 2].time = ${new Date(orderSeries[orderSeries.length - 2].name).toISOString()}`);
    // // console.log(`BEGIN: orderSeries[len - 1].time = ${new Date(orderSeries[orderSeries.length - 1].name).toISOString()}`);
    //
    // this.accountsSeries[0].series.push(this.mapAccountsToAccountItem(newAccounts));
    //
    // if (this.accountsSeries[0].series.length > this.startTimeMinutesBefore) {
    //   const accounts = this.accountsSeries[0].series.shift();
    //
    //   this.initialBalance = { totalUSD: accounts[0].totalUSD, totalCrypto: accounts[0].totalCrypto };
    // }
    //
    // this.accountsSeries = [ ...this.accountsSeries ];
    //
    // // orderSeries = this.orderSeries[0].series;
    // // console.log(`END: orderSeries.length = ${orderSeries.length}`);
    // // console.log(`END: orderSeries[0].time = ${new Date(orderSeries[0].name).toISOString()}`);
    // // console.log(`END: orderSeries[1].time = ${new Date(orderSeries[1].name).toISOString()}`);
    // // console.log(`END: orderSeries[len - 2].time = ${new Date(orderSeries[orderSeries.length - 2].name).toISOString()}`);
    // // console.log(`END: orderSeries[len - 1].time = ${new Date(orderSeries[orderSeries.length - 1].name).toISOString()}`);

  onLegendLabelClick(event) {
    console.log(`onLegendLabelClick(): event = ${JSON.stringify(event)}`);
  }

  select(event) {
    console.log(`select(): event = ${JSON.stringify(event)}`);
  }

  activate(event) {
    console.log(`activate(): event = ${JSON.stringify(event)}`);
  }

  deactivate(event) {
    console.log(`deactivate(): event = ${JSON.stringify(event)}`);
  }

  accountDetails(model) {
    return this.accountMap.get(model.series);
  }

  columnsToDisplay = [ 'name', 'value' ];

  accountToRows(model) {
    if (! model) {
      return;
    }

    const account = this.accountMap.get(model.series);
    const usdAccount = account.data.accounts.find(account => account.currency === 'USD');
    const btcAccount = account.data.accounts.find(account => account.currency === 'BTC');

    const rows = [
      { name: 'Total in USD', value: account.data.totalUSD },
      { name: 'Total in BTC', value: account.data.totalCrypto },
      { name: 'USD Balance', value: usdAccount.balance },
      { name: 'USD Rate', value: btcAccount.exchangeRateUSD },
      { name: 'BTC Balance', value: btcAccount.balance },
      { name: 'BTC Rate', value: usdAccount.exchangeRateCrypto }
    ];

    return rows;
  }

  private mapAccountsToItemSeriesArray(accounts: StatusCard.StatusCard[]): ItemSeries[] {
    return accounts.map(account => {
      return {
        name: account.name,
        series: this.mapAccountToItemSeries(account)
      };
    });
  }

  private mapAccountToItemSeries(account: StatusCard.StatusCard) {
    return account.data.accounts.map(account => {
      return { name: account.currency, value: +account.balance * +account.exchangeRateUSD};
    });
  }
}


