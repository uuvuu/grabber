
import { Component, OnInit } from '@angular/core';
import { ObservableMedia } from '@angular/flex-layout';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { AccountValuesChartComponent } from './account-values/account-values-chart.component';

import { AccountsPerformanceChartComponent } from './accounts-performance/accounts-performance-chart.component';
import { DashboardServiceGQL } from './dashboard.service';
import { OrderChartComponent } from './order-chart/order-chart.component';
import {
  CurrentAccountCardComponent,
  LastFilledOrderCardComponent,
  MaxAccountCardComponent,
  MinAccountCardComponent,
  OrderSuccessRateCardComponent,
  StartingAccountCardComponent,
  Order30DVolumeCardComponent
} from './status-card/status-card.component';

import { DashboardCard, DashboardCardProperties } from './dashboard-card';
import { DashboardCardsService } from './dashboard-cards.service';


// the implementation of this responsive dashboard was entirely inspired by:
// https://medium.com/@nima_ap/creating-a-responsive-dashboard-in-angular-5-from-scratch-147f6a493d9e
// https://github.com/nima200/angular-dashboard/tree/master/src/app/dashboard

  
@Component({
   selector: 'app-dashboard',
   templateUrl: './dashboard.component.html',
   styleUrls: [ './dashboard.component.scss' ]
})
export class DashboardComponent implements OnInit {
  statusCards: DashboardCard[] = [];
  chartCards: DashboardCard[] = [];

  spanSize: Observable<number>;

  gridSpanSizeLarge: Observable<number>;
  gridSpanSizeMedium: Observable<number>;
  gridSpanSizeSmall: Observable<number>;

  initialized = false;

  constructor(
    private observableMedia: ObservableMedia,
    private cardsService: DashboardCardsService,
    private dashboardServiceGQL: DashboardServiceGQL) {

    this.cardsService.statusCards.subscribe(cards => {
      this.statusCards = cards;
    });

    this.cardsService.chartCards.subscribe(cards => {
      this.chartCards = cards;
    });
  }

  ngOnInit() {
    this.dashboardServiceGQL
      .fetch()
      .subscribe(() => this.initialized = true);

    const gridMap = new Map([
      [ 'xs', 1 ],
      [ 'sm', 2 ],
      [ 'md', 12 ],
      [ 'lg', 18 ],
      [ 'xl', 18 ]
    ]);

    const gridMapLarge = new Map([
      [ 'xs', 1 ],
      [ 'sm', 2 ],
      [ 'md', 8 ],
      [ 'lg', 14 ],
      [ 'xl', 14 ]
    ]);

    const gridMapMedium = new Map([
      [ 'xs', 1 ],
      [ 'sm', 2 ],
      [ 'md', 2 ],
      [ 'lg', 4 ],
      [ 'xl', 4 ]
    ]);

    const gridMapSmall = new Map([
      [ 'xs', 1 ],
      [ 'sm', 1 ],
      [ 'md', 2 ],
      [ 'lg', 2 ],
      [ 'xl', 2 ]
    ]);

    let startColumns = gridMap['xl'];
    let startColumnsLarge = gridMapLarge['xl'];
    let startColumnsMedium = gridMapMedium['xl'];
    let startColumnsSmall = gridMapSmall['xl'];

    gridMap.forEach((columns, mediaQueryAlias) => {
      if (this.observableMedia.isActive(mediaQueryAlias)) {
        startColumns = columns;
      }
    });

    gridMapLarge.forEach((columns, mediaQueryAlias) => {
      if (this.observableMedia.isActive(mediaQueryAlias)) {
        startColumnsLarge = columns;
      }
    });

    gridMapMedium.forEach((columns, mediaQueryAlias) => {
      if (this.observableMedia.isActive(mediaQueryAlias)) {
        startColumnsMedium = columns;
      }
    });

    gridMapSmall.forEach((columns, mediaQueryAlias) => {
      if (this.observableMedia.isActive(mediaQueryAlias)) {
        startColumnsSmall = columns;
      }
    });

    this.spanSize = this.observableMedia.asObservable()
      .pipe(map(change => gridMap.get(change.mqAlias)))
      .pipe(startWith(startColumns));

    this.gridSpanSizeLarge = this.observableMedia.asObservable()
      .pipe(map(change => gridMapLarge.get(change.mqAlias)))
      .pipe(startWith(startColumnsLarge));

    this.gridSpanSizeMedium = this.observableMedia.asObservable()
      .pipe(map(change => gridMapMedium.get(change.mqAlias)))
      .pipe(startWith(startColumnsMedium));

    this.gridSpanSizeSmall = this.observableMedia.asObservable()
      .pipe(map(change => gridMapSmall.get(change.mqAlias)))
      .pipe(startWith(startColumnsSmall));

    this.createCards();
  }

  createCards(): void {
    // let cardProperties;
    // let card;
    //
    // cardProperties = {
    //   name: { key: DashboardCard.metadata.NAME, value: 'account-values-chart' },
    //   title: { key: DashboardCard.metadata.TITLE, value: 'Account Values' },
    //
    //   rows: { key: DashboardCard.metadata.ROWS, value: this.gridSpanSizeMedium },
    //   columns: { key: DashboardCard.metadata.COLUMNS, value: this.gridSpanSizeLarge }
    // };
    //
    // card = new DashboardCard(cardProperties, AccountValuesChartComponent);
    //
    // this.cardsService.addChartCard(card);

    let cardProperties: DashboardCardProperties = {
      name: { key: DashboardCard.metadata.NAME, value: 'starting-account-value' },
      title: { key: DashboardCard.metadata.TITLE, value: 'Starting Account' },

      rows: { key: DashboardCard.metadata.ROWS, value: this.gridSpanSizeSmall },
      columns: { key: DashboardCard.metadata.COLUMNS, value: this.gridSpanSizeSmall }
    };

    let card = new DashboardCard(cardProperties, StartingAccountCardComponent);

    this.cardsService.addStatusCard(card);

    cardProperties = {
      name: { key: DashboardCard.metadata.NAME, value: 'current-account-value' },
      title: { key: DashboardCard.metadata.TITLE, value: 'Current Account' },

      rows: { key: DashboardCard.metadata.ROWS, value: this.gridSpanSizeSmall },
      columns: { key: DashboardCard.metadata.COLUMNS, value: this.gridSpanSizeSmall }
    };

    card = new DashboardCard(cardProperties, CurrentAccountCardComponent);

    this.cardsService.addStatusCard(card);

    cardProperties = {
      name: { key: DashboardCard.metadata.NAME, value: 'min-account-value' },
      title: { key: DashboardCard.metadata.TITLE, value: 'Minimum Account' },

      rows: { key: DashboardCard.metadata.ROWS, value: this.gridSpanSizeSmall },
      columns: { key: DashboardCard.metadata.COLUMNS, value: this.gridSpanSizeSmall }
    };

    card = new DashboardCard(cardProperties, MinAccountCardComponent);

    this.cardsService.addStatusCard(card);

    cardProperties = {
      name: { key: DashboardCard.metadata.NAME, value: 'max-account-value' },
      title: { key: DashboardCard.metadata.TITLE, value: 'Maximum Account' },

      rows: { key: DashboardCard.metadata.ROWS, value: this.gridSpanSizeSmall },
      columns: { key: DashboardCard.metadata.COLUMNS, value: this.gridSpanSizeSmall }
    };

    card = new DashboardCard(cardProperties, MaxAccountCardComponent);

    this.cardsService.addStatusCard(card);

    cardProperties = {
      name: { key: DashboardCard.metadata.NAME, value: 'last-filled-order' },
      title: { key: DashboardCard.metadata.TITLE, value: 'Last Filled Order' },

      rows: { key: DashboardCard.metadata.ROWS, value: this.gridSpanSizeSmall },
      columns: { key: DashboardCard.metadata.COLUMNS, value: this.gridSpanSizeSmall }
    };

    card = new DashboardCard(cardProperties, LastFilledOrderCardComponent);

    this.cardsService.addStatusCard(card);

    cardProperties = {
      name: { key: DashboardCard.metadata.NAME, value: 'order-success-rate' },
      title: { key: DashboardCard.metadata.TITLE, value: 'Order Success Rate' },

      rows: { key: DashboardCard.metadata.ROWS, value: this.gridSpanSizeSmall },
      columns: { key: DashboardCard.metadata.COLUMNS, value: this.gridSpanSizeSmall }
    };

    card = new DashboardCard(cardProperties, OrderSuccessRateCardComponent);

    this.cardsService.addStatusCard(card);

    cardProperties = {
      name: { key: DashboardCard.metadata.NAME, value: 'order-30d-volume' },
      title: { key: DashboardCard.metadata.TITLE, value: 'Order 30 Day Volume' },

      rows: { key: DashboardCard.metadata.ROWS, value: this.gridSpanSizeSmall },
      columns: { key: DashboardCard.metadata.COLUMNS, value: this.gridSpanSizeSmall }
    };

    card = new DashboardCard(cardProperties, Order30DVolumeCardComponent);

    this.cardsService.addStatusCard(card);

    cardProperties = {
      name: { key: DashboardCard.metadata.NAME, value: 'accounts-performance-chart' },
      title: { key: DashboardCard.metadata.TITLE, value: 'Account Performance' },

      rows: { key: DashboardCard.metadata.ROWS, value: this.gridSpanSizeMedium },
      columns: { key: DashboardCard.metadata.COLUMNS, value: this.gridSpanSizeLarge }
    };

    card = new DashboardCard(cardProperties, AccountsPerformanceChartComponent);

    this.cardsService.addChartCard(card);

    cardProperties = {
      name: { key: DashboardCard.metadata.NAME, value: 'orders-chart' },
      title: { key: DashboardCard.metadata.TITLE, value: 'Orders' },

      rows: { key: DashboardCard.metadata.ROWS, value: this.gridSpanSizeMedium },
      columns: { key: DashboardCard.metadata.COLUMNS, value: this.gridSpanSizeLarge }
    };

    card = new DashboardCard(cardProperties, OrderChartComponent);

    this.cardsService.addChartCard(card);
  }
}
