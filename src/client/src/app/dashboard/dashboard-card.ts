
import { InjectionToken } from '@angular/core';

import { Observable } from 'rxjs';


type DashboardCardProperties = {
  name: {
    key: InjectionToken<string>,
    value: string
  },
  title: {
    key: InjectionToken<string>,
    value: string
  },

  rows: {
    key: InjectionToken<number>,
    value: Observable<number>
  }
  columns: {
    key: InjectionToken<number>,
    value: Observable<number>
  }
};


class DashboardCard {
  static metadata: any = {
    NAME: new InjectionToken<string>('name'),
    TITLE: new InjectionToken<string>('title'),

    ROWS: new InjectionToken<number>('rows'),
    COLUMNS: new InjectionToken<number>('columns')
  };

  constructor(
    private _input: DashboardCardProperties,
    private _component: any,
    /* private _services: { provide: any, useClass: any, deps: any[] }[] */) {
  }

  get inputs(): any {
    return this._input;
  }

  get component(): any {
    return this._component;
  }

  // get services(): any {
  //  return this._services;
  // }
}


export { DashboardCard, DashboardCardProperties };
