
import gql from 'graphql-tag';

import { Injectable } from '@angular/core';

import { Subscription } from 'apollo-angular';

import { Account } from './account';


@Injectable({
  providedIn: 'root',
})
export class AccountsUpdatedServiceGQL extends Subscription<Account.Accounts> {
  document = gql`
    subscription accountsUpdated {
      accountsUpdated {
        _id
        
        time
        
        profileId
        
        totalUSD
        totalCrypto
        
        exchangeRateCrypto
        
        accounts {
          accountId
          
          currency
          
          balance
          available
          hold
          
          exchangeRateUSD
          exchangeRateCrypto
        }
      }
    }
  `;
}
