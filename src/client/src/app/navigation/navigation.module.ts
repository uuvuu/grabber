
import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';

import { NavigationComponent } from './navigation.component';


@NgModule({
  declarations: [
    NavigationComponent
  ],
  exports: [ NavigationComponent ],
  // providers: [ ],
  imports: [
    SharedModule
  ]
})
export class NavigationModule { }
