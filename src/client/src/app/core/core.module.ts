
import { NgModule, Optional, SkipSelf } from '@angular/core';
// import { CommonModule } from '@angular/common';

// import { LoggerService } from './logger.service';
import { GraphQLModule } from './graphql.module';
import { throwIfAlreadyLoaded } from './module-import-guard';
// import { NavComponent } from './nav/nav.component';
// import { SpinnerComponent } from './spinner/spinner.component';
// import { SpinnerService } from './spinner/spinner.service';


@NgModule({
  // declarations: [ NavComponent, SpinnerComponent ],
  // exports: [ NavComponent, SpinnerComponent ]
  // providers: [ LoggerService, SpinnerService ],
  imports: [
    // CommonModule, /* we use ngFor */

    GraphQLModule
  ]
})
export class CoreModule {
  constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}

