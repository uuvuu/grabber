
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { Apollo, ApolloModule } from 'apollo-angular';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { split } from 'apollo-link';
import { HttpLink, HttpLinkModule } from 'apollo-angular-link-http';
import { WebSocketLink } from 'apollo-link-ws';

import { getMainDefinition } from 'apollo-utilities';
import { OperationDefinitionNode } from 'graphql';

import { environment } from '../../environments/environment';


const httpUri = `${environment.httpUri}/graphql`;
const wsUri = `${environment.wsUri}/graphql`;


@NgModule({
  imports: [
    HttpClientModule,

    ApolloModule,
    HttpLinkModule
  ]
})
export class GraphQLModule {
  constructor(apollo: Apollo, httpClient: HttpClient) {
    const httpLink = new HttpLink(httpClient).create({ uri: httpUri });

    const subscriptionLink = new WebSocketLink({ uri: wsUri, options: { reconnect: true }});

    const link = split(
      ({ query }) => {
        const { kind, operation } = getMainDefinition(query) as OperationDefinitionNode;

        return kind === 'OperationDefinition' && operation === 'subscription';
      },
      subscriptionLink,
      httpLink
    );

    apollo.create({ link, cache: new InMemoryCache() });
  }
}

