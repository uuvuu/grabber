
import { retry } from '../retry';


describe('retry', () => {
  const mockLogger = { log: jest.fn() };

  test('retry succeeds on first try', async () => {
    expect.assertions(3);

    const returnValue = 5;

    const operation = jest.fn().mockResolvedValue(returnValue);

    const wrappedOperation = () => {
      return operation(returnValue);
    };

    const value = await retry(wrappedOperation, 1, 2, mockLogger);

    expect(value).toEqual(returnValue);
    expect(operation).toHaveBeenCalledTimes(1);
    expect(operation).toHaveBeenCalledWith(returnValue);
  });

  test('retry succeeds on subsequent try', async () => {
    expect.assertions(3);

    const returnValue = 5;

    const operation = jest.fn();
    operation.mockRejectedValueOnce('failed async operation');
    operation.mockResolvedValue(returnValue);

    const wrappedOperation = () => {
      return operation(returnValue);
    };

    const value = await retry(wrappedOperation, 1, 2, mockLogger);

    expect(value).toEqual(returnValue);
    expect(operation).toHaveBeenCalledTimes(2);
    expect(operation).toHaveBeenLastCalledWith(returnValue);
  });

  test('retry rejects after exhausted retries', async () => {
    expect.assertions(2);

    const returnValue = 5;

    const operation = jest.fn();
    operation.mockRejectedValueOnce('failed async operation 1');
    operation.mockRejectedValueOnce('failed async operation 2');
    operation.mockResolvedValue(returnValue);

    const wrappedOperation = () => {
      return operation(returnValue);
    };

    await expect(retry(wrappedOperation, 1, 2, mockLogger)).rejects.toEqual('failed async operation 2');

    expect(operation).toHaveBeenCalledTimes(2);
  });
});
