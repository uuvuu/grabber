
import * as lolex from 'lolex';

import { DateUtils } from '../DateUtils';


describe('Date utils', () => {
  let clock;

  beforeAll(() => {
    clock = lolex.install({ shouldAdvanceTime: true });
  });

  afterAll(() => {
    clock.uninstall();
  });

  test('getPastDateFromMillis() with zero intervals throws', () => {
    expect(() => DateUtils.getPastDateFromMillis(0)).toThrow();
  });

  test('getPastDateFromMillis() without alignment', () => {
    const millis = 12345;

    const expectedDate = new Date();
    expectedDate.setTime(expectedDate.getTime() - millis);

    const pastDate = DateUtils.getPastDateFromMillis(millis);

    expect(pastDate).toEqual(expectedDate);
  });

  test('getPastDateFromMillis() with alignment', () => {
    const millis = 12345;

    const expectedDate = new Date();
    expectedDate.setTime(expectedDate.getTime() - millis);

    expectedDate.setMilliseconds(0);
    expectedDate.setSeconds(0);

    const pastDate = DateUtils.getPastDateFromMillis(millis, DateUtils.TimeAlignment.MINUTES);

    expect(pastDate).toEqual(expectedDate);
  });

  test('newDatePairWithDiff() with undefined date throws', () => {
    const diffInMillis = 60000;

    expect(() => DateUtils.newDatePairWithDiff(undefined, diffInMillis)).toThrow();
  });

  test('newDatePairWithDiff() with invalid date throws', () => {
    const diffInMillis = 60000;

    expect(() => DateUtils.newDatePairWithDiff(new Date('a'), diffInMillis)).toThrow();
  });

  test('newDatePairWithDiff() with positive diff and minute alignment', () => {
    const diffInMillis = 60000;

    const date = new Date();

    const expectedStartDate = new Date(date);
    expectedStartDate.setSeconds(0);
    expectedStartDate.setMilliseconds(0);

    const expectedEndDate = new Date(expectedStartDate);
    expectedEndDate.setTime(expectedEndDate.getTime() + diffInMillis);

    const datePair = DateUtils.newDatePairWithDiff(date, diffInMillis, DateUtils.TimeAlignment.MINUTES);

    expect(datePair.startDate).toEqual(expectedStartDate);
    expect(datePair.endDate).toEqual(expectedEndDate);
  });

  test('newDatePairWithDiff() with positive diff and second alignment', () => {
    const diffInMillis = 60000;

    const date = new Date();

    const expectedStartDate = new Date(date);
    expectedStartDate.setMilliseconds(0);

    const expectedEndDate = new Date(expectedStartDate);
    expectedEndDate.setTime(expectedEndDate.getTime() + diffInMillis);

    const datePair = DateUtils.newDatePairWithDiff(date, diffInMillis, DateUtils.TimeAlignment.SECONDS);

    expect(datePair.startDate).toEqual(expectedStartDate);
    expect(datePair.endDate).toEqual(expectedEndDate);
  });

  test('newDatePairWithDiff() with negative diff and minute alignment', () => {
    const diffInMillis = -60000;

    const date = new Date();

    const expectedEndDate = new Date(date);
    expectedEndDate.setSeconds(0);
    expectedEndDate.setMilliseconds(0);

    const expectedStartDate = new Date(expectedEndDate);
    expectedStartDate.setTime(expectedStartDate.getTime() + diffInMillis);

    const datePair = DateUtils.newDatePairWithDiff(date, diffInMillis, DateUtils.TimeAlignment.MINUTES);

    expect(datePair.startDate).toEqual(expectedStartDate);
    expect(datePair.endDate).toEqual(expectedEndDate);
  });

  test('newDatePairWithDiff() with negative diff and second alignment', () => {
    const diffInMillis = -60000;

    const date = new Date();

    const expectedEndDate = new Date(date);
    expectedEndDate.setMilliseconds(0);

    const expectedStartDate = new Date(expectedEndDate);
    expectedStartDate.setTime(expectedStartDate.getTime() + diffInMillis);

    const datePair = DateUtils.newDatePairWithDiff(date, diffInMillis, DateUtils.TimeAlignment.SECONDS);

    expect(datePair.startDate).toEqual(expectedStartDate);
    expect(datePair.endDate).toEqual(expectedEndDate);
  });

  test('incrementDatePair() with undefined dates throws', () => {
    const incrementInMillis = 60000;

    expect(() => DateUtils.incrementDatePair(undefined, incrementInMillis)).toThrow();
  });

  test('incrementDatePair() with zero increment', () => {
    const incrementInMillis = 0;

    const startDate = new Date();
    startDate.setSeconds(0);
    startDate.setMilliseconds(0);

    const endDate = new Date(startDate);
    endDate.setSeconds(59);
    endDate.setMilliseconds(999);

    const expectedStartDate = startDate;
    const expectedEndDate = endDate;

    const datePair = DateUtils.incrementDatePair({ startDate: startDate, endDate: endDate }, incrementInMillis);

    expect(datePair.startDate).toEqual(expectedStartDate);
    expect(datePair.endDate).toEqual(expectedEndDate);
  });

  test('incrementDatePair() with defined dates and increment', () => {
    const incrementInMillis = 60000;

    const startDate = new Date();
    startDate.setSeconds(0);
    startDate.setMilliseconds(0);

    const endDate = new Date(startDate);
    endDate.setSeconds(59);
    endDate.setMilliseconds(999);

    const expectedStartDate = new Date(startDate);
    expectedStartDate.setTime(expectedStartDate.getTime() + incrementInMillis);

    const expectedEndDate = new Date(endDate);
    expectedEndDate.setTime(expectedEndDate.getTime() + incrementInMillis);

    const datePair = DateUtils.incrementDatePair({ startDate: startDate, endDate: endDate }, incrementInMillis);

    expect(datePair.startDate).toEqual(expectedStartDate);
    expect(datePair.startDate.getTime() - startDate.getTime()).toEqual(incrementInMillis);
    expect(datePair.endDate).toEqual(expectedEndDate);
    expect(datePair.endDate.getTime() - endDate.getTime()).toEqual(incrementInMillis);
  });

  test('incrementDatePair() dates defined, but startDate undefined throws', () => {
    const incrementInMillis = 60000;

    expect(() => DateUtils.incrementDatePair({ startDate: undefined, endDate: new Date() }, incrementInMillis)).toThrow();
  });

  test('incrementDatePair() dates defined, but endDate undefined throws', () => {
    const incrementInMillis = 60000;

    expect(() => DateUtils.incrementDatePair({ startDate: new Date(), endDate: undefined }, incrementInMillis)).toThrow();
  });

  test('walkDatePair() dates undefined throws', async () => {
    expect.assertions(1);

    await expect(DateUtils.walkDatePair(undefined, 1, (date: Date) => {
      return Promise.resolve();
    })).rejects.toBeDefined();
  });

  test('walkDatePair() startDate undefined throws', async () => {
    expect.assertions(1);

    await expect(DateUtils.walkDatePair({ startDate: undefined, endDate: new Date() }, 1, (date: Date) => {
      return Promise.resolve();
    })).rejects.toBeDefined();
  });

  test('walkDatePair() endDate undefined throws', async () => {
    expect.assertions(1);

    await expect(DateUtils.walkDatePair({ startDate: new Date(), endDate: undefined }, 1, (date: Date) => {
      return Promise.resolve();
    })).rejects.toBeDefined();
  });

  test('walkDatePair() endDate less than startDate throws', async () => {
    expect.assertions(1);

    const startDate = new Date();
    const endDate = new Date(startDate);
    endDate.setMinutes(endDate.getMinutes() - 1);

    await expect(DateUtils.walkDatePair({ startDate: startDate, endDate: endDate }, 1, (date: Date) => {
      return Promise.resolve();
    })).rejects.toBeDefined();
  });

  test('walkDatePair() interval undefined throws', async () => {
    expect.assertions(1);

    const startDate = new Date();
    const endDate = new Date(startDate);

    await expect(DateUtils.walkDatePair({ startDate: startDate, endDate: endDate }, undefined, (date: Date) => {
      return Promise.resolve();
    })).rejects.toBeDefined();
  });

  test('walkDatePair() interval == 0 throws', async () => {
    expect.assertions(1);

    const startDate = new Date();
    const endDate = new Date(startDate);

    await expect(DateUtils.walkDatePair({ startDate: startDate, endDate: endDate }, 0, (date: Date) => {
      return Promise.resolve();
    })).rejects.toBeDefined();
  });

  test('walkDatePair() interval < 0 throws', async () => {
    expect.assertions(1);

    const startDate = new Date();
    const endDate = new Date(startDate);

    await expect(DateUtils.walkDatePair({ startDate: startDate, endDate: endDate }, -1, (date: Date) => {
      return Promise.resolve();
    })).rejects.toBeDefined();
  });

  test('walkDatePair() interval is not integer throws', async () => {
    expect.assertions(1);

    const startDate = new Date();
    const endDate = new Date(startDate);

    await expect(DateUtils.walkDatePair({ startDate: startDate, endDate: endDate }, .5, (date: Date) => {
      return Promise.resolve();
    })).rejects.toBeDefined();
  });

  test('walkDatePair() callback undefined throws', async () => {
    expect.assertions(1);

    const startDate = new Date();
    const endDate = new Date(startDate);

    await expect(DateUtils.walkDatePair({ startDate: startDate, endDate: endDate }, 1, undefined)).rejects.toBeDefined();
  });

  test('walkDatePair() startDate == endDate', async () => {
    expect.assertions(1);

    const startDate = new Date();
    const endDate = new Date(startDate);
    const datePair = { startDate: startDate, endDate: endDate };
    const callback = jest.fn().mockImplementation(date => {});

    await DateUtils.walkDatePair(datePair, 1, callback);

    expect(callback).not.toHaveBeenCalled();
  });

  test('walkDatePair() startDate == endDate + interval - 1', async () => {
    expect.assertions(1);

    const interval = 10;
    const startDate = new Date();
    const endDate = new Date(startDate);
    endDate.setTime(endDate.getTime() + interval - 1);

    const datePair = { startDate: startDate, endDate: endDate };
    const callback = jest.fn().mockImplementation(date => {});

    await DateUtils.walkDatePair(datePair, interval, callback);

    expect(callback).toHaveBeenCalledTimes(1);
  });

  test('walkDatePair() startDate == endDate + interval', async () => {
    expect.assertions(1);

    const interval = 10;
    const startDate = new Date();
    const endDate = new Date(startDate);
    endDate.setTime(endDate.getTime() + interval);

    const datePair = { startDate: startDate, endDate: endDate };
    const callback = jest.fn().mockImplementation(date => {});

    await DateUtils.walkDatePair(datePair, interval, callback);

    expect(callback).toHaveBeenCalledTimes(1);
  });

  test('walkDatePair() startDate == endDate + interval + 1', async () => {
    expect.assertions(1);

    const interval = 10;
    const startDate = new Date();
    const endDate = new Date(startDate);
    endDate.setTime(endDate.getTime() + interval + 1);

    const datePair = { startDate: startDate, endDate: endDate };
    const callback = jest.fn().mockImplementation(date => {});

    await DateUtils.walkDatePair(datePair, interval, callback);

    expect(callback).toHaveBeenCalledTimes(2);
  });

  test('walkDatePair() original startDate and endDates not modified', async () => {
    expect.assertions(2);

    const interval = 10;
    const startDate = new Date();
    const endDate = new Date(startDate);
    endDate.setTime(endDate.getTime() + interval);

    const datePair = { startDate: startDate, endDate: endDate };

    const originalStartDate = new Date(startDate);
    const originalEndDate = new Date(endDate);

    await DateUtils.walkDatePair(datePair, interval, (date: Date) => {
      return Promise.resolve();
    });

    expect(startDate.getTime()).toEqual(originalStartDate.getTime());
    expect(endDate.getTime()).toEqual(originalEndDate.getTime());
  });
});
