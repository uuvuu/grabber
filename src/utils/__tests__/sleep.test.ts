
import { sleep } from '../sleep';


describe('sleep', () => {
  test('sleeps a minimum of specified seconds', async () => {
    expect.assertions(1);

    const sleepTimeInMillis = 500;

    const preSleepDate = new Date();

    await sleep(sleepTimeInMillis);

    const postSleepDate = new Date();

    expect(postSleepDate.getTime() >= preSleepDate.getTime() + sleepTimeInMillis).toBeTruthy();
  });
});
