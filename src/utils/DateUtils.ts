
namespace DateUtils {
  export type DatePair = {
    startDate: Date;
    endDate: Date;
  };

  export enum TimeAlignment {
    SECONDS,
    MINUTES
  }

  export function alignDate(date: Date, alignment?: TimeAlignment) {
    const alignedDate = new Date(date);

    if (alignment === TimeAlignment.MINUTES) {
      alignedDate.setSeconds(0);
    }

    if (alignment === TimeAlignment.MINUTES || alignment === TimeAlignment.SECONDS) {
      alignedDate.setMilliseconds(0);
    }

    return alignedDate;
  }

  export function getPastDateFromMillis(millis: number, alignment?: TimeAlignment): Date {
    if (! Number.isInteger(millis) || millis <= 0) {
      throw 'millis must be an integer greater than 0';
    }

    const startDate = new Date();

    startDate.setTime(startDate.getTime() - millis);

    return alignment ? alignDate(startDate, alignment) : startDate;
  }

  export async function walkDatePair(dates: DatePair, intervalInMillis: number, callback: (date: Date) => Promise<any>) {
    if (! dates) {
      throw 'dates must be defined';
    }

    if (! Number.isInteger(intervalInMillis) || intervalInMillis <= 0) {
      throw 'intervalInMillis must be an integer greater than 0';
    }

    if (! callback) {
      throw 'callback must be defined';
    }

    validateDatePair(dates);

    const startDateCopy = new Date(dates.startDate);

    while (startDateCopy < dates.endDate) {
      await callback(startDateCopy);
      startDateCopy.setMilliseconds(startDateCopy.getMilliseconds() + intervalInMillis);
    }
  }

  export function newDatePairWithDiff(date: Date, diffInMillis: number, alignment?: TimeAlignment): DatePair {
    if (! isValidDate(date)) {
      throw `date must be valid. date received was ${date}`;
    }

    const date1 = typeof alignment !== 'undefined' ? alignDate(new Date(date), alignment) : new Date(date);

    const date2 = new Date(date1);
    date2.setTime(date2.getTime() + diffInMillis);

    const startDate = date1 <= date2 ? date1 : date2;
    const endDate = date1 > date2 ? date1 : date2;

    return { startDate: startDate, endDate: endDate };
  }

  // increment must be an integer
  export function incrementDatePair(dates: DatePair, incrementInMillis: number): DatePair {
    if (! Number.isInteger(incrementInMillis)) {
      throw 'incrementInMillis must be an integer';
    }

    validateDatePair(dates);

    const newDates = {
      startDate: <Date>undefined,
      endDate: <Date>undefined
    };

    if (incrementInMillis === 0) {
      newDates.startDate = new Date(dates.startDate);
      newDates.endDate = new Date(dates.endDate);
    } else {
      newDates.startDate = new Date(dates.startDate);
      newDates.startDate.setTime(newDates.startDate.getTime() + incrementInMillis);
      newDates.endDate = new Date(dates.endDate);
      newDates.endDate.setTime(newDates.endDate.getTime() + incrementInMillis);
    }

    return newDates;
  }

  function validateDatePair(dates: DatePair) {
    if (! dates) {
      throw 'datePair must be defined';
    }

    if (! isValidDate(dates.startDate)) {
      throw `startDate must be defined and a valid date. startDate was: ${dates.startDate}`;
    }

    if (! isValidDate(dates.endDate)) {
      throw `endDate must be defined and a valid date. endDate was: ${dates.endDate} `;
    }

    if (dates.endDate < dates.startDate) {
      throw 'endDate must be equal to or greater than startDate';
    }
  }

  function isValidDate(date) {
    return date.constructor.name === 'Date' && !Number.isNaN(date.getTime());
  }
}

export { DateUtils };
