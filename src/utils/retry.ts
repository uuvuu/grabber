
import { sleep } from './sleep';


const retry = <T>(operation, delay, times, logger): Promise<T> => new Promise((resolve, reject) => {
  return operation()
    .then(resolve)
    .catch(reason => {
      logger.log('warn', `Request failed. Reason = ${reason}`);

      // 400 errors are legitimate bad requests that the server has responded to
      if (reason.toString().match(/HTTP 400/)) {
        return reject(reason);
      }

      logger.log('warn', `Retrying request. Retry attempts remaining: ${times - 1}`);

      if (times - 1 > 0) {
        return sleep(delay)
          .then(retry.bind(undefined, operation, delay, times - 1, logger))
          .then(resolve)
          .catch(reject);
      }

      return reject(reason);
    });
});


export { retry };

