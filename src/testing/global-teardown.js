
// as .js global setup file is needed instead of a .ts file because of this issue:
// https://github.com/facebook/jest/issues/5164#issuecomment-376006851
require('ts-node/register');


// reference typescript modules via require
// eg: const MongodbMemoryServer = require('mongodb-memory-server');


module.exports = async function() {
};

