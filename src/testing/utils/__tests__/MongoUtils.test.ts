
import * as lolex from 'lolex';

import { Document } from 'mongoose';

import { CandleUtils } from '../../../domain/candle/entities/Candle';
import { CandleDocumentFactory } from '../../entities/CandleDocumentFactory';

import { MongoUtils } from '../MongoUtils';


interface TestChildInterface {
  childField: string;
}

interface TestParentInterface {
  parentField: string;

  children: [ TestChildInterface ];
}

interface TestParentDocument extends TestParentInterface, Document {
  parentField: string;

  children: [ TestChildDocument ];
}


interface TestChildDocument extends TestChildInterface, Document {
  childField: string;
}

describe('Mongo utils', () => {
  let clock;
  let candleDocumentFactory;

  beforeAll(() => {
    clock = lolex.install({ shouldAdvanceTime: true });
    candleDocumentFactory = new CandleDocumentFactory(new Date(), 10);
  });

  afterAll(() => {
    clock.uninstall();
  });

  test('document is converted to interface', () => {
    const parentField = 'a';
    const childField1 = 'b';
    const childField2 = 'c';

    const document = {
      _id: '1',
      __v: 0,

      parentField: parentField,

      children: [
        { _id: '1', __v: 0, childField: childField1 },
        { _id: '2', __v: 0, childField: childField2 }
      ] as TestChildDocument[]
    } as TestParentDocument;

    const intf = {
      parentField: parentField,

      children: [
        { childField: childField1 },
        { childField: childField2 }
      ] as TestChildInterface[]
    } as TestParentInterface;

    const convertedInterface = MongoUtils.documentToInterface(document);

    expect(convertedInterface).toEqual(intf);
  });

  test('document converted to clean document', () => {
    const parentField = 'a';
    const childField1 = 'b';
    const childField2 = 'c';

    const document = {
      _id: '1',
      __v: 0,

      parentField: parentField,

      children: [
        { _id: '1', __v: 0, childField: childField1 },
        { _id: '2', __v: 0, childField: childField2 }
      ] as TestChildDocument[]
    } as TestParentDocument;

    const convertedDocument = MongoUtils.documentToCleanDocument(document);

    expect(convertedDocument).toEqual(document);
  });

  test('documents are converted to interfaces', () => {
    const parentField1 = 'a';
    const parentField2 = 'b';
    const childField11 = 'c';
    const childField12 = 'd';
    const childField21 = 'e';
    const childField22 = 'f';

    const documents = [
      {
        _id: '1',
        __v: 0,

        parentField: parentField1,

        children: [
          { _id: '1', __v: 0, childField: childField11 },
          { _id: '2', __v: 0, childField: childField12 }
        ] as TestChildDocument[]
      },
      {
        _id: '1',
        __v: 0,

        parentField: parentField2,

        children: [
          { _id: '1', __v: 0, childField: childField21 },
          { _id: '2', __v: 0, childField: childField22 }
        ] as TestChildDocument[]
      }
    ] as TestParentDocument[];

    const intfs = [
      {
        parentField: parentField1,

        children: [
          { childField: childField11 },
          { childField: childField12 }
        ] as TestChildInterface[]
      },
      {
        parentField: parentField2,

        children: [
          { childField: childField21 },
          { childField: childField22 }
        ] as TestChildInterface[]
      }
    ] as TestParentInterface[];

    const convertedInterfaces = MongoUtils.documentsToInterfaces(documents);

    expect(convertedInterfaces).toEqual(intfs);
  });

  test('documents converted to clean documents', () => {
    const parentField1 = 'a';
    const parentField2 = 'b';
    const childField11 = 'c';
    const childField12 = 'd';
    const childField21 = 'e';
    const childField22 = 'f';

    const documents = [
      {
        _id: '1',
        __v: 0,

        parentField: parentField1,

        children: [
          { _id: '1', __v: 0, childField: childField11 },
          { _id: '2', __v: 0, childField: childField12 }
        ] as TestChildDocument[]
      },
      {
        _id: '1',
        __v: 0,

        parentField: parentField2,

        children: [
          { _id: '1', __v: 0, childField: childField21 },
          { _id: '2', __v: 0, childField: childField22 }
        ] as TestChildDocument[]
      }
    ] as TestParentDocument[];

    const convertedDocuments = MongoUtils.documentsToCleanDocuments(documents);

    expect(convertedDocuments).toEqual(documents);
  });

  test('toMatchNewDocument() to match when single new documents match', () => {
    const candle = candleDocumentFactory.newCandleDocument();
    const candleDocument = CandleUtils.copyCandleDocument(candle);

    candleDocument._id = 1;
    candleDocument.__v = 0;

    const matchResults = MongoUtils.toMatchNewDocument(candleDocument, candle);

    expect(matchResults.actual).toEqual(candleDocument);
    expect(matchResults.pass).toEqual(true);

    // @ts-ignore
    expect(candleDocument).toMatchNewDocument(candle);
  });

  test('toMatchNewDocument() not to match when single new documents don\'t match', () => {
    const candle = candleDocumentFactory.newCandleDocument();
    const candleDocument = CandleUtils.copyCandleDocument(candle);

    candleDocument._id = 1;
    candleDocument.__v = 0;

    candleDocument.low += '1';

    const matchResults = MongoUtils.toMatchNewDocument(candleDocument, candle);

    expect(matchResults.actual).toEqual(candleDocument);
    expect(matchResults.pass).toEqual(false);

    // @ts-ignore
    expect(candleDocument).not.toMatchNewDocument(candle);
  });

  test('toMatchNewDocuments() to match when list of new documents match', () => {
    const candles = candleDocumentFactory.newCandleDocuments(2);
    const candleDocuments = CandleUtils.copyCandleDocuments(candles);

    for (let i = 0; i < candleDocuments.length; i++) {
      candleDocuments[i]._id = i + 1;
      candleDocuments[i].__v = 0;
    }

    const matchResults = MongoUtils.toMatchNewDocuments(candleDocuments, candles);

    expect(matchResults.actual).toEqual(candleDocuments);
    expect(matchResults.pass).toEqual(true);

    // @ts-ignore
    expect(candleDocuments).toMatchNewDocuments(candles);
  });

  test('toMatchNewDocuments() not to match when list of new documents don\'t match', () => {
    const candles = candleDocumentFactory.newCandleDocuments(2);
    const candleDocuments = CandleUtils.copyCandleDocuments(candles);

    for (let i = 0; i < candleDocuments.length; i++) {
      candleDocuments[i]._id = i + 1;
      candleDocuments[i].__v = 0;
    }

    candleDocuments[0].low += '1';

    const matchResults = MongoUtils.toMatchNewDocuments(candleDocuments, candles);

    expect(matchResults.actual).toEqual(candleDocuments);
    expect(matchResults.pass).toEqual(false);

    // @ts-ignore
    expect(candleDocuments).not.toMatchNewDocument(candles);
  });

  test('toMatchDocument() to match when single documents match', () => {
    const candle = candleDocumentFactory.newCandleDocument({ _id: '1', __v: 1 });
    const candleDocument = CandleUtils.copyCandleDocument(candle);

    const matchResults = MongoUtils.toMatchDocument(candleDocument, candle);

    expect(matchResults.actual).toEqual(candleDocument);
    expect(matchResults.pass).toEqual(true);

    // @ts-ignore
    expect(candleDocument).toMatchDocument(candle);
  });

  test('toMatchDocument() not to match when single documents don\'t match', () => {
    const candle = candleDocumentFactory.newCandleDocument({ _id: '1', __v: 1 });
    const candleDocument = CandleUtils.copyCandleDocument(candle);

    candleDocument.low += '1';

    const matchResults = MongoUtils.toMatchDocument(candleDocument, candle);

    expect(matchResults.actual).toEqual(candleDocument);
    expect(matchResults.pass).toEqual(false);

    // @ts-ignore
    expect(candleDocument).not.toMatchNewDocument(candle);
  });

  test('toMatchDocuments() to match when list of documents match', () => {
    const candles = candleDocumentFactory.newCandleDocuments(2);

    for (let i = 0; i < candles.length; i++) {
      candles[i]._id = i + 1;
      candles[i].__v = 0;
    }

    const candleDocuments = CandleUtils.copyCandleDocuments(candles);

    const matchResults = MongoUtils.toMatchNewDocuments(candleDocuments, candles);

    expect(matchResults.actual).toEqual(candleDocuments);
    expect(matchResults.pass).toEqual(true);

    // @ts-ignore
    expect(candleDocuments).toMatchDocument(candles);
  });

  test('toMatchDocuments() not to match when list of documents don\'t match', () => {
    const candles = candleDocumentFactory.newCandleDocuments(2);

    for (let i = 0; i < candles.length; i++) {
      candles[i]._id = i + 1;
      candles[i].__v = 0;
    }

    const candleDocuments = CandleUtils.copyCandleDocuments(candles);

    candleDocuments[0].low += '1';

    const matchResults = MongoUtils.toMatchNewDocuments(candleDocuments, candles);

    expect(matchResults.actual).toEqual(candleDocuments);
    expect(matchResults.pass).toEqual(false);

    // @ts-ignore
    expect(candleDocuments).not.toMatchDocument(candles);
  });
});

