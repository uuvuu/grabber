
import * as diff from 'jest-diff';

import { Document } from 'mongoose';


namespace MongoUtils {
  export function documentToInterface<I, D extends I & Document>(document: D): I {
    return JSON.parse(JSON.stringify(document, (k, v) => (k === '_id' || k === '__v') ? undefined : v)) as I;
  }

  export function documentsToInterfaces<I, D extends I & Document>(documents: D[]): I[] {
    return documents.map(document => documentToInterface(document));
  }

  // used to convert a mongo document to just the interface it represents with the additional
  // _id and __v properties. useful to comparing two mongo documents
  export function documentToCleanDocument<D extends Document>(document: D): D {
    return JSON.parse(JSON.stringify(document, undefined)) as D;
  }

  export function documentsToCleanDocuments<D extends Document>(documents: D[]): D[] {
    return documents.map(document => documentToCleanDocument(document));
  }

  function toMatch(received, expected, documentConverterFn, _this) {
    const receivedCopy = documentConverterFn(received);
    const expectedCopy = documentConverterFn(expected);

    // cheap comparison -- it breaks on easily. ie: one object has time field as a string
    // and the other has time field as a Date and they both stringify to the same string
    const pass = JSON.stringify(receivedCopy) === JSON.stringify(expectedCopy);

    let message;

    if (pass) {
      message = () => {
        return _this.utils.matcherHint('.not.toBe') + '\n\n'
          + `Expected value to not be (using deep equals):\n`
          + `  ${_this.utils.printExpected(expectedCopy)}\n`
          + `Received:\n`
          + `  ${_this.utils.printReceived(receivedCopy)}`;
      };
    } else {
      message = () => {
        const diffString = diff(expectedCopy, receivedCopy, {
          expand: _this.expand,
        });

        return (
          _this.utils.matcherHint('.toBe') + '\n\n'
          + `Expected value to be (using deep equals):\n`
          + `  ${_this.utils.printExpected(expectedCopy)}\n`
          + `Received:\n`
          + `  ${_this.utils.printReceived(receivedCopy)}`
          + (diffString ? `\n\nDifference:\n\n${diffString}` : '')
        );
      };
    }

    return {actual: received, message, pass};
  }

  export function toMatchNewDocument(received: any, expected: any) {
    // @ts-ignore
    return toMatch(received, expected, documentToInterface, this);
  }

  export function toMatchDocument(received: any, expected: any) {
    // @ts-ignore
    return toMatch(received, expected, documentToCleanDocument, this);
  }

  export function toMatchNewDocuments(received: any[], expected: any[]) {
    // @ts-ignore
    return toMatch(received, expected, documentsToInterfaces, this);
  }

  export function toMatchDocuments(received: any[], expected: any[]) {
    // @ts-ignore
    return toMatch(received, expected, documentsToCleanDocuments, this);
  }
}


export { MongoUtils };

