
function argvHelper(argv: string[]) {
  const newArgv = [] as string[];

  if (argv && argv.length) {
    argv.forEach(arg => {
      arg.split(/\s+/).forEach(item => {
        newArgv.push(item);
      });
    });
  }

  newArgv.unshift('run.ts');
  newArgv.unshift('node');

  return newArgv;
}


export { argvHelper };
