
import { Order } from '../../domain/order/entities/Order';


const DEFAULTS = {
  _id: undefined,
  __v: undefined,

  orderId: undefined,

  product: 'BTC-USD',

  price: '1000',
  size: '1',

  side: Order.Side.BUY,

  stp: Order.STP.CB,    // self-trade prevention flag (optional)

  type: Order.Type.LIMIT,   // (default is limit)

  timeInForce: Order.TimeInForce.GTT,    // (optional, default is GTC)

  postOnly: true,

  createdTime: undefined,
  expireTime: undefined,
  doneTime: undefined,
  candleTime: undefined,       // the latest candle time that this trade was based on

  doneReason: 'filled',

  fillFees: '0.0000000000000000',
  filledSize: '1',

  executedValue: '1000.00',

  status: Order.Status.DONE,

  settled: true,

  clearanceStatus: 'filled'
};


class OrderDocumentFactory {
  private _date: Date;

  private _orderId = 1;

  private _createdTime: Date;

  constructor(now: Date, private _millisBetweenOrders: number) {
    this._date = new Date(now);
  }

  private getProperty(propertyName: string, properties?: any) {
    if (! DEFAULTS.hasOwnProperty(propertyName)) {
      throw `property ${propertyName} does not exist on OrderDocument`;
    }

    if (propertyName === 'createdTime') {
      if (properties && properties[propertyName]) {
        return properties[propertyName];
      } else {
        this._createdTime = this.nextDate();
        return this._createdTime;
      }
    }

    if (propertyName === 'expireTime') {
      if (properties && properties[propertyName]) {
        return properties[propertyName];
      } else {
        const expireTime = new Date(this._createdTime);
        expireTime.setMilliseconds(0);
        return expireTime;
      }
    }

    if (propertyName === 'doneTime') {
      if (properties && properties[propertyName]) {
        return properties[propertyName];
      } else {
        const doneTime = new Date(this._createdTime);
        doneTime.setSeconds(doneTime.getSeconds() + 30);
        return doneTime;
      }
    }

    if (propertyName === 'candleTime') {
      if (properties && properties[propertyName]) {
        return properties[propertyName];
      } else {
        const candleTime = new Date(this._createdTime);
        candleTime.setMinutes(candleTime.getMinutes() - 1);
        candleTime.setSeconds(0);
        candleTime.setMilliseconds(0);
        return candleTime;
      }
    }

    return properties && properties[propertyName] ? '' + properties[propertyName] : DEFAULTS[propertyName];
  }

  private nextDate(date?: Date) {
    // quick trick to determine if lolex has been 'installed' during the test run
    if (! Date['clock']) {
      throw 'date must be stubbed before creating test order documents';
    }

    if (! date) {
      if (! this._date) {
        this._date = new Date();
        this._date.setMilliseconds(0);
        this._date.setSeconds(0);
      }

      date = new Date(this._date);

      this._date.setTime(this._date.getTime() + this._millisBetweenOrders);
    }

    return date;
  }

  newOrderDocument(properties?: any) {
    const orderDocument = {} as Order.OrderDocument;

    if (properties && '_id' in properties) {
      orderDocument['_id'] = this.getProperty('_id', properties);
    }

    if (properties && '__v' in properties) {
      orderDocument['__v'] = this.getProperty('__v', properties);
    }

    orderDocument['product'] = this.getProperty('product');

    orderDocument['orderId'] = '' + this._orderId++;

    orderDocument['price'] = this.getProperty('price', properties);
    orderDocument['size'] = this.getProperty('size', properties);

    orderDocument['side'] = this.getProperty('side', properties);

    orderDocument['stp'] = this.getProperty('stp', properties);

    orderDocument['type'] = this.getProperty('type', properties);

    orderDocument['timeInForce'] = this.getProperty('timeInForce', properties);

    orderDocument['postOnly'] = this.getProperty('postOnly', properties);

    orderDocument['createdTime'] = this.getProperty('createdTime', properties);
    orderDocument['expireTime'] = this.getProperty('expireTime', properties);
    orderDocument['doneTime'] = this.getProperty('doneTime', properties);
    orderDocument['candleTime'] = this.getProperty('candleTime', properties);

    orderDocument['doneReason'] = this.getProperty('doneReason', properties);

    orderDocument['fillFees'] = this.getProperty('fillFees', properties);
    orderDocument['filledSize'] = this.getProperty('filledSize', properties);

    orderDocument['executedValue'] = this.getProperty('executedValue', properties);

    orderDocument['status'] = this.getProperty('status', properties);

    orderDocument['settled'] = this.getProperty('settled', properties);

    orderDocument['clearanceStatus'] = this.getProperty('clearanceStatus', properties);

    return orderDocument;
  }

  newOrderDocuments(count: number, properties?: any) {
    const documents = [] as Order.OrderDocument[];

    for (let i = 0; i < count; i++) {
      documents.push(this.newOrderDocument(properties));
    }

    return documents;
  }
}


export { OrderDocumentFactory };
