

import { Account } from '../../domain/account/entities/Account';


const EXCHANGE_RATE_CRYPTO = 5000;


const ACCOUNT_DEFAULTS = {
  _id: undefined,

  currency: 'USD',

  accountId: undefined,

  balance: '1000',
  available: '1000',
  hold: '0',

  exchangeRateUSD: '1',
  exchangeRateCrypto: 1 / + EXCHANGE_RATE_CRYPTO
};

const ACCOUNTS_DEFAULTS = {
  _id: undefined,
  __v: undefined,

  time: undefined,

  profileId: undefined,

  accounts: [],

  totalUSD: undefined
};


class AccountFactory {
  private _lastAccountId = 1;

  constructor() { }

  private getProperty(propertyName: string, properties?: any) {
    if (! ACCOUNT_DEFAULTS.hasOwnProperty(propertyName)) {
      throw `property ${propertyName} does not exist on AccountDocument`;
    }

    return properties && properties[propertyName] ? '' + properties[propertyName] : ACCOUNT_DEFAULTS[propertyName];
  }

  newAccount(properties?: any) {
    const accountDocument = {} as Account;

    if (properties && '_id' in properties) {
      accountDocument['_id'] = this.getProperty('_id', properties);
    }

    accountDocument['currency'] = this.getProperty('currency', properties);

    accountDocument['accountId'] = '' + this._lastAccountId++;

    accountDocument['balance'] = this.getProperty('balance', properties);
    accountDocument['available'] = this.getProperty('available', properties);
    accountDocument['hold'] = this.getProperty('hold', properties);

    accountDocument['exchangeRateUSD'] = this.getProperty('exchangeRateUSD', properties);
    accountDocument['exchangeRateCrypto'] = +this.getProperty('exchangeRateCrypto', properties);

    return accountDocument;
  }

  newAccounts(count: number, properties?: any) {
    const accounts = [] as Account[];

    for (let i = 0; i < count; i++) {
      accounts.push(this.newAccount(properties));
    }

    return accounts;
  }
}

class AccountsDocumentFactory {
  private readonly _accountFactory;

  private _date: Date;
  private _lastProfileId = 1;

  constructor(now: Date, private _millisBetweenAccounts: number) {
    this._date = new Date(now);
    this._accountFactory = new AccountFactory();
  }

  private getProperty(propertyName: string, properties?: any) {
    if (! ACCOUNTS_DEFAULTS.hasOwnProperty(propertyName)) {
      throw `property ${propertyName} does not exist on AccountsDocument`;
    }

    if (propertyName === 'time') {
      if (properties && properties[propertyName]) {
        return properties[propertyName];
      } else {
        return this.nextDate();
      }
    }

    if (propertyName === 'accounts') {
      if (properties && properties[propertyName]) {
        return properties[propertyName];
      } else {
        const accounts = [] as Account[];

        accounts.push(this._accountFactory.newAccount());

        accounts.push(this._accountFactory.newAccount({
          currency: 'BTC',

          balance: '0.5',
          available: '0.5',
          hold: '0',

          exchangeRateUSD: EXCHANGE_RATE_CRYPTO,
          exchangeRateCrypto: 1
        }));

        return accounts;
      }
    }

    return properties && properties[propertyName] ? '' + properties[propertyName] : ACCOUNTS_DEFAULTS[propertyName];
  }

  private nextDate(date?: Date) {
    // quick trick to determine if lolex has been 'installed' during the test run
    if (! Date['clock']) {
      throw 'date must be stubbed before creating test accounts documents';
    }

    if (! date) {
      if (! this._date) {
        this._date = new Date();
        this._date.setMilliseconds(0);
        this._date.setSeconds(0);
      }

      date = new Date(this._date);

      this._date.setTime(this._date.getTime() + this._millisBetweenAccounts);
    }

    return date;
  }

  resetDate() {
    this._date = new Date();
  }

  newAccountsDocument(properties?: any) {
    const accountsDocument = {} as Account.AccountsDocument;

    if (properties && '_id' in properties) {
      accountsDocument['_id'] = this.getProperty('_id', properties);
    }

    if (properties && '__v' in properties) {
      accountsDocument['__v'] = this.getProperty('__v', properties);
    }

    accountsDocument['time'] = this.getProperty('time', properties);

    accountsDocument['profileId'] = '' + this._lastProfileId++;

    accountsDocument['accounts'] = this.getProperty('accounts', properties);

    let totalUSD = 0;

    accountsDocument['accounts'].forEach(account => {
      totalUSD += +account.balance * +account.exchangeRateUSD;
    });

    accountsDocument['exchangeRateCrypto'] = EXCHANGE_RATE_CRYPTO;

    accountsDocument['totalUSD'] = '' + totalUSD;
    accountsDocument['totalCrypto'] = totalUSD / EXCHANGE_RATE_CRYPTO;

    return accountsDocument;
  }

  newAccountsDocuments(count: number, properties?: any) {
    const documents = [] as Account.AccountsDocument[];

    for (let i = 0; i < count; i++) {
      documents.push(this.newAccountsDocument(properties));
    }

    return documents;
  }
}


export { AccountFactory, AccountsDocumentFactory };
