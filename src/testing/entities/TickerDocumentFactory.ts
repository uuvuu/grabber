
import { Ticker } from '../../domain/ticker/entities/Ticker';


const DEFAULTS = {
  _id: undefined,
  __v: undefined,

  product: 'BTC-USD',

  time: undefined,

  tradeId: undefined,
  sequence: undefined,

  price: '10',
  side: Ticker.Side.BUY,
  lastSize: '2.5',
  bestBid: '9.9',
  bestAsk: '10.1',

  open24h: '9.5',
  low24h: '9.1',
  high24h: '11.2',

  volume24h: '15',
  volume30d: '67',

  source: 'cbpro-websocket'
};


class TickerDocumentFactory {
  private _date: Date;

  private _lastTradeId = 1;
  private _lastSequenceId = 1000;

  constructor(now: Date, private _millisBetweenTickers: number) {
    this._date = new Date(now);
  }

  private getProperty(propertyName: string, properties?: any) {
    if (! DEFAULTS.hasOwnProperty(propertyName)) {
      throw `property ${propertyName} does not exist on TickerDocument`;
    }

    if (propertyName === 'time') {
      if (properties && properties[propertyName]) {
        return properties[propertyName];
      } else {
        return this.nextDate();
      }
    }

    return properties && properties[propertyName] ? '' + properties[propertyName] : DEFAULTS[propertyName];
  }

  private nextDate(date?: Date) {
    // quick trick to determine if lolex has been 'installed' during the test run
    if (! Date['clock']) {
      throw 'date must be stubbed before creating test ticker documents';
    }

    if (! date) {
      if (! this._date) {
        this._date = new Date();
        this._date.setMilliseconds(0);
        this._date.setSeconds(0);
      }

      date = new Date(this._date);

      this._date.setTime(this._date.getTime() + this._millisBetweenTickers);
    }

    return date;
  }

  newTickerDocument(properties?: any) {
    const tickerDocument = {};

    if (properties && '_id' in properties) {
      tickerDocument['_id'] = this.getProperty('_id', properties);
    }

    if (properties && '__v' in properties) {
      tickerDocument['__v'] = this.getProperty('__v', properties);
    }

    tickerDocument['product'] = this.getProperty('product');

    tickerDocument['time'] = this.getProperty('time', properties);

    tickerDocument['tradeId'] = this._lastTradeId++;
    tickerDocument['sequence'] = this._lastSequenceId++;

    tickerDocument['price'] = this.getProperty('price', properties);
    tickerDocument['side'] = this.getProperty('side', properties);
    tickerDocument['lastSize'] = this.getProperty('lastSize', properties);
    tickerDocument['bestBid'] = this.getProperty('bestBid', properties);
    tickerDocument['bestAsk'] = this.getProperty('bestAsk', properties);

    tickerDocument['open24h'] = this.getProperty('open24h', properties);
    tickerDocument['low24h'] = this.getProperty('low24h', properties);
    tickerDocument['high24h'] = this.getProperty('high24h', properties);

    tickerDocument['volume24h'] = this.getProperty('volume24h', properties);
    tickerDocument['volume30d'] = this.getProperty('volume30d', properties);

    tickerDocument['source'] = this.getProperty('source', properties);

    return tickerDocument as Ticker.TickerDocument;
  }

  newTickerDocuments(count: number, properties?: any) {
    const documents = [] as Ticker.TickerDocument[];

    for (let i = 0; i < count; i++) {
      documents.push(this.newTickerDocument(properties));
    }

    return documents;
  }
}


export { TickerDocumentFactory };
