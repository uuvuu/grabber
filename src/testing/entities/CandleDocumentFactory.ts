
import { Candle } from '../../domain/candle/entities/Candle';


const DEFAULTS = {
  _id: undefined,
  __v: undefined,

  product: 'BTC-USD',

  time: undefined,

  low: '0',
  high: '10',
  open: '4',
  close: '6',

  volume: '10',

  source: 'cbpro-api',
  interpolated: false
};


class CandleDocumentFactory {
  private _date: Date;

  constructor(now: Date, private _millisBetweenCandles: number) {
    this._date = new Date(now);
  }

  private getProperty(propertyName: string, properties?: any) {
    if (! DEFAULTS.hasOwnProperty(propertyName)) {
      throw `property ${propertyName} does not exist on CandleDocument`;
    }

    if (propertyName === 'time') {
      if (properties && properties[propertyName]) {
        return properties[propertyName];
      } else {
        return this.nextDate();
      }
    }

    return properties && properties[propertyName] ? '' + properties[propertyName] : DEFAULTS[propertyName];
  }

  private nextDate(date?: Date) {
    // quick trick to determine if lolex has been 'installed' during the test run
    if (! Date['clock']) {
      throw 'date must be stubbed before creating test candle documents';
    }

    if (! date) {
      if (! this._date) {
        this._date = new Date();
        this._date.setMilliseconds(0);
        this._date.setSeconds(0);
      }

      date = new Date(this._date);

      this._date.setTime(this._date.getTime() + this._millisBetweenCandles);
    }

    return date;
  }

  newCandleDocument(properties?: any) {
    const candleDocument = {};

    if (properties && '_id' in properties) {
      candleDocument['_id'] = this.getProperty('_id', properties);
    }

    if (properties && '__v' in properties) {
      candleDocument['__v'] = this.getProperty('__v', properties);
    }

    candleDocument['product'] = this.getProperty('product');

    candleDocument['time'] = this.getProperty('time', properties);

    candleDocument['low'] = this.getProperty('low', properties);
    candleDocument['high'] = this.getProperty('high', properties);
    candleDocument['open'] = this.getProperty('open', properties);
    candleDocument['close'] = this.getProperty('close', properties);
    candleDocument['volume'] = this.getProperty('volume', properties);

    candleDocument['source'] = this.getProperty('source', properties);
    candleDocument['interpolated'] = this.getProperty('interpolated', properties);

    return candleDocument as Candle.CandleDocument;
  }

  newCandleDocuments(count: number, properties?: any) {
    const documents = [] as Candle.CandleDocument[];

    for (let i = 0; i < count; i++) {
      documents.push(this.newCandleDocument(properties));
    }

    return documents;
  }
}


export { CandleDocumentFactory };
