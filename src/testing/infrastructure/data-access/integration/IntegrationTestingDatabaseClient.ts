
import { inject, injectable } from 'inversify';
import { MongoMemoryServer } from 'mongodb-memory-server';

import { TYPES as CONFIG_TYPES } from '../../../../infrastructure/config/module';
import { DatabaseClient } from '../../../../infrastructure/data-access/DatabaseClient';
import { MongoConfig } from '../../../../infrastructure/data-access/MongoConfigOptionsGroup';
import { Logger } from '../../../../infrastructure/logger/Logger';
import { TYPES as LOGGER_TYPES } from '../../../../infrastructure/logger/module';


@injectable()
class IntegrationTestingDatabaseClient extends DatabaseClient {
  private _mongoServer;

  constructor(
    @inject(CONFIG_TYPES.Config) readonly config: MongoConfig,
    @inject(LOGGER_TYPES.Logger) readonly logger: Logger) {

    super(config, logger);
  }

  async init() {
    this._mongoServer = new MongoMemoryServer();
    this._connectionString = await this._mongoServer.getConnectionString();

    this._logger.log('info', '\nIntegration test created in-memory mongod at mongoUri = ' + this._connectionString);

    await super.init();
  }

  async disconnect() {
    await super.disconnect();
    this._mongoServer.stop();

    this._logger.log('info', '\nIntegration test destroyed in-memory mongod at mongoUri = ' + this._connectionString);
  }

  async reset() {
    const promises = [];

    // @ts-ignore
    for (const model of Object.values(this._dbClient.models)) {
      // @ts-ignore
      promises.push(model.remove({}).exec());
    }

    return Promise.all(promises);
  }
}


export { IntegrationTestingDatabaseClient };
