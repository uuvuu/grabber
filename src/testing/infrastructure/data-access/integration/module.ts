
import 'reflect-metadata';

import { AsyncContainerModule, interfaces } from 'inversify';

import { DatabaseClient, DbClient } from '../../../../infrastructure/data-access/DatabaseClient';
import { TYPES } from '../../../../infrastructure/data-access/module';
import { referenceContainer } from '../../../../infrastructure/inversify';

import { IntegrationTestingDatabaseClient } from './IntegrationTestingDatabaseClient';


const IntegrationTestingDatabaseModule = new AsyncContainerModule(async (bind: interfaces.Bind) => {
  bind<DatabaseClient>(TYPES.DatabaseClient).to(IntegrationTestingDatabaseClient).inSingletonScope();

  try {
    const databaseClient = referenceContainer.get<DatabaseClient>(TYPES.DatabaseClient);

    await databaseClient.init();
    const dbClient = databaseClient.dbClient;

    bind<DbClient>(TYPES.DbClient).toConstantValue(dbClient);
  } catch (error) {
    // let this error be handled/logged at the caller level
    throw error;
  }
});


export { IntegrationTestingDatabaseModule };
