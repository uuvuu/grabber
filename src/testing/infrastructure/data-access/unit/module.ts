
import 'reflect-metadata';

import { AsyncContainerModule, interfaces } from 'inversify';

import { DbClient } from '../../../../infrastructure/data-access/DatabaseClient';
import { referenceContainer } from '../../../../infrastructure/inversify';
import { TYPES } from '../../../../infrastructure/data-access/module';
import { DatabaseClient } from '../../../../infrastructure/data-access/__mocks__/DatabaseClient';


const DatabaseModule = new AsyncContainerModule(async (bind: interfaces.Bind) => {
  bind<DatabaseClient>(TYPES.DatabaseClient).to(DatabaseClient).inSingletonScope();

  try {
    const databaseClient = referenceContainer.get<DatabaseClient>(TYPES.DatabaseClient);

    await databaseClient.init();
    const dbClient = databaseClient.dbClient;

    bind<DbClient>(TYPES.DbClient).toConstantValue(dbClient);
  } catch (error) {
    // let this error be handled/logged at the caller level
    throw error;
  }
});


export { DatabaseModule, TYPES };
