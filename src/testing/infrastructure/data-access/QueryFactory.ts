
import { Query } from 'mongoose';


class QueryFactory {
  public static newQuery() {
    const query = new Query();

    query.sort = jest.spyOn(query, 'sort').mockImplementation((sort: any) => {
      return query;
    });

    query.limit = jest.spyOn(query, 'limit').mockImplementation((limit: number) => {
      return query;
    });

    return query;
  }
}


export { QueryFactory };
