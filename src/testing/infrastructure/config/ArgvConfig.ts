
import { Command } from 'commander';
import { injectable } from 'inversify';

import { Config as BaseConfig } from '../../../infrastructure/config/Config';

import { argvHelper } from '../../utils/argv';


@injectable()
class ArgvConfig extends BaseConfig {
  // add some command-line argv items that Command expects
  public static argvHelper(argv: string[]) {
    return argvHelper(argv);
  }

  public constructor(command = new Command().name('ConfigMock'), argv = process.argv) {
    super(command, argv);
  }
}

export { ArgvConfig };
