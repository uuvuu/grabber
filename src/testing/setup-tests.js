
// reference typescript modules via require
const MongoUtils = require('./utils/MongoUtils');

expect.extend({
  toMatchDocument: MongoUtils.MongoUtils.toMatchDocument,
  toMatchDocuments: MongoUtils.MongoUtils.toMatchDocuments,
  toMatchNewDocument: MongoUtils.MongoUtils.toMatchNewDocument,
  toMatchNewDocuments: MongoUtils.MongoUtils.toMatchNewDocuments
});
