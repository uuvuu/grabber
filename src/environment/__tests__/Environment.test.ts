
import { Environment } from '../environment';


describe('Environment', () => {
  beforeEach(() => {
    delete process.env['NODE_ENV'];
  });

  test('development environment is setup when process.env[\'NODE_ENV\'] is undefined', () => {
    expect(Environment.environment.production).toEqual(false);
  });

  test('development environment is setup when process.env[\'NODE_ENV\'] !== \'production\'', () => {
    process.env['NODE_ENV'] = 'development';

    expect(Environment.environment.production).toEqual(false);
  });

  test('prod environment is setup when process.env[\'NODE_ENV\'] === \'production\'', () => {
    process.env['NODE_ENV'] = 'production';

    expect(Environment.environment.production).toEqual(true);
  });
});
