
// do not use this directly, use the Environment class instead
const production_environment = {
  production: true,

  dashboardNumberOfPointsOrders: 5760,
  dashboardNumberOfPointsAccounts: 2000
};


export { production_environment };
