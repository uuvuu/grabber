
// do not use this directly, use the Environment class instead
const development_environment = {
  production: false,

  dashboardNumberOfPointsOrders: 576,
  dashboardNumberOfPointsAccounts: 200
};


export { development_environment };
