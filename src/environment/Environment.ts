

class Environment {
  static get environment() {
    if (process.env['NODE_ENV'] && process.env['NODE_ENV'] === 'production') {
      return require('./environment.prod').production_environment;
    }

    return require('./environment.dev').development_environment;
  }
}


export { Environment };
