
import { Document } from 'mongoose';


namespace Candle {
  // candle source was the live ws feed or the from the api
  export type CandleSource = 'cbpro-websocket' | 'cbpro-api';

  export interface Candle {
    product: string;

    time: Date;

    low: string;
    high: string;
    open: string;
    close: string;
    volume: string;

    source: CandleSource;

    /*
     source = 'cbpro-api'
     was this candle's data interpolated from an earlier candle. this happens when there
     are holes in the candle data via an api call. we interpolate the data, but we don't
     know what the true data was.

     source = 'feed'
     this will never be true for candles that come from the ws feed. however, there may be
     0 volume candle intervals in which case we will populate the candle with a previous
     candle's data. this is different from the 'cbpro-api' interpolation case where we don't know
     the true data. in the 'feed' data case, we konw the true volume was 0 and we set the
     other fields appropriately.
     */
    interpolated: boolean;
  }


  export interface CandleDocument extends Candle, Document {
  }
}

namespace CandleUtils {
  export function candleComparator(candle1: Candle.CandleDocument, candle2: Candle.CandleDocument) {
    if (candle1.time < candle2.time) {
      return -1;
    }

    if (candle1.time > candle2.time) {
      return 1;
    }

    return 0;
  }


  export function copyCandleDocument(candleDocument: Candle.CandleDocument) {
    const candle = JSON.parse(JSON.stringify(candleDocument)) as Candle.CandleDocument;

    candle.time = new Date(candle.time);

    return candle;
  }

  export function copyCandleDocuments(candleDocuments: Candle.CandleDocument[]) {
    return candleDocuments.map(candleDocument => {
      return copyCandleDocument(candleDocument);
    });
  }

  export function copyNewCandleDocument(candleDocument: Candle.CandleDocument) {
    const candle = JSON.parse(JSON.stringify(candleDocument)) as Candle.CandleDocument;

    candle.time = new Date(candle.time);

    delete candle._id;
    delete candle.__v;

    return candle;
  }

  export function copyNewCandleDocuments(candleDocuments: Candle.CandleDocument[]) {
    return candleDocuments.map(candleDocument => {
      return copyNewCandleDocument(candleDocument);
    });
  }

  export function interpolateCandle(candleDocument: Candle.CandleDocument, date: Date) {
    const candle = copyNewCandleDocument(candleDocument);

    candle.time = new Date(date);
    candle.low = candleDocument.close;
    candle.high = candleDocument.close;
    candle.open = candleDocument.close;
    candle.close = candleDocument.close;
    candle.volume = '0';
    candle.source = 'cbpro-api';
    candle.interpolated = true;

    return candle;
  }
}


export { Candle, CandleUtils };
