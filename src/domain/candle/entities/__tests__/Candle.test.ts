
import * as lolex from 'lolex';

import { CandleDocumentFactory } from '../../../../testing/entities/CandleDocumentFactory';

import { CandleUtils } from '../Candle';

describe('Candle', () => {
  let clock;
  let candleDocumentFactory;

  beforeAll(() => {
    clock = lolex.install({ shouldAdvanceTime: true });
    candleDocumentFactory = new CandleDocumentFactory(new Date(), 60000);
  });

  afterAll(() => {
    clock.uninstall();
  });

  test('copyCandleDocument() copies candle', () => {
    const candle = candleDocumentFactory.newCandleDocument();
    const copiedCandle = CandleUtils.copyCandleDocument(candle);

    // @ts-ignore
    expect(copiedCandle).toMatchDocument(candle);
    expect(copiedCandle.time.constructor.name).toEqual(candle.time.constructor.name);
  });

  test('copyCandleDocuments() copies candles', () => {
    const candles = candleDocumentFactory.newCandleDocuments(2);
    const copiedCandles = CandleUtils.copyCandleDocuments(candles);

    // @ts-ignore
    expect(copiedCandles).toMatchDocuments(candles);
  });

  test('candleComparator()', () => {
    const [ candle1, candle2 ] = candleDocumentFactory.newCandleDocuments(2);

    expect(CandleUtils.candleComparator(candle1, candle2)).toEqual(-1);
    expect(CandleUtils.candleComparator(candle2, candle1)).toEqual(1);

    const copiedCandle = CandleUtils.copyCandleDocument(candle1);

    expect(CandleUtils.candleComparator(candle1, copiedCandle)).toEqual(0);
  });
});

