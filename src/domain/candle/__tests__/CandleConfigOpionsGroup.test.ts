
import { injectable } from 'inversify';

import { TYPES as CONFIG_TYPES } from '../../../infrastructure/config/module';
import { referenceContainer } from '../../../infrastructure/inversify';
import { ArgvConfig } from '../../../testing/infrastructure/config/ArgvConfig';

import { CandleConfigOptionsGroup } from '../CandleConfigOptionsGroup';


describe('CandleConfig config', () => {
  @injectable()
  class TestConfig extends ArgvConfig {
    public constructor() {
      super();

      this.configOptionsGroups.push(new CandleConfigOptionsGroup());
    }
  }

  let originalArgv;

  beforeEach(() => {
    referenceContainer.bind<ArgvConfig>(CONFIG_TYPES.Config).to(TestConfig).inSingletonScope();

    originalArgv = process.argv;
  });

  afterEach(() => {
    process.argv = originalArgv;

    referenceContainer.unbindAll();
  });

  test('validateOptions() fails if durationInMillis is not an integer', () => {
    const originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper(['-d a']);

    expect(() => { referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config); }).toThrow();

    process.argv = originalArgv;
  });

  test('validateOptions() fails if durationInMillis is less than 1', () => {
    const originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper(['-d 0']);

    expect(() => { referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config); }).toThrow();

    process.argv = originalArgv;
  });

  test('validateOptions() fails if durationInMillis is not divisible by 1000', () => {
    const originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper(['-d 1230']);

    expect(() => { referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config); }).toThrow();

    process.argv = originalArgv;
  });
});
