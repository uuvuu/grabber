
import { injectable } from 'inversify';

import { TYPES as CONFIG_TYPES } from '../../../infrastructure/config/module';
import { referenceContainer } from '../../../infrastructure/inversify';
import { ArgvConfig } from '../../../testing/infrastructure/config/ArgvConfig';

import { CandleReaderConfig, CandleReaderConfigOptionsGroup } from '../CandleReaderConfigOptionsGroup';


describe('CandleReaderConfig config', () => {
  @injectable()
  class TestConfig extends ArgvConfig {
    public constructor() {
      super();

      this.configOptionsGroups.push(new CandleReaderConfigOptionsGroup());
    }
  }

  let originalArgv;
  let testArgv;

  beforeAll(() => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([]);
  });

  afterAll(() => {
    process.argv = originalArgv;
  });

  beforeEach(() => {
    referenceContainer.bind<ArgvConfig>(CONFIG_TYPES.Config).to(TestConfig).inSingletonScope();

    testArgv = process.argv;
  });

  afterEach(() => {
    process.argv = testArgv;

    referenceContainer.unbindAll();
  });

  test('correct config when using numberOfCandles', () => {
    const numberOfCandles = 10;
    process.argv = ArgvConfig.argvHelper([ `-n  ${numberOfCandles}` ]);

    // @ts-ignore
    const config = referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config) as CandleReaderConfig;

    expect(config.durationInMillis).toEqual(CandleReaderConfigOptionsGroup.DEFAULTS.DURATION_IN_MILLIS);
    expect(config.endDate).toEqual(CandleReaderConfigOptionsGroup.DEFAULTS.NOW());
    expect(config.numberOfCandles).toEqual(numberOfCandles);
  });

  test('correct config using startDate', () => {
    const startDate = '2018-08-10T01:47:00.000Z';
    process.argv = ArgvConfig.argvHelper([ `-s ${startDate}` ]);

    // @ts-ignore
    const config = referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config) as CandleReaderConfig;

    expect(config.durationInMillis).toEqual(CandleReaderConfigOptionsGroup.DEFAULTS.DURATION_IN_MILLIS);
    expect(config.endDate).toEqual(CandleReaderConfigOptionsGroup.DEFAULTS.NOW());

    expect(config.startDate).toEqual(new Date(startDate));
  });

  test('validateOptions() fails if neither startDate nor numberOfCandles are set', () => {
    expect(() => { referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config); }).toThrow();
  });

  test('validateOptions() fails if numberOfCandles is not an integer', () => {
    process.argv = ArgvConfig.argvHelper([ '-n a' ]);

    expect(() => { referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config); }).toThrow();
  });

  test('validateOptions() fails if numberOfCandles is less than 1', () => {
    process.argv = ArgvConfig.argvHelper([ '-n 0' ]);

    expect(() => { referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config); }).toThrow();
  });

  test('validateOptions() fails if startDate >= NOW', () => {
    process.argv = ArgvConfig.argvHelper([ `-s ${CandleReaderConfigOptionsGroup.DEFAULTS.NOW().toISOString()}` ]);

    expect(() => { referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config); }).toThrow();
  });

  test('validateOptions() fails if startDate > endDate using startDate', () => {
    const endDate = '2018-08-10T01:47:00.000Z';

    const date = new Date(endDate);
    date.setMilliseconds(date.getMilliseconds() + 1);

    const startDate = date.toISOString();

    process.argv = ArgvConfig.argvHelper([ `-s ${startDate} -e ${endDate}` ]);

    expect(() => { referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config); }).toThrow();
  });

  test('validateOptions() fails if startDate > endDate using numberOfCandles', () => {
    const endDate = '2018-08-10T01:47:00.000Z';

    process.argv = ArgvConfig.argvHelper([ `-n 10 -e ${endDate}` ]);

    expect(() => { referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config); }).toThrow();
  });

  test('validateOptions() fails if endDate >= NOW', () => {
    process.argv = ArgvConfig.argvHelper([ `-e ${CandleReaderConfigOptionsGroup.DEFAULTS.NOW().toISOString()}` ]);

    expect(() => { referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config); }).toThrow();
  });

  test('validateOptions() fails if both startDate and numberOfCandles are provided', () => {
    process.argv = ArgvConfig.argvHelper([ `-s ${CandleReaderConfigOptionsGroup.DEFAULTS.NOW().toISOString()} -n 10` ]);

    expect(() => { referenceContainer.get<ArgvConfig>(CONFIG_TYPES.Config); }).toThrow();
  });
});
