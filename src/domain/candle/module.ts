
import 'reflect-metadata';

import { ContainerModule, interfaces } from 'inversify';

import { CandleRepositoryImpl, TYPES } from './repositories/impl/CandleRepositoryImpl';
import { CandleRepository } from './repositories/CandleRepository';


const CandleModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<CandleRepository>(TYPES.CandleRepository).to(CandleRepositoryImpl).inSingletonScope();
  }
);


export { CandleModule, TYPES };
