
import { Command } from 'commander';

import { ConfigError, ConfigErrorFactory } from '../../infrastructure/config/Config';
import { ConfigOptionsGroup } from '../../infrastructure/config/ConfigOptionsGroup';


class DEFAULTS {
  static readonly DURATION_IN_MILLIS = 60000;
}

class CandleConfigOptionsGroup extends ConfigOptionsGroup {
  static readonly DEFAULTS = DEFAULTS;

  constructor() {
    super();

    this.addOption({
      durationInMillis: {
        flag: '-d',
        description: `Candle interval duration in milliseconds.`,
        coercionFunction: this.parseInt,
        defaultValue: CandleConfigOptionsGroup.DEFAULTS.DURATION_IN_MILLIS
      }
    });
  }

  validateOptions(command: Command, configErrorFactory: ConfigErrorFactory): ConfigError {
    if (! Number.isInteger(command.durationInMillis) || command.durationInMillis < 1 || command.durationInMillis % 1000 !== 0) {
      return configErrorFactory.newError(`durationInMillis invalid. durationInMillis must be a whole number, evenly divisible by 1000.`);
    }

    return undefined;
  }
}

interface CandleConfig {
  durationInMillis: number;
}


export { CandleConfig, CandleConfigOptionsGroup };
