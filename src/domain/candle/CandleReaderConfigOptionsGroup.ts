

import { Command } from 'commander';

import { ConfigError, ConfigErrorFactory } from '../../infrastructure/config/Config';
import { DateUtils } from '../../utils/DateUtils';
import { CandleConfig, CandleConfigOptionsGroup } from './CandleConfigOptionsGroup';


class DEFAULTS extends CandleConfigOptionsGroup.DEFAULTS {
  private static _now;

  static NOW() {
    if (! DEFAULTS._now) {
      DEFAULTS._now = new Date();
      DEFAULTS._now.setSeconds(0);
      DEFAULTS._now.setMilliseconds(0);
    }

    return new Date(DEFAULTS._now);
  }
}

class CandleReaderConfigOptionsGroup extends CandleConfigOptionsGroup {
  static readonly DEFAULTS = DEFAULTS;

  constructor() {
    super();

    this.addOption({
      startDate: {
        flag: '-s',
        description: 'Start date of candles.',
        coercionFunction: this.parseDate
      }
    });

    this.addOption({
      endDate: {
        flag: '-e',
        description: 'End date of candles.',
        coercionFunction: this.parseDate,
        defaultValue: CandleReaderConfigOptionsGroup.DEFAULTS.NOW()
      }
    });

    this.addOption({
      numberOfCandles: {
        flag: '-n',
        description: 'Number of candles from the current time.',
        coercionFunction: this.parseInt
      }
    });
  }

  validateOptions(command: Command, configErrorFactory: ConfigErrorFactory): ConfigError {
    const configError = super.validateOptions(command, configErrorFactory);

    if (configError) {
      return configError;
    }

    let numberOfCandlesSpecified = false;

    if (typeof command.numberOfCandles !== 'undefined' && typeof command.startDate !== 'undefined') {
      return configErrorFactory.newError(`numberOfCandles and startDate cannot both be specified on the command line.`);
    }

    if (typeof command.numberOfCandles !== 'undefined') {
      numberOfCandlesSpecified = true;

      if (! Number.isInteger(command.numberOfCandles) || command.numberOfCandles < 1) {
        return configErrorFactory.newError(`numberOfCandles must be an integer that is greater than 0.`);
      } else {
        command.startDate = DateUtils.getPastDateFromMillis(command.numberOfCandles * command.durationInMillis, DateUtils.TimeAlignment.MINUTES);
      }
    } else if (typeof command.startDate === 'undefined') {
      return configErrorFactory.newError('startDate or numberOfCandles must be specified on the command line.');
    }

    if (isNaN(command.startDate.getTime())) {
      return configErrorFactory.newError(`startDate is an invalid date.`);
    }

    if (command.startDate >= CandleReaderConfigOptionsGroup.DEFAULTS.NOW()) {
      return configErrorFactory.newError(`startDate is in the future.`);
    }

    if (command.startDate > command.endDate) {
      if (numberOfCandlesSpecified) {
        return configErrorFactory.newError(`numberOfCandles results in a startDate that is after endDate.`);
      } else {
        return configErrorFactory.newError(`startDate is after endDate.`);
      }
    }

    if (command.endDate > CandleReaderConfigOptionsGroup.DEFAULTS.NOW()) {
      return configErrorFactory.newError(`endDate is in the future.`);
    }

    return undefined;
  }
}

interface CandleReaderConfig extends CandleConfig {
  startDate: Date;
  endDate: Date;
  numberOfCandles: number;
}


export { CandleReaderConfig, CandleReaderConfigOptionsGroup };
