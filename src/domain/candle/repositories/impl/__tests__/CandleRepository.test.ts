
import * as lolex from 'lolex';

import { AsyncContainerModule, ContainerModule, injectable } from 'inversify';

import { LoggerConfigOptionsGroup } from '../../../../../infrastructure/logger/LoggerConfigOptionsGroup';
import { TYPES as CONFIG_TYPES } from '../../../../../infrastructure/config/module';
import { ProductConfig, ProductConfigOptionsGroup } from '../../../../../infrastructure/config/ProductConfigOptionsGroup';
import { ModelFactory } from '../../../../../infrastructure/data-access/factories/ModelFactory';
import { ModelFactoryModule, TYPES as FACTORY_TYPES } from '../../../../../infrastructure/data-access/factories/module';
import { referenceContainer } from '../../../../../infrastructure/inversify';
import { LoggerModule } from '../../../../../infrastructure/logger/module';
import { CandleDocumentFactory } from '../../../../../testing/entities/CandleDocumentFactory';
import { ArgvConfig } from '../../../../../testing/infrastructure/config/ArgvConfig';
import { DatabaseModule } from '../../../../../testing/infrastructure/data-access/unit/module';
import { QueryFactory } from '../../../../../testing/infrastructure/data-access/QueryFactory';

import { CandleModule, TYPES } from '../../../module';

import { CandleRepository } from '../../CandleRepository';

import { CandleModel, CandleSchema } from '../CandleRepositoryImpl';


const DEPENDENCIES: ContainerModule[] = [
  CandleModule,
  LoggerModule,
  ModelFactoryModule
];

const ASYNC_DEPENDENCIES: AsyncContainerModule[] = [
  DatabaseModule
];


describe('CandleRepository', () => {
  @injectable()
  class Config extends ArgvConfig {
    public constructor() {
      super();

      this.configOptionsGroups.push(new LoggerConfigOptionsGroup('TestCandleRepository'));
      this.configOptionsGroups.push(new ProductConfigOptionsGroup());
    }
  }

  let clock;
  let candleDocumentFactory;

  let model: CandleModel;
  let candleRepository: CandleRepository;
  let originalCandleLimit: number;

  let originalArgv;

  beforeAll(async () => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([]);

    clock = lolex.install({ shouldAdvanceTime: true });
    candleDocumentFactory = new CandleDocumentFactory(new Date(), 60000);

    originalCandleLimit = CandleSchema.statics.limit;
    CandleSchema.statics.limit = 3;

    referenceContainer.bind<Config>(CONFIG_TYPES.Config).to(Config).inSingletonScope();

    referenceContainer.load(...DEPENDENCIES);

    try {
      await referenceContainer.loadAsync(...ASYNC_DEPENDENCIES);
    } catch (error) {
      // let this error be handled/logged at the caller level
      throw error;
    }

    // @ts-ignore
    const config = referenceContainer.get<Config>(CONFIG_TYPES.Config) as ProductConfig;

    const modelFactory = referenceContainer.get<ModelFactory>(FACTORY_TYPES.ModelFactory);

    model = modelFactory.newProductModel(config.product, 'candle', TYPES.CandleModel, CandleSchema);

    candleRepository = referenceContainer.get<CandleRepository>(TYPES.CandleRepository);
  });

  afterAll(() => {
    referenceContainer.unbindAll();
    CandleSchema.statics.limit = originalCandleLimit;
    clock.uninstall();

    process.argv = originalArgv;
  });

  test('is bound to container', () => {
    expect(candleRepository).toBeDefined();
  });

  test('findById() returns document if found', async () => {
    expect.assertions(2);

    const candleDocument = candleDocumentFactory.newCandleDocument();

    const originalFindById = model.findById;

    model.findById = jest.fn().mockImplementation((id, callback) => callback(undefined, candleDocument));

    await expect(candleRepository.findById(candleDocument._id)).resolves.toEqual(candleDocument);

    expect(model.findById).toHaveBeenCalledTimes(1);

    model.findById = originalFindById;
  });

  test('findById() rejects if nothing found', async () => {
    expect.assertions(2);

    await expect(candleRepository.findById('abcd')).rejects.toBeUndefined();

    expect(model.findById).toHaveBeenCalledTimes(1);
  });

  test('findById() rejects with error if there is an error', async () => {
    expect.assertions(2);

    const error = 'error';
    const originalFindById = model.findById;

    model.findById = jest.fn().mockImplementation((id, callback) => callback(error));

    await expect(candleRepository.findById('abcd')).rejects.toEqual(error);

    expect(model.findById).toHaveBeenCalledTimes(1);

    model.findById = originalFindById;
  });

  test('count() is correct', async() => {
    expect.assertions(1);

    const count = 10;

    const originalCount = model.countDocuments;

    model.countDocuments = jest.fn().mockImplementation((id, callback) => callback(undefined, count));

    await expect(candleRepository.count()).resolves.toEqual(count);

    model.countDocuments = originalCount;
  });

  test('count() rejects with error if there is an error', async() => {
    expect.assertions(2);

    const error = 'error';

    const originalCount = model.countDocuments;

    model.countDocuments = jest.fn().mockImplementation((id, callback) => callback(error));

    await expect(candleRepository.count()).rejects.toEqual(error);

    expect(model.countDocuments).toHaveBeenCalledTimes(1);

    model.countDocuments = originalCount;
  });

  test('pages() is correct', async () => {
    expect.assertions(1);

    const count = 10;
    const limit = 2;

    const originalCount = model.countDocuments;

    model.limit = limit;
    model.countDocuments = jest.fn().mockImplementation((id, callback) => callback(undefined, count));

    await expect(candleRepository.pages()).resolves.toEqual(count / limit);

    model.countDocuments = originalCount;
  });

  test('pages() rejects with error if there is an error', async () => {
    expect.assertions(2);

    const error = 'error';

    const originalCount = model.countDocuments;

    model.countDocuments = jest.fn().mockImplementation((id, callback) => callback(error));

    await expect(candleRepository.pages()).rejects.toEqual(error);

    expect(model.countDocuments).toHaveBeenCalledTimes(1);

    model.countDocuments = originalCount;
  });

  test('findManyByPage() item count == 0', async () => {
    expect.assertions(7);

    const pageOffset = new Date();
    const pageQuery = model.pageQuery(pageOffset);

    const query = QueryFactory.newQuery();

    model.find = jest.spyOn(model, 'find').mockImplementation(() => query);

    query.exec = jest.spyOn(query, 'exec').mockImplementation(callback => callback(undefined, []));

    await expect(candleRepository.findManyByPage(pageOffset)).resolves.toEqual([]);

    expect(model.find).toHaveBeenCalledTimes(1);
    expect(model.find).toHaveBeenCalledWith(pageQuery.filter);
    expect(query.sort).toHaveBeenCalledTimes(1);
    expect(query.sort).toHaveBeenCalledWith(pageQuery.sort);
    expect(query.limit).toHaveBeenCalledTimes(1);
    expect(query.limit).toHaveBeenCalledWith(CandleSchema.statics.limit);

    // @ts-ignore
    model.find.mockRestore();
    // @ts-ignore
    query.exec.mockRestore();
  });

  test('findManyByPage() item count > 0', async () => {
    expect.assertions(7);

    const pageOffset = new Date();
    const pageQuery = model.pageQuery(pageOffset);

    const candleDocs = candleDocumentFactory.newCandleDocuments(2);

    const query = QueryFactory.newQuery();

    model.find = jest.spyOn(model, 'find').mockImplementation(() => {
      return query;
    });

    query.exec = jest.spyOn(query, 'exec').mockImplementation(callback => {
      return callback(undefined, candleDocs);
    });

    await expect(candleRepository.findManyByPage(pageOffset)).resolves.toEqual(candleDocs);

    expect(model.find).toHaveBeenCalledTimes(1);
    expect(model.find).toHaveBeenCalledWith(pageQuery.filter);
    expect(query.sort).toHaveBeenCalledTimes(1);
    expect(query.sort).toHaveBeenCalledWith(pageQuery.sort);
    expect(query.limit).toHaveBeenCalledTimes(1);
    expect(query.limit).toHaveBeenCalledWith(CandleSchema.statics.limit);

    // @ts-ignore
    model.find.mockRestore();
    // @ts-ignore
    query.exec.mockRestore();
  });

  test('findManyByPage() rejects with error if there is an error', async () => {
    expect.assertions(7);

    const pageOffset = new Date();
    const pageQuery = model.pageQuery(pageOffset);

    const query = QueryFactory.newQuery();

    model.find = jest.spyOn(model, 'find').mockImplementation(() => query);

    const error = 'error';
    query.exec = jest.spyOn(query, 'exec').mockImplementation(() => {
      throw error;
    });

    await expect(candleRepository.findManyByPage(pageOffset)).rejects.toEqual(error);

    expect(model.find).toHaveBeenCalledTimes(1);
    expect(model.find).toHaveBeenCalledWith(pageQuery.filter);
    expect(query.sort).toHaveBeenCalledTimes(1);
    expect(query.sort).toHaveBeenCalledWith(pageQuery.sort);
    expect(query.limit).toHaveBeenCalledTimes(1);
    expect(query.limit).toHaveBeenCalledWith(CandleSchema.statics.limit);

    // @ts-ignore
    model.find.mockRestore();
    // @ts-ignore
    query.exec.mockRestore();
  });

  test('save() valid candle', async () => {
    expect.assertions(2);

    const candle = candleDocumentFactory.newCandleDocument();

    model.insertMany = jest.spyOn(model, 'insertMany').mockImplementation(
      (docs, options, callback) => callback(undefined, docs)
    );

    await expect(candleRepository.insertMany([candle])).resolves.toEqual([candle]);

    expect(model.insertMany).toHaveBeenCalledTimes(1);

    // @ts-ignore
    model.insertMany.mockRestore();
  });
});


