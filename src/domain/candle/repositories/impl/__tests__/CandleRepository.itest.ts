
import * as lolex from 'lolex';

import { AsyncContainerModule, ContainerModule, injectable } from 'inversify';

import { TYPES as CONFIG_TYPES } from '../../../../../infrastructure/config/module';
import { ProductConfig, ProductConfigOptionsGroup } from '../../../../../infrastructure/config/ProductConfigOptionsGroup';
import { ModelFactory } from '../../../../../infrastructure/data-access/factories/ModelFactory';
import { TYPES as DB_TYPES } from '../../../../../infrastructure/data-access/module';
import { ModelFactoryModule, TYPES as FACTORY_TYPES } from '../../../../../infrastructure/data-access/factories/module';
import { referenceContainer } from '../../../../../infrastructure/inversify';
import { LoggerConfigOptionsGroup } from '../../../../../infrastructure/logger/LoggerConfigOptionsGroup';
import { LoggerModule } from '../../../../../infrastructure/logger/module';
import { ArgvConfig } from '../../../../../testing/infrastructure/config/ArgvConfig';
import { IntegrationTestingDatabaseClient } from '../../../../../testing/infrastructure/data-access/integration/IntegrationTestingDatabaseClient';
import { IntegrationTestingDatabaseModule } from '../../../../../testing/infrastructure/data-access/integration/module';
import { CandleDocumentFactory } from '../../../../../testing/entities/CandleDocumentFactory';

import { CandleModule, TYPES } from '../../../module';

import { CandleRepository } from '../../CandleRepository';

import { CandleModel, CandleSchema } from '../CandleRepositoryImpl';


const DEPENDENCIES: ContainerModule[] = [
  CandleModule,
  LoggerModule,
  ModelFactoryModule
];

const ASYNC_DEPENDENCIES: AsyncContainerModule[] = [
  IntegrationTestingDatabaseModule
];


describe('CandleRepository Integration', () => {
  @injectable()
  class Config extends ArgvConfig {
    public constructor() {
      super();

      this.configOptionsGroups.push(new LoggerConfigOptionsGroup('TestCandleRepository'));
      this.configOptionsGroups.push(new ProductConfigOptionsGroup());
    }
  }

  let clock;
  let candleDocumentFactory;

  let databaseClient: IntegrationTestingDatabaseClient;
  let candleRepository: CandleRepository;
  let model: CandleModel;
  let originalCandleLimit: number;

  let originalArgv;

  let testStartDate;

  beforeAll(async () => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([]);

    clock = lolex.install({ shouldAdvanceTime: true });

    testStartDate = new Date();

    candleDocumentFactory = new CandleDocumentFactory(testStartDate, 60000);

    originalCandleLimit = CandleSchema.statics.limit;
    CandleSchema.statics.limit = 3;

    referenceContainer.bind<Config>(CONFIG_TYPES.Config).to(Config).inSingletonScope();

    referenceContainer.load(...DEPENDENCIES);

    try {
      await referenceContainer.loadAsync(...ASYNC_DEPENDENCIES);
    } catch (error) {
      // let this error be handled/logged at the caller level
      throw error;
    }

    databaseClient = referenceContainer.get<IntegrationTestingDatabaseClient>(DB_TYPES.DatabaseClient);

    // @ts-ignore
    const config = referenceContainer.get<Config>(CONFIG_TYPES.Config) as ProductConfig;

    const modelFactory = referenceContainer.get<ModelFactory>(FACTORY_TYPES.ModelFactory);
    model = modelFactory.newProductModel(config.product, 'candle', TYPES.CandleModel, CandleSchema);

    candleRepository = referenceContainer.get<CandleRepository>(TYPES.CandleRepository);
  });

  afterAll(async () => {
    // make sure clock.uninstall() comes first as there are some sleep() calls in infrastructure teardown
    // if lolex.install() is installed without the { shouldAdvanceTime: true } option, the sleep() will
    // never return
    clock.uninstall();

    await databaseClient.reset();
    await databaseClient.disconnect();

    referenceContainer.unbindAll();

    CandleSchema.statics.limit = originalCandleLimit;

    process.argv = originalArgv;
  });

  beforeEach(async () => {
    await model.remove({});
  });

  test('insertMany() returns inserted candles with _id and __v set', async () => {
    const totalCandleCount = CandleSchema.statics.limit + 1;

    expect.assertions(2 + totalCandleCount * 2);

    const candles = candleDocumentFactory.newCandleDocuments(totalCandleCount);

    const insertedCandles = await candleRepository.insertMany(candles);

    expect(insertedCandles.length).toEqual(totalCandleCount);

    // @ts-ignore
    expect(insertedCandles).toMatchNewDocuments(candles);

    insertedCandles.forEach(insertedCandle => {
      expect('_id' in insertedCandle).toBeTruthy();
      expect('__v' in insertedCandle).toBeTruthy();
    });
  });

  test('findById() returns one of the inserted candles', async() => {
    const totalCandleCount = CandleSchema.statics.limit + 1;

    expect.assertions(1);

    const candles = candleDocumentFactory.newCandleDocuments(totalCandleCount);

    const insertedCandles = await candleRepository.insertMany(candles);

    const candle = await candleRepository.findById(insertedCandles[insertedCandles.length - 1]._id);

    // @ts-ignore
    expect(candle).toMatchDocument(insertedCandles[insertedCandles.length - 1]);
  });

  test('findManyByPage() returns correct candles by page', async () => {
    const pageCount = CandleSchema.statics.limit;
    const totalCandleCount = CandleSchema.statics.limit + 1;

    expect.assertions(3);

    const candles = candleDocumentFactory.newCandleDocuments(totalCandleCount);

    const insertedCandles = await candleRepository.insertMany(candles);

    expect(insertedCandles.length).toEqual(totalCandleCount);

    let dbCandles = await candleRepository.findManyByPage(testStartDate);

    expect(dbCandles.length).toEqual(pageCount);

    const dateForNextPage = new Date(dbCandles[dbCandles.length - 1].time);
    dateForNextPage.setMilliseconds(dateForNextPage.getMilliseconds() + 60000);

    dbCandles = await candleRepository.findManyByPage(dateForNextPage);

    expect(dbCandles.length).toEqual(1);
  });

  test('page() returns correct number of pages', async () => {
    const pageCount = CandleSchema.statics.limit;
    const totalCandleCount = CandleSchema.statics.limit + 1;

    expect.assertions(1);

    const candles = candleDocumentFactory.newCandleDocuments(totalCandleCount);

    await candleRepository.insertMany(candles);
    const pages = await candleRepository.pages();

    expect(pages).toEqual(Math.ceil(totalCandleCount / pageCount));
  });

  test('count() returns correct number of pages', async () => {
    const totalCandleCount = CandleSchema.statics.limit + 1;

    expect.assertions(1);

    const candles = candleDocumentFactory.newCandleDocuments(totalCandleCount);

    await candleRepository.insertMany(candles);
    const count = await candleRepository.count();

    expect(count).toEqual(totalCandleCount);
  });
});

