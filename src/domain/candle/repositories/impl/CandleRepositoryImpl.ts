
import { inject, injectable } from 'inversify';
import { Model, Schema } from 'mongoose';

import { Config } from '../../../../infrastructure/config/Config';
import { TYPES as CONFIG_TYPES } from '../../../../infrastructure/config/module';
import { ModelFactory } from '../../../../infrastructure/data-access/factories/ModelFactory';
import { TYPES as FACTORY_TYPES } from '../../../../infrastructure/data-access/factories/module';
import { Pageable, Query, SortOrder } from '../../../../infrastructure/data-access/Query';
import { BaseRepositoryImpl } from '../../../../infrastructure/data-access/repositories/impl/BaseRepositoryImpl';

import { Candle } from '../../entities/Candle';

import { CandleRepository } from '../CandleRepository';


const TYPES = {
  CandleModel: Symbol.for('CandleModel'),
  CandleRepository: Symbol.for('CandleRepository')
};


const CANDLES_LIMIT = 5000;


interface CandleModel extends Model<Candle.CandleDocument>, Pageable<Date> {
}


const CandleSchema = new Schema({
  product: { type: String, required: true },

  // interval bucket startDate time
  time: { type: Date, required: true, index: true, unique: true },

  low: { type: String, required: true },      // lowest price during the bucket interval
  high: { type: String, required: true },     // highest price during the bucket interval
  open: { type: String, required: true },     // opening price (first trade) in the bucket interval
  close: { type: String, required: true },    // closing price (last trade) in the bucket interval
  volume: { type: String, required: true },   // volume of trading activity during the bucket interval

  // see docs in src/domain/candle/entities/Candle.ts
  source: { type: String, required: true },
  interpolated: { type: Boolean, required: true }
});


CandleSchema.statics.limit = CANDLES_LIMIT;

CandleSchema.statics.offset = (value: Date): Query<Candle.CandleDocument> => {
  return {
    filter: {
      time: {
        $gte: new Date(value)
      }
    }
  };
};

CandleSchema.statics.pageQuery = (value: Date): Query<Candle.CandleDocument> => {
  const query = CandleSchema.statics.offset(new Date(value));

  query.limit = CandleSchema.statics.limit;
  query.sort = { time: 1 };

  return query;
};


@injectable()
class CandleRepositoryImpl extends BaseRepositoryImpl<Date, Candle.Candle, Candle.CandleDocument, CandleModel> implements CandleRepository {
  public constructor(
    @inject(CONFIG_TYPES.Config) readonly config: Config,
    @inject(FACTORY_TYPES.ModelFactory) readonly modelFactory: ModelFactory) {

    super(config, modelFactory);
  }

  protected init() {
    this._model =
      this._modelFactory.newProductModel(this._config.product, 'candle', TYPES.CandleModel, CandleSchema);
  }

  async findLatestItem(): Promise<Candle.CandleDocument> {
    const query = {
      sort: {
        time: SortOrder.DESCENDING
      },
      limit: 1
    };

    // @ts-ignore
    const items = await this.findManyByQuery(query);
    return items[0];
  }
}


export { CandleModel, CandleRepositoryImpl, CandleSchema, TYPES };

