
import { BaseRepository } from '../../../infrastructure/data-access/repositories/BaseRepository';

import { Candle } from '../entities/Candle';


interface CandleRepository extends BaseRepository<Date, Candle.CandleDocument> {
  findLatestItem(): Promise<Candle.CandleDocument>;
}


export { CandleRepository };
