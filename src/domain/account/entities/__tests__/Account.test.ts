
import * as lolex from 'lolex';

import { AccountsDocumentFactory } from '../../../../testing/entities/AccountsDocumentFactory';

import { AccountUtils } from '../Account';


describe('Account', () => {
  let clock;
  let accountsDocumentFactory;

  beforeAll(() => {
    clock = lolex.install({ shouldAdvanceTime: true });
    accountsDocumentFactory = new AccountsDocumentFactory(new Date(), 60000);
  });

  afterAll(() => {
    clock.uninstall();
  });

  test('copyAccountsDocument() copies accounts document', () => {
    const accounts = accountsDocumentFactory.newAccountsDocument();
    const copiedAccounts = AccountUtils.copyAccountsDocument(accounts);

    // @ts-ignore
    expect(copiedAccounts).toMatchDocument(accounts);
    expect(copiedAccounts.time.constructor.name).toEqual(accounts.time.constructor.name);
  });

  test('copyAccountsDocuments() copies accounts documents', () => {
    const accountsDocuments = accountsDocumentFactory.newAccountsDocuments(2);
    const copiedAccountsDocuments = AccountUtils.copyAccountsDocuments(accountsDocuments);

    // @ts-ignore
    expect(copiedAccountsDocuments).toMatchDocuments(accountsDocuments);
  });

  test('accountsComparator()', () => {
    const [ accountsDocument1, accountsDocument2 ] = accountsDocumentFactory.newAccountsDocuments(2);

    expect(AccountUtils.accountsComparator(accountsDocument1, accountsDocument2)).toEqual(-1);
    expect(AccountUtils.accountsComparator(accountsDocument2, accountsDocument1)).toEqual(1);

    const copiedAccountsDocument = AccountUtils.copyAccountsDocument(accountsDocument2);

    expect(AccountUtils.accountsComparator(accountsDocument2, copiedAccountsDocument)).toEqual(0);
  });
});


