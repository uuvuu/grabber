
import { Document } from 'mongoose';

namespace Account {
  export interface Account {
    currency: string;

    accountId: string;

    balance: string;
    available: string;
    hold: string;

    exchangeRateUSD: string;

    // based on how we do some aggregations these values may be either a number or a string
    // representing that number. we can't use $toString in the $group stage of the aggregation
    // pipeline
    exchangeRateCrypto: string | number;
  }

  export interface Accounts {
    time: Date;

    profileId: string;

    accounts: Account[];

    // based on how we do some aggregations these values may be either a number or a string
    // representing that number. we can't use $toString in the $group stage of the aggregation
    // pipeline
    totalUSD: string | number;

    // based on how we do some aggregations these values may be either a number or a string
    // representing that number. we can't use $toString in the $group stage of the aggregation
    // pipeline
    exchangeRateCrypto: string | number;

    // based on how we do some aggregations these values may be either a number or a string
    // representing that number. we can't use $toString in the $group stage of the aggregation
    // pipeline
    totalCrypto: string | number;
  }

  export interface AccountsDocument extends Accounts, Document {
  }
}

namespace AccountUtils {
  export function copyAccount(account: Account) {
    const copiedAccount = JSON.parse(JSON.stringify(account)) as Account;

    return copiedAccount;
  }

  export function copyAccounts(accounts: Account[]) {
    return accounts.map(account => {
      return copyAccount(account);
    });
  }

  export function copyAccountsDocument(accountsDocument: Account.AccountsDocument) {
    const accounts = JSON.parse(JSON.stringify(accountsDocument)) as Account.AccountsDocument;

    accounts.time = new Date(accounts.time);

    return accounts;
  }

  export function copyAccountsDocuments(accountsDocuments: Account.AccountsDocument[]) {
    return accountsDocuments.map(accountsDocument => {
      return copyAccountsDocument(accountsDocument);
    });
  }

  export function copyNewAccountsDocument(accountsDocument: Account.AccountsDocument) {
    const accounts = JSON.parse(JSON.stringify(accountsDocument)) as Account.AccountsDocument;

    accounts.time = new Date(accounts.time);

    delete accounts._id;
    delete accounts.__v;

    accounts.accounts.forEach(account => delete account['_id'] );

    return accounts;
  }

  export function copyNewAccountsDocuments(accountsDocuments: Account.AccountsDocument[]) {
    return accountsDocuments.map(accountsDocument => {
      return copyNewAccountsDocument(accountsDocument);
    });
  }

  export function accountsComparator(accounts1: Account.AccountsDocument, accounts2: Account.AccountsDocument) {
    if (accounts1.time < accounts2.time) {
      return -1;
    }

    if (accounts1.time > accounts2.time) {
      return 1;
    }

    return 0;
  }
}


export { Account, AccountUtils };
