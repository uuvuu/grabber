
import 'reflect-metadata';

import { ContainerModule, interfaces } from 'inversify';

import { AccountProductRepositoryImpl } from './repositories/impl/AccountProductRepositoryImpl';

import { TYPES } from './repositories/impl/AccountRepositoryImpl';
import { AccountRepository } from './repositories/AccountRepository';


const AccountModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<AccountRepository>(TYPES.AccountRepository).to(AccountProductRepositoryImpl).inSingletonScope();
  }
);


export { AccountModule, TYPES };
