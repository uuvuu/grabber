
import { BaseRepository } from '../../../infrastructure/data-access/repositories/BaseRepository';

import { Account } from '../entities/Account';


interface AccountRepository extends BaseRepository<Date, Account.AccountsDocument> {
  findFirstAndLastAccount(): Promise<Account.AccountsDocument[]>;

  findMaxAccount(): Promise<Account.AccountsDocument>;
  findMinAccount(): Promise<Account.AccountsDocument>;

  findLatestItem(): Promise<Account.AccountsDocument>;
}


export { AccountRepository };
