
import * as lolex from 'lolex';

import { AsyncContainerModule, ContainerModule, injectable } from 'inversify';

import { ProductConfig, ProductConfigOptionsGroup } from '../../../../../infrastructure/config/ProductConfigOptionsGroup';
import { ModelFactory } from '../../../../../infrastructure/data-access/factories/ModelFactory';
import { LoggerConfigOptionsGroup } from '../../../../../infrastructure/logger/LoggerConfigOptionsGroup';
import { TYPES as CONFIG_TYPES } from '../../../../../infrastructure/config/module';
import { ModelFactoryModule, TYPES as FACTORY_TYPES } from '../../../../../infrastructure/data-access/factories/module';
import { referenceContainer } from '../../../../../infrastructure/inversify';
import { LoggerModule } from '../../../../../infrastructure/logger/module';
import { AccountsDocumentFactory } from '../../../../../testing/entities/AccountsDocumentFactory';
import { ArgvConfig } from '../../../../../testing/infrastructure/config/ArgvConfig';
import { DatabaseModule } from '../../../../../testing/infrastructure/data-access/unit/module';

import { AccountModule, TYPES } from '../../../module';

import { AccountRepository } from '../../AccountRepository';

import { AccountsModel, AccountSchema, AccountsSchema } from '../AccountRepositoryImpl';


const DEPENDENCIES: ContainerModule[] = [
  AccountModule,
  LoggerModule,
  ModelFactoryModule
];

const ASYNC_DEPENDENCIES: AsyncContainerModule[] = [
  DatabaseModule
];


describe('AccountRepository', () => {
  @injectable()
  class Config extends ArgvConfig {
    public constructor() {
      super();

      this.configOptionsGroups.push(new LoggerConfigOptionsGroup('TestAccountRepository'));
      this.configOptionsGroups.push(new ProductConfigOptionsGroup());
    }
  }

  let clock;
  let accountsDocumentFactory;

  let model: AccountsModel;
  let accountRepository: AccountRepository;
  let originalAccountLimit: number;

  let originalArgv;

  let config;

  function aggregateQuery(query) {
    return [
      {$match: query.filter},
      expect.anything(),
      {$match: {'accounts.currency': {$in: config.product.split('-')}}},
      expect.anything(),
      expect.anything(),
      expect.anything(),
      {
        $addFields: {
          accounts: {
            $map: {
              as: 'account',
              in: {
                $mergeObjects: [
                  '$$account',
                  {
                    exchangeRateCrypto: {
                      $cond: [
                        expect.anything(),
                        1,
                        expect.anything()
                      ]
                    }
                  }
                ]
              },
              input: '$accounts'
            }
          }
        }
      },
      {
        $addFields: {
          totalCrypto: {
            $reduce: {
              in: {
                $add: [
                  '$$value', {
                    $multiply: [
                      {
                        $toDouble: '$$this.balance'
                      },
                      '$$this.exchangeRateCrypto'
                    ]
                  }
                ]
              },
              initialValue: 0,
              input: '$accounts'
            }
          }
        }
      },
      {$sort: query.sort},
      {$limit: query.limit}
    ];
  }

  beforeAll(async () => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([]);

    clock = lolex.install({ shouldAdvanceTime: true });
    accountsDocumentFactory = new AccountsDocumentFactory(new Date(), 60000);

    originalAccountLimit = AccountSchema.statics.limit;
    AccountSchema.statics.limit = 3;

    referenceContainer.bind<Config>(CONFIG_TYPES.Config).to(Config).inSingletonScope();

    referenceContainer.load(...DEPENDENCIES);

    try {
      await referenceContainer.loadAsync(...ASYNC_DEPENDENCIES);
    } catch (error) {
      // let this error be handled/logged at the caller level
      throw error;
    }

    // @ts-ignore
    config = referenceContainer.get<Config>(CONFIG_TYPES.Config) as ProductConfig;

    const modelFactory = referenceContainer.get<ModelFactory>(FACTORY_TYPES.ModelFactory);

    model = modelFactory.newModel('account', TYPES.AccountsModel, AccountsSchema);

    accountRepository = referenceContainer.getNamed<AccountRepository>(TYPES.AccountRepository, 'account');
  });

  afterAll(() => {
    referenceContainer.unbindAll();
    AccountSchema.statics.limit = originalAccountLimit;
    clock.uninstall();

    process.argv = originalArgv;
  });

  test('is bound to container', () => {
    expect(accountRepository).toBeDefined();
  });

  test('findById() returns document if found', async () => {
    expect.assertions(2);

    const accountsDocument = accountsDocumentFactory.newAccountsDocument();

    const originalFindById = model.findById;

    model.findById = jest.fn().mockImplementation((id, callback) => callback(undefined, accountsDocument));

    await expect(accountRepository.findById(accountsDocument._id)).resolves.toEqual(accountsDocument);

    expect(model.findById).toHaveBeenCalledTimes(1);

    model.findById = originalFindById;
  });

  test('findById() rejects if nothing found', async () => {
    expect.assertions(2);

    await expect(accountRepository.findById('abcd')).rejects.toBeUndefined();

    expect(model.findById).toHaveBeenCalledTimes(1);
  });

  test('findById() rejects with error if there is an error', async () => {
    expect.assertions(2);

    const error = 'error';
    const originalFindById = model.findById;

    model.findById = jest.fn().mockImplementation((id, callback) => callback(error));

    await expect(accountRepository.findById('abcd')).rejects.toEqual(error);

    expect(model.findById).toHaveBeenCalledTimes(1);

    model.findById = originalFindById;
  });

  test('count() is correct', async() => {
    expect.assertions(1);

    const count = 10;

    const originalCount = model.countDocuments;

    model.countDocuments = jest.fn().mockImplementation((id, callback) => callback(undefined, count));

    await expect(accountRepository.count()).resolves.toEqual(count);

    model.countDocuments = originalCount;
  });

  test('count() rejects with error if there is an error', async() => {
    expect.assertions(2);

    const error = 'error';

    const originalCount = model.countDocuments;

    model.countDocuments = jest.fn().mockImplementation((id, callback) => callback(error));

    await expect(accountRepository.count()).rejects.toEqual(error);

    expect(model.countDocuments).toHaveBeenCalledTimes(1);

    model.countDocuments = originalCount;
  });

  test('pages() is correct', async () => {
    expect.assertions(1);

    const count = 10;
    const limit = 2;

    const originalCount = model.countDocuments;

    model.limit = limit;
    model.countDocuments = jest.fn().mockImplementation((id, callback) => callback(undefined, count));

    await expect(accountRepository.pages()).resolves.toEqual(count / limit);

    model.countDocuments = originalCount;
  });

  test('pages() rejects with error if there is an error', async () => {
    expect.assertions(2);

    const error = 'error';

    const originalCount = model.countDocuments;

    model.countDocuments = jest.fn().mockImplementation((id, callback) => callback(error));

    await expect(accountRepository.pages()).rejects.toEqual(error);

    expect(model.countDocuments).toHaveBeenCalledTimes(1);

    model.countDocuments = originalCount;
  });

  test('findManyByPage() item count == 0', async () => {
    expect.assertions(3);

    const pageOffset = new Date();
    const pageQuery = model.pageQuery(pageOffset);

    model.aggregate =
      jest.spyOn(model, 'aggregate')
        .mockImplementation((aggregations: any[], callback: Function): Promise<any> => callback(undefined, []));

    await expect(accountRepository.findManyByPage(pageOffset)).resolves.toEqual([]);

    expect(model.aggregate).toHaveBeenCalledTimes(1);

    expect(model.aggregate).toHaveBeenCalledWith(
      aggregateQuery(pageQuery),
      expect.anything()
    );

    // @ts-ignore
    model.aggregate.mockRestore();
  });

  test('findManyByPage() item count > 0', async () => {
    expect.assertions(3);

    const pageOffset = new Date();
    const pageQuery = model.pageQuery(pageOffset);

    const accountsDocuments = accountsDocumentFactory.newAccountsDocuments(2);

    model.aggregate =
      jest.spyOn(model, 'aggregate')
        .mockImplementation(
          (aggregations: any[], callback: Function): Promise<any> => callback(undefined, accountsDocuments));

    await expect(accountRepository.findManyByPage(pageOffset)).resolves.toEqual(accountsDocuments);

    expect(model.aggregate).toHaveBeenCalledTimes(1);
    expect(model.aggregate).toHaveBeenCalledWith(
      aggregateQuery(pageQuery),
      expect.anything()
    );

    // @ts-ignore
    model.aggregate.mockRestore();
  });

  test('findManyByPage() rejects with error if there is an error', async () => {
    expect.assertions(1);

    const pageOffset = new Date();
    // const pageQuery = model.pageQuery(pageOffset);

    const error = 'error';

    model.aggregate =
      jest.spyOn(model, 'aggregate')
        .mockImplementation((aggregations: any[], callback: Function): Promise<any> => callback(error, undefined));

    await expect(accountRepository.findManyByPage(pageOffset)).rejects.toEqual(error);

    // @ts-ignore
    model.aggregate.mockRestore();
  });

  test('save() valid account', async () => {
    expect.assertions(2);

    const accounts = accountsDocumentFactory.newAccountsDocument();

    model.insertMany = jest.spyOn(model, 'insertMany').mockImplementation(
      (docs, options, callback) => callback(undefined, docs)
    );

    await expect(accountRepository.insertMany([accounts])).resolves.toEqual([accounts]);

    expect(model.insertMany).toHaveBeenCalledTimes(1);

    // @ts-ignore
    model.insertMany.mockRestore();
  });
});


