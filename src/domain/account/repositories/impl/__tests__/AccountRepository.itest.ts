
import * as lolex from 'lolex';

import { AsyncContainerModule, ContainerModule, injectable } from 'inversify';

import { TYPES as CONFIG_TYPES } from '../../../../../infrastructure/config/module';
import { ProductConfig, ProductConfigOptionsGroup } from '../../../../../infrastructure/config/ProductConfigOptionsGroup';
import { ModelFactory } from '../../../../../infrastructure/data-access/factories/ModelFactory';
import { TYPES as DB_TYPES } from '../../../../../infrastructure/data-access/module';
import { ModelFactoryModule, TYPES as FACTORY_TYPES } from '../../../../../infrastructure/data-access/factories/module';
import { referenceContainer } from '../../../../../infrastructure/inversify';
import { LoggerConfigOptionsGroup } from '../../../../../infrastructure/logger/LoggerConfigOptionsGroup';
import { LoggerModule } from '../../../../../infrastructure/logger/module';
import { AccountsDocumentFactory } from '../../../../../testing/entities/AccountsDocumentFactory';
import { ArgvConfig } from '../../../../../testing/infrastructure/config/ArgvConfig';
import { IntegrationTestingDatabaseClient } from '../../../../../testing/infrastructure/data-access/integration/IntegrationTestingDatabaseClient';
import { IntegrationTestingDatabaseModule } from '../../../../../testing/infrastructure/data-access/integration/module';

import { Account } from '../../../entities/Account';
import { AccountModule, TYPES } from '../../../module';

import { AccountRepository } from '../../AccountRepository';

import { AccountsModel, AccountsSchema } from '../AccountRepositoryImpl';


const DEPENDENCIES: ContainerModule[] = [
  AccountModule,
  LoggerModule,
  ModelFactoryModule
];

const ASYNC_DEPENDENCIES: AsyncContainerModule[] = [
  IntegrationTestingDatabaseModule
];


describe('AccountRepository Integration', () => {
  @injectable()
  class Config extends ArgvConfig {
    public constructor() {
      super();

      this.configOptionsGroups.push(new LoggerConfigOptionsGroup('TestAccountRepository'));
      this.configOptionsGroups.push(new ProductConfigOptionsGroup());
    }
  }

  let clock;
  let accountsDocumentFactory;

  let databaseClient: IntegrationTestingDatabaseClient;

  // @ts-ignore
  let accountRepository: AccountRepository;
  // @ts-ignore
  let model: AccountsModel;

  let originalAccountLimit: number;

  let originalArgv;

  let testStartDate;

  beforeAll(async () => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([]);

    clock = lolex.install({ shouldAdvanceTime: true });

    testStartDate = new Date();

    accountsDocumentFactory = new AccountsDocumentFactory(testStartDate, 60000);

    originalAccountLimit = AccountsSchema.statics.limit;
    AccountsSchema.statics.limit = 3;

    referenceContainer.bind<Config>(CONFIG_TYPES.Config).to(Config).inSingletonScope();

    referenceContainer.load(...DEPENDENCIES);

    try {
      await referenceContainer.loadAsync(...ASYNC_DEPENDENCIES);
    } catch (error) {
      // let this error be handled/logged at the caller level
      throw error;
    }

    databaseClient = referenceContainer.get<IntegrationTestingDatabaseClient>(DB_TYPES.DatabaseClient);

    // @ts-ignore
    const config = referenceContainer.get<Config>(CONFIG_TYPES.Config) as ProductConfig;

    const modelFactory = referenceContainer.get<ModelFactory>(FACTORY_TYPES.ModelFactory);
    model = modelFactory.newProductModel(config.product, 'account', TYPES.AccountsModel, AccountsSchema);

    accountRepository = referenceContainer.getNamed<AccountRepository>(TYPES.AccountRepository, 'account');
  });

  afterAll(async () => {
    // make sure clock.uninstall() comes first as there are some sleep() calls in infrastructure teardown
    // if lolex.install() is installed without the { shouldAdvanceTime: true } option, the sleep() will
    // never return
    clock.uninstall();

    await databaseClient.reset();
    await databaseClient.disconnect();

    referenceContainer.unbindAll();

    AccountsSchema.statics.limit = originalAccountLimit;

    process.argv = originalArgv;
  });

  beforeEach(async () => {
    accountsDocumentFactory.resetDate();
    await model.remove({});
  });

  test.skip('insertMany() returns inserted accounts with _id and __v set', async () => {
    const totalAccountsCount = AccountsSchema.statics.limit + 1;

    expect.assertions(2 + totalAccountsCount * 2);

    const accounts = accountsDocumentFactory.newAccountsDocuments(totalAccountsCount);

    const insertedAccounts = await accountRepository.insertMany(accounts);

    expect(insertedAccounts.length).toEqual(totalAccountsCount);

    // @ts-ignore
    expect(insertedAccounts).toMatchNewDocuments(accounts);

    insertedAccounts.forEach(insertedAccount => {
      expect('_id' in insertedAccount).toBeTruthy();
      expect('__v' in insertedAccount).toBeTruthy();
    });
  });

  test('findById() returns one of the inserted accounts', async() => {
    const totalAccountsCount = AccountsSchema.statics.limit + 1;

    expect.assertions(1);

    const accountsList = accountsDocumentFactory.newAccountsDocuments(totalAccountsCount);

    const insertedAccounts = await accountRepository.insertMany(accountsList);

    const accounts = await accountRepository.findById(insertedAccounts[insertedAccounts.length - 1]._id);

    // @ts-ignore
    expect(accounts).toMatchDocument(insertedAccounts[insertedAccounts.length - 1]);
  });

  test('findManyByPage() returns correct accounts by page', async () => {
    const pageCount = AccountsSchema.statics.limit;
    const totalAccountsCount = AccountsSchema.statics.limit + 1;

    expect.assertions(3);

    const accountsList = accountsDocumentFactory.newAccountsDocuments(totalAccountsCount);

    const insertedAccounts = await accountRepository.insertMany(accountsList);

    expect(insertedAccounts.length).toEqual(totalAccountsCount);

    let accounts = await accountRepository.findManyByPage(testStartDate);

    expect(accounts.length).toEqual(pageCount);

    const dateForNextPage = new Date(accounts[accounts.length - 1].time);
    dateForNextPage.setMilliseconds(dateForNextPage.getMilliseconds() + 60000);

    accounts = await accountRepository.findManyByPage(dateForNextPage);

    expect(accounts.length).toEqual(1);
  });

  test('page() returns correct number of pages', async () => {
    const pageCount = AccountsSchema.statics.limit;
    const totalAccountsCount = AccountsSchema.statics.limit + 1;

    expect.assertions(1);

    const accounts = accountsDocumentFactory.newAccountsDocuments(totalAccountsCount);

    await accountRepository.insertMany(accounts);
    const pages = await accountRepository.pages();

    expect(pages).toEqual(Math.ceil(totalAccountsCount / pageCount));
  });

  test('count() returns correct number of pages', async () => {
    const totalAccountsCount = AccountsSchema.statics.limit + 1;

    expect.assertions(1);

    const accounts = accountsDocumentFactory.newAccountsDocuments(totalAccountsCount);

    await accountRepository.insertMany(accounts);
    const count = await accountRepository.count();

    expect(count).toEqual(totalAccountsCount);
  });

  test.skip('findFirstAndLastAccount() returns first and last accounts', async () => {
    expect.assertions(2);

    const accounts = accountsDocumentFactory.newAccountsDocuments(10);

    // because this is an aggregation and we cannot use the $toString operator, sums will come out as
    // numbers. therefore convert them to numbers here for comparison later
    accounts.forEach(account => {
      account.totalUSD = +account.totalUSD;
    });

    // call numericalAmountsToFixedString() to prevent precision errors in tests
    const firstAccount = numericalAmountsToFixedString(accounts[0]);
    const lastAccount = numericalAmountsToFixedString(accounts[accounts.length - 1]);

    await accountRepository.insertMany(accounts);

    let [ dbFirstAccount, dbLastAccount ] = await accountRepository.findFirstAndLastAccount();

    dbFirstAccount = numericalAmountsToFixedString(dbFirstAccount) as Account.AccountsDocument;
    dbLastAccount = numericalAmountsToFixedString(dbLastAccount) as Account.AccountsDocument;

    // @ts-ignore
    expect(dbFirstAccount as Account.Accounts).toMatchNewDocument(firstAccount);

    // @ts-ignore
    expect(dbLastAccount as Account.Accounts).toMatchNewDocument(lastAccount);
  });

  function numericalAmountsToFixedString(accounts: Account.Accounts) {
    if (typeof accounts.accounts[0].exchangeRateCrypto === 'number') {
      accounts.accounts[0].exchangeRateCrypto = (accounts.accounts[0].exchangeRateCrypto as number).toFixed(6);
    }

    if (typeof accounts.accounts[1].exchangeRateCrypto === 'number') {
      accounts.accounts[1].exchangeRateCrypto = (accounts.accounts[1].exchangeRateCrypto as number).toFixed(6);
    }

    if (typeof accounts.totalUSD === 'number') {
      accounts.totalUSD = (accounts.totalUSD as number).toFixed(6);
    }

    if (typeof accounts.totalCrypto === 'number') {
      accounts.totalCrypto = (accounts.totalCrypto as number).toFixed(6);
    }

    if (typeof accounts.exchangeRateCrypto === 'number') {
      accounts.exchangeRateCrypto = (accounts.exchangeRateCrypto as number).toFixed(6);
    }

    return accounts;
  }
});

