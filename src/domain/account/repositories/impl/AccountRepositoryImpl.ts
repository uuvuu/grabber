
import { inject, injectable } from 'inversify';
import { MongoError } from 'mongodb';
import { Model, Schema } from 'mongoose';

import { Config } from '../../../../infrastructure/config/Config';
import { TYPES as CONFIG_TYPES } from '../../../../infrastructure/config/module';
import { TYPES as FACTORY_TYPES } from '../../../../infrastructure/data-access/factories/module';
import { ModelFactory } from '../../../../infrastructure/data-access/factories/ModelFactory';
import { Pageable, Query, SortOrder } from '../../../../infrastructure/data-access/Query';
import { BaseRepositoryImpl } from '../../../../infrastructure/data-access/repositories/impl/BaseRepositoryImpl';

import { Account } from '../../entities/Account';

import { AccountRepository } from '../AccountRepository';


const TYPES = {
  AccountsModel: Symbol.for('AccountsModel'),
  AccountRepository: Symbol.for('AccountRepository')
};


const ACCOUNTS_LIMIT = 5000;


interface AccountsModel extends Model<Account.AccountsDocument>, Pageable<Date> {
}

const AccountSchema = new Schema({
  currency: { type: String, required: true, index: true },

  accountId: { type: String, required: true },

  balance: { type: String, required: true },
  available: { type: String, required: true },
  hold: { type: String, required: true },

  exchangeRateUSD: { type: String, required: true }
});

const AccountsSchema = new Schema({
  time: { type: Date, required: true, index: true },

  profileId: { type: String, required: true },

  accounts: [ AccountSchema ],

  totalUSD: String
});


AccountsSchema.statics.limit = ACCOUNTS_LIMIT;

AccountsSchema.statics.offset = (value: Date): Query<Account.Accounts> => {
  return {
    filter: {
      time: {
        $gte: new Date(value)
      }
    }
  };
};

AccountsSchema.statics.pageQuery = (value: Date): Query<Account.Accounts> => {
  const query = AccountsSchema.statics.offset(new Date(value));

  query.limit = AccountsSchema.statics.limit;
  query.sort = { time: 1 };

  return query;
};


@injectable()
class AccountRepositoryImpl extends BaseRepositoryImpl<Date, Account.Accounts, Account.AccountsDocument, AccountsModel> implements AccountRepository {
  protected constructor(
    @inject(CONFIG_TYPES.Config) readonly config: Config,
    @inject(FACTORY_TYPES.ModelFactory) readonly modelFactory: ModelFactory) {

    super(config, modelFactory);
  }

  protected get collectionName() {
    return 'account';
  }

  protected init() {
    this._model = this._modelFactory.newModel(this.collectionName, TYPES.AccountsModel, AccountsSchema);
  }

  async findMaxAccount(): Promise<Account.AccountsDocument> {
    return this.findAccountSortedByTotalUSD(SortOrder.DESCENDING);
  }

  async findMinAccount(): Promise<Account.AccountsDocument> {
    return this.findAccountSortedByTotalUSD(SortOrder.ASCENDING);
  }

  async findAccountSortedByTotalUSD(sortDirection: SortOrder) {
    const aggregations = this.getFilteringAggregations();

    aggregations.push({ $sort: { totalUSD: sortDirection } });
    aggregations.push({ $limit: 1 });

    return new Promise<Account.AccountsDocument>((resolve, reject) => {
      return this._model.aggregate(aggregations, (error: MongoError, results: any) => {
        if (error) {
          reject(error);
          return;
        }

        resolve(results[0]);
      });
    });
  }

  async findFirstAndLastAccount(): Promise<Account.AccountsDocument[]> {
    return new Promise<Account.AccountsDocument[]>(async (resolve, reject) => {
      let aggregations = this.getFilteringAggregations();

      try {
        aggregations.push({ $sort: { time: 1 }});
        aggregations.push({ $limit: 1 });

        aggregations.push(
          {
            $group: {
              _id: undefined,
              accounts: { $first: '$$ROOT' }
            }
          }
        );

        const firstAccount = await this._model.aggregate(aggregations);

        aggregations = this.getFilteringAggregations();

        aggregations.push({ $sort: { time: -1 }});
        aggregations.push({ $limit: 1 });

        aggregations.push(
          {
            $group: {
              _id: undefined,
              accounts: { $first: '$$ROOT' }
            }
          }
        );

        const lastAccount = await this._model.aggregate(aggregations);

        resolve([ firstAccount[0].accounts, lastAccount[0].accounts ]);

      } catch (error) {
        reject(error);
      }
    });
  }

  async findLatestItem(): Promise<Account.AccountsDocument> {
    const aggregations = this.getFilteringAggregations();

    aggregations.push({ $sort: SortOrder.DESCENDING });
    aggregations.push({ $limit: 1 });

    return new Promise<Account.AccountsDocument>((resolve, reject) => {
      return this._model.aggregate(aggregations, (error: MongoError, results: any) => {
        if (error) {
          reject(error);
          return;
        }

        resolve(results[0]);
      });
    });
  }

  protected getFilteringAggregations() {
    return [
      { $unwind: '$accounts' },
      {
        $group: {
          _id: '$_id',
          time: { $first: '$time' },
          profileId: { $first: '$profileId' },
          accounts:  {$push: '$accounts' },
          // totalUSD: { $first: '$totalUSD' },
          // totalUSDProductFiltered: { $sum: { $multiply: [ { $toDouble: '$accounts.balance' }, { $toDouble: '$accounts.exchangeRateUSD' } ] } },
          totalUSD: { $sum: { $multiply: [{ $toDouble: '$accounts.balance'}, { $toDouble: '$accounts.exchangeRateUSD' }]}}
        }
      },
    ] as any[];
  }
}


export { AccountsModel, AccountRepositoryImpl, AccountSchema, AccountsSchema, TYPES };

