
import { inject, injectable } from 'inversify';

import { Config } from '../../../../infrastructure/config/Config';
import { TYPES as CONFIG_TYPES } from '../../../../infrastructure/config/module';
import { ProductConfig } from '../../../../infrastructure/config/ProductConfigOptionsGroup';
import { ModelFactory } from '../../../../infrastructure/data-access/factories/ModelFactory';
import { TYPES as FACTORY_TYPES } from '../../../../infrastructure/data-access/factories/module';
import { Query, SortOrder } from '../../../../infrastructure/data-access/Query';

import { Account } from '../../entities/Account';

import { AccountRepositoryImpl } from './AccountRepositoryImpl';


@injectable()
class AccountProductRepositoryImpl extends AccountRepositoryImpl {

  private readonly _products: string[];

  public constructor(
    @inject(CONFIG_TYPES.Config) readonly config: Config & ProductConfig,
    @inject(FACTORY_TYPES.ModelFactory) readonly modelFactory: ModelFactory) {

    super(config, modelFactory);

    this._products = config.product.split('-').map(product => product.toUpperCase());
  }

  async findManyByQuery(query: Query<Account.AccountsDocument>): Promise<Account.AccountsDocument[]> {
    return this.productAggregateQuery(query);
  }

  async findManyByPage(pageOffset: Date): Promise<Account.AccountsDocument[]> {
    const query = this._model.pageQuery(pageOffset);

    return this.productAggregateQuery(query);
  }

  async findLatestItem(): Promise<Account.AccountsDocument> {
    const query = {
      sort: {
        time: SortOrder.DESCENDING
      },
      limit: 1
    } as Query<Account.AccountsDocument>;

    return new Promise<Account.AccountsDocument>((resolve, reject) => {
      const queryResults = this.productAggregateQuery(query)
        .then(accountsList => resolve(accountsList[0]))
        .catch(error => reject(error));

      return queryResults;
    });
  }

  private async productAggregateQuery(query: Query<Account.AccountsDocument>): Promise<Account.AccountsDocument[]> {
    const aggregations = [];

    if (query.filter) {
      aggregations.push({ $match: query.filter });
    }

    aggregations.push(...this.getFilteringAggregations());

    if (query.sort) {
      aggregations.push({ $sort: query.sort });
    }

    if (query.limit) {
      aggregations.push({ $limit: query.limit });
    }

    return this.aggregate(aggregations);
  }

  protected getFilteringAggregations() {
    const cryptoCurrency = this._products.find(element => element !== 'USD');
    const cryptoCurrencyExchangeRateFieldName = 'exchangeRateCrypto';
    const cryptoCurrencyExchangeRateFieldNameRef = '$' + cryptoCurrencyExchangeRateFieldName;
    const totalCryptoCurrencyFieldName = 'totalCrypto';

    // TODO cleanup this 'mongo' mongo query
    // TODO this is also a cheap and poor way to get hourly rollups of accounts data. best stored in its
    // own pre-aggregated table
    
    return [
      { $unwind: '$accounts' },
      { $match: { 'accounts.currency': { $in: this._products }}},
      {
        $group: {
          _id: '$_id',
          time: { $first: '$time' },
          profileId: { $first: '$profileId' },
          accounts: { $push: '$accounts' },
          totalUSD: { $sum: { $multiply: [ { $toDouble: '$accounts.balance' }, { $toDouble: '$accounts.exchangeRateUSD' } ] } }
        }
      },
      {
        $addFields: {
            [ cryptoCurrencyExchangeRateFieldName ]: {
                $filter: {
                    input: '$accounts',
                    as: 'account',
                    cond: { $eq: [ '$$account.currency', cryptoCurrency ] }
                }
            }
        }
      },
      {
        $addFields: {
          [ cryptoCurrencyExchangeRateFieldName ]: {
            $let: {
              vars: { first: { $arrayElemAt: [ cryptoCurrencyExchangeRateFieldNameRef, 0 ] }},
              in: { $toDouble: '$$first.exchangeRateUSD' }
            }
          }
        }
      },
      {
        $addFields: {
          accounts: {
            $map: {
              input: '$accounts',
              as: 'account',
              in: {
                $mergeObjects: [
                  '$$account',
                  {
                    [ cryptoCurrencyExchangeRateFieldName ]: {
                      $cond: [
                        { $eq: [ '$$account.currency', cryptoCurrency ] },
                        1,
                        { $divide: [ 1, cryptoCurrencyExchangeRateFieldNameRef ] }
                      ]
                    }
                  }
                ]
              }
            }
          }
        }
      },
      {
        $addFields: {
          [ totalCryptoCurrencyFieldName ]: {
            $reduce: {
              input: '$accounts',
              initialValue: 0,
              in: {
                $add: [
                  '$$value',
                  { $multiply: [ { $toDouble: '$$this.balance' }, '$$this.' + cryptoCurrencyExchangeRateFieldName ] }
                ]
              }
            }
          }
        }
      }
    ] as any[];
  }
}


export { AccountProductRepositoryImpl };

