
import 'reflect-metadata';

import { ContainerModule, interfaces } from 'inversify';

import { FillRepository } from './repositories/FillRepository';
import { OrderRepository } from './repositories/OrderRepository';
import { FillRepositoryImpl, TYPES as FILL_TYPES } from './repositories/impl/FillRepositoryImpl';
import { OrderRepositoryImpl, TYPES as ORDER_TYPES } from './repositories/impl/OrderRepositoryImpl';


const OrderModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<FillRepository>(FILL_TYPES.FillRepository).to(FillRepositoryImpl).inSingletonScope();
    bind<OrderRepository>(ORDER_TYPES.OrderRepository).to(OrderRepositoryImpl).inSingletonScope();
  }
);


export { OrderModule, FILL_TYPES, ORDER_TYPES };
