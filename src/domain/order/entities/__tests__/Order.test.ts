
import * as lolex from 'lolex';

import { OrderDocumentFactory } from '../../../../testing/entities/OrderDocumentFactory';
import { OrderUtils } from '../Order';


describe('Order', () => {
  let clock;
  let orderDocumentFactory;

  beforeAll(() => {
    clock = lolex.install({ shouldAdvanceTime: true });
    orderDocumentFactory = new OrderDocumentFactory(new Date(), 60000);
  });

  afterAll(() => {
    clock.uninstall();
  });

  test('copyOrderDocument() copies order', () => {
    const order = orderDocumentFactory.newOrderDocument();
    const copiedOrder = OrderUtils.copyOrderDocument(order);

    // @ts-ignore
    expect(copiedOrder).toMatchDocument(order);
    expect(copiedOrder.createdTime.constructor.name).toEqual(order.createdTime.constructor.name);
  });

  test('copyOrderDocuments() copies orders', () => {
    const orders = orderDocumentFactory.newOrderDocuments(2);
    const copiedOrders = OrderUtils.copyOrderDocument(orders);

    // @ts-ignore
    expect(copiedOrders).toMatchDocuments(orders);
  });

  test('orderComparator()', () => {
    const [ order1, order2 ] = orderDocumentFactory.newOrderDocuments(2);

    expect(OrderUtils.orderComparator(order1, order2)).toEqual(-1);
    expect(OrderUtils.orderComparator(order2, order1)).toEqual(1);

    const copiedOrder = OrderUtils.copyOrderDocument(order2);

    expect(OrderUtils.orderComparator(order2, copiedOrder)).toEqual(0);
  });
});
