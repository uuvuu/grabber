
import { Document } from 'mongoose';


namespace Order {
  export enum CancelAfter {
    MIN = <any>'min',
    HOUR = <any>'hour',
    DAY = <any>'day'
  }

  export enum Side {
    BUY = <any>'buy',
    NONE = <any>'none',
    SELL = <any>'sell'
  }

  // self-trade prevention flag
  export enum STP {
    DC = <any>'dc',
    CO = <any>'co',
    CN = <any>'cn',
    CB = <any>'cb'
  }

  export enum Type {
    LIMIT = <any>'limit',
    MARKET = <any>'market'
  }

  export enum TimeInForce {
    GTC = <any>'GTC',
    GTT = <any>'GTT',
    IOC = <any>'IOC',
    FOK = <any>'FOK'
  }

  export enum Status {
    OPEN = <any>'open',
    PENDING = <any>'pending',
    ACTIVE = <any>'active',
    DONE = <any>'done',
    SETTLED = <any>'settled',
    REJECTED = <any>'rejected'
  }

  export interface Order {
    orderId: string;

    product: string;

    price: string;
    size: string;

    side: Side;

    stp: STP;    // self-trade prevention flag (optional)

    funds: string;
    specifiedFunds: string;

    type: Type;    // (default is limit)

    timeInForce: TimeInForce;    // (optional, default is GTC)

    postOnly: boolean;

    createdTime: Date;
    expireTime: Date;
    doneTime: Date;
    candleTime: Date;       // the latest candle time that this trade was based on

    doneReason: string;
    rejectedReason: string;

    fillFees: string;
    filledSize: string;

    executedValue: string;

    status: Status;

    settled: boolean;

    clearanceStatus: string;
  }

  export interface OrderDocument extends Order, Document {
  }
}


namespace OrderUtils {
  export function orderComparator(order1: Order.OrderDocument, order2: Order.OrderDocument) {
    if (order1.createdTime < order2.createdTime) {
      return -1;
    }

    if (order1.createdTime > order2.createdTime) {
      return 1;
    }

    return 0;
  }

  export function copyOrderDocument(orderDocument: Order.OrderDocument) {
    const order = JSON.parse(JSON.stringify(orderDocument)) as Order.OrderDocument;

    order.createdTime = new Date(order.createdTime);

    return order;
  }

  export function copyOrderDocuments(orderDocuments: Order.OrderDocument[]) {
    return orderDocuments.map(orderDocument => {
      return copyOrderDocument(orderDocument);
    });
  }

  export function copyNewOrderDocument(orderDocument: Order.OrderDocument) {
    const order = JSON.parse(JSON.stringify(orderDocument)) as Order.OrderDocument;

    order.createdTime = new Date(order.createdTime);

    delete order._id;
    delete order.__v;

    return order;
  }

  export function copyNewOrderDocuments(orderDocuments: Order.OrderDocument[]) {
    return orderDocuments.map(orderDocument => {
      return copyNewOrderDocument(orderDocument);
    });
  }
}


export { Order, OrderUtils };
