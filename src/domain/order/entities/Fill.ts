
import { Document } from 'mongoose';


type Side = 'buy' | 'sell';


interface Fill {
  tradeId: number;
  orderId: string;

  product: string;

  price: string;
  size: string;

  createdTime: Date;

  liquidity: string;     // T for Taker, M for Maker

  fee: string;
  settled: boolean;

  side: Side;

  usdVolume: string;
}


interface FillDocument extends Fill, Document {
}


namespace FillUtils {
  export function fillComparator(fill1: FillDocument, fill2: FillDocument) {
    if (fill1.createdTime < fill2.createdTime) {
      return -1;
    }

    if (fill1.createdTime > fill2.createdTime) {
      return 1;
    }

    return 0;
  }

  export function copyFillDocument(fillDocument: FillDocument) {
    const fill = JSON.parse(JSON.stringify(fillDocument)) as FillDocument;

    fill.createdTime = new Date(fill.createdTime);

    return fill;
  }

  export function copyFillDocuments(fillDocuments: FillDocument[]) {
    return fillDocuments.map(fillDocument => {
      return copyFillDocument(fillDocument);
    });
  }

  export function copyNewFillDocument(fillDocument: FillDocument) {
    const fill = JSON.parse(JSON.stringify(fillDocument)) as FillDocument;

    fill.createdTime = new Date(fill.createdTime);

    delete fill._id;
    delete fill.__v;

    return fill;
  }

  export function copyNewFillDocuments(fillDocuments: FillDocument[]) {
    return fillDocuments.map(fillDocument => {
      return copyNewFillDocument(fillDocument);
    });
  }
}


export { Fill, FillDocument, FillUtils };
