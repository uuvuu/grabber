

import { BaseRepository } from '../../../infrastructure/data-access/repositories/BaseRepository';

import { Order } from '../entities/Order';


interface OrderRepository extends BaseRepository<Date, Order.OrderDocument> {
  findLatestItem(): Promise<Order.OrderDocument>;
  findLatestFilledOrder(): Promise<Order.OrderDocument>;
  findOrderSuccessRate(): Promise<Object>;
  findOrder30DayVolume(): Promise<Object>;
}


export { OrderRepository };
