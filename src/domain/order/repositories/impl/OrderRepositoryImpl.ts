
import { inject, injectable } from 'inversify';
import { Model, Schema } from 'mongoose';

import { Config } from '../../../../infrastructure/config/Config';
import { TYPES as CONFIG_TYPES } from '../../../../infrastructure/config/module';
import { ModelFactory } from '../../../../infrastructure/data-access/factories/ModelFactory';
import { TYPES as FACTORY_TYPES } from '../../../../infrastructure/data-access/factories/module';
import { Pageable, Query, SortOrder } from '../../../../infrastructure/data-access/Query';
import { BaseRepositoryImpl } from '../../../../infrastructure/data-access/repositories/impl/BaseRepositoryImpl';
import { Order } from '../../entities/Order';
import { OrderRepository } from '../OrderRepository';


const TYPES = {
  OrderModel: Symbol.for('OrderModel'),
  OrderRepository: Symbol.for('OrderRepository')
};


const ORDERS_LIMIT = 5000;


interface OrderModel extends Model<Order.OrderDocument>, Pageable<Date> {
}


const OrderSchema = new Schema({
  orderId: { type: String, required: true, unique: true },

  product: { type: String, required: true },

  price: { type: String, required: true },
  size: { type: String, required: true },

  side: { type: String, required: true },

  stp: { type: String },

  funds: { type: String },
  specifiedFunds: { type: String },

  type: { type: String, required: true },

  timeInForce: { type: String, required: true },

  postOnly: { type: Boolean, required: true },

  createdTime: { type: Date, required: true, index: true },
  expireTime: { type: Date },
  doneTime: { type: Date },
  candleTime: { type: Date, required: true },

  doneReason: { type: String },
  rejectedReason: { type: String },

  fillFees: { type: String, required: true },
  filledSize: { type: String, required: true },

  executedValue: { type: String, required: true },

  status: { type: String, required: true },

  settled: { type: Boolean, required: true },

  clearanceStatus: { type: String, index: true }
});


OrderSchema.statics.limit = ORDERS_LIMIT;

OrderSchema.statics.offset = (value: Date): Query<Order.Order> => {
  return {
    filter: {
      createdTime: {
        $gte: new Date(value)
      }
    }
  };
};

OrderSchema.statics.pageQuery = (value: Date): Query<Order.Order> => {
  const query = OrderSchema.statics.offset(new Date(value));

  query.limit = OrderSchema.statics.limit;
  query.sort = { time: 1 };

  return query;
};


@injectable()
class OrderRepositoryImpl extends BaseRepositoryImpl<Date, Order.Order, Order.OrderDocument, OrderModel> implements OrderRepository {
  public constructor(
    @inject(CONFIG_TYPES.Config) readonly config: Config,
    @inject(FACTORY_TYPES.ModelFactory) readonly modelFactory: ModelFactory) {

    super(config, modelFactory);
  }

  protected init() {
    this._model =
      this._modelFactory.newProductModel(this._config.product, 'order', TYPES.OrderModel, OrderSchema);
  }

  async findLatestItem(): Promise<Order.OrderDocument> {
    const query = {
      sort: {
        createdTime: SortOrder.DESCENDING
      },
      limit: 1
    } as Query<Order.OrderDocument>;

    return new Promise<Order.OrderDocument>((resolve, reject) => {
      const queryPromise = this.findManyByQuery(query)
        .then(orders => {
          resolve(orders[0]);
        })
        .catch(error => {
          reject(error);
        });

      return queryPromise;
    });
  }

  async findLatestFilledOrder(): Promise<Order.OrderDocument> {
    const query = {
      filter: { $or: [ { clearanceStatus: 'filled' }, { clearanceStatus: 'partially filled' } ] },
      sort: {
        createdTime: SortOrder.DESCENDING
      },
      limit: 1
    } as Query<Order.OrderDocument>;

    return new Promise<Order.OrderDocument>((resolve, reject) => {
      const queryPromise = this.findManyByQuery(query)
        .then(orders => {
          resolve(orders[0]);
        })
        .catch(error => {
          reject(error);
        });

      return queryPromise;
    });
  }

  async findOrderSuccessRate() {
    const acceptedOrdersFilter = {
      $or: [
        { clearanceStatus: 'filled' }, { clearanceStatus: 'partially filled' },
        { clearanceStatus: 'expire time exceeded' }, { clearanceStatus: 'post only' },
      ]
    };

    const filledOrdersFilter = {
      $or: [ { clearanceStatus: 'filled' }, { clearanceStatus: 'partially filled' } ]
    };

    const numAcceptedOrdersPromise = this.count(acceptedOrdersFilter);
    const numFilledOrdersPromise = this.count(filledOrdersFilter);

    const promises = Promise.all([numAcceptedOrdersPromise, numFilledOrdersPromise]);

    return new Promise<any>((resolve, reject) => {
      return promises
        .then(([ numAcceptedOrders, numFilledOrders ]) => {
          resolve({
            numAcceptedOrders: numAcceptedOrders,
            numFilledOrders: numFilledOrders,
            successRate: numFilledOrders / numAcceptedOrders
          });
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  async findOrder30DayVolume() {
    const thirtyDaysAgo = new Date();
    thirtyDaysAgo.setDate(thirtyDaysAgo.getDate() - 30);

    const aggregations = [];

    aggregations.push({
      $match: {
        $and: [
          { createdTime: { $gte: thirtyDaysAgo } },
          { $or: [ { clearanceStatus: 'filled' }, { clearanceStatus: 'partially filled'} ] }
        ]
      }
    });

    aggregations.push(
      {
        $group: {
          _id: undefined,
          usdVolume: { $sum: { $toDouble: '$executedValue' }},
          btcVolume: { $sum: { $toDouble: '$filledSize' }},
          filledOrderCount: { $sum: 1 }
        }
      }
    );

    return new Promise<any>((resolve, reject) => {
      return this.aggregate(aggregations)
        .then(data => {
          resolve({
            usdVolume: data[0].usdVolume,
            btcVolume: data[0].btcVolume,
            numFilledOrders: data[0].filledOrderCount
          });
        })
        .catch(error => {
          reject(error);
        });
    });
  }
}


export { OrderModel, OrderRepositoryImpl, OrderSchema, TYPES };
