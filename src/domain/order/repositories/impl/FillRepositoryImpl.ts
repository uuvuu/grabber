

import { inject, injectable } from 'inversify';
import { Model, Schema } from 'mongoose';

import { Config } from '../../../../infrastructure/config/Config';
import { TYPES as CONFIG_TYPES } from '../../../../infrastructure/config/module';
import { ModelFactory } from '../../../../infrastructure/data-access/factories/ModelFactory';
import { TYPES as FACTORY_TYPES } from '../../../../infrastructure/data-access/factories/module';
import { Pageable, Query } from '../../../../infrastructure/data-access/Query';
import { BaseRepositoryImpl } from '../../../../infrastructure/data-access/repositories/impl/BaseRepositoryImpl';
import { Fill, FillDocument } from '../../entities/Fill';
import { FillRepository } from '../FillRepository';


const TYPES = {
  FillModel: Symbol.for('FillModel'),
  FillRepository: Symbol.for('FillRepository')
};


const FILLS_LIMIT = 5000;


interface FillModel extends Model<FillDocument>, Pageable<Date> {
}


const FillSchema = new Schema({
  tradeId: { type: Number, required: true, unique: true },
  orderId: { type: String, required: true },

  product: { type: String, required: true },

  price: { type: String, required: true },
  size: { type: String, required: true },

  createdTime: { type: Date, required: true },

  liquidity: { type: String, required: true },

  fee: { type: String, required: true },
  settled: { type: Boolean, required: true },

  side: { type: String, required: true },

  usdVolume: { type: String, required: true }
});


FillSchema.statics.limit = FILLS_LIMIT;

FillSchema.statics.offset = (value: Date): Query<Fill> => {
  return {
    filter: {
      createdTime: {
        $gte: new Date(value)
      }
    }
  };
};

FillSchema.statics.pageQuery = (value: Date): Query<Fill> => {
  const query = FillSchema.statics.offset(new Date(value));

  query.limit = FillSchema.statics.limit;
  query.sort = { time: 1 };

  return query;
};


@injectable()
class FillRepositoryImpl extends BaseRepositoryImpl<Date, Fill, FillDocument, FillModel> implements FillRepository {
  public constructor(
    @inject(CONFIG_TYPES.Config) readonly config: Config,
    @inject(FACTORY_TYPES.ModelFactory) readonly modelFactory: ModelFactory) {

    super(config, modelFactory);
  }

  protected init() {
    this._model =
      this._modelFactory.newProductModel(this._config.product, 'fill', TYPES.FillModel, FillSchema);
  }
}


export { FillModel, FillRepositoryImpl, FillSchema, TYPES };
