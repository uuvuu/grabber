

import { BaseRepository } from '../../../infrastructure/data-access/repositories/BaseRepository';

import { FillDocument } from '../entities/Fill';


interface FillRepository extends BaseRepository<Date, FillDocument> {}


export { FillRepository };
