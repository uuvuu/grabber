
import { inject, injectable } from 'inversify';
import { Model, Schema } from 'mongoose';

import { Config } from '../../../../infrastructure/config/Config';
import { TYPES as CONFIG_TYPES } from '../../../../infrastructure/config/module';
import { ModelFactory } from '../../../../infrastructure/data-access/factories/ModelFactory';
import { TYPES as FACTORY_TYPES } from '../../../../infrastructure/data-access/factories/module';
import { Pageable, Query } from '../../../../infrastructure/data-access/Query';
import { BaseRepositoryImpl } from '../../../../infrastructure/data-access/repositories/impl/BaseRepositoryImpl';
import { Ticker } from '../../entities/Ticker';
import { TickerRepository } from '../TickerRepository';


const TYPES = {
  TickerModel: Symbol.for('TickerModel'),
  TickerRepository: Symbol.for('OrderRepository')
};


const TICKERS_LIMIT = 5000;


interface TickerModel extends Model<Ticker.TickerDocument>, Pageable<Date> {
}


const TickerSchema = new Schema({
  product: { type: String, required: true },

  time: { type: Date, required: true, index: true },

  tradeId: { type: Number, required: true, unique: true },
  sequence: { type: Number, required: true, unique: true },

  price: { type: String, required: true },
  side: { type: String, required: true },
  lastSize: { type: String, required: true },
  bestBid: { type: String, required: true },
  bestAsk: { type: String, required: true },

  open24h: { type: String, required: true },
  low24h: { type: String, required: true },
  high24h: { type: String, required: true },

  volume24h: { type: String, required: true },
  volume30d: { type: String, required: true },

  // see docs in src/domain/ticker/entities/Ticker.ts
  source: { type: String, required: true }
});


TickerSchema.statics.limit = TICKERS_LIMIT;

TickerSchema.statics.offset = (value: Date): Query<Ticker.Ticker> => {
  return {
    filter: {
      time: {
        $gte: new Date(value)
      }
    }
  };
};

TickerSchema.statics.pageQuery = (value: Date): Query<Ticker.Ticker> => {
  const query = TickerSchema.statics.offset(new Date(value));

  query.limit = TickerSchema.statics.limit;
  query.sort = { time: 1 };

  return query;
};


@injectable()
class TickerRepositoryImpl extends BaseRepositoryImpl<Date, Ticker.Ticker, Ticker.TickerDocument, TickerModel> implements TickerRepository {
  public constructor(
    @inject(CONFIG_TYPES.Config) readonly config: Config,
    @inject(FACTORY_TYPES.ModelFactory) readonly modelFactory: ModelFactory) {

    super(config, modelFactory);
  }

  protected init() {
    this._model =
      this._modelFactory.newProductModel(this._config.product, 'ticker', TYPES.TickerModel, TickerSchema);
  }
}


export { TickerModel, TickerRepositoryImpl, TickerSchema, TYPES };
