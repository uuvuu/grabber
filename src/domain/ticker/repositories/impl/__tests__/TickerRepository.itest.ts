
import * as lolex from 'lolex';

import { AsyncContainerModule, ContainerModule, injectable } from 'inversify';

import { TYPES as CONFIG_TYPES } from '../../../../../infrastructure/config/module';
import { ProductConfig, ProductConfigOptionsGroup } from '../../../../../infrastructure/config/ProductConfigOptionsGroup';
import { ModelFactory } from '../../../../../infrastructure/data-access/factories/ModelFactory';
import { TYPES as DB_TYPES } from '../../../../../infrastructure/data-access/module';
import { ModelFactoryModule, TYPES as FACTORY_TYPES } from '../../../../../infrastructure/data-access/factories/module';
import { referenceContainer } from '../../../../../infrastructure/inversify';
import { LoggerConfigOptionsGroup } from '../../../../../infrastructure/logger/LoggerConfigOptionsGroup';
import { LoggerModule } from '../../../../../infrastructure/logger/module';
import { ArgvConfig } from '../../../../../testing/infrastructure/config/ArgvConfig';
import { IntegrationTestingDatabaseClient } from '../../../../../testing/infrastructure/data-access/integration/IntegrationTestingDatabaseClient';
import { IntegrationTestingDatabaseModule } from '../../../../../testing/infrastructure/data-access/integration/module';
import { TickerDocumentFactory } from '../../../../../testing/entities/TickerDocumentFactory';

import { TickerModule, TYPES } from '../../../module';

import { TickerRepository } from '../../TickerRepository';

import { TickerModel, TickerSchema } from '../TickerRepositoryImpl';


const DEPENDENCIES: ContainerModule[] = [
  TickerModule,
  LoggerModule,
  ModelFactoryModule
];

const ASYNC_DEPENDENCIES: AsyncContainerModule[] = [
  IntegrationTestingDatabaseModule
];


describe('TickerRepository Integration', () => {
  @injectable()
  class Config extends ArgvConfig {
    public constructor() {
      super();

      this.configOptionsGroups.push(new LoggerConfigOptionsGroup('TestTickerRepository'));
      this.configOptionsGroups.push(new ProductConfigOptionsGroup());
    }
  }

  let clock;
  let tickerDocumentFactory;

  let databaseClient: IntegrationTestingDatabaseClient;
  let tickerRepository: TickerRepository;
  let model: TickerModel;
  let originalTickerLimit: number;

  let originalArgv;

  let testStartDate;

  beforeAll(async () => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([]);

    clock = lolex.install({ shouldAdvanceTime: true });

    testStartDate = new Date();

    tickerDocumentFactory = new TickerDocumentFactory(testStartDate, 60000);

    originalTickerLimit = TickerSchema.statics.limit;
    TickerSchema.statics.limit = 3;

    referenceContainer.bind<Config>(CONFIG_TYPES.Config).to(Config).inSingletonScope();

    referenceContainer.load(...DEPENDENCIES);

    try {
      await referenceContainer.loadAsync(...ASYNC_DEPENDENCIES);
    } catch (error) {
      // let this error be handled/logged at the caller level
      throw error;
    }

    databaseClient = referenceContainer.get<IntegrationTestingDatabaseClient>(DB_TYPES.DatabaseClient);

    // @ts-ignore
    const config = referenceContainer.get<Config>(CONFIG_TYPES.Config) as ProductConfig;

    const modelFactory = referenceContainer.get<ModelFactory>(FACTORY_TYPES.ModelFactory);
    model = modelFactory.newProductModel(config.product, 'ticker', TYPES.TickerModel, TickerSchema);

    tickerRepository = referenceContainer.get<TickerRepository>(TYPES.TickerRepository);
  });

  afterAll(async () => {
    // make sure clock.uninstall() comes first as there are some sleep() calls in infrastructure teardown
    // if lolex.install() is installed without the { shouldAdvanceTime: true } option, the sleep() will
    // never return
    clock.uninstall();

    await databaseClient.reset();
    await databaseClient.disconnect();

    referenceContainer.unbindAll();

    TickerSchema.statics.limit = originalTickerLimit;

    process.argv = originalArgv;
  });

  beforeEach(async () => {
    await model.remove({});
  });

  test('create() with single ticker returns inserted ticker with _id and __v set', async () => {
    expect.assertions(4);

    const ticker = tickerDocumentFactory.newTickerDocument();

    const insertedTickers = await tickerRepository.create(ticker);

    expect(insertedTickers.length).toEqual(1);

    // @ts-ignore
    expect(insertedTickers).toMatchNewDocuments([ ticker ]);

    insertedTickers.forEach(insertedTicker => {
      expect('_id' in insertedTicker).toBeTruthy();
      expect('__v' in insertedTicker).toBeTruthy();
    });
  });

  test('create() with with multiple tickers returns inserted tickers with _id and __v set', async () => {
    const totalTickerCount = TickerSchema.statics.limit + 1;

    expect.assertions(2 + totalTickerCount * 2);

    const tickers = tickerDocumentFactory.newTickerDocuments(totalTickerCount);

    const insertedTickers = await tickerRepository.create(...tickers);

    expect(insertedTickers.length).toEqual(totalTickerCount);

    // @ts-ignore
    expect(insertedTickers).toMatchNewDocuments(tickers);

    insertedTickers.forEach(insertedTicker => {
      expect('_id' in insertedTicker).toBeTruthy();
      expect('__v' in insertedTicker).toBeTruthy();
    });
  });

  test('insertMany() returns inserted tickers with _id and __v set', async () => {
    const totalTickerCount = TickerSchema.statics.limit + 1;

    expect.assertions(2 + totalTickerCount * 2);

    const tickers = tickerDocumentFactory.newTickerDocuments(totalTickerCount);

    const insertedTickers = await tickerRepository.insertMany(tickers);

    expect(insertedTickers.length).toEqual(totalTickerCount);

    // @ts-ignore
    expect(insertedTickers).toMatchNewDocuments(tickers);

    insertedTickers.forEach(insertedTicker => {
      expect('_id' in insertedTicker).toBeTruthy();
      expect('__v' in insertedTicker).toBeTruthy();
    });
  });

  test('findById() returns one of the inserted tickers', async() => {
    const totalTickerCount = TickerSchema.statics.limit + 1;

    expect.assertions(1);

    const tickers = tickerDocumentFactory.newTickerDocuments(totalTickerCount);

    const insertedTickers = await tickerRepository.insertMany(tickers);

    const ticker = await tickerRepository.findById(insertedTickers[insertedTickers.length - 1]._id);

    // @ts-ignore
    expect(ticker).toMatchDocument(insertedTickers[insertedTickers.length - 1]);
  });

  test('findManyByPage() returns correct tickers by page', async () => {
    const pageCount = TickerSchema.statics.limit;
    const totalTickerCount = TickerSchema.statics.limit + 1;

    expect.assertions(3);

    const tickers = tickerDocumentFactory.newTickerDocuments(totalTickerCount);

    const insertedTickers = await tickerRepository.insertMany(tickers);

    expect(insertedTickers.length).toEqual(totalTickerCount);

    let dbTickers = await tickerRepository.findManyByPage(testStartDate);

    expect(dbTickers.length).toEqual(pageCount);

    const dateForNextPage = new Date(dbTickers[dbTickers.length - 1].time);
    dateForNextPage.setMilliseconds(dateForNextPage.getMilliseconds() + 60000);

    dbTickers = await tickerRepository.findManyByPage(dateForNextPage);

    expect(dbTickers.length).toEqual(1);
  });

  test('page() returns correct number of pages', async () => {
    const pageCount = TickerSchema.statics.limit;
    const totalTickerCount = TickerSchema.statics.limit + 1;

    expect.assertions(1);

    const tickers = tickerDocumentFactory.newTickerDocuments(totalTickerCount);

    await tickerRepository.insertMany(tickers);
    const pages = await tickerRepository.pages();

    expect(pages).toEqual(Math.ceil(totalTickerCount / pageCount));
  });

  test('count() returns correct number of pages', async () => {
    const totalTickerCount = TickerSchema.statics.limit + 1;

    expect.assertions(1);

    const tickers = tickerDocumentFactory.newTickerDocuments(totalTickerCount);

    await tickerRepository.insertMany(tickers);
    const count = await tickerRepository.count();

    expect(count).toEqual(totalTickerCount);
  });
});

