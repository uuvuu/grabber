
import * as lolex from 'lolex';

import { AsyncContainerModule, ContainerModule, injectable } from 'inversify';

import { LoggerConfigOptionsGroup } from '../../../../../infrastructure/logger/LoggerConfigOptionsGroup';
import { TYPES as CONFIG_TYPES } from '../../../../../infrastructure/config/module';
import { ProductConfig, ProductConfigOptionsGroup } from '../../../../../infrastructure/config/ProductConfigOptionsGroup';
import { ModelFactory } from '../../../../../infrastructure/data-access/factories/ModelFactory';
import { ModelFactoryModule, TYPES as FACTORY_TYPES } from '../../../../../infrastructure/data-access/factories/module';
import { referenceContainer } from '../../../../../infrastructure/inversify';
import { LoggerModule } from '../../../../../infrastructure/logger/module';
import { TickerDocumentFactory } from '../../../../../testing/entities/TickerDocumentFactory';
import { ArgvConfig } from '../../../../../testing/infrastructure/config/ArgvConfig';
import { DatabaseModule } from '../../../../../testing/infrastructure/data-access/unit/module';
import { QueryFactory } from '../../../../../testing/infrastructure/data-access/QueryFactory';

import { TickerModule, TYPES } from '../../../module';

import { TickerRepository } from '../../TickerRepository';

import { TickerModel, TickerSchema } from '../TickerRepositoryImpl';


const DEPENDENCIES: ContainerModule[] = [
  TickerModule,
  LoggerModule,
  ModelFactoryModule
];

const ASYNC_DEPENDENCIES: AsyncContainerModule[] = [
  DatabaseModule
];


describe('TickerRepository', () => {
  @injectable()
  class Config extends ArgvConfig {
    public constructor() {
      super();

      this.configOptionsGroups.push(new LoggerConfigOptionsGroup('TestTickerRepository'));
      this.configOptionsGroups.push(new ProductConfigOptionsGroup());
    }
  }

  let clock;
  let tickerDocumentFactory;

  let model: TickerModel;
  let tickerRepository: TickerRepository;
  let originalTickerLimit: number;

  let originalArgv;

  beforeAll(async () => {
    originalArgv = process.argv;
    process.argv = ArgvConfig.argvHelper([]);

    clock = lolex.install({ shouldAdvanceTime: true });
    tickerDocumentFactory = new TickerDocumentFactory(new Date(), 1000);

    originalTickerLimit = TickerSchema.statics.limit;
    TickerSchema.statics.limit = 3;

    referenceContainer.bind<Config>(CONFIG_TYPES.Config).to(Config).inSingletonScope();

    referenceContainer.load(...DEPENDENCIES);

    try {
      await referenceContainer.loadAsync(...ASYNC_DEPENDENCIES);
    } catch (error) {
      // let this error be handled/logged at the caller level
      throw error;
    }

    // @ts-ignore
    const config = referenceContainer.get<Config>(CONFIG_TYPES.Config) as ProductConfig;

    const modelFactory = referenceContainer.get<ModelFactory>(FACTORY_TYPES.ModelFactory);

    model = modelFactory.newProductModel(config.product, 'ticker', TYPES.TickerModel, TickerSchema);

    tickerRepository = referenceContainer.get<TickerRepository>(TYPES.TickerRepository);
  });

  afterAll(() => {
    referenceContainer.unbindAll();
    TickerSchema.statics.limit = originalTickerLimit;
    clock.uninstall();

    process.argv = originalArgv;
  });

  test('is bound to container', () => {
    expect(tickerRepository).toBeDefined();
  });

  test('findById() returns document if found', async () => {
    expect.assertions(2);

    const tickerDocument = tickerDocumentFactory.newTickerDocument();

    const originalFindById = model.findById;

    model.findById = jest.fn().mockImplementation((id, callback) => callback(undefined, tickerDocument));

    await expect(tickerRepository.findById(tickerDocument._id)).resolves.toEqual(tickerDocument);

    expect(model.findById).toHaveBeenCalledTimes(1);

    model.findById = originalFindById;
  });

  test('findById() rejects if nothing found', async () => {
    expect.assertions(2);

    await expect(tickerRepository.findById('abcd')).rejects.toBeUndefined();

    expect(model.findById).toHaveBeenCalledTimes(1);
  });

  test('findById() rejects with error if there is an error', async () => {
    expect.assertions(2);

    const error = 'error';
    const originalFindById = model.findById;

    model.findById = jest.fn().mockImplementation((id, callback) => callback(error));

    await expect(tickerRepository.findById('abcd')).rejects.toEqual(error);

    expect(model.findById).toHaveBeenCalledTimes(1);

    model.findById = originalFindById;
  });

  test('count() is correct', async() => {
    expect.assertions(1);

    const count = 10;

    const originalCount = model.countDocuments;

    model.countDocuments = jest.fn().mockImplementation((id, callback) => callback(undefined, count));

    await expect(tickerRepository.count()).resolves.toEqual(count);

    model.countDocuments = originalCount;
  });

  test('count() rejects with error if there is an error', async() => {
    expect.assertions(2);

    const error = 'error';

    const originalCount = model.countDocuments;

    model.countDocuments = jest.fn().mockImplementation((id, callback) => callback(error));

    await expect(tickerRepository.count()).rejects.toEqual(error);

    expect(model.countDocuments).toHaveBeenCalledTimes(1);

    model.countDocuments = originalCount;
  });

  test('pages() is correct', async () => {
    expect.assertions(1);

    const count = 10;
    const limit = 2;

    const originalCount = model.countDocuments;

    model.limit = limit;
    model.countDocuments = jest.fn().mockImplementation((id, callback) => callback(undefined, count));

    await expect(tickerRepository.pages()).resolves.toEqual(count / limit);

    model.countDocuments = originalCount;
  });

  test('pages() rejects with error if there is an error', async () => {
    expect.assertions(2);

    const error = 'error';

    const originalCount = model.countDocuments;

    model.countDocuments = jest.fn().mockImplementation((id, callback) => callback(error));

    await expect(tickerRepository.pages()).rejects.toEqual(error);

    expect(model.countDocuments).toHaveBeenCalledTimes(1);

    model.countDocuments = originalCount;
  });

  test('findManyByPage() item count == 0', async () => {
    expect.assertions(7);

    const pageOffset = new Date();
    const pageQuery = model.pageQuery(pageOffset);

    const query = QueryFactory.newQuery();

    model.find = jest.spyOn(model, 'find').mockImplementation(() => query);

    query.exec = jest.spyOn(query, 'exec').mockImplementation(callback => callback(undefined, []));

    await expect(tickerRepository.findManyByPage(pageOffset)).resolves.toEqual([]);

    expect(model.find).toHaveBeenCalledTimes(1);
    expect(model.find).toHaveBeenCalledWith(pageQuery.filter);
    expect(query.sort).toHaveBeenCalledTimes(1);
    expect(query.sort).toHaveBeenCalledWith(pageQuery.sort);
    expect(query.limit).toHaveBeenCalledTimes(1);
    expect(query.limit).toHaveBeenCalledWith(TickerSchema.statics.limit);

    // @ts-ignore
    model.find.mockRestore();
    // @ts-ignore
    query.exec.mockRestore();
  });

  test('findManyByPage() item count > 0', async () => {
    expect.assertions(7);

    const pageOffset = new Date();
    const pageQuery = model.pageQuery(pageOffset);

    const tickerDocs = tickerDocumentFactory.newTickerDocuments(2);

    const query = QueryFactory.newQuery();

    model.find = jest.spyOn(model, 'find').mockImplementation(() => {
      return query;
    });

    query.exec = jest.spyOn(query, 'exec').mockImplementation(callback => {
      return callback(undefined, tickerDocs);
    });

    await expect(tickerRepository.findManyByPage(pageOffset)).resolves.toEqual(tickerDocs);

    expect(model.find).toHaveBeenCalledTimes(1);
    expect(model.find).toHaveBeenCalledWith(pageQuery.filter);
    expect(query.sort).toHaveBeenCalledTimes(1);
    expect(query.sort).toHaveBeenCalledWith(pageQuery.sort);
    expect(query.limit).toHaveBeenCalledTimes(1);
    expect(query.limit).toHaveBeenCalledWith(TickerSchema.statics.limit);

    // @ts-ignore
    model.find.mockRestore();
    // @ts-ignore
    query.exec.mockRestore();
  });

  test('findManyByPage() rejects with error if there is an error', async () => {
    expect.assertions(7);

    const pageOffset = new Date();
    const pageQuery = model.pageQuery(pageOffset);

    const query = QueryFactory.newQuery();

    model.find = jest.spyOn(model, 'find').mockImplementation(() => query);

    const error = 'error';
    query.exec = jest.spyOn(query, 'exec').mockImplementation(() => {
      throw error;
    });

    await expect(tickerRepository.findManyByPage(pageOffset)).rejects.toEqual(error);

    expect(model.find).toHaveBeenCalledTimes(1);
    expect(model.find).toHaveBeenCalledWith(pageQuery.filter);
    expect(query.sort).toHaveBeenCalledTimes(1);
    expect(query.sort).toHaveBeenCalledWith(pageQuery.sort);
    expect(query.limit).toHaveBeenCalledTimes(1);
    expect(query.limit).toHaveBeenCalledWith(TickerSchema.statics.limit);

    // @ts-ignore
    model.find.mockRestore();
    // @ts-ignore
    query.exec.mockRestore();
  });

  test('create() with single ticker returns inserted ticker', async () => {
    expect.assertions(2);

    const ticker = tickerDocumentFactory.newTickerDocument();

    const createSpy = jest.spyOn(model, 'create');

    await expect(tickerRepository.create(ticker)).resolves.toEqual([ ticker ]);

    expect(createSpy).toHaveBeenCalledTimes(1);

    // @ts-ignore
    model.create.mockClear();
  });

  test('create() with with multiple tickers returns inserted tickers', async () => {
    expect.assertions(2);

    const ticker1 = tickerDocumentFactory.newTickerDocument();
    const ticker2 = tickerDocumentFactory.newTickerDocument();

    const createSpy = jest.spyOn(model, 'create');

    await expect(tickerRepository.create(ticker1, ticker2)).resolves.toEqual([ ticker1, ticker2 ]);

    expect(createSpy).toHaveBeenCalledTimes(1);

    // @ts-ignore
    model.create.mockClear();
  });

  test('insertMany() valid tickers', async () => {
    expect.assertions(2);

    const ticker1 = tickerDocumentFactory.newTickerDocument();
    const ticker2 = tickerDocumentFactory.newTickerDocument();

    model.insertMany = jest.spyOn(model, 'insertMany').mockImplementation(
      (docs, options, callback) => callback(undefined, docs)
    );

    await expect(tickerRepository.insertMany([ ticker1, ticker2 ])).resolves.toEqual([ ticker1, ticker2 ]);

    expect(model.insertMany).toHaveBeenCalledTimes(1);

    // @ts-ignore
    model.insertMany.mockRestore();
  });
});


