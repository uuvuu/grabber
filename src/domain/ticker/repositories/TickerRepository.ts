

import { BaseRepository } from '../../../infrastructure/data-access/repositories/BaseRepository';

import { Ticker } from '../entities/Ticker';


interface TickerRepository extends BaseRepository<Date, Ticker.TickerDocument> {}


export { TickerRepository };
