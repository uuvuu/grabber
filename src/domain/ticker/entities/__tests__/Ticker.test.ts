
import * as lolex from 'lolex';

import { TickerDocumentFactory } from '../../../../testing/entities/TickerDocumentFactory';
import { TickerUtils } from '../Ticker';


describe('Ticker', () => {
  let clock;
  let tickerDocumentFactory;

  beforeAll(() => {
    clock = lolex.install({ shouldAdvanceTime: true });
    tickerDocumentFactory = new TickerDocumentFactory(new Date(), 60000);
  });

  afterAll(() => {
    clock.uninstall();
  });

  test('copyTickerDocument() copies ticker', () => {
    const ticker = tickerDocumentFactory.newTickerDocument();
    const copiedTicker = TickerUtils.copyTickerDocument(ticker);

    // @ts-ignore
    expect(copiedTicker).toMatchDocument(ticker);
    expect(copiedTicker.time.constructor.name).toEqual(ticker.time.constructor.name);
  });

  test('copyTickerDocuments() copies tickers', () => {
    const tickers = tickerDocumentFactory.newTickerDocuments(2);
    const copiedTickers = TickerUtils.copyTickerDocument(tickers);

    // @ts-ignore
    expect(copiedTickers).toMatchDocuments(tickers);
  });

  test('tickerComparator()', () => {
    const [ ticker1, ticker2 ] = tickerDocumentFactory.newTickerDocuments(2);

    expect(TickerUtils.tickerComparator(ticker1, ticker2)).toEqual(-1);
    expect(TickerUtils.tickerComparator(ticker2, ticker1)).toEqual(1);

    const copiedTicker = TickerUtils.copyTickerDocument(ticker2);

    expect(TickerUtils.tickerComparator(ticker2, copiedTicker)).toEqual(0);
  });
});
