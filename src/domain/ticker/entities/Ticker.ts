
import { Document } from 'mongoose';


namespace Ticker {
  type TickerSource = 'cbpro-websocket';

  export enum Side {
    BUY = <any>'buy',      // 'buy' is taker side
    SELL = <any>'sell'
  }

  export interface Ticker {
    product: string;

    time: Date;

    tradeId: number;
    sequence: number;

    price: string;
    side: Side;
    lastSize: string;
    bestBid: string;
    bestAsk: string;

    open24h: string;
    low24h: string;
    high24h: string;

    volume24h: string;
    volume30d: string;

    source: TickerSource;
  }

  export interface TickerDocument extends Ticker, Document {
  }
}


namespace TickerUtils {
  export function tickerComparator(ticker1: Ticker.TickerDocument, ticker2: Ticker.TickerDocument) {
    if (ticker1.time < ticker2.time) {
      return -1;
    }

    if (ticker1.time > ticker2.time) {
      return 1;
    }

    return 0;
  }

  export function copyTickerDocument(tickerDocument: Ticker.TickerDocument) {
    const ticker = JSON.parse(JSON.stringify(tickerDocument)) as Ticker.TickerDocument;

    ticker.time = new Date(ticker.time);

    return ticker;
  }

  export function copyTickerDocuments(tickerDocuments: Ticker.TickerDocument[]) {
    return tickerDocuments.map(tickerDocument => {
      return copyTickerDocument(tickerDocument);
    });
  }

  export function copyNewTickerDocument(tickerDocument: Ticker.TickerDocument) {
    const ticker = JSON.parse(JSON.stringify(tickerDocument)) as Ticker.TickerDocument;

    ticker.time = new Date(ticker.time);

    delete ticker._id;
    delete ticker.__v;

    return ticker;
  }

  export function copyNewTickerDocuments(tickerDocuments: Ticker.TickerDocument[]) {
    return tickerDocuments.map(tickerDocument => {
      return copyNewTickerDocument(tickerDocument);
    });
  }
}


export { Ticker, TickerUtils };
