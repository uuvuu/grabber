
import 'reflect-metadata';

import { ContainerModule, interfaces } from 'inversify';

import { TickerRepositoryImpl, TYPES } from './repositories/impl/TickerRepositoryImpl';
import { TickerRepository } from './repositories/TickerRepository';


const TickerModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<TickerRepository>(TYPES.TickerRepository).to(TickerRepositoryImpl).inSingletonScope();
  }
);


export { TickerModule, TYPES };
